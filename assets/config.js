// cpanel - site_templates/countdown_tech/assets/config.js.tt Copyright(c) 2016 cPanel, Inc.
//                                                          All rights Reserved.
// copyright@cpanel.net                                        http://cpanel.net
// This code is subject to the cPanel license. Unauthorized copying is prohibited

window.cpanel = {
    data: {
        email: "arbid.hueppa@animatiomx.com",
        logo: "Colegio Carmen Serdan",
        social: [
            
            {
                icon: 'facebook',
                link: "https:\/\/www.facebook.com\/animatiomx\/"
            },
            
            
            {
                icon: 'twitter',
                link: "https:\/\/twitter.com\/ANIMATIOMX"
            },
            
            
            {
                icon: 'instagram',
                link: "https:\/\/www.instagram.com\/animatiomx\/?hl=es"
            },
            
            
            
            
        ]
    },
    style: {
        primary: "#3844d8",
    },
    slides: [
        {
            type: 'countdown',
            backgroundImage: "",
            backgroundColor: "",
            color: "#ffffff",
            buttonText: "",
            buttonLink: "",
            endTime: '2017-09-18T05:00:00.000Z'
        }
    ]
};
