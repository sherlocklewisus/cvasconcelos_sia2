<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioGrupo extends Model
{
    protected $table = 'grupos_horario';
    public $timestamps = false;

    protected $primaryKey = "id";

    public function periodos() {
    	return $this->hasOne('App\CiclosEscolares','idPeriodo','idperiodo');
    }

    public function niveles() {
    	return $this->hasOne('App\Secciones','idSeccion','idseccion');
    }

    public function grados() {
    	return $this->hasOne('App\Grados','idGrado','idgrado');
    }

    public function grupos() {
    	return $this->hasOne('App\Grupos','idGrupo','idgrupo');
    }
}
