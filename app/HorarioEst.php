<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioEst extends Model
{
    protected $table = 'horarios_materias';
    public $timestamps = false;

    protected $primaryKey = "idHorario";

    public function niveles() {
    	return $this->hasOne('App\Secciones','idSeccion','idseccion');
    }

    public function grados() {
    	return $this->hasOne('App\Grados','idGrado','idgrado');
    }

    public function grupos() {
    	return $this->hasOne('App\Grupos','idGrupo','idgrupo');
    }

    public function periodo() {
        return $this->hasOne("App\periodosescolares","idSeccion","idseccion")->where('Activo', '=', 1);
    }
}
