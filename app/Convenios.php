<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Convenios extends Model
{
    use SoftDeletes;

    protected $table = "convenios";
    protected $primaryKey = "id_convenio";    

    public static function generarNumConvenio()
    {
        $identifier = "CONV";
        $fecha = Carbon::now()->format("Ymd");
        $last_id = (Convenios::max("id_convenio")+1);
        $id_formato = sprintf("%04d",$last_id);
        $numConvenio = $identifier.$fecha.$id_formato;
        return $numConvenio;
    }
     public function alumnos() {
        return $this->hasOne('App\Alumnos','idAlumno','idAlumno');
    }
}
