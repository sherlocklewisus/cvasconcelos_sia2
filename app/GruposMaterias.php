<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GruposMaterias extends Model
{
    protected $table = "grupos_materias";
    protected $primaryKey = "id";

    public static function material($id) {
        return GruposMaterias::where ('id_grupo', '=', $id)
        ->get();
    }

    public function grupo()
    {
    	return $this->belongsTo("App\Grupos","id_grupo","idGrupo");
    }

    public function materia()
    {
    	return $this->belongsTo("App\Materias","id_materia","idMateria");
    }

    public function grupo_materia_profesor()
    {
        return $this->belongsTo("App\GrupoMateriaProfesor","id","id_materia_grupo");
    }

    public function tareas()
    {
        return $this->belongsTo("App\Tareas","id","id_materia_grupo");
    }

    public function relacion() {
        return $this->hasMany("App\GrupoMateriaProfesor","id","id_materia_grupo");
    }

    public function relacionGrupo() {
        return $this->hasOne("App\Grupos","idGrupo","id_grupo");
    }

    public function relacionMateria() {
        return $this->hasOne("App\Materias","idMateria","id_materia");
    }

    public function materiasdisponibles() {
        return $this->hasOne("App\Materias","idMateria","id_materia");
    }
}
