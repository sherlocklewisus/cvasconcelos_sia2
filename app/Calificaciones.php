<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Calificaciones extends Model
{
    use SoftDeletes;
    protected $table = 'calificaciones';
    protected $primaryKey = 'idCalificacion';
    protected $dates = ["deleted_at"];

    protected $fillable = ["idAlumno","idMateria","idPeriodoEvaluacion","CalificacionFinal"];

    public function periodoEvaluacion()
    {
    	return $this->belongsTo("App\periodosevaluacion","idPeriodoEvaluacion","idPeriodoEvaluacion");
    }

    public function alumno()
    {
        return $this->belongsTo("App\Alumnos","idAlumno","idAlumno");
    }

    public function materia()
    {
    	return $this->belongsTo("App\Materias","idMateria","idMateria");
    } 

    public function grupo()
    {
        return $this->belongsTo("App\Grupos","idGrupo","idGrupo");
    }

}
