<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;


class tutoresalumnos extends Model
{
 use SoftDeletes;

    protected $table = 'tutoresalumnos';
    protected $primaryKey = 'idTutoresAlumnos';
  	protected $dates = ["deleted_at"];

        public function parentesco()
  	{
  		return $this->hasOne("App\Parentescos","idParentescos","idParentesco");
  	}
  	
}
