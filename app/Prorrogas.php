<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prorrogas extends Model
{
    protected $table = "prorrogas";
    protected $primaryKey = "idProrroga";
    public $timestamps = false;

    public function alumnos() {
    	return $this->hasOne('App\Alumnos','idAlumno','idAlumno');
    }
}
