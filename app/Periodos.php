<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Periodos extends Model {

   protected $table = 'periodosescolares';
   protected $primaryKey = 'idPeriodo';

   public static function periodosEval($id) {
      return Periodos::where ('idPeriodo','=',$id)
      ->get();
   }
}
