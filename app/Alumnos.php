<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use App\Alumnos;
use Storage;
use Carbon\Carbon;

class Alumnos extends Model
{
    use SoftDeletes;

    protected $table = "alumnos";
    protected $primaryKey = "idAlumno";
    //public $timestamps = false;

    protected $dates = ["deleted_at","FechaEntregaDocumento"];

    public static function alumno($id) {
        return Alumnos::where('idGrupo', '=', $id)
        ->get();
    }


    public function setActualizacionEntregaDocumentoAttribute($valor)
    {
        if(empty($valor))
        {
            $this->attributes["ActualizacionEntregaDocumento"] = Carbon::now()->format("Y/m/d");
             Log::info("asignando valor default en  ActualizacionEntregaDocumento");
        }
        else
        {
            $this->attributes["ActualizacionEntregaDocumento"] = Carbon::parse($valor)->format("Y/m/d");
            Log::info("asignando valor ".$valor." en  ActualizacionEntregaDocumento");
        }
    }

    public function setFechaEntregaDocumentoAttribute($valor)
    {
        if(empty($valor))
        {
            $this->attributes["FechaEntregaDocumento"] = Carbon::now()->format("Y/m/d");
            Log::info("asignando valor default en  FechaEntregaDocumento");
        }
        else
        {
            $this->attributes["FechaEntregaDocumento"] =  Carbon::parse($valor)->format("Y/m/d");
            Log::info("asignando valor ".$valor." en  FechaEntregaDocumento");
        }
    }

    public function setSaldoAttribute($valor)
    {
        if(empty($valor))
        {
            $this->attributes["Saldo"] = 0;
            Log::info("asignando valor default en  Saldo");
        }
        else
        {
            $this->attributes["Saldo"] = $valor;
            Log::info("asignando valor ".$valor." en  Saldo");
        }
    }

    public function setAbonoAttribute($valor)
    {
        if(empty($valor))
        {
            $this->attributes["Abono"] = 0;
            Log::info("asignando valor default en  Abono");
        }
        else
        {
            $this->attributes["Abono"] = $valor;
            Log::info("asignando valor ".$valor." en  Abono");
        }
    }

    public function setPorcentajeBecaAttribute($valor)
    {
        if(empty($valor))
        {
            $this->attributes["PorcentajeBeca"] = 0;
            Log::info("asignando valor default en  Beca");
        }
        else
        {
            $this->attributes["PorcentajeBeca"] = $valor;
            Log::info("asignando valor ".$valor." en  PorcentajeBeca");
        }
    }

    public function user() {
        return $this->hasOne("App\User","id","id_usuario")->withTrashed();
    }

    public function seccion() {
        return $this->hasOne("App\Secciones","idSeccion","idSeccion");
    }

    public function tutores() {
        return $this->belongsToMany("App\Tutores","tutoresalumnos","idAlumno","idTutor");
    }

    public function grado() {
        return $this->hasOne("App\Grados","idGrado","idGrado");
    }

    public function grupo() {
        return $this->hasOne("App\Grupos","idGrupo","idGrupo");
    }

    public function periodo() {
        return $this->hasOne("App\periodosescolares","idSeccion","idSeccion")->where('Activo', '=', 1);
    }

    public function religion() {
        return $this->hasOne("App\Religiones","id","id_religion");
    }

    public function seguro() {
        return $this->hasOne("App\Seguros","id","id_seguro");
    }

    public function calificaciones() {
        return $this->hasMany("App\Calificaciones","idAlumno","idAlumno");
    }

    public function documentos() {
        return $this->hasMany("App\DocumentosAlumnos","idAlumno","idAlumno");
    }

    public function estado() {
        return $this->hasOne("App\Estados","idEstados","idEstado");
    }

    public function municipio() {
        return $this->hasOne("App\Municipios","id","idCiudad");
    }

    public function pages() {
        return $this->hasOne("App\Pages","id_usuario","id_usuario")->orderBy('id', 'DESC');
    }
}
