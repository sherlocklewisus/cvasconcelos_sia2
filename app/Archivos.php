<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivos extends Model
{
    protected $table = 'documentos_adjuntos';
    protected $primaryKey = 'idDoctos';
    public $timestamps = false;
}
