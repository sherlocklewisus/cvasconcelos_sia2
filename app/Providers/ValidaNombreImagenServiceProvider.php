<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class ValidaNombreImagenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('max_nombre_imagen', function ($attribute, $value, $parameters, $validator) {
            $img = $value;
            $nombre_imagen = $img->getClientOriginalName();

            if(strlen($nombre_imagen) > $parameters[0])
            {
                return false;
            }

            return true;
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
