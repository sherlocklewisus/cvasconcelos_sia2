<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriasReprobadas extends Model
{
    //
    protected $table ='materiasreprobadas';
    protected $primarykey = 'idMateriaReprobada';
    public $timestamps=false;
}
