<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class actividadesextra extends Model
{
    //
    protected $table = "actividadesextras";
    protected $primarykey= "id";
    public $timestamps = false;
}
