<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\User;
use DB;
use Storage;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'typeUser',"ImagenPerfil"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ["deleted_at"];

    public function setpasswordAttribute($valor)
    {
        if(!empty($valor))
        {
            $this->attributes["password"] = bcrypt($valor);
        }
        
    }

    public function setimagenPerfilAttribute($path)
    {
        $img = $path;
        /*si no existe mi varaiable Imagen Perfil y no me han mandado ninguna
            imagen signa laimagen por defecto
        */
        if(empty($this->attributes["ImagenPerfil"]) && $img == null)
        {
            $this->attributes["ImagenPerfil"] = "default-profile.png";
            Log::info("attibuto vacio && imagen vacia");
        }
        else if(!empty($this->attributes["ImagenPerfil"]) && $img == null)
        {
            /*si existe una imagen en Imagen perfil y no me han mandado
                ninguna imagen conserva la misma
            */
            $this->attributes["ImagenPerfil"];
            Log::info("attibuto lleno && imagen vacia");
        }
        else if(empty($this->attributes["ImagenPerfil"]) && $img != null)
        {
            $route_file = time()."_".$img->getClientOriginalName();
            $this->attributes["ImagenPerfil"] = $route_file;
            Storage::disk("imgUsuarios")->put($route_file,file_get_contents($img->getRealPath()));
            Log::info("attibuto vacio && imagen llena");
        }
        else
        {
            //verifico si no es la imagen por defecto sino la elimino
            if($this->attributes["ImagenPerfil"] != "default-profile.png")
            {
                //elimino el archivo anterior
                if(Storage::disk("imgUsuarios")->exists($this->attributes["ImagenPerfil"]))
                {
                    Storage::disk("imgUsuarios")->delete($this->attributes["ImagenPerfil"]);
                    Log::info("attibuto imagen eliminada");
                }
            }
            //guardo el nuevo archivo
           $route_file = time()."_".$img->getClientOriginalName();
            $this->attributes["ImagenPerfil"] = $route_file;
            Storage::disk("imgUsuarios")->put($route_file,file_get_contents($img->getRealPath()));
            Log::info("attibuto lleno && imagen llena");
        }
    }



    public function impress()
    {
        Log::info("funcion impress ");
        $id = DB::table("pages")->insertGetId(["impress"=>Carbon::now(),"id_usuario"=>$this->id]);
        Log::info("impress id insertado es: ".$id."");
        //DB::table("users")->where("id","=",$this->id)->update(["last_impress"=>Carbon::now()]);
        /*$this->impress = Carbon::now();
        $this->save();*/
    }

    public function historyPagesClean()
    {
        DB::table("users")->where("id","=",$this->id)->update(["pages"=>""]);
    }

    public function close_session()
    {
        DB::table("users")->where("id","=",$this->id)->update(["close_session"=>Carbon::now()]);
    }

    public function pageNavigateUser()
    {
        //obtenemos dinamicamente la url y la segmentamos por medio de /
        $url = explode("public",$_SERVER['REQUEST_URI']);
        $total_elementos = count($url);
        $page = $url[$total_elementos-1];
        $arrP = [];
        $arrP = explode("?", $page);
        $T_E = count($arrP);
        $page = $arrP[0];
 
        $max_id = DB::table("pages")
        ->where("id_usuario","=",$this->id)
        ->max("id");

        $p = $this->pages()->where("id","=",$max_id)->first();

        $arr_pages = [];

        if($p)
        {
            if($p->pages != "")
                $arr_pages = explode(" , ", $p->pages);
        }
        
        if(!in_array($page, $arr_pages))
        {
            array_push($arr_pages, $page);
        }
        
        $pages = implode(" , ",$arr_pages);

        //DB::table("users")->where("id","=",$this->id)->update(["pages"=>$pages]);
        

        DB::table("pages")
        ->where("id_usuario","=",$this->id)
        ->where("id","=",$max_id)
        ->update(["pages"=>$pages]);
    }

    public function consultaMensajes() {
        $mensajes = DB::table('mensajes')
        ->select(
            'mensajes.idMensaje',
            'idUsuarioEnvia',
            'idUsuarioRecibe',
            'Asunto',
            'Relevancia',
            'Mensaje',
            'fechaenvio',
            'status'
        )
        ->orderBy('idMensaje', 'DESC')
        ->limit(5)
        ->get();

        return $mensajes;
    }

    public function usuarios_perfil() {
        $usuarios = DB::table('colaboradores')
        ->select(
            'colaboradores.idColaborador',
            'ImagenPerfil',
            'id_usuario'
        )
        ->get();

        return $usuarios;
    }

    public function rol()
    {
        return $this->hasOne("App\Roles","idRol","typeUser");
    }


    public function colaborador()
    {
        //pertenece a la tabla colaboradores
        return $this->hasOne("App\Colaboradores","id_usuario","id");
    }

    public function alumno()
    {
        //pertenece a la tabla colaboradores
        return $this->hasOne("App\Alumnos","id_usuario","id");
    }

    public function tutor()
    {
        //pertenece a la tabla colaboradores
        return $this->hasOne("App\Tutores","id_usuario","id");
    }


    public function pages()
    {
        return $this->hasMany("App\Pages","id_usuario","id");
    }

    public function pages_visit()
    {
        $max_id = DB::table("pages")->where("id_usuario","=",$this->id)->max("id");
        return Pages::where("id","=",$max_id)->first();
    }
}
