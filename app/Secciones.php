<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Secciones extends Model
{
    protected $table = 'secciones';
    protected $primaryKey = "idSeccion";
    public $timestamps = false;

    public function grados() {
    	return $this->hasMany("App\Grados", "idSeccion", "idSeccion");
    }

    public static function periodos($id) {
      	return Self::where ('idSeccion','=',$id)
      	->get();
   	}
}