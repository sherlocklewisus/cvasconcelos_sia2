<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajes extends Model
{
    protected $table = 'mensajes';
    public $timestamps = false;
    
    public function correos() {
        return $this->hasOne("App\Correos","Email","Email");
    }
}
