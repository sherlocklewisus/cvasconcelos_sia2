<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForosLista extends Model
{
    protected $table = "foros_general";
    protected $primaryKey = "idForo";
    public $timestamps = false;

    public function seccional() {
    	return $this->hasOne("App\Secciones","idSeccion","idseccion");
    }

    public function gradual() {
    	return $this->hasOne("App\Grados","idGrado","idgrado");
    }

    public function grupal() {
    	return $this->hasOne("App\Grupos","idGrupo","idgrupo");
    }

    public function material() {
    	return $this->hasOne("App\Materias","idMateria","idmateria");
    }
}
