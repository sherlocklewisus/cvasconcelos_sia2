<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class CiclosEscolares extends Model
{
	//use SoftDeletes;

    protected $table = "periodosescolares";
    protected $primaryKey = "idPeriodo";
    public $timestamps = false;

    //protected $dates = ["deleted_at"];

    public function seccion()
    {
    	return $this->belongsTo("App\Secciones","idSeccion","idSeccion");
    }

    
    public function periodosEvaluacion()
    {
    	return $this->hasMany("App\PeriodosEvaluacion","idCicloEscolar","idPeriodo");
    }

    public static function cicloEval($id) {
        return Self::where('idSeccion','=',$id)
        ->get();
    }
    
    public static function fechas($id){
        return Self::where('idPeriodo', '=', $id)
        ->get();
    }
    public static function pedirCiclo($id) {
        return Self::where('idSeccion','=',$id)
        ->get();
    }
}
