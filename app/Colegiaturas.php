<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colegiaturas extends Model
{
    protected $table = "colegiaturas";
    protected $primaryKey = "idReferencia";
    public $timestamps = false;

    public function alumnos() {
    	return $this->hasmany('App\Alumnos','idAlumno','idAlumno');
    }

    public function periodospagos() {
    	return $this->hasmany('App\PeriodosPagos','idPeriodoPago','idPeriodoPago');
    }

    public function setFechaProrrogaAttribute($val)
    {
        if(!empty($val))
        {
            $this->attributes["FechaProrroga"] = $val;
        }
        else
        {
            $this->attributes["FechaProrroga"] = null;
        }
    }

    public function setRecargoPorPagoRetrasadoAttribute($val)
    {
        if(!empty($val))
        {
            $this->attributes["RecargoPorPagoRetrasado"] = $val;
        }
        else
        {
            $this->attributes["RecargoPorPagoRetrasado"] = 0.0;
        }
    }
}
