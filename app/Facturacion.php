<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturacion extends Model
{
    protected $table = "facturas";
    protected $primaryKey = "idFactura";
    public $timestamps = false;

    public function alumnos() {
    	return $this->hasOne('App\Alumnos','idAlumno','idAlumno');
    }

    public function parentescos() {
    	return $this->hasOne('App\Parentescos','idParentescos','solicito');
    }
}
