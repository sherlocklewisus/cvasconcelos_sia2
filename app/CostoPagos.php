<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoPagos extends Model
{
    protected $table = "costo_colegiaturas";
    protected $primaryKey = "id_costo_colegiatura";
}
