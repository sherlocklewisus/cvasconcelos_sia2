<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleArqueo extends Model
{
    protected $table = "detalle_arqueo";
    protected $primaryKey = "id_detalle_arqueo";

    public function denominacion()
    {
        return $this->hasOne("App\Denominaciones","idDenominacion","idDenominacion");
    }
}
