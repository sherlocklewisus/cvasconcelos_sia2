<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoMateriaProfesor extends Model
{
    protected $table = "materia_profesor_grupo";
    protected $primaryKey = "id";

    public function profesor()
    {
    	return $this->belongsTo("App\Colaboradores","id_profesor","idColaborador");
    }

    public function grupomateria() {
    	return $this->hasOne("App\GruposMaterias","id","id_materia_grupo");
    }
}
