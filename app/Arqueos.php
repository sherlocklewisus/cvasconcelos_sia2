<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Arqueos extends Model
{
    protected $tables = "arqueos";
    protected $primaryKey = "idArqueo";

    public function colaborador() {
        return $this->hasOne("App\Colaboradores","idColaborador","idColaborador");
    }

    public function detalle_arqueos()
    {
        return $this->hasMany("App\DetalleArqueo","id_arqueo","idArqueo");
    }

    public function tipoDependencia()
    {
        return $this->hasOne("App\TipoDependencias","idTipoDependencias","idTipoDependencia");
    }

    public function tipoResolucion()
    {
        return $this->hasOne("App\TipoResoluciones","idTipoResolucion","idTipoResolucion");
    }

    public function funcionarioDeManejo()
    {
        return $this->hasOne("App\Colaboradores","idColaborador","idColaborador");
    }

    public function funcionarioDeControl()
    {
        return $this->hasOne("App\Colaboradores","idColaborador","idFuncionarioControl");
    }

    public static function generarNumArqueo()
    {
        $identifier = "ARQ";
        $fecha = Carbon::now()->format("Ymd");
        $last_id = (Arqueos::max("idArqueo")+1);
        $id_formato = sprintf("%04d",$last_id);
        $numConvenio = $identifier.$fecha.$id_formato;
        return $numConvenio;
    }
}
