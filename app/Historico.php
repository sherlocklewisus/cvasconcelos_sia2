<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $table = "historico";
    protected $primaryKey = "idHistorico";

    public function ciclo()
    {
        return $this->hasOne("App\CiclosEscolares","idPeriodo","idCiclo");
    }

    public function seccion()
    {
        return $this->hasOne("App\Secciones","idSeccion","idSeccion");
    }

    public function grado()
    {
        return $this->hasOne("App\Grados","idGrado","idGrado");
    }

    public function grupo()
    {
        return $this->hasOne("App\Grupos","idGrupo","idGrupo");
    }

    public function alumno()
    {
        return $this->hasOne("App\Alumnos","idAlumno","idAlumno");
    }
}
