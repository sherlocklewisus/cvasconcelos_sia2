<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleEntradasSalidas extends Model
{
    protected $table = "detalleentradassalidas";
    protected $primaryKey = "idDetalleEntradaSalida";
}
