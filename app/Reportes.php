<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reportes extends Model
{
    protected $table = 'reportes';
    protected $primaryKey = 'IdReporte';
    public $timestamps = false;

    // public function scopeSearch($query, $title) {
    // 	return $query->where('Tiporeporte', '=', "$title");
    // }
}
