<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoOperaciones extends Model
{
    protected $table = "tipooperaciones";
    protected $primaryKey = "idTipoOperacion";
}
