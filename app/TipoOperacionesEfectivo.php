<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoOperacionesEfectivo extends Model
{
    protected $table = "tipooperacionesefectivo";
    protected $primaryKey = "idTipoOPeracionEfectivo";
}
