<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveCiclosEscolaresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "cicloEscolar"=>"required",
            "FechaInicio"=>"required",
            "FechaFin"=>"required",
            "idSeccion"=>"required|exists:secciones",
        ];
    }

    public function messages()
    {
        return [
            "cicloEscolar.required"=>" Debe ingresar un nombre al ciclo escolar para continuar.",
            "FechaInicio.required"=>" Debe ingresar una fecha de inicio para continuar.",
            "FechaFin.required"=>" Debe ingresar una fecha de fin para continuar.",
            "idSeccion.required"=>" Debe seleccionar una sección para continuar.",
            "idSeccion.exists"=>" La sección seleccionada no es correcta, intente con otra.",
        ];
    }
}
