<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class SaveAlumnosRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "grado"=>"required",
            "grupo"=>"required",
            "grupo2"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "grado.required"=>"El campo grado es requerido",
            "grupo.required"=>"El campo grupo es requerido",
            "grupo2.required"=>"El nuevo grupo es requerido",
        ];
    }
}
