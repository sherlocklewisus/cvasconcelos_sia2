<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class GruposCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "idSeccion"=>"required|exists:secciones",
            "idGrado"=>"required|exists:grados",
            "grupo"=>"required|".Rule::unique('grupos')->where("idGrado",$this->idGrado),
        ];
    }

    public function messages()
    {
        return [
            "idSeccion.required"=>"El campo Seccion es obligatorio.",
            "idGrado.required"=>"El campo Grados es obligatorio.",
            "grupo.required"=>"El campo Grupo es obligatorio.",
            "grupo.unique"=>"El grupo ya se encuentra registrado, intente con uno diferente.",
        ];
    }
}
