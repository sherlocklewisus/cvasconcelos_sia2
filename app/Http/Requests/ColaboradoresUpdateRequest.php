<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ColaboradoresUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "username"=>"required",
            "Nombre"=>"required",
            "Apellido"=>"required",
            // "idRol"=>"required|exists:roles",
            "Email"=>"required|email",
            "Password"=>"min:6",
            "confirma_password"=>"required_with:Password|same:Password",
            "Telefono"=>"required",
            "Domicilio"=>"required",
            "ImagenPerfil"=>"file|image",
        ];
    }

    public function messages()
    {
        return [
            "username.required"=>"El campo username es requerido",
            "Nombre.required"=>"El campo nombre es requerido",
            "Apellido.required"=>"El campo apellido es requerido",
            "Password.min"=>"El campo password debe tener al menos :min caracteres",
            "confirma_password.required_with"=>"El campo confirma password es requerido si el campo password tiene un valor",
            "confirma_password.same"=>"El campo password y el campo de confirmacion no coinciden",
            "idRol.required"=>"El campo de seleccion es requerido",
            "idRol.exists"=>"La opcion del campo rol no es valida",
            "Email.required"=>"El campo email es requerido",
            "Email.email"=>"El campo email no tiene el formato correcto",
            "Email.unique"=>"El campo email ya esta registrado",
            "Telefono.required"=>"El campo telefono es requerido",
            "Domicilio.required"=>"El campo domicilio es requerido",
            "ImagenPerfil.file"=>"El campo Imagen perfil debe ser un archivo",
            "ImagenPerfil.image"=>"El archivo debe ser de tipo imagen jpg,png o gif",
            "ImagenPerfil.max_nombre_imagen"=>"El nombre de la imagen no debe contener como maximo 100 caracteres",
        ];
    }
}
