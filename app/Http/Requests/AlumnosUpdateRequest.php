<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Alumnos;

class AlumnosUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $alumno = Alumnos::findOrFail($this->id_alumno);

        return [
            // "name"=>"required",
            // "Password"=>"min:6",
            // "Email" => "required|email|".Rule::unique('users')->ignore((!$alumno->user?"":$alumno->user->email), 'email'),
            // "confirma_password"=>"required_with:Password|same:Password",
            "Nombres"=>"required",
            "ApellidoPaterno"=>"required|regex:/^[\pL\s\-]+$/u|max:45",
            "ApellidoMaterno"=>"required|regex:/^[\pL\s\-]+$/u|max:45",
            "FechaNacimiento"=>"date|before:".Carbon::now()->subYear(1)->format("Y/m/d"),
            "idSeccion"=>"required|exists:secciones,idSeccion",
            "idGrado"=>"required|exists:grados,idGrado",
            "idGrupo"=>"required|exists:grupos,idGrupo",
            "idEstado"=>"required|exists:estados,idEstados",
            "idCiudad"=>"required|exists:municipios,id",
            // "id_religion"=>"required|exists:religiones,id",
            "religion_otros"=>"required_if:id_religion,6", // 6 -- otros
            "id_seguro"=>"exists:seguros,id",
            "seguro_otros"=>"required_if:id_seguro,7",//7 -- otros
            "CodigoPostal"=>"required",
            "Colonia"=>"required",
            "Calle"=>"required",
            "NumeroExterior"=>"required",
            /*"FechaEntregaDocumento"=>"date",*/
            "ActualizacionEntregaDocumento"=>"date",
            "DetalleEnfermedad"=>"required_if:Enfermedad,1",
            "NotaDocumentos"=>"max:300",
            "Saldo"=>"numeric",
            "Abono"=>"numeric",
            "PorcentajeBeca"=>"required_if:Beca,1|numeric",
            "imagenPerfil"=>"file|image",
        ];
    }

    public function messages()
    {
        return [
                "name.required"=>"El campo usuario es requerido",
                "Password.min"=>"El campo password debe tener al menos :min caracteres",
                "confirma_password.required_with"=>"El campo confirma password es requerido si el campo password tine un valor",
                "confirma_password.same"=>"El campo password y el campo de confirmacion no coinciden",
                "Nombres.required"=>"El campo nombre es requerido",
                "Email.required"=>"El campo email es requerido",
                "Email.email"=>"El campo email no tiene el formato correcto",
                "Email.unique"=>"El campo email ya se encuentra registrado",
                "ApellidoPaterno.required"=>"El campo Apellido paterno es requerido",
                "ApellidoPaterno.alpha"=>"El campo Apellido paterno solo debe contener letras",
                "ApellidoPaterno.max"=>"El campo apellido Paterno debe tener como maximo 45 caracteres",
                "ApellidoMaterno.alpha"=>"El campo Apellido materno solo debe contener letras",
                "ApellidoMaterno.max"=>"El campo apellido materno debe tener como maximo 45 caracteres",
                "FechaNacimiento.date"=>"El campo fecha de nacimiento debe contener una fecha valida",
                "FechaNacimiento.before"=>"El campo Fecha de nacimiento debe contener una fecha menor a ".Carbon::now()->subYear(1)->format("Y/m/d"),
                "idSeccion.required"=>"El campo de seleccion secciones es requerido",
                "idSeccion.exists"=>"La opcion seleccionada del menu secciones no es valida",
                "idGrado.required"=>"El campo de seleccion grado es requerido",
                "idGrado.exists"=>"La opcion seleccionada del menu grado no es valida",
                "idGrupo.required"=>"El campo de seleccion grupo es requerido",
                "idGrupo.exists"=>"La opcion seleccionada del menu grupo no es valida",
                "idEstado.required"=>"El campo de seleccion estado es requerido",
                "idEstado.exists"=>"La opcion seleccionada del menu estado no es valida",
                "idCiudad.required"=>"El campo de seleccion municipio es requerido",
                "idCiudad.exists"=>"La opcion seleccionada del menu municipio no es valida",
                "id_religion.required"=>"El menu de seleccion religion es obligatorio",
                "id_religion.exists"=>"La opcion seleccionada delmenu religion no es valida",
                "religion_otros.required_if"=>"El campo otra religion es requerido",
                "id_seguro.exists"=>"La opcion seleccionada del menu seguro no es valida",
                "seguro_otros.required_if"=>"El campo otro seguro es requerido",
                "CodigoPostal.required"=>"el campo :attribute es requerido",
                "Colonia.required"=>"el campo :attribute es requerido",
                "Calle.required"=>"el campo :attribute es requerido",
                "NumeroExterior.required"=>"el campo :attribute es requerido",
                /*"FechaEntregaDocumento.date"=>"El campo Fecha entrega documento debe ser una fecha valida",*/
                "ActualizacionEntregaDocumento.date"=>"El campo actualizacion entregaDocumento debe ser de tipo date",
                /*"ActualizacionEntregaDocumento.after"=>"La fecha de actualizacion debe ser posterior a la fecha de entrega",*/
                "NotaDocumentos.max"=>"El campo nota documentos debe tener como maximo 300 caracteres",
                "DetalleEnfermedad.required_if"=>"El campo detalle enfermedad es requerido",
                "Saldo.numeric"=>"El campo saldo debe ser numerico",
                "Abono.numeric"=>"El campo abono debe ser numerico",
                "PorcentajeBeca.required_if"=>"El campo porcentaje beca es obligatorio",
                "PorcentajeBeca.numeric"=>"El campo porcentaje beca debe ser numerico"
            ];
    }
}
