<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColaboradoresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "Nombre"=>"required",
            "Apellido"=>"required",
            "idRol"=>"required|exists:roles",
            "Email"=>"required|email|unique:colaboradores",
            "Telefono"=>"required",
            "Domicilio"=>"required",
            "ImagenPerfil"=>"file|image",
        ];
    }

    public function messages()
    {
        return [
            "username.required"=>"El campo username es requerido",
            "Password.required"=>"El campo password es requerido",
            "Password.min"=>"El campo password debe contener al menos 6 caracteres",
            "Nombre.required"=>"El campo nombre es requerido",
            "Nombre.regex"=>"El campo nombre solo debe contener letras",
            "Apellido.required"=>"El campo apellido es requerido",
            "idRol.required"=>"El campo Rol es requerido",
            "idRol.exists"=>"La opcion del campo rol no es valida",
            "Email.required"=>"El campo email es requerido",
            "Email.emal"=>"El campo email no tiene el formato correcto",
            "Email.unique"=>"El campo email es ya se encuentra registrado",
            "Telefono.required"=>"El campo telefono es requerido",
            "Domicilio.required"=>"El campo domicilio es requerido",
            "ImagenPerfil.image"=>"El archivo debe ser de tipo imagen jpg,png o gif",
            "ImagenPerfil.max_nombre_imagen"=>"El nombre de la imagen no debe contener como maximo 100 caracteres",
        ];
    }
}
