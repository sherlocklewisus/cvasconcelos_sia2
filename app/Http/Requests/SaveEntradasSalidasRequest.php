<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveEntradasSalidasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "FechaOperacion"=>"required",
            "idTipoOperacionEfectivoES"=>"required",
            "idTipoNivel"=>"required",
            /*"Costo"=>"required|numeric",*/
            "Monto"=>"required|numeric",
            "Cantidad"=>"required|numeric",
            /*"TotalPago"=>"required|numeric",*/
            "DescripcionConcepto"=>"max:45",
        ];
    }

    public function messages()
    {
        return [
            "FechaOperacion.required"=>"El campo fecha operación es requerido",
            "idTipoOperacionEfectivoES.required"=>"El campo Tipo operacion efectivo es requerido",
            "idTipoNivel.required"=>"Debes seleccionar la operación a realizar",
            "Costo.required"=>"El campo costo es requerido",
            "Costo.numeric"=>"El campo costo debe ser numerico",
            "Monto.required"=>"El campo Monto es requerido",
            "Monto.numeric"=>"El campo Monto debe ser numerico",
            "Cantidad.required"=>"El campo Cantidad es requerido",
            "Cantidad.numeric"=>"El campo Cantidad debe ser numerico",
            "TotalPago.required"=>"El campo Total a pagar es requerido",
            "TotalPago.numeric"=>"El campo Total a pagar debe ser numerico",
            "DescripcionConcepto"=>"El campo Descripcion Concepto debe contener 45 caracteres como maximo",
        ];
    }
}
