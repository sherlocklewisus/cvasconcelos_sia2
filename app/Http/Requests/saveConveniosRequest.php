<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class saveConveniosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "numero_convenio"=>"required|unique:convenios,num_convenio",
            "Descripcionconv"=>"required|max:300",
            "rd_alumno"=>"required|exists:alumnos,idAlumno",
            "FechaInicio"=>"required|date",
            "FechaVencimiento"=>"required|date",
        ];
    }

    public function messages()
    {
        return [
            "numero_convenio.required"=>"El campo numero de convenio es requerido",
            "numero_convenio.unique"=>"El campo numero de convenio ya se encuentra registrado, introduce uno diferente",
            "Descripcionconv.required"=>"El campo Descripcion es requerido",
            "Descripcionconv.max"=>"El campo Descripcion debe tener maximo :max caracteres",
            "rd_alumno.required"=>"Debes seleccionar un alumno",
            "rd_alumno.exists"=>"El alumno seleccionado no se encuentra registrado en la base de datos",
            "FechaInicio.required"=>"El campo fecha inicio es requerido",
            "FechaInicio.date"=>"El campo fecha inicio debe contener una fecha valida",
            "FechaVencimiento.required"=>"El campo fecha vencimiento es requerido",
            "FechaVencimiento.date"=>"El campo fecha vencimiento debe contener una fecha valida",
        ];
    }
}
