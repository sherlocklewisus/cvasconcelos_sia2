<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveColegiaturasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "rd_alumno"=>"required|exists:alumnos,idAlumno",
            "idPeriodoPago"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "rd_alumno.required"=>"Debes seleccionar un alumno",
            "rd_alumno.exists"=>"El alumnos no se encuentra regsitrado",
            "idPeriodoPago.required"=>"Debes seleccionar una opción de la lista Periodo de Pago",
        ];
    }

}
