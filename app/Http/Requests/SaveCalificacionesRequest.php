<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveCalificacionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "alumnos.*"=>Rule::unique("calificaciones","idAlumno")->where(function($query){
                $query->where("idMateria",$this->idMateria)
                      ->where("idPeriodoEvaluacion",$this->idPeriodoEvaluacion);
            }),
            "CalificacionParcial[*][*]"=>"numeric",
        ];
    }

    public function messages()
    {
        return [
            "alumnos.*.unique"=>"El alumno  ya tiene una calificacion asignada",
            "CalificacionParcial.*.*.numeric"=>"La calificacion debe ser numerica",
        ];
    }
}
