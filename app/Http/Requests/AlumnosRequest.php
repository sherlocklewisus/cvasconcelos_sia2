<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class AlumnosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // "Email"=>"required|email",
            "Nombres"=>"required",
            "ApellidoPaterno"=>"required|regex:/^[\pL\s\-]+$/u|max:50",
            "ApellidoMaterno"=>"required|regex:/^[\pL\s\-]+$/u|max:50",
            "FechaNacimiento"=>"date|before:".Carbon::now()->subYear(1)->format("Y/m/d"),
            "idSeccion"=>"required|exists:secciones,idSeccion",
            "idGrado"=>"required|exists:grados,idGrado",
            "idEstado"=>"required|exists:estados,idEstados",
            "Telefono"=>"digits_between:7,10",
            "CodigoPostal"=>"required",
            "Colonia"=>"required",
            "Calle"=>"required",
            "NumeroExterior"=>"required",
            "FechaEntregaDocumento"=>"date",
            "ActualizacionEntregaDocumento"=>"date|after:".$this->FechaEntregaDocumento,
            "DetalleEnfermedad"=>"required_if:Enfermedad,1",
            "detalle_ejercicio"=>"required_if:ejercicio,1",
            "NotaDocumentos"=>"max:300",
            "Saldo"=>"numeric",
            "Abono"=>"numeric",
            "PorcentajeBeca"=>"required_if:Beca,1|numeric",
            "imagenPerfil"=>"file|image",
        ];
    }

    public function messages()
    {

        return [
                "Email.required"=>"El campo email es requerido",
                "Email.email"=>"El campo email no tiene el formato correcto",
                "Nombres.required"=>"El campo nombre es requerido",
                "ApellidoPaterno.required"=>"El campo Apellido paterno es requerido",
                "ApellidoPaterno.alpha"=>"El campo Apellido paterno solo debe contener letras",
                "ApellidoPaterno.max"=>"El campo apellido Paterno debe tener como maximo 45 caracteres",
                "ApellidoMaterno.required_without"=>"El campo Apellido Materno es obligatorio, si el campo extranjero no esta seleccionado",
                "ApellidoMaterno.alpha"=>"El campo Apellido materno solo debe contener letras",
                "ApellidoMaterno.max"=>"El campo apellido materno debe tener como maximo 45 caracteres",
                "FechaNacimiento.date"=>"El campo fecha de nacimiento debe contener una fecha valida",
                "FechaNacimiento.before"=>"El campo Fecha de nacimiento debe contener una fecha menor a ".Carbon::now()->subYear(1)->format("Y/m/d"),
                "idSeccion.required"=>"El campo de seleccion secciones es requerido",
                "idSeccion.exists"=>"La opcion seleccionada del menu secciones no es valida",
                "idGrado.required"=>"El campo de seleccion grado es requerido",
                "idGrado.exists"=>"La opcion seleccionada del menu grado no es valida",
                "idGrupo.required"=>"El campo de seleccion grupo es requerido",
                "idGrupo.exists"=>"La opcion seleccionada del menu grupo no es valida",
                "idEstado.required"=>"El campo de seleccion estado es requerido",
                "idEstado.exists"=>"La opcion seleccionada del menu estado no es valida",
                "Ciudad.required"=>"El campo de seleccion municipio es requerido",
                "Ciudad.exists"=>"La opcion seleccionada del menu municipio no es valida",
                "id_religion.required"=>"El menu de seleccion religion es obligatorio",
                "id_religion.exists"=>"La opcion seleccionada delmenu religion no es validad",
                "Telefono.digits_between"=>"El campo telefono debe contener entre 7 y 10 digitos",
                "Religion.max"=>"El campo religion debe tener como maximo 45 caracteres",
                "CodigoPostal.required"=>"el campo :attribute es requerido",
                "Colonia.required"=>"el campo :attribute es requerido",
                "Calle.required"=>"el campo :attribute es requerido",
                "NumeroExterior.required"=>"el campo :attribute es requerido",
                "FechaEntregaDocumento.date"=>"El campo Fecha entrega documento debe ser una fecha valida",
                /*"ActualizacionEntregaDocumento.date"=>"El campo actualizacion entregaDocumento debe ser de tipo date",
                "ActualizacionEntregaDocumento.after"=>"El campo actualizacion entregaDocumento debe ser posterior a la fecha de entrega",*/
                "NotaDocumentos.max"=>"El campo nota documentos debe tener como maximo 300 caracteres",
                "DetalleEnfermedad.required_if"=>"El campo detalle enfermedad es requerido",
                "detalle_ejercicio.required_if"=>"El campo detalle ejercicio es requerido",
                "Saldo.numeric"=>"El campo saldo debe ser numerico",
                "Abono.numeric"=>"El campo abono debe ser numerico",
                "PorcentajeBeca.required_if"=>"El campo porcentaje beca es obligatorio",
                "PorcentajeBeca.numeric"=>"El campo porcentaje beca debe ser numerico",
                "imagenPerfil.max_nombre_imagen"=>"El nombre de la imagen no debe contener como maximo 100 caracteres",
            ];
    }
}
