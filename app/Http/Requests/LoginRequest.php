<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email"=>"required|email",
            "password"=>"required|min:6",
        ];
    }

    public function messages()
    {
        return [
            "email.required"=>"El campo email es requerido",
            "email.email"=>"El campo email no tiene en formato correcto",
            "password.required"=>"El campo password es requerido",
            "password.min"=>"El campo password deb contener como minimo 6 caracteres",
        ];
    }
}
