<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveArqueosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /*
            "saldo_inicial"=>"required|numeric",
            */
            "FechaCorte"=>"required|date",
            "codigo"=>"required",
            "id_funcionario_manejo"=>"required|exists:colaboradores,idColaborador",
            "FuncionarioControl"=>"required|exists:colaboradores,idColaborador",
            "fecha_elab_arqueo"=>"required|date",
            "fecha_conteo"=>"required|date",
            "hora_elab_arqueo"=>"required",
            "hora_conteo"=>"required",
        ];
    }

    public function messages()
    {
        return [
            /*"saldo_inicial.required"=>"El campo saldo inicial es requerido",
            "saldo_inicial.numeric"=>"El campo saldo inicial debe ser numerico",*/
            "FechaCorte.required"=>"El campo Fecha Corte es requerido",
            "FechaCorte.date"=>"El campo Fecha Corte debe ser una fecha valida",
            "codigo.required"=>"El campo codigo es requerido",
            "id_funcionario_manejo.required"=>"El campo funcionario es requerido",
            "id_funcionario_manejo.exists"=>"El funcionario debe estar registrado en la base de datos",
            "FuncionarioControl.required"=>"El campo FuncionarioControl es requerido",
            "FuncionarioControl.exists"=>"El FuncionarioControl debe estar registrado en la base de datos",
            "hora_elab_arqueo.required"=>"El campo hora es requerido",
            "fecha_elab_arqueo.required"=>"El campo Fecha elaboracion arqueo es requerido",
            "fecha_elab_arqueo.date"=>"El campo Fecha elaboracion debe contener una fecha valida",
            "fecha_conteo.required"=>"El campo fecha conteo arqueo es requerido",
            "fecha_conteo.date"=>"El campo fecha conteo debe contener una fecha valida",
            "hora_conteo.required"=>"El campo hora conteo es requerido",
        ];
    }
}
