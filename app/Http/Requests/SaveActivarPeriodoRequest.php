<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveActivarPeriodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "idSeccion"=>"required|exists:secciones",
            "idCiclo"=>"required",
            "IdPeriodoEvaluacion"=>"required",
            "slp_dias_activo"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "idSeccion.required"=>"El campo Seccion es requerido",
            "idSeccion.exists"=>"La sección seleccionada no se encuntra registrada",
            "idCiclo.required"=>"El campo ciclo es requerido",
            "IdPeriodoEvaluacion.required"=>"El campo : es requerido",
            "slp_dias_activo.required"=>"El campo dias es requerido",
        ];
    }
}
