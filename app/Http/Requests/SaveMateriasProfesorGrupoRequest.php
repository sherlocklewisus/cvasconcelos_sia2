<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\GruposMaterias;

class SaveMateriasProfesorGrupoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "materias_grupo.*"=>"unique:materia_profesor_grupo,id_materia_grupo",
        ];
    }

    public function messages()
    {
        return [
            "materias_grupo.*.unique"=>"La materia-grupo ya se encuentra asignada a un profesor",
        ];
    }
}
