<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveTareaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "idMateria"=>"required",
            "tarea"=>"required",
            "Descripcion"=>"required",
            "FechaEntrega"=>"required",
        ];
    }

    public function messages()
    {
        return [
            "idMateria.required"=>"El campo Materia es requerido.",
            "tarea.required"=>"El campo Nombre de la tarea es requerido.",
            "Descripcion.required"=>"El campo Descripción es requerido.",
            "FechaEntrega.required"=>"El campo Fecha de entrega es requerido.",
        ];
    }
}
