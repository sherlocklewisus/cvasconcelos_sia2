<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;

use Closure;

class IsAlumno
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $type = $this->auth->user()->typeUser;

        if($type == 1) {
            $pageHome = "inicio-master";
        } else if($type == 2) {
            $pageHome = "inicio-directivo";
        } else if($type == 3) {
            $pageHome = "inicio-director";
        } else if($type == 4) {
            $pageHome = "inicio-profesor";
        } else if($type == 5) {
            $pageHome = "inicio-control-escolar";
        }else if($type == 9){
            $pageHome = "inicio-control-escolar";
        }else if($type == 10){
            $pageHome = "inicio-control-escolar";
        }else if($type == 6) {
            $pageHome = "inicio-caja";
        } else if($type == 7) {
            $pageHome = "inicio-tutor";
        } else {
            $pageHome = "inicio-alumno";
        }

        if ($type != '8') {
            //$this->auth->logout();
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->to($pageHome);
            }
        }

        return $next($request);
    }
}
