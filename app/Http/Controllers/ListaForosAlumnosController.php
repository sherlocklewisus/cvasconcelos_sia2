<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\ForosLista;
use App\GruposMaterias;
use App\GruposMateriaProfesor;
use Auth;
use DB;

class ListaForosAlumnosController extends Controller
{
    public function index() {
    	$user = Auth::user()->alumno->idGrupo;

		$foros = ForosLista::where('idgrupo', '=', $user)->orderBy('idForo', 'ASC')->get();
		$foros->each(function($foros) {
            $foros->seccional;
            $foros->gradual;
            $foros->grupal;
            $foros->material;
        });

    	return view('sistema/alumnos/lista-foros', ['foros' => $foros]);
    }
}
