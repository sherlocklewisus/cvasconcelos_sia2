<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Alumnos;
use App\Horarios;
use App\HorarioEst;
use App\tutoresalumnos;
use App\Materias;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\PeriodosEscolares;


use DB;
use Auth;

class VerHorarioTutorController extends Controller
{
    public function index() {
        $seleccionado = $_GET['id'];
        $alumnos = Alumnos::where('idAlumno', '=', $seleccionado)->get();
        $alumnos->each(function($alumnos) {
            $alumnos->periodo;
            $alumnos->seccion;
            $alumnos->grado;
            $alumnos->grupo;
        });

        $horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios) {
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $estructura = Horarios::get();
        $materias = Materias::get();
        $secciones = Secciones::get();
        $grados = Grados::get();
        $grupos = Grupos::get();


    	return view ('sistema/tutores/ver-horario-tutor', ['horarios' => $horarios, 'estructura' => $estructura, 'alumnos' => $alumnos, 'materias' => $materias]);
    }
}
