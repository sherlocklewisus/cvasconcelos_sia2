<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\ForosLista;
use DB;
use Auth;

class ListadoForosController extends Controller {

	public function index() {
		$user = Auth::user()->colaborador->idColaborador;

        $foros = ForosLista::where('idprofesor', '=', $user)->orderBy('idForo', 'ASC')->get();
        $foros->each(function($foros) {
            $foros->seccional;
            $foros->gradual;
            $foros->grupal;
            $foros->material;
        });

    	return view('sistema/profesores/listado-foros', ['foros' => $foros]);
    }
}
