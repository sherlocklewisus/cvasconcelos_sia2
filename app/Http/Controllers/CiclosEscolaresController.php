<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SaveCiclosEscolaresRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

use App\CiclosEscolares;
use App\Secciones;
use App\Alumnos;
use App\Grados;
use App\Grupos;
use App\User;
use App\Colaboradores;
use App\Historico;
use Exception;
use DB;
use Auth;

class CiclosEscolaresController extends Controller
{
    public function index(Request $request) {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $ciclosEscolares = CiclosEscolares::where("idSeccion",'=',$secciones2->idSeccion)->orWhere('idSeccion','=',1)->orderBy("FechaFin","DESC")->paginate(10);
              }
              else{
                  $ciclosEscolares = CiclosEscolares::where("idSeccion",'=',$secciones2->idSeccion)->orWhere('idSeccion','=',4)->orderBy("FechaFin","DESC")->paginate(10);
              }

          }
          else{
            $secciones = Secciones::all();
            $ciclosEscolares = CiclosEscolares::whereRaw("idSeccion LIKE ?",["%".$request->idSeccion."%"])->orderBy("FechaFin","DESC")->paginate(10);
          }

        return view("sistema.ciclos-escolares.lista-ciclos-escolares",compact("ciclosEscolares","secciones"));
    }

    public function create() {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }

          }
          else{
            $secciones = Secciones::get();
          }

        return view ('sistema.ciclos-escolares.alta-ciclos-escolares', ['secciones' => $secciones]);
    }

    public function store(SaveCiclosEscolaresRequest $request) {
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";

        DB::beginTransaction();

        $validadorFecha = CiclosEscolares::whereRaw("FechaInicio <= ? AND  FechaFin >= ? AND idSeccion = ?",[$request->FechaInicio,$request->FechaFin,$request->idSeccion])->get();
        
        $ciclo_escolar = new CiclosEscolares();
        $ciclo_escolar->Periodo = $request->cicloEscolar;
        $ciclo_escolar->FechaInicio = $request->FechaInicio;
        $ciclo_escolar->FechaFin = $request->FechaFin;
        $ciclo_escolar->idSeccion = $request->idSeccion;
        $ciclo_escolar->Activo = 2;

        $cont = count($validadorFecha);

        if($cont == 0) {
            $escolar = $ciclo_escolar->save();

            if(!$escolar) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El ciclo escolar se ha guardado correctamente.";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("ciclos-escolares/create");
            }

            return Response()->json(["mensaje"=>"El ciclo escolar se ha guardado correctamente."]);
        } else {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Las fechas elegidas ya se encuentran registradas, intentelo de nuevo.";

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("ciclos-escolares/create")->withInput();
            }

            return Response()->json(["mensaje"=>"El ciclo escolar se ha guardado correctamente."]);
        }
    }

    public function obtLastCiclo($idSeccion)
    {
        $ciclo = CiclosEscolares::whereRaw("idSeccion = ?",[$idSeccion])->first();

        return Response()->json($ciclo);
    }

    public function obtCicloActivo($idSeccion)
    {
        $ciclo = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$idSeccion])->first();

        return Response()->json($ciclo);
    }

    public function obtSiguienteCiclo($idSeccion)
    {
        $cicloActivo = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$idSeccion])->first();
        //dd($cicloActivo->data->idPeriodo);
        $ciclo = CiclosEscolares::whereRaw("idSeccion = ? AND idPeriodo > ?",[$idSeccion,$cicloActivo->idPeriodo])->first();
        return Response()->json($ciclo);
    }

    public function abrirNuevoCiclo()
    {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }

          }
          else{
            $secciones = Secciones::all();
          }
        return view("sistema.ciclos-escolares.abrirNuevoCiclo",compact("secciones"));
    }

    public function activarNuevoCiclo(Request $request)
    {
        //dd($request);
        $mensaje_texto = "";
        $status = "";

        DB::beginTransaction();

        try
        {
            //activamos el nuevo ciclo
            $ciclo = CiclosEscolares::find($request->idCiclo);
            $ciclo->Activo = 1;
            $c = $ciclo->save();

            if(!$c)
            {
                throw new Exception("No se pudo actualizar el ciclo");
            }

            //desactivamos los ciclos antriores
            $ac = CiclosEscolares::where("idPeriodo","<>",$ciclo->idPeriodo)
                                ->where("idSeccion","=",$ciclo->idSeccion)
                                ->update(["Activo"=>0]);

            if(!$ac)
            {
                throw new Exception("No se actualizaron los ciclos anteriores");
            }

            DB::commit();
            $mensaje_texto = "El ciclo se actualizo con exito!";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $mensaje_texto = "No se pudo actualizar el ciclo";
            $status = 500;
        }

        return Response()->json(["mensaje"=>$mensaje_texto],$status);
    }

    public function pasarAlumnosDeAnio($idSeccion)
    {
        //return $this->crearHistoricoActual();

        //dd($idSeccion);
        //exit();

        $mensaje_texto = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            $alumnos = Alumnos::where("idSeccion","=",$idSeccion)->get();

            if(count($alumnos) == 0)
            {
                throw new Exception("No hay alumnos registrados");
            }

            /*
            OBTENGO LOS CICLOS ESCOLARES ACTIVOS DE CADA SECCION
            ESTO LO HAGO FUERA DEL CICLO PARA EVITAR REPETIR CONSULTAS
            */

            $cicloKinderActual = CiclosEscolares::where("idSeccion","=",1)
                                                ->where("Activo","=",1)
                                                ->first();
            
            $cicloPrimariaActual = CiclosEscolares::where("idSeccion","=",2)
                                                ->where("Activo","=",1)
                                                ->first();
            
            $cicloSecundariaActual = CiclosEscolares::where("idSeccion","=",3)
                                                ->where("Activo","=",1)
                                                ->first();
            
            $cicloBachillerActual = CiclosEscolares::where("idSeccion","=",4)
                                                ->where("Activo","=",1)
                                                ->first();

            foreach($alumnos as $alumno)
            {
                $seccion = Secciones::find($alumno->idSeccion);
                $grado = Grados::find($alumno->idGrado);

                switch($seccion->idSeccion)
                {
                    case 1:
                        //KINDER
                        if($alumno->idGrado < 3)
                        {
                            $idGrado = $alumno->idGrado;
                            $alumno->idGrado = $idGrado+1;
                            $groupActual = Grupos::find($alumno->idGrupo);

                            $nextGroup = Grupos::where("Grupo","=",$groupActual->Grupo)
                                                ->where("idGrado","=",$idGrado+1)
                                                ->first();
                            if($nextGroup == null)
                            {
                                $nextGroup = new Grupos();
                                $nextGroup->Grupo = $groupActual->Grupo;
                                $nextGroup->idGrado = $idGrado+1;
                                $nextGroup->save();
                            }

                            $alumno->idGrupo = $nextGroup->idGrupo;

                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloKinderActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloKinderActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();
                            
                        }
                        else
                        {
                            $alumno->idSeccion = 2;
                            $alumno->idGrado = 4;
                            $alumno->idGrupo = null;
                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloPrimariaActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloPrimariaActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();
                        }
                        break;
                    
                    case 2:
                        //PRIMARIA

                        if($alumno->idGrupo == null)
                        {
                            break;
                        }

                        if($alumno->idGrado < 8)
                        {
                            $idGrado = $alumno->idGrado;
                            $alumno->idGrado = $idGrado+1;
                            $groupActual = Grupos::find($alumno->idGrupo);

                            $nextGroup = Grupos::where("Grupo","=",$groupActual->Grupo)
                                                ->where("idGrado","=",$idGrado+1)
                                                ->first();
                            if($nextGroup == null)
                            {
                                $nextGroup = new Grupos();
                                $nextGroup->Grupo = $groupActual->Grupo;
                                $nextGroup->idGrado = $idGrado+1;
                                $nextGroup->save();
                            }

                            $alumno->idGrupo = $nextGroup->idGrupo;

                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloPrimariaActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloPrimariaActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();
                            
                        }
                        else
                        {
                            $alumno->idSeccion = 3;
                            $alumno->idGrado = 9;
                            $alumno->idGrupo = null;
                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloSecundariaActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloSecundariaActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();
                        }
                        break;
                    
                    case 3:
                        //SECUNDARIA
                        if($alumno->idGrupo == null)
                        {
                            break;
                        }

                        if($alumno->idGrado<15)
                        {
                            $idGrado = $alumno->idGrado;
                            $alumno->idGrado = $idGrado+1;

                            $groupActual = Grupos::find($alumno->idGrupo);

                            $nextGroup = Grupos::where("Grupo","=",$groupActual->Grupo)
                                                ->where("idGrado","=",$idGrado+1)
                                                ->first();
                            if($nextGroup == null)
                            {
                                $nextGroup = new Grupos();
                                $nextGroup->Grupo = $groupActual->Grupo;
                                $nextGroup->idGrado = $idGrado+1;
                                $nextGroup->save();
                            }

                            $alumno->idGrupo = $nextGroup->idGrupo;

                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloSecundariaActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloSecundariaActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();

                        }
                        else
                        {
                            //si el alumno termino tercero de secundaria pasara a bachiller
                            $alumno->idSeccion = 4;
                            $alumno->idGrado = 16;
                            $alumno->idGrupo = null;
                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloBachillerActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloBachillerActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();

                        }

                        break;
                    
                    case 4:
                        //id grupos bachiller
                        /*
                            "1 SEMESTRE"=>"16",
                            "2 SEMESTRE"=>"17",
                            "3 SEMESTRE"=>"18",
                            "4 SEMESTRE"=>"19",
                            "5 SEMESTRE"=>"20",
                            "6 SEMESTRE"=>"21"
                        */
                        if($alumno->idGrupo == null)
                        {
                            break;
                        }

                        if($alumno->idGrado < 21)
                        {

                            $idGrado = $alumno->idGrado;

                            $alumno->idGrado = $idGrado+1;

                            $groupActual = Grupos::find($alumno->idGrupo);

                            $nextGroup = Grupos::where("Grupo","=",$groupActual->Grupo)
                                                ->where("idGrado","=",$idGrado+1)
                                                ->first();
                            if($nextGroup == null)
                            {
                                $nextGroup = new Grupos();
                                $nextGroup->Grupo = $groupActual->Grupo;
                                $nextGroup->idGrado = $idGrado+1;
                                $nextGroup->save();
                            }

                            $alumno->idGrupo = $nextGroup->idGrupo;

                            $al = $alumno->save();

                            if(!$al)
                            {
                                throw new Exception("No se pudieron actualizar los alumnos");
                            }

                            $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                                        ->where("idCiclo","=", $cicloBachillerActual->idPeriodo)
                                        ->where("idSeccion","=", $alumno->idSeccion)
                                        ->where("idGrado","=", $alumno->idGrado)
                                        ->where("idGrupo","=",$alumno->idGrupo)
                                        ->get();
                           
                            if(count($h)==0)
                            {
                                $h = new Historico();
                                $h->idAlumno = $alumno->idAlumno;
                                $h->idCiclo = $cicloBachillerActual->idPeriodo;
                                $h->idSeccion = $alumno->idSeccion;
                                $h->idGrado = $alumno->idGrado;
                                $h->idGrupo = $alumno->idGrupo;
                                $h->save();
                            }
                            else{
                                throw new Exception("Ya se han cambiado los alumnos ha este ciclo");
                            }
                            
                            $h->save();

                        }
                        else
                        {
                            /*
                            si el alumnos ya concluyó todos los
                            los grados se da de baja en el sistema
                            */
                            $user = User::find($alumno->id_usuario);
                            
                            $user->delete();
                            $alumno->delete();

                        }

                        

                       /* echo $alumno->Nombres;
                        echo $seccion->Seccion;
                        echo $grado->Grado."<br />";*/

                        break;
                }

                
                

            }

            /*foreach(Grados::select("Grado","idGrado")->where("idSeccion","=",4)->get() as $g)
            {
                echo $g->Grado." ".$g->idGrado."<br />";
            }*/
            //DB::rollback();
            DB::commit();
            $mensaje_texto = "Lo alumnos se actualizaron con exito!";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $mensaje_texto = $ex->getMessage();
            $status = 500;
            //echo $ex->getMessage();
        }

        return Response()->json(["mensaje"=>$mensaje_texto],$status);
        
    }

    private function crearHistoricoActual()
    {
        $mensaje_texto = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            $alumnos = Alumnos::all();
            foreach($alumnos as $alumno)
            {
                //debe de haber algun cilco activo sino devolvera errero
                $ciclo = CiclosEscolares::where("idSeccion","=",$alumno->idSeccion)
                                        ->where("Activo","=",1)
                                        ->first();

                if(!$ciclo)
                {
                    throw new Exception("Necesitar tener un ciclo activo en la seccion ".$alumno->idSeccion);
                }
                
                //return ([$ciclo,$alumno]);
            
                $h = Historico::where("idAlumno","=",$alumno->idAlumno)
                              ->where("idCiclo","=", $ciclo->idPeriodo)
                              ->where("idSeccion","=", $alumno->idSeccion)
                              ->where("idGrado","=", $alumno->idGrado)
                              ->where("idGrupo","=",$alumno->idGrupo)
                              ->get();
                
                

                if(count($h)==0)
                {
                    $h = new Historico();
                    $h->idAlumno = $alumno->idAlumno;
                    $h->idCiclo = $ciclo->idPeriodo;
                    $h->idSeccion = $alumno->idSeccion;
                    $h->idGrado = $alumno->idGrado;
                    $h->idGrupo = $alumno->idGrupo;
                    $hh = $h->save();

                    if(!$hh)
                    {
                        throw new Exception("se se pudo agregar el historico");
                    }

                    //return $h;
                }
                else
                {
                    throw new Exception("ya existe un historico en el ciclo actual :)");
                }
            }

            DB::commit();
            $mensaje_texto = "El historico se creo con exito";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $mensaje_texto = $ex->getMessage()." ".$ex->getLine();
            $status = 500;
        }

        return Response()->json(["mensaje"=>$mensaje_texto],$status);
    }

}
