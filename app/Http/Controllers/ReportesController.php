<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlumnosRequest;
use App\Http\Requests\AlumnosUpdateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Route;

use App\Reportes;
use App\Alumnos;
use App\Tutores;
use DB;

class ReportesController extends Controller
{

    public function index() {
    	$reportes = Reportes::orderBy('IdReporte', 'DESC')->paginate(5);

        $alumnos = DB::table('alumnos')
        ->select(
            'alumnos.idAlumno',
            'Nombres',
            'ApellidoPaterno',
            'ApellidoMaterno'
        )
        ->orderBy('idAlumno', 'ASC')
        ->get();

        $tutores = DB::table('tutores')
        ->select(
            'tutores.idTutor',
            'Nombres',
            'Apellidos'
        )
        ->orderBy('idTutor', 'ASC')
        ->get();

	   	return view('SIAO/lista-reportes', ['reportes' => $reportes, 'alumnos' => $alumnos, 'tutores' => $tutores]);
    }

    public function destroy($id) {
    	Reportes::destroy($id);
    	return back();
    }

    public function reporte(Request $request) {
        $html = "<h1>Hola mundo</h1>";
        $reportes = Reportes::orderBy("idReporte","DESC")->paginate(5);
        $alumnos = Alumnos::orderBy("idAlumno","DESC")->get();
        $tutores = Tutores::orderBy("idTutor","DESC")->get();

        $PDF = \PDF::loadView('SIAO/lista-reportes',compact("reportes","alumnos","tutores"));
        return $PDF->stream("REPORTES.pdf");
    }
}
