<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\Tutores;
use App\User;
use DB;
use App\Alumnos;
use App\Secciones;
use DomPDF;
use App\Tareas;
use App\periodosescolares;
use App\Materias;
use App\Calificaciones;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use App\tutoresalumnos;
use App\CiclosEscolares;
use Auth;
//use Knp\Snappy\Pdf;

class TutoresController extends Controller {
    public function index(Request $request) {

        $listTutores = Tutores::distinct("*")
                                ->select("tutores.*")
                                ->leftJoin("tutoresalumnos as x2","tutores.idTutor","=","x2.idTutor")
                                ->leftJoin("alumnos as x3","x2.idAlumno","=","x3.idAlumno")
                                ->whereNull("tutores.deleted_at")
                                ->whereRaw("tutores.Nombres LIKE ? AND (concat(x3.Nombres,x3.ApellidoPaterno,x3.ApellidoMaterno) LIKE ? OR concat(x3.Nombres,x3.ApellidoPaterno,x3.ApellidoMaterno) is null)",["%".$request->Nombre."%","%".$request->hijo."%"])
                                ->orderBy('tutores.idTutor', 'DESC')
                                ->paginate(10);

                                //dd($listTutores);
                                
	    return view('sistema/colaboradores/listado-tutor', compact('listTutores'));
    }

    public function destroy(Request $request, $id) {
        $tutor = Tutores::findOrFail($id);
        $user ="";
        $us = true;
        $search = User::where('id','=',$id)->get();
        if(!$search)
        {
            $user = User::findOrFail($tutor->id_usuario);
        }

        DB::beginTransaction();

        $tutor->EliminadoPor = $request->elimino;
        $tutor->MotivoEliminar = $request->MotivoA;
        $tutor->save();

        $tu = $tutor->delete();
        if($user != ""){
            $us = $user->delete();
        }

        if(!$tu || !$us) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que sucedio un error, intentelo nuevamente.";
        } else {
            DB::commit();
            $msj = "";
            
            if(count($tutor->alumnos_hijos()->get()) > 0) {
                foreach ($tutor->alumnos_hijos()->get() as $alumno) {
                    $msj = "El tutor se ha eliminado correctamente, los alumnos relacionados ahora estan disponibles. Da clic <a href='".route('relAlumTuto.index')."'>aquí</a> para asignarles un nuevo tutor.";
                }

                $relacion = tutoresalumnos::where('idTutor', '=', $tutor->idTutor)->get();
                
                foreach($relacion as $rel) {
                    $alumno = tutoresalumnos::findOrFail($rel->idTutoresAlumnos);
                    $delete = $alumno->forceDelete();
                }
            }
            else
            {
                    $msj = "El tutor se ha eliminado correctamente";
            }
            
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = $msj;
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista-tutor?value=7");
        }

        return Response()->json(["mensaje"=>$texto_mensaje]);

    }

    public function getAlumnosTutores(Request $request){

        $var = Alumnos::leftjoin("secciones","secciones.idSeccion","=","alumnos.idSeccion")
        ->leftjoin("grados","alumnos.idGrado","=","grados.idGrado")
        ->leftjoin("grupos","alumnos.idGrupo","=","grupos.idGrupo")
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno,secciones.seccion,grados.Grado) LIKE ?", ["%".$request ->nombre."%"])->get();
        //dd($listAlumnos);
        return Response()->json($var);

        
    }

    public function show($id) {

         
        $tutor = DB::table('tutores')
            ->select(
                'tutores.idTutor',
                'username',
                'password',
                'Nombres',
                'Apellidos',
                'Email',
                'TelefonoCasa',
                'TelefonoMovil',
                'FechaNacimiento',
                'Ocupacion',
                'idEstado',
                'idCiudad',
                'CodigoPostal',
                'Colonia',
                'Calle',
                'NumeroExterior',
                'NumeroInterior',
                'idEstadoCivil',
                'Religion',
                'Bautizado',
                'IglesiaAsistida',
                'Distrito'
                
            )
            ->where('idTutor', '=' ,$id)
            ->orderBy('idTutor', 'ASC')
            ->get();

          
            $estados = DB::table('estados')
                    ->select(
                        'estados.idEstados', 
                        'Estado'
                    )
                    ->get();
              
               $ciudad = DB::table('municipios')
               ->select(
                        'municipios.id',
                        'nombre'
                        )
               ->get();



       return view('sistema/colaboradores/editar-tutores', ['tutor' => $tutor, 'ciudad' => $ciudad, 'estados' => $estados ]);

    }

    public function acceso_historico_tutores(Request $request)
    {
        $listAlumnos = Alumnos::orderBy('idAlumno', 'DESC')->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ?",["%".$request->Nombre."%"])->get();
        if (count($listAlumnos) > 0) {
                       $listTutores = Tutores::select("tutores.Nombres as Nombre","tutores.Apellidos","tutores.Email","tutores.Ocupacion")->join("users","users.id","tutores.id_usuario")
            ->join("tutoresalumnos","tutoresalumnos.idTutor","tutores.idTutor")
            ->join("alumnos","alumnos.idAlumno","tutoresalumnos.idAlumno")
            ->whereRaw("tutores.Nombres LIKE ? OR tutores.Apellidos LIKE ? OR CONCAT(tutores.Nombres,tutores.Apellidos) LIKE ? OR CONCAT(alumnos.Nombres,alumnos.ApellidoPaterno,alumnos.ApellidoMaterno) LIKE ?",["%".$request->Nombre."%","%".$request->Nombre."%","%".$request->Nombre."%","%".$request->Nombre."%"])
                ->orderBy("tutores.idTutor")->paginate(10);
        }else{
             $listTutores = Tutores::select("tutores.Nombres as Nombre","tutores.Apellidos","tutores.Email","tutores.Ocupacion")->join("users","users.id","=","tutores.id_usuario")
            ->whereRaw("tutores.Nombres LIKE ? OR tutores.Apellidos LIKE ? OR CONCAT(tutores.Nombres,tutores.Apellidos) LIKE ?",["%".$request->Nombre."%","%".$request->Nombre."%","%".$request->Nombre."%"])
                ->orderBy("idTutor")->paginate(10);
        }

        return view("sistema.colaboradores.acceso_historico_tutores",compact("listTutores"));
    }

    public function registro_historico_tutores()
    {
        $listTutores = Tutores::orderBy('idTutor', 'ASC')->onlyTrashed()->get();
        return view("sistema.colaboradores.registro_historico_tutores",compact("listTutores"));
    }

    public function restore(Request $request, $id)
    {
        //dd("todo ok");
        $tutor = Tutores::withTrashed()->where("idTutor","=",$id)->first();
        //dd($tutor);
        $user ="";
        $us = true;
        $search = User::where('id','=',$id)->get();
        if(!$search)
        {
            $user = User::withTrashed()->where("id","=",$tutor->id_usuario)->first();
        }
        //dd($user);
        
        DB::beginTransaction();
            $tu = $tutor->restore();
            if($user != ""){
                $us = $user->restore();
            }

        if(!$tu || !$us)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "El tutor no se pudo restablecer";
            $status = 500;//Internal Error
        }
        else
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El tutor se restablecio con exito!";
            $status = 200;//ok
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            //return redirect("/historico_alumnos");
            return redirect("/tutores/".$tutor->idTutor);
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }


    public function reporte_lista_tutor_alumnos(Request $request)
    {
       
            //dd($listAlumnos);
            //$listAlumnos = Alumnos::orderBy("idAlumno","DESC")->paginate(5);
            //dd($listAlumnos);
            //return $listAlumnos;
            //dd($listAlumnos);
        $listTutores = Tutores::distinct("*")
                                ->select("tutores.*")
                                ->leftJoin("tutoresalumnos as x2","tutores.idTutor","=","x2.idTutor")
                                ->leftJoin("alumnos as x3","x2.idAlumno","=","x3.idAlumno")
                                ->whereNull("tutores.deleted_at")
                                ->whereRaw("tutores.Nombres LIKE ? OR concat(x3.Nombres,x3.ApellidoPaterno,x3.ApellidoMaterno) LIKE ?",["%".$request->Nombre."%","%".$request->hijo."%"])
                                ->orderBy('tutores.idTutor', 'ASC')
                                ->get();

        /*$alumnos = Alumnos::orderBy("alumnos.idSeccion")
                            ->orderBy("alumnos.idGrado")
                            ->orderBy("alumnos.idGrupo")
                            ->get();*/
                            //dd($alumnos);
        /*$alumnos  = Tutores::distinct("*")
                                ->select("tutores.*")
                                ->leftJoin("tutoresalumnos as x2","tutores.idTutor","=","x2.idTutor")
                                ->leftJoin("alumnos as x3","x2.idAlumno","=","x3.idAlumno")
                                ->whereNull("tutores.deleted_at")
                                ->whereRaw("tutores.Nombres LIKE ? OR concat(x3.Nombres,x3.ApellidoPaterno,x3.ApellidoMaterno) LIKE ?",["%".$request->Nombre."%","%".$request->hijo."%"])
                                ->orderBy('tutores.idTutor', 'ASC')
                                ->get();*/
                                //dd($alumnos);
        return \DomPDF::loadView("SIAO.tutores.reporte_lista_tutor_alumnos",compact("alumnos","listTutores"))->stream('archivo.pdf');
        /*return \PDF::loadView("SIAO.tutores.reporte_lista_tutor_alumnos", [])->stream('nombre-archivo.pdf');*/
        /*return PDF::loadFile('https://github.com/barryvdh/laravel-snappy/issues/46')->stream('github.pdf');*/
        //return \PDF::loadHtml('<h1>Titulo</h1>')->stream('github.pdf');
        //dd($_SERVER);
        //return "server";
        //return PDF::loadFile('http://github.com/')->stream('github.pdf'); 

        /*$snappy = new Pdf();
        echo $snappy->getOutput(array('http://www.github.com','http://www.knplabs.com','http://www.php.net'));*/
        //return $snappy->generateFromHtml("https://github.com/","github.pdf");//("SIAO.tutores.reporte_lista_tutor_alumnos");
        //return $snappy->stream("alumnos.pdf");

        /*$pdf = \PDF::loadView("SIAO.tutores.reporte_lista_tutor_alumnos");
        return $pdf->stream("alumnos.pdf");*/
        //return DomPDF::loadFile('http://www.github.com')->stream('github.pdf');
    }


    public function reporte_perfiles()
    {
        $usuarios = User::all();
        return DomPDF::loadView("SIAO.usuarios.reporte_perfiles",["usuarios"=>$usuarios])->stream('perfiles.pdf');

    }

    public function reporte_perfil($id)
    {
        $user = User::findOrFail($id);
        return DomPDF::loadView("SIAO.usuarios.reporte_perfil",["user"=>$user])->stream('perfil.pdf');
        //return view("SIAO.usuarios.reporte_perfil",["user"=>$user]);
    }


    public function tareas_hijo()
    {
        $id_hijo = $_GET['id'];

        $hijo = Alumnos::find($id_hijo);
        return view("sistema.tutores.tareas_hijos",compact("hijo"));
    }

    public function detalle_tarea_hijo()
    {
        $id_grupo_materia = $_GET['matter'];

        $tareas = Tareas::whereRaw("tareas.id_materia_grupo = ?",[$id_grupo_materia])->get();
        return view("sistema/tutores/detalle_tareas_hijo",compact("tareas"));
    }

    public function calificaciones_hijo()
    {

        $seleccionado = $_GET['id'];
        $alumnos = Alumnos::where('idAlumno', '=', $seleccionado)->get();
        $alumnos->each(function($alumnos) {
            $alumnos->periodo;
            $alumnos->seccion;
            $alumnos->grado;
            $alumnos->grupo;
        });

        $alumnosx2 = Alumnos::where('idAlumno', '=', $seleccionado)->pluck('idGrupo');
        
        $relaciones = GruposMaterias::where('id_grupo', '=', $alumnosx2)->pluck('id_materia');
        
        $materias = Materias::whereIn('idMateria', $relaciones)->get();
        $profesor = GrupoMateriaProfesor::orderBy('id', 'ASC')->get();

        $profesor->each(function($profesor) {
            $profesor->profesor;
        });

        $relacionmaterias = GruposMaterias::whereIn('id_materia', $materias)->get();
                

        return view('sistema/tutores/calificaciones_hijo', ['alumnos' => $alumnos, 'materias' => $materias]);
    }

    public function historial_academico_hijo()
    {
        $seleccionado = $_GET['id'];
        $alumnos = Alumnos::where('idAlumno', '=', $seleccionado)->get();
        $alumnos->each(function($alumnos) {
            $alumnos->periodo;
            $alumnos->seccion;
            $alumnos->grado;
            $alumnos->grupo;
        });

        $alumnosx2 = Alumnos::where('idAlumno', '=', $seleccionado)->pluck('idGrupo');
        $relaciones = GruposMaterias::where('id_grupo', '=', $alumnosx2)->pluck('id_materia');
        $materias = Materias::whereIn('idMateria', $relaciones)->get();

        $profesor = GrupoMateriaProfesor::orderBy('id', 'ASC')->get();
        $profesor->each(function($profesor) {
            $profesor->profesor;
        });
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        //$materias = Materias::whereIn('idMateria', $relaciones)->pluck('idMateria');
        $relacionmaterias = GruposMaterias::whereIn('id_materia', $materias)->get();

        $calificaciones = Calificaciones::orderBy('idCalificacion', 'ASC')->get();

        //return view("sistema/tutores/calificaciones_hijo",compact("alumnos", "periodos", "materias", "calificaciones", "relaciones", "relacionmaterias"));
        return view('sistema/tutores/historial_academico_hijo', ['alumnos' => $alumnos, 'materias' => $materias]);

        //return view("sistema.tutores.historial_academico_hijo",compact("alumnos", "periodos", "materias", "calificaciones", "relaciones", "relacionmaterias"));
    }

    public function establecer_hijo(Request $request)
    {
        //dd(url()->previous());
        $url = explode("/", url()->previous());
        $cad = "";
        for($i=0;$i<(count($url)-1);$i++)
        {
            $cad.=$url[$i]."/";
        }
        //dd($url[0] = $url[0]."?id_hijo=".Session::get("hijo"));
        Session::put("hijo",$request->id_hijo);
        //return back();
        return redirect($cad.Session::get("hijo"));

        //return Response()->json(["mensaje"=>"se establecio el hijo".$request->id_hijo],200);
    }

    public function update(Request $request, $id){
    {
       // dd($request);
             $tuto = Tutores::find($id);

    
            $this->validate($request, [
            "username"=>"required",
            "password" => "min:8|confirmed",
            "Nombres"=>"required|regex:/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/",
            "Apellidos"=>"required|regex:/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/",
            "Email"=>  Rule::unique('tutores')->ignore($tuto->Email,"Email"),
            "TelefonoCasa"=>"required|min:7",
            "TelefonoMovil"=>"min:7",
            "Ocupacion"=>"required",
            "Estados"=>"required",
            // "idCiudad"=>"required",
            "CodigoPostal"=>"required",
            "Colonia"=>"required",
            "Calle"=>"required",
            "NumeroExterior"=>"required",
            "EstadoCivil"=>"required",
            "id_religion"=>"required",
            /*"Bautizado"=>"required",
            "IglesiaAsistida"=>"required",
            "Distrito"=>"required",*/
        ]);
         $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";
            $tt = true;
        DB::beginTransaction();

            $user = User::find($tuto->id_usuario);
            $user->name = $request->username;
            $user->email = $request->Email;
            $user->password=$request->password;
            $user->typeUser = 7;
            $user->ImagenPerfil = $request->imagenPerfil;
            $us = $user ->save();


            $tuto = Tutores::find($id);
            $tuto->username = $request->username;
            $tuto->password = $request->password;
            $tuto->Nombres = $request->Nombres;
            $tuto->Apellidos = $request->Apellidos;
            $tuto->Email = $request->Email;
            $tuto->TelefonoCasa = $request->TelefonoCasa;
            $tuto->TelefonoMovil = $request->TelefonoMovil;
            $tuto->TelefonoExtra1 = $request->TelefonoExtra1;
            $tuto->TelefonoExtra2 = $request->TelefonoExtra2;
            $tuto->FechaNacimiento = $request->FechaNacimiento;
            $tuto->Ocupacion = $request->Ocupacion;
            $tuto->idEstado = $request->Estados;
            $tuto->idCiudad = $request->idCiudad;
            $tuto->CodigoPostal = $request->CodigoPostal;
            $tuto->Colonia = $request->Colonia;
            $tuto->Calle = $request->Calle;
            $tuto->NumeroExterior = $request->NumeroExterior;
            $tuto->NumeroInterior = $request->NumeroInterior;
            $tuto->idEstadoCivil = $request->EstadoCivil;
            //$tuto->Religion = $request->Religion;
            if($request->id_religion == 6)
            {
                if($request->original_id_religion <= 6)
                {
                    $religion = new Religiones();
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    $tuto->Religion = $religion->id;
                }
                else
                {
                    $religion = Religiones::find($request->original_id_religion);
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    $tuto->Religion = $religion->id;
                }
            }
            else
            {
                $tuto->Religion = $request->id_religion;
            }
            $tuto->Bautizado = $request->Bautizado;
            $tuto->IglesiaAsistida = $request->IglesiaAsistida;
            $tuto->Distrito = $request->Distrito;
            $tu = $tuto->save(); 

            if($request->Alumno == NULL || $request->parentesco == NULL)
            {

            }
            else{
                 $messages = [
            'unique' => 'ERROR!! El :attribute ya tiene un tutor asignado'
                ];
      
        $this->validate($request, [
                    "Alumno"=>"required|unique:tutoresalumnos,idAlumno",
                    "parentesco"=>"required",
                    /*"Tutor"=>"required"*/
        ],$messages);
                if (count($request->Alumno) > 0 ) {
            foreach ($request->Alumno as $Al) {
            $tutoal = new tutoresalumnos();
            $tutoal->idAlumno = $Al;
            $tutoal->idParentesco = $request->parentesco;
            $tutoal->idTutor = $tuto->idTutor;
            $tt = $tutoal ->save();
        }
        }else{

        }
            }
        
            if(!$tu || !$us || !$tt) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El Tutor no se pudo actualizar";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El Tutor se actualizo con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect('editar-tutores/'.$tuto->idTutor.'?value=7');

            }
           return Response()->json(["mensaje"=>"El Tutor se actualizo con exito!"]);  
    }

    }
}