<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use App\PeriodosPagos;
use App\Secciones;
use App\CostoPagos;
use App\Periodos;
use DB;

class ListaPagosController extends Controller
{
    public function index() {
    	 $coleg = DB::table('costo_colegiaturas')->pluck('id_costo_colegiatura');


        $colegiaturas = CostoPagos::join('periodospagos', 'costo_colegiaturas.id_costo_colegiatura', '=', 'periodospagos.idCosto')
        ->join('periodosescolares', 'periodospagos.idPeriodoEscolar', '=', 'periodosescolares.idPeriodo')
        ->join('secciones','costo_colegiaturas.idSeccion2','=','secciones.idSeccion')
        ->whereIn("periodospagos.idCosto",$coleg)->groupBy('costo_colegiaturas.idSeccion2')->get();
       $costos = CostoPagos::join('periodospagos', 'costo_colegiaturas.id_costo_colegiatura', '=', 'periodospagos.idCosto')
        ->join('periodosescolares', 'periodospagos.idPeriodoEscolar', '=', 'periodosescolares.idPeriodo')
        ->join('secciones','costo_colegiaturas.idSeccion2','=','secciones.idSeccion')
        ->whereIn("periodospagos.idCosto",$coleg)->groupBy('periodospagos.idCosto')->get();

      
        return view ('sistema/caja/listado-pagos', ['colegiaturas' => $colegiaturas, 'costos' => $costos] );
    }
}
