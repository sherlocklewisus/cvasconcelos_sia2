<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\Grados;
use App\Grupos;
use App\Materias;
use App\GruposMaterias;
use DB;
class EditarMateriasController extends Controller
{
    public function getGrado(Request $request, $id) {
        if($request->ajax()){
            $grado = Grados::grado($id);
            return response()->json($grado);
        }
    }

    public function getGrupo(Request $request, $id) {
        if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function show($id) {
        $materia = DB::table('materias')
            ->select(
                'materias.idMateria',
                'Nombre',
                'Semestre',
                'idPeriodo',
                'idSeccion',
                'idColaborador',
                'idGrado',
                'idGrupo',
                'DescripcionHorario',
                'DescripcionCurso',
                'ObjetivosGenerales',
                'ObjetivosEspecificos',
                'Actividades',
                'Bibliografia'
            )
            ->where('idMateria', '=' ,$id)
            ->orderBy('idMateria', 'ASC')
            ->first();

             $periodo = DB::table('periodosescolares')
            ->select('periodosescolares.idPeriodo',
                'Periodo')
            ->get();
             $seccion = DB::table('secciones')
            ->select('secciones.idSeccion',
                'Seccion')
            ->get();

            $colaborador = DB::table('colaboradores')
            ->select('colaboradores.idRol',
                'idColaborador',
                'Nombre',
                'Apellido'
                )
             ->where('idRol', '=' ,'4')
             ->get();

              $grado = DB::table('grados')
            ->select('grados.idGrado',
                'idSeccion',
                'Grado'
                )
             ->get();
             
            $materias = Materias::where('idMateria', '=', $id)->pluck('idGrado');
            $material = Materias::where('idMateria', '=', $id)->pluck('idMateria');
            $relacion = GruposMaterias::where('id_materia', '=', $material)->pluck('id_grupo');

            $grupos = Grupos::whereIn('idGrupo', $relacion)->where('idGrado', '=', $materias)->get();
            $agrupados = Grupos::whereNotIn('idGrupo', $relacion)->where('idGrado', '=', $materias)->get();   
          

       return view('sistema/colaboradores/editar-materias', ['materia' => $materia, 'periodo' => $periodo, 'secciones' => $seccion, 'colaborador' => $colaborador, 'grupos' => $grupos, 'agrupados' => $agrupados]);
   }

     public function update(Request $request, $id){
        $this->validate($request, [
                "Nombre"=>"required",
                "Periodo"=>"required",
                "idSeccion"=>"required",
                "idGrado"=>"required",
                // "Grupo"=>"required",
                // "Docente"=>"required",
                // "DescripcionHorario"=>"required|max:45",
                // "DescripcionCurso"=>"required|max:45",
                // "ObjetivosGenerales"=>"required|max:45",
                // "ObjetivosEspecificos"=>"required|max:45",
                // "Actividades"=>"required|max:500",
                // "Bibliografia"=>"required|max:45",
                "grupos"=>"required_if:check_asignar_materias,1",
                "grupos.*"=>"exists:grupos,idGrupo"
                ],[
                    "grupos.required_if"=>"Debes seleccionar al menos un grupo",
                    "grupos.*.exists"=>"El grupo seleccionado no es valido",
                ]);

                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "";
         try{   
        DB::beginTransaction();

        
            $materia = Materias::find($id);
            $materia->Nombre = $request->Nombre;
            $materia->idPeriodo = $request->Periodo;
            $materia->idSeccion = $request->idSeccion;
            $materia->idGrado = $request->idGrado;
            // $materia->idGrupo = $request->Grupo;
            // $materia->Semestre = $request->Semestre;
            // $materia->idColaborador = $request->Docente;
            $materia->DescripcionHorario = $request->DescripcionHorario;
            $materia->DescripcionCurso = $request->DescripcionCurso;
            $materia->ObjetivosGenerales = $request->ObjetivosGenerales;
            $materia->ObjetivosEspecificos = $request->ObjetivosEspecificos;
            $materia->Actividades = $request->Actividades;
            $materia->Bibliografia = $request->Bibliografia;
            $materia->save(); 


            if(!$materia) {
                        throw new Exception("Parece que ocurrio un error, intentelo de nuevo.");        
                    }
            if($request->check_asignar_materias == 1)
                    {
                        $grupos = $request->grupos;

                        foreach($grupos as $id_grupo)
                        {
                            $id_materia_grupo = DB::table("grupos_materias")
                            ->insertGetId([
                              "id_grupo"=>$id_grupo,
                              "id_materia"=>$materia->idMateria
                            ]);

                        if(!$id_materia_grupo)
                        {
                            throw new Exception("Parece que ocurrio un error, intentelo de nuevo.");
                        }
                    }

                    if($request->check_asignar_materias == 1 && $request->check_asignar_profesor == 1)
                    {
                        $profesor = $request->profesor;

                        $ma_prof_group = DB::table("materia_profesor_grupo")
                                    ->insert([
                                        "id_materia_grupo"=>$id_materia_grupo,
                                        "id_profesor"=>$profesor
                                        ]);

                        if(!$ma_prof_group)
                        {
                            throw new Exception("Parece que ocurrio un error, intentelo de nuevo.");
                        }

                    }
                }

                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "La materia se ha actualizado correctamente.";
                $status = 200;
            }

             catch(Exception $ex)
            {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = $ex->getMessage();
                $status = 500;         
            }
            
            if($request->cambioOpcion == "1") {
                $materiagrupo = GruposMaterias::where([
                    ["id_materia", "=", $id],
                    ["id_grupo", "=", $request->groupOrig]
                ])->pluck('id');
                //dd($materiagrupo);

                $materiagru = GruposMaterias::find($materiagrupo);
                $materiagru->id_grupo = $request->groupNew;
                $materiagru->save();
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect('editar-materias/'.$materia->idMateria.'?value=9');
            }
           return Response()->json(["mensaje"=>"La materia se ha actualizado correctamente."]);  
      
    }

}
