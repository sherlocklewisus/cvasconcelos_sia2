<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\Materias;
use App\Secciones;
use DB;
use App\Colaboradores;
use App\User;
use Auth;


class MateriaslistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
          $tipodeus = Auth::user()->rol->idRol == 9;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
                $listMaterias = Materias::orderBy('idMateria', 'DESC')->whereRaw("Nombre LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ?",["%".$request->Nombre."%","%".$secciones2->idSeccion."%","%".$request->grado."%"])->paginate(10);
                $listMaterias->each(function($listMaterias) {
                    $listMaterias->seccion;
                    $listMaterias->grado;
                });

          }
          else{
            $secciones = Secciones::get();
            $listMaterias = Materias::orderBy('idMateria', 'DESC')->whereRaw("Nombre LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ?",["%".$request->Nombre."%","%".$request->Seccion."%","%".$request->grado."%"])->paginate(10);
            $listMaterias->each(function($listMaterias) {
                $listMaterias->seccion;
                $listMaterias->grado;
            });
          }

        /*$listMaterias = Materias::select("materias.*")
            ->orderBy('idMateria', 'DESC')
            ->whereRaw("materias.Nombre LIKE ? and concat(x2.Seccion,x3.Grado) LIKE ?",["%".$request->Nombre."%","%".$request->Seccion."".$request->grado."%"])->paginate(10);*/
        
        return view('sistema/colaboradores/lista-materias', ['listMaterias' => $listMaterias, 'secciones' => $secciones]);
    }

    public function getSeccion(Request $request, $id) {
        if($request->ajax()){
            $seccion = Grados::gradosNivel($id);
            return response()->json($seccion);
        }
    }


    public function destroy(Request $request, $id)
    {
       
        $materia = Materias::findOrFail($id);
        
        
        $ma = $materia->delete();
        if(!$ma) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se puede borrar materia";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "Se elimino con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("lista-materias?value=9");
            }

        return Response()->json(["mensaje"=>"La materia se borro con exito!"]);

        return redirect('lista-materias');

    }
}
