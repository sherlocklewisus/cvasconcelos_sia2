<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\User;
use App\Alumnos;
use App\periodosescolares;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Historico;

use DB;

class AlumnosTraspasoController extends Controller
{
    public function index(Request $request) {

    	// Estructura de control -> La sentencia de busqueda contiene todos los parametros.
    	if($request->nivel != "" && $request->grado != "" && $request->grupo != "") {
        	$estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
        		['idSeccion', '=', $request->nivel],
        		['idGrado', '=', $request->grado],
        		['idGrupo', '=', $request->grupo]
        	])->orderBy("idAlumno","DESC")->paginate(25);

        	$estudiantes->each(function($estudiantes) {
	            $estudiantes->seccion;
	            $estudiantes->grado;
	            $estudiantes->grupo;
	        });

        // Estructura de control -> La sentencia de busqueda contiene todos solo dos de los parametros.
        } elseif ($request->nivel != "" && $request->grado != "") {
        	$estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
        		['idSeccion', '=', $request->nivel],
        		['idGrado', '=', $request->grado]
        	])->orderBy("idAlumno","DESC")->paginate(25);

        	$estudiantes->each(function($estudiantes) {
	            $estudiantes->seccion;
	            $estudiantes->grado;
	            $estudiantes->grupo;
	        });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        } else {
        	$estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where('idSeccion', '=', $request->nivel)->orderBy("idAlumno","DESC")->paginate(25);

        	$estudiantes->each(function($estudiantes) {
	            $estudiantes->seccion;
	            $estudiantes->grado;
	            $estudiantes->grupo;
	        });
        }

        // Obtenemos la fecha actual del servidor.
        $fecha = date('Y-m-d');

        // Variables de consultas a BD.
                if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $cuarto = User::get();
        $secciones = Secciones::get();
        $periodos = periodosescolares::get();
        $periodoescolar = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', 4]])->get();

        // Estructura de control -> Busqueda de ciclos por medio de las secciones elegidas
        $nivelado = isset($_GET['nivel'])? $_GET['nivel'] : "";
        if($nivelado == "" || $nivelado == null) {
        	$ciclosescolares = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', 0]])->get();
        } else {
        	$ciclosescolares = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', $nivelado]])->get();
        }

        return view ('sistema/colaboradores/lista-alumnos-traspaso', ['estudiantes' => $estudiantes, 'secciones' => $secciones, 'cuarto' => $cuarto, 'ciclosescolares' => $ciclosescolares, 'periodoescolar' => $periodoescolar, 'periodos' => $periodos]);
    }

    public function getPostGrado(Request $request, $id) {
        if($request->ajax()){
        	$fecha = date('Y-m-d');

        	if($id == 4) {
        		$busquedaPeriodo = periodosescolares::where([['idSeccion', '=', $id],['FechaFin', '<', $fecha],['Activo', '=', 1]])->pluck('idPeriodo');
        		$busquedaGrupo = Grupos::whereIn('id_ciclo', $busquedaPeriodo)->pluck('idGrado');
        		$busquedaGrado = Grados::whereIn('idGrado', $busquedaGrupo)->get();

        		return response()->json($busquedaGrado);
        	} else {
        		$gradoValue = Grados::grade($id);
            	return response()->json($gradoValue);
        	}
        }
    }

    public function getPostGrupo(Request $request, $id) {
        if($request->ajax()){
            $grupoValue = Grupos::group($id);
            return response()->json($grupoValue);
        }
    }

    public function getPostSeccion(Request $request, $id) {
        if($request->ajax()){
            $seccionValue = periodosescolares::where('idPeriodo', '=', $id)->get();

	        foreach($seccionValue as $niv) {
	            $value = $niv->idSeccion;

	            if($request->ajax()){
	                $evaluacion = Secciones::periodos($value);
	                return response()->json($evaluacion);
	            }
	        }
        }
    }

    public function getPostGrade(Request $request, $id) {
        if($request->ajax()){
        	$fecha = date('Y-m-d');

        	if($id == 4) {
        		$busquedaPeriodo = periodosescolares::where([['idSeccion', '=', $id],['FechaFin', '<', $fecha],['Activo', '=', 1]])->pluck('idPeriodo');
        		$busquedaGrupo = Grupos::whereIn('id_ciclo', $busquedaPeriodo)->pluck('idGrado');
        		$busquedaGrado = Grados::whereIn('idGrado', $busquedaGrupo)->get();

        		return response()->json($busquedaGrado);
        	} else {
        		$gradoValue = Grados::grade($id);
            	return response()->json($gradoValue);
        	}
        }
    }

    public function getPostGroup(Request $request, $id) {
        if($request->ajax()){
            $grupoValue = Grupos::where('idGrado', '=', $id)->orderBy('Grupo', 'ASC')->get();
            return response()->json($grupoValue);
        }
    }

    public function store(Request $request) {
    	if($request->cicloescolarcerrar == "" || $request->cicloescolarcerrar == null) {
	    	// Variable -> Creamos el arreglo para los valores del formulario.
	    	$pasables = array();
	    	$terminales = array();

			// Estructura de control -> Obtenemos los valores enviados desde el formulario y los almacenamos en el arreglo creado anteriormente.
	    	for ($i = 0; $i < count($request->chequeo); $i++) {     
	    		$pasables[] = ($request->chequeo[$i]);
			}
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
			for ($a = 0; $a < count($request->finalizar); $a++) {     
	    		$terminales[] = ($request->finalizar[$a]);
			}

			// Estructura de control -> Verificamos el tipo de operación que se va a realizar.
			if($pasables == "" || $pasables == null) {
				if($terminales == "" || $terminales == null) {
					DB::rollback();
					$tipo_mensaje = "mensaje-danger";
					$texto_mensaje = "Debe seleccionar minimo un alumno para poder realizar un cambio exitoso.";

					$alumnos = array();
					$cierre = "&cerrar=";
				} else {

					// Sentencias de busqueda, busqueda por parametros a BD.
					$alumnos = Alumnos::whereIn('idAlumno', $terminales)->get();
				}
			} else {

				// Sentencias de busqueda, busqueda por parametros a BD.
				$alumnos = Alumnos::whereIn('idAlumno', $pasables)->get();
			}

			if(count($alumnos) == 0) {
				DB::rollback();
				$tipo_mensaje = "mensaje-danger";
				$texto_mensaje = "Debe seleccionar minimo un alumno para poder realizar un cambio exitoso.";

				$cierre = "&cerrar=";
			} else {
				foreach($alumnos as $alum) {
					$histAlum = $alum->idAlumno;
					$histGrup = $alum->idGrupo;

					$grupos = Grupos::where('idGrupo', '=', $histGrup)->get();

					foreach ($grupos as $grup) {
						$histGrad = $grup->idGrado;

						$grados = Grados::where('idGrado', '=', $histGrad)->get();

						foreach ($grados as $grad) {
							$histSecc = $grad->idSeccion;
							$histOrde = $grad->ordenCampos;
							$fecha = date('Y-m-d');

							$periodos = periodosescolares::where([
								['idSeccion', '=', $histSecc],
								['FechaFin', '<=', $fecha],
								['Activo', '=', 1]
							])->get();

							foreach($periodos as $peri) {
								$histPeri = $peri->idPeriodo;

								// Función de guardado para los datos recolectados en las consultas.
								DB::beginTransaction();

								$Historico = new Historico();
								$Historico ->idCiclo = $histPeri;
								$Historico ->idAlumno = $histAlum;
								$Historico ->idSeccion= $histSecc;
								$Historico ->idGrado = $histGrad;
								$Historico ->idGrupo = $histGrup;
								$save = $Historico->save();

								if(!$save) {

									// Mensaje en caso de error en el proceso de guardado.
									DB::rollback();
									$tipo_mensaje = "mensaje-danger";
									$texto_mensaje = "No se pudo generar el historico del grupo seleccionado.";

									$cierre = "&cerrar=";
								} else {
						            // En caso correcto, se realiza el guardado y continua la función.
									DB::commit();

									// Consulta de edición de los datos.
									$estudiante = Alumnos::findOrFail($histAlum);

									if($terminales == "" || $terminales == null) {
										if($request->grupoNew == "" || $request->grupoNew == null) {
											DB::rollback();
											$tipo_mensaje = "mensaje-danger";
											$texto_mensaje = "Los campos de ciclo, nivel, grado y grupo son obligatorios.";

											$cierre = "&cerrar=";
										} else if($request->grupoNew == "new") {
											$verificarciclo = periodosescolares::where('idPeriodo', '=', $request->cicloNew)->pluck('idPeriodo');
											$nuevogrupo = Grupos::whereIn('id_ciclo', $verificarciclo)->get();

											if(count($nuevogrupo) == 0) {
												$antiguoGrupo = Grupos::where('idGrupo', '=', $histGrup)->get();

												foreach($antiguoGrupo as $ant) {
													$nombreGrupo = $ant->Grupo;
													$ordenGrupo = $ant->ordenCampos;
												}

												$grupo = new Grupos();
												$grupo->idGrado = $request->gradoNew;
												$grupo->Grupo = $nombreGrupo;
												$grupo->id_ciclo = $request->cicloNew;
												$grupo->GrupoActual = 0;
												$grupo->ordenCampos = $ordenGrupo;
												$group = $grupo->save();

												if(!$group) {
													DB::rollback();
													$tipo_mensaje = "mensaje-danger";
													$texto_mensaje = "Parece que ocurrio un error al crear el grupo, intentelo nuevamente.";

													$cierre = "&cerrar=";
												} else {
													DB::commit();

													$ultimogrupo = Grupos::where([['id_ciclo', '=', $request->cicloNew],['Grupo', '=', $nombreGrupo],['ordenCampos', '=', $ordenGrupo]])->get();

													foreach($ultimogrupo as $namegroup) {
														$finalgrupo = $namegroup->idGrupo;
													}

													$estudiante ->idSeccion = $request->seccionNew;
													$estudiante ->idGrado = $request->gradoNew;
													$estudiante ->idGrupo = $finalgrupo;
													$estudian = $estudiante->save();

													if(!$estudian) {
														DB::rollback();
														$tipo_mensaje = "mensaje-danger";
														$texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";

														$cierre = "&cerrar=";
													} else {
														DB::commit();
														$tipo_mensaje = "mensaje-success";
														$texto_mensaje = "Los cambios han sido realizados de forma correcta.";

														$cierre = "&cerrar=";
													}
												}
											} else {
												foreach($nuevogrupo as $grupocreado) {
													$nombreGrupo = $grupocreado->Grupo;
													$ordenGrupo = $grupocreado->ordenCampos;
												}

												$antiguoGrupo = Grupos::where('idGrupo', '=', $histGrup)->get();

												foreach($antiguoGrupo as $ant) {
													$nameGroup = $ant->Grupo;
													$orderGroup = $ant->ordenCampos;
												}

												if($nameGroup == $nombreGrupo) {
													//dd("Existe el grupo");
													$ultimogrupo = Grupos::where([['id_ciclo', '=', $request->cicloNew],['Grupo', '=', $nombreGrupo],['ordenCampos', '=', $ordenGrupo]])->get();

													foreach($ultimogrupo as $namegroup) {
														$finalgrupo = $namegroup->idGrupo;
													}

													$estudiante ->idSeccion = $request->seccionNew;
													$estudiante ->idGrado = $request->gradoNew;
													$estudiante ->idGrupo = $finalgrupo;
													$estudian = $estudiante->save();

													if(!$estudian) {
														DB::rollback();
														$tipo_mensaje = "mensaje-danger";
														$texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";

														$cierre = "&cerrar=";
													} else {
														DB::commit();
														$tipo_mensaje = "mensaje-success";
														$texto_mensaje = "Los cambios han sido realizados de forma correcta.";

														$cierre = "&cerrar=";
													}
												} else {
													//dd("No existe");
													$antiguoGrupo = Grupos::where('idGrupo', '=', $histGrup)->get();

													foreach($antiguoGrupo as $ant) {
														$nombreGrupo = $ant->Grupo;
														$ordenGrupo = $ant->ordenCampos;
													}

													$grupo = new Grupos();
													$grupo->idGrado = $request->gradoNew;
													$grupo->Grupo = $nombreGrupo;
													$grupo->id_ciclo = $request->cicloNew;
													$grupo->GrupoActual = 0;
													$grupo->ordenCampos = $ordenGrupo;
													$group = $grupo->save();

													if(!$group) {
														DB::rollback();
														$tipo_mensaje = "mensaje-danger";
														$texto_mensaje = "Parece que ocurrio un error al crear el grupo, intentelo nuevamente.";

														$cierre = "&cerrar=";
													} else {
														DB::commit();

														$ultimogrupo = Grupos::where([['id_ciclo', '=', $request->cicloNew],['Grupo', '=', $nombreGrupo],['ordenCampos', '=', $ordenGrupo]])->get();

														foreach($ultimogrupo as $namegroup) {
															$finalgrupo = $namegroup->idGrupo;
														}

														$estudiante ->idSeccion = $request->seccionNew;
														$estudiante ->idGrado = $request->gradoNew;
														$estudiante ->idGrupo = $finalgrupo;
														$estudian = $estudiante->save();

														if(!$estudian) {
															DB::rollback();
															$tipo_mensaje = "mensaje-danger";
															$texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";

															$cierre = "&cerrar=";
														} else {
															DB::commit();
															$tipo_mensaje = "mensaje-success";
															$texto_mensaje = "Los cambios han sido realizados de forma correcta.";

															$cierre = "&cerrar=";
														}
													}
												}
											}
										} else {
											$estudiante ->idSeccion = $request->seccionNew;
											$estudiante ->idGrado = $request->gradoNew;
											$estudiante ->idGrupo = $request->grupoNew;
											$estudian = $estudiante->save();

											if(!$estudian) {
												DB::rollback();
												$tipo_mensaje = "mensaje-danger";
												$texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";

												$cierre = "&cerrar=";
											} else {
												DB::commit();
												$tipo_mensaje = "mensaje-success";
												$texto_mensaje = "Los cambios han sido realizados de forma correcta.";

												$cierre = "&cerrar=";
											}
										}
									} else {
										$estudiante ->idSeccion = 0;
										$estudiante ->idGrado = 0;
										$estudiante ->idGrupo = 0;
										$estudian = $estudiante->save();

										if(!$estudian) {
											DB::rollback();
											$tipo_mensaje = "mensaje-danger";
											$texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";

											$cierre = "&cerrar=";
										} else {
											DB::commit();
											$tipo_mensaje = "mensaje-success";
											$texto_mensaje = "Los cambios han sido realizados de forma correcta.";

											$cierre = "&cerrar=";
										}
									}

									$cerrarGrupos = Grupos::where('id_ciclo', '=', $histPeri)->pluck('idGrupo');
									$verifAlumnos = Alumnos::whereIn('idGrupo', $cerrarGrupos)->get();

									if(count($verifAlumnos) == 0) {
										$cierre = "&cerrar=".$histPeri;
									} else {
										$cierre = "&cerrar=";
									}
								}
							}
						}
					}
				}
			}
		} else {
			$cerrarGrupos = Grupos::where('id_ciclo', '=', $request->cicloescolarcerrar)->pluck('idGrupo');
			$verifAlumnos = Alumnos::whereIn('idGrupo', $cerrarGrupos)->get();

			if(count($verifAlumnos) == 0) {

				$cierreescolar = PeriodosEscolares::findOrFail($request->cicloescolarcerrar);
				$cierreescolar ->Activo = 0;
				$cierreperiodo = $cierreescolar->save();

				if(!$cierreperiodo) {
					DB::rollback();
					$tipo_mensaje = "mensaje-danger";
					$texto_mensaje = "Parece que ocurrio un error al cerrar el ciclo, por favor, intentelo de nuevo.";

					$cierre = "&cerrar=".$request->cicloescolarcerrar;
				} else {
					DB::commit();
					$tipo_mensaje = "mensaje-success";
					$texto_mensaje = "El ciclo escolar se ha cerrado correctamente.";

					$cierre = "&cerrar=";
				}
			} else {
				DB::rollback();
				$tipo_mensaje = "mensaje-danger";
				$texto_mensaje = "Parece que ocurrio un error al cerrar el ciclo, por favor, intentelo de nuevo.";

				$cierre = "&cerrar=".$request->cicloescolarcerrar;
			}
		}

		$primero = $request->col_1;
	    $segundo = $request->col_2;
	    $tercero = $request->col_3;
	    $cuarto = $request->col_4;
	    $final = $cierre;

		if(!$request->ajax()) {
			Session::flash($tipo_mensaje, $texto_mensaje);
			return redirect("lista-alumnos-traspaso?value=1&nivel=".$primero."&grado=".$segundo."&Numero=".$cuarto."&grupo=".$tercero."".$final);
		}

		return Response()->json(["mensaje"=>"El historico se genero correctamente."]);

    }
}
