<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SaveActivarPeriodoRequest;
use Illuminate\Support\Facades\Session;

use App\Materias;
use App\periodosevaluacion;
use App\CiclosEscolares;
use App\Secciones;
use Carbon\Carbon;
use DB;
use Exception;
use App\Colaboradores;
use App\User;
use App\Mensajes;
use Auth;

class periodosevaluacionController extends Controller
{
  /**
   * 
   *  Función principal, variables de consulta a BD.
   * 
  **/

  public function index() {
      $tipodeus = Auth::user()->rol->idRol == 9;
      if($tipodeus == 'true'){
          $tipo = Auth::user()->id;
          $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
          $seccion = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
      }
      else{
        $seccion = Secciones::get();
      }

     return view('sistema/colaboradores/periodos-evaluacion', ['seccion' => $seccion]);
  }

  /**
   * 
   *  Petición GET, consulta a modelos externos.
   * 
  **/

  public function getSeccion(Request $request, $id) {
    if($request->ajax()){
      $seccion = CiclosEscolares::cicloEval($id);
      return response()->json($seccion);
    }
  }

  /**
   * 
   *  Función de guardado, request del formulario.
   * 
  **/

  public function store(Request $request) {
    $count = $request->cantidades;
      
    DB::beginTransaction();

    for($cont = 1; $cont <= $count; $cont++)
      {

        $this->validate($request, [
          "seccion"=>"required",
          "cicloescolar"=>"required",
          "iniciodate_".$cont=>"required",
          "findate_".$cont=>"required",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";

        $periodos = new PeriodosEvaluacion();
        $periodos ->idSeccion = $request->seccion;
        $periodos ->idCicloEscolar = $request->cicloescolar;
        $periodos ->FechaEvaluacionInicial = $request['iniciodate_'.$cont];
        $periodos ->FechaEvaluacionFinal = $request['findate_'.$cont];
        $periodos ->Descripcion = $request['comentario_'.$cont];


        $evaluacion = $periodos->save();

      }
        // 

    if(!$evaluacion) {
      DB::rollback();
      $tipo_mensaje = "mensaje-danger";
      $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
    } else {
      DB::commit();
      $tipo_mensaje = "mensaje-success";

        if($count == 1) {
          $texto_mensaje = "El periodo de evaluación se ha guardado correctamente.";
         
          
        } else {
          $texto_mensaje = "Los periodos de evaluación se han guardado correctamente.";
        }
    }
    $this->MensajePeriodo($periodos ->idSeccion, $periodos ->FechaEvaluacionInicial,$periodos ->FechaEvaluacionFinal);
    if(!$request->ajax()) {
      Session::flash($tipo_mensaje,$texto_mensaje);
      return redirect("periodosevaluacion");
    }

    if($count == 1) {
      $texto_mensaje = "El periodo de evaluación se ha guardado correctamente 1.";
    } else {
      $texto_mensaje = "Los periodos de evaluación se han guardado correctamente.";
    }



  }

  public function MensajePeriodo($seccion, $inicio, $fin)
  {
    $tipo = Auth::user()->id;
    // $mensaje ->idSeccion = $request->seccion;
     $listColaboradores = Colaboradores:: join("users","users.id","=","colaboradores.id_usuario")
                  ->join("materia_profesor_grupo","materia_profesor_grupo.id_profesor","=","colaboradores.id_usuario")
                  ->join("grupos_materias","grupos_materias.id","=","materia_profesor_grupo.id_materia_grupo")
                  ->join("grupos","grupos.idGrupo","=","grupos_materias.id_grupo")
                  ->join("grados","grados.idGrado","=","grupos.idGrado")
                  ->join("secciones","secciones.idSeccion","=","grados.idSeccion")
                  ->where("idRol","=",4)
                  ->where("secciones.idSeccion","=",$seccion)
                  ->select('users.email')
                  ->groupBy("colaboradores.id_usuario")
                  ->get();
// dd($listColaboradores);
      DB::beginTransaction();
        $array = explode(', ', $listColaboradores);
        $numeroRecursos = count($listColaboradores);
        // dd($array);
        // $i = 0;
        foreach($listColaboradores as $col=>$email ) {
         
          // dd($email->email);

        // for($i = 1; $i <= $numeroRecursos; $i++) {
            $fechaActual = date('d-m-Y H:i:s');
            $mensajes = new Mensajes();
            $mensajes ->idUsuarioEnvia = $tipo;
            $mensajes ->idUsuarioRecibe = $email->email;
            $mensajes ->Asunto = "Periodo de Evaluacion";
            $mensajes ->Relevancia = "2";
            $mensajes ->Mensaje = "Se abrio un nuevo periodo de evaluacion del ". $inicio . " al ". $fin;
            $mensajes ->fechaenvio = $fechaActual;
            $mensajes ->archivos_ruta = "";
            // $i=$i+1;
           
             // dd($mensajes ->idUsuarioEnvia);
            $msj = $mensajes->save();

        }
        if(!$msj) {

            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";

        } else 
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El mensaje se ha guardado y enviado correctamente.";
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El mensaje se ha enviado correctamente."]);
  }

  /**
   * 
   *  Petición GET, consulta a modelos externos.
   * 
  **/

 public function getMaterias(Request $request) {
    $var = Materias::join('secciones','secciones.idSeccion','=','materias.idSeccion')
    ->join('grados','grados.idGrado','=','materias.idGrado')
    ->join('grupos','grupos.idGrupo','=','materias.idGrupo')
    ->whereRaw("concat(Nombre) like ?", ["%".$request ->nombre."%"])->get();
    return Response()->json($var);
  }

  /**
   * 
   *  Petición GET, funciones a vistas externos.
   * 
  **/

  public function listPeriodosEvaluacionJson($id_seccion) {
      $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$id_seccion])->first();
      $periodosEvaluacion = PeriodosEvaluacion::whereRaw("idSeccion = ? AND idCicloEscolar = ?",[$id_seccion,$cicloActual->idPeriodo])->get();
      return Response()->json($periodosEvaluacion);
  }

  public function activar_periodo() {
      $tipodeus = Auth::user()->rol->idRol == 9;
      if($tipodeus == 'true'){
          $tipo = Auth::user()->id;
          $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
          $seccion = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
      }
      else{
        $seccion = Secciones::get();
      }

    return view('sistema/periodosEvaluacion/activar_periodo', ['seccion' => $seccion]);
  }

  /**
   * 
   *  Petición GET, consulta a modelos externos.
   * 
  **/

  public function getCiclado(Request $request, $id) {
    if($request->ajax()){
      $seccion = CiclosEscolares::where('idSeccion','=',$id)->where('Activo','=',1)->get();
      return response()->json($seccion);
    }
  }

  public function getEvaluacion(Request $request, $id) {
    if($request->ajax()){
      $periodo = periodosevaluacion::pedirPeriodo($id);
      return response()->json($periodo);
    }
  }

  public function getFecha(Request $request, $id) {
    if($request->ajax()){
      $fecha = periodosevaluacion::where('IdPeriodoEvaluacion', '=', $id)->get();
      return response()->json($fecha);
    }
  }
  public function getFechas(Request $request, $id) {
    if($request->ajax()){
      $fecha = CiclosEscolares::where('idPeriodo','=',$id)->get();
      return response()->json($fecha);
    }
  }
  /**
   * 
   *  Petición GET, funciones a vistas externos.
   * 
  **/

  public function save_activar_periodo(SaveActivarPeriodoRequest $request) {
    $tipo_mensaje = "mensaje-success";
    $texto_mensaje = "";

    DB::beginTransaction();
    $texto_mensaje = "";
    $status = 200;

    $fecha = $request->vencimiento;
    $nuevafecha = strtotime ('+'.$request->slp_dias_activo.' day', strtotime($fecha));
    $nuevafecha = date ('Y-m-j', $nuevafecha);

    $periodo = PeriodosEvaluacion::find($request->IdPeriodoEvaluacion);
    $periodo->calificar = 1;
    $periodo->ActivarHastaFecha = $nuevafecha;
    $evaluacion = $periodo->save();

    if(!$evaluacion) {
      DB::rollback();
      $tipo_mensaje = "mensaje-danger";
      $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
    } else {
      DB::commit();
      $tipo_mensaje = "mensaje-success";
      $texto_mensaje = "El periodo de evaluación se ha activado correctamente.";
    }

    if(!$request->ajax()) {
      Session::flash($tipo_mensaje, $texto_mensaje);
      return redirect("activar_periodo");
    }

    return Response()->json(["mensaje"=>"El periodo de evaluación se ha activado correctamente."]);
  }
}

