<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Colegiaturas;
use App\Alumnos;
use App\CostoPagos;
use App\CostoColegiaturas;
use App\PeriodosPagos;
use App\periodosescolares;
use DateTime;
use DB;
use App\Secciones;
use PDFS;
class ListaColegiaturasCajaController extends Controller
{
    public function index(Request $request) {
         $mes = date('Y-m-d');
        $mesStart = date('Y-m-01');
        $mesEnd = date('Y-m-31');

        $secciones = Secciones::get();
        if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('idSeccion = '.$request->nivel.' AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
               
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
            });
        }

        elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        }
        elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('idSeccion = '.$request->nivel.' AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);
            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

         else {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw("Beca = 1 or BecaSEP = 1 AND concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

    	return view ('sistema/caja/listado-colegiaturas', ['alumnos'=>$estudiantes,'secciones'=>$secciones]);
    }
    public function colegiaturas(Request $request)
    {
        $mes = date('Y-m-d');
        $mesStart = date('Y-m-01');
        $mesEnd = date('Y-m-31');

       $secciones = Secciones::get();
        if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('idSeccion = '.$request->nivel)
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
               
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.')')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
            });
        }

        elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.')')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        }
        elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('idSeccion = '.$request->nivel)
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.')')
            ->orderBy("idAlumno","DESC")->paginate(20);
            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.')')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

         else {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

        return view ('sistema/caja/colegiaturas', ['alumnos'=>$estudiantes,'secciones'=>$secciones]);
    }
    public function becarios (Request $request)
    {
        $mes = date('Y-m-d');
        $mesStart = date('Y-m-01');
        $mesEnd = date('Y-m-31');

        $secciones = Secciones::get();
        if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('idSeccion = '.$request->nivel.' AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
               
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
            });
        }

        elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        }
        elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('idSeccion = '.$request->nivel.' AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);
            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

         else {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw("Beca = 1 or BecaSEP = 1 AND concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }
        return view ('sistema/caja/becarios', ['alumnos'=>$estudiantes,'secciones'=>$secciones]);
    }

    public function store(Request $request) {
    	$this->validate($request, [
            "usuario"=>"required",
            "fechaapagar"=>"required",
        ]);

        $server = "localhost";
        $user = "siacadem_root";
        $pass = "animatiomx2017";
        $bd = "siacadem_sia";

		$var = $request->fechaapagar;
		$nuevafecha = date("Y-m-d", strtotime($var));

		// Creamos la conexión
		$conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexión de la base de datos.");

		// Generamos la consulta
		$sql = "UPDATE colegiaturas SET Prorroga = 1, FechaProrroga = '".$nuevafecha."' WHERE idReferencia = ". $request->usuario;

		// Formato de datos utf8
		mysqli_set_charset($conexion, "utf8");

		//Guardamos en una variable los datos de consulta & conexión
		if(!$result = mysqli_query($conexion, $sql)) die("Ha sucedido un error.");

        if(!$result) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! La prorroga se ha agregado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista-colegiaturas");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! La prorroga se ha agregado correctamente."]);
    }
    public function updatebeca(Request $request)
    {
        $result = DB::table('alumnos')->where('idAlumno','=',$request->idAlumno)->update(['PorcentajeBeca'=>$request->porcentaje]);
        if($result) 
        {
            return Response()->json(['Se ha modificado el porcentaje de la beca!']);
        }
    }
    public function pdfbecarios(Request $request)
    {
        # code...
         $mes = date('Y-m-d');
        $mesStart = date('Y-m-01');
        $mesEnd = date('Y-m-31');

        $secciones = Secciones::get();
        if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('idSeccion = '.$request->nivel.' AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
               
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
            });
        }

        elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        }
        elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('idSeccion = '.$request->nivel.' AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);
            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.') AND (Beca = 1 or BecaSEP = 1)')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

         else {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw("Beca = 1 or BecaSEP = 1 AND concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }
       $pdf = PDFS::loadView('sistema/caja/pdf-becarios', ['alumnos'=>$estudiantes,'secciones'=>$secciones]);
       $pdf->setPaper("letter","landscape");
        return $pdf->inline("pdf-becarios.pdf");
    }
}
