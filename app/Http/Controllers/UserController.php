<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use Redirect;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


public function passwordReset(Request $request, $id)
    {
        
            $this->validate($request, [
            "password" => "min:8|confirmed"
        ]);
            $resetPass = User::find($id);
            $resetPass ->password=$request->password;
            $reset = $resetPass ->save();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";
            if(!$reset)
            {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
                $status = 500;//error interno
            }
            else
            {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "¡En hora buena! Su contraseña se ha actualizado correctamente.";
                $status = 200;//ok
            }

        
        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            //return redirect("perfil-usuario");
            return Redirect::back();
        }
        return Response()->json(["mensaje"=>$texto_mensaje],$status);

    }
}
