<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Prontuario;
use App\CriteriosEvaluacion;
use App\Bloques;
use App\RasgoEvaluar;
use App\GruposMaterias;
use App\Archivos;
use DB;

class ProntuarioController extends Controller
{
	public function index() {
		$ultimoProntuario = DB::table('prontuarios')
        ->select(
            'prontuarios.idProntuario'
        )
        ->orderBy('idProntuario', 'DESC')
        ->limit(1)
        ->get();

        $materias = DB::table('materias')
        ->select(
            'materias.idMateria',
            'Nombre'
        )
        ->orderBy('idMateria', 'ASC')
        ->get();

        $relacionMaterias = DB::table('materia_profesor_grupo')
        ->select(
            'materia_profesor_grupo.id',
            'id_materia_grupo',
            'id_profesor'
        )
        ->orderBy('id', 'ASC')
        ->get();

        $materiasGrupos = DB::table('grupos_materias')
        ->select(
            'grupos_materias.id',
            'id_materia'
        )
        ->orderBy('id', 'ASC')
        ->get();

        $profesor = DB::table('colaboradores')
        ->select(
            'colaboradores.idColaborador',
            'Email',
            'Nombre',
            'Apellido'
        )
        ->orderBy('idColaborador', 'ASC')
        ->get();

        $grupo = DB::table('grupos')
        ->select(
            'grupos.idGrupo',
            'Grupo'
        )
        ->orderBy('idGrupo', 'ASC')
        ->get();

        //dd($usuario, $ultimoProntuario);
	    //return $usuario;

	    return view('sistema/profesores/agregar-prontuarios', ['ultimoProntuario' => $ultimoProntuario, 'materias' => $materias, 'relacionMaterias' => $relacionMaterias, 'profesor' => $profesor, 'grupo' => $grupo, 'materiasGrupos' => $materiasGrupos]);
	}

  public function store(Request $request) {
    $tipo_mensaje = "mensaje-success";
    $texto_mensaje = "";

    DB::beginTransaction();

    if(count($request->file('files_add1'))  > 0) {
      $file = $request->file('files_add1');
      $nombre = $file->getClientOriginalName();
      \Storage::disk('documentos')->put($nombre, \File::get($file));

      $archivos = new Archivos();
      $archivos ->idAlumno = $request->userpro;
      $archivos ->nombreDocto = $nombre;
      $archivos ->status = 1;
      $archiv = $archivos->save();
    }

    /*$validacion = Validator::make($inputs->all(), [
        'archivoExamenMedicoDetalle'=> 'max:2560',//indicamos el valor maximo
]);

if ($validacion->fails()) {
   return ('Supera el tamaño máximo permitido.'); 
} else {
  //aquí en el caso de que este todo bien
}
*/
    if(!$archiv) {
      DB::rollback();
      $tipo_mensaje = "mensaje-danger";
      $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
    } else {
      DB::commit();
      $tipo_mensaje = "mensaje-success";
      $texto_mensaje = "¡En hora buena! El prontuario se ha guardado correctamente.";
    }

    if(!$request->ajax()) {
      Session::flash($tipo_mensaje,$texto_mensaje);
      return redirect("prontuario");
    }

    return Response()->json(["mensaje"=>"¡En hora buena! El prontuario se ha guardado correctamente."]);
  }
}
