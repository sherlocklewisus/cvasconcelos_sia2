<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use App\Alumnos;
use App\Secciones;
use DomPDF;
use App\Tareas;
use App\periodosescolares;
use App\Materias;
use App\Calificaciones;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use App\tutoresalumnos;
use App\CiclosEscolares;
use App\periodosevaluacion;
use App\HorarioEst;
use App\PeriodosPagos;
use App\Colegiaturas;
use App\CostoColegiaturas;
use App\User;
use Auth;

use PDF;
use PDFS;
use DB;

class PdfController extends Controller
{
	// INICIO --> PDF's ALUMNOS.
		// Función para descargar sin visualizar el archivo de formulario.
		function descargaAlumnos($id) {

			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir($id);

			// Verificamos que exista un arreglo.
			if(isset($data['alumnos'][0])) {
				$pdf = PDFS::loadView("SIAO/pdf-alumno-formulario", $data);
				return $pdf->download('archivo.pdf');
			} else {
				return 'No se halló información suficiente para generar el reporte';
			}
		}

		// Función para visualizar el archivo de formulario.
		function vistaAlumnoForm($id) {

			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir($id);

			// Verificamos que exista un arreglo.
			if(isset($data['alumnos'][0])) {
				$pdf = PDFS::loadView("sistema/colaboradores/pdf-alumno-formulario", $data);
				return $pdf->inline('PDFALUMNO');
				//return \PDF::loadView("SIAO/pdf-alumno-formulario", $data)->stream('archivo.pdf');
				//return view("SIAO/prueba", $data);
			} else {
				return 'No se halló información suficiente para generar el reporte';
			}
		}

		// Función para visualizar el archivo de formulario.
		function perfilAlumno($id) {

			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir2($id);

			// Verificamos que exista un arreglo.
			if(isset($data['alumnos'][0])) {
				return PDFS::loadView("SIAO/pdf-perfil", $data)->stream('archivo.pdf');
				//return view("SIAO/prueba", $data);
			} else {
				return 'No se halló información suficiente para generar el reporte';
			}
		}

		// Función para visualizar el archivo de listado.
		function vistaAlumnoLista(Request $request) {

			$tipodeus = Auth::user()->rol->idRol;
          if($tipodeus == '10')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }
                $cuarto = User::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);
                    if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                    }
                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                      if($secciones2->idSeccion == '2'){
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orWhere('idSeccion','=',1)->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                      }
                      else{
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orWhere('idSeccion','=',4)->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                      }
                }


          }

          // si es usuario 9
           elseif($tipodeus == '9')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
                
                $cuarto = User::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                      
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                                      }


          }
          // fin de condicion para usuario 9
          else{
            if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                };
                $cuarto = User::get();
                $secciones = Secciones::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                    ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }
          }
       

        $relacionalt = DB::table('tutoresalumnos')
        ->select('tutoresalumnos.idTutoresAlumnos',
             'idTutoresAlumnos',
             'idTutor',
             'idAlumno',
             'idParentesco')
        ->get();
        $Roles = Auth::user()->rol->idRol;

				$pdf = PDFS::loadView('sistema.colaboradores/pdf-alumno-lista', ['estudiantes' => $estudiantes, 'secciones' => $secciones, 'cuarto' => $cuarto, 'relacionalt'=>$relacionalt,'roles'=>$Roles]);
				return $pdf->inline('reporte.pdf');
		}
		// imprimir Becarios
		function becarioslista($id) {

			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimirB($id);

			// Verificamos que exista un arreglo.
			if(isset($data['alumnos'][0])) {
				$pdf = PDFS::loadView('sistema.caja/pdf-becarios', $data);
				return $pdf->inline('reporte.pdf');
			} else {
				return 'No se halló información suficiente para generar el reporte';
			}
		}

		// Obtenemos información de los alumnos en base su $id.
		function imprimir($id) {

			// Consulta MySQL.
			$query = "SELECT a.idAlumno as 'id',
			a.imagenPerfil as 'Ruta',
			a.Nombres as 'Nombre',
			a.ApellidoPaterno as 'Paterno',
			a.ApellidoMaterno as 'Materno',
			a.FechaNacimiento as 'Fecha',
			a.CURP as 'Curp',
			a.CodigoPostal as 'Codigo',
			a.Colonia as 'Colonia',
			a.Calle as 'Calle',
			a.NumeroExterior as 'Ext',
			a.NumeroInterior as 'Int',
			a.Telefono as 'Tel',
			a.Bautizado as 'Bautizado',
			a.IglesiaAsistida as 'Iglesia',
			a.Distrito as 'Distrito',
			a.Enfermedad as 'Enfermedad',
			a.DetalleEnfermedad as 'DetalleEnf',
			a.alergias as 'Alergias',
			a.detalle_alergia as 'DetalleAler',
			a.ejercicio as 'Ejercicio',
			a.detalle_ejercicio as 'DetalleEjer',
			a.NombreContactoEmergencia as 'NombreContacto',
			a.TelefonoContactoEmergencia1 as 'TelefonoContacto1',
			a.TelefonoContactoEmergencia2 as 'TelefonoContacto2',
			a.id_seguro as 'Seguro',
			a.AfiliacionMedica as 'Afiliacion',
			a.EscuelaProcedencia as 'Procedencia',
			a.RazonParaEstudiar as 'Razones',
			a.FechaEntregaDocumento as 'EntregaDoctos',
			a.ActualizacionEntregaDocumento as 'ActualizaDoctos',
			a.ActaNacimiento as 'Acta',
			a.CertificadoPrimaria as 'CertificadoPri',
			a.CertificadoSecundaria as 'CertificadoSec',
			a.DocumentoCURP as 'DocCURP',
			a.CartaConducta as 'Carta',
			a.Fotografias as 'Fotos',
			a.BoletasPrimaria as 'BoletaPri',
			a.BoletasSecundaria as 'BoletaSe',
			a.BoletasSecundariaOriginal as 'BoletaSeOr',
			a.BoletasSecundariaCopia as 'BoletaSeCo',
			a.ConstanciadeEER1 as 'Constancia1',
			a.ConstanciadeEER2 as 'Constancia2',
			a.ConstanciadeEER3 as 'Constancia3',
			a.ConstanciaEstudios as 'ConstanciaEst',
			a.SolicitudInscripcion as 'Solicitud',
			a.ActaNacimientoTutor as 'ActaT',
			a.CredencialTutor as 'CredencialT',
			a.NotaDocumentos as 'NotaDoctos',
			a.Saldo as 'Saldo',
			a.Abono as 'Abono',
			a.Beca as 'Beca',
			a.BecaSEP as 'BecaSep',
			a.PorcentajeBeca as 'PorcenBeca',
			s.Seccion as 'Seccion',
			g.Grado as 'Grado',
			f.Grupo as 'Grupo',
			e.Estado as 'Estado',
			m.nombre as 'Municipio',
			r.religion as 'Religion',
			u.email as 'Email'
			FROM alumnos a
			join secciones s on s.idSeccion = a.idSeccion
			join grados g on g.idGrado = a.idGrado
			join grupos f on f.idGrupo = a.idGrupo
			join estados e on e.idEstados = a.idEstado 
			join municipios m on m.id = a.idCiudad 
			join religiones r on r.id = a.id_religion
			join users u on u.id = a.id_usuario
			WHERE idAlumno = ?";

			$alumnos = DB::select($query,[$id]);
			$data['alumnos'] = $alumnos;
			return $data;
		}

		// Obtenemos información de los alumnos en base su $id, $nivel, $grado y $grupo.
		function imprimir1($id) {

			// Verificamos los datos obtenidos por GET, si no existen, les damos un valor NULL.
			$idnivel = isset($_GET['nivel'])? $_GET['nivel'] : "";
			$idgrado = isset($_GET['grado'])? $_GET['grado'] : "";
			$idgrupo = isset($_GET['grupo'])? $_GET['grupo'] : "";

			// Verificamos el valor de las variables, si es mayor a NULL, agregamos la condición a la consulta.
			$x1 = ($idnivel > "" ? " AND s.Seccion = '".$idnivel."'" : "");
			$x2 = ($idgrado > "" ? " AND g.Grado = '".$idgrado."'" : "");
			$x3 = ($idgrupo > "" ? " AND f.Grupo = '".$idgrupo."'" : "");

			//dd($idnivel, $idgrado, $idgrupo);

			// Consulta MySQL.
			$query = "SELECT a.idAlumno as 'id',
			a.imagenPerfil as 'Ruta',
			a.Nombres as 'Nombre',
			a.ApellidoPaterno as 'Paterno',
			a.ApellidoMaterno as 'Materno',
			a.FechaNacimiento as 'Fecha',
			a.CURP as 'Curp',
			a.CodigoPostal as 'Codigo',
			a.Colonia as 'Colonia',
			a.Calle as 'Calle',
			a.NumeroExterior as 'Ext',
			a.NumeroInterior as 'Int',
			a.Telefono as 'Tel',
			a.Bautizado as 'Bautizado',
			a.IglesiaAsistida as 'Iglesia',
			a.Distrito as 'Distrito',
			a.Enfermedad as 'Enfermedad',
			a.DetalleEnfermedad as 'DetalleEnf',
			a.alergias as 'Alergias',
			a.detalle_alergia as 'DetalleAler',
			a.NombreContactoEmergencia as 'NombreContacto',
			a.TelefonoContactoEmergencia1 as 'TelefonoContacto1',
			a.TelefonoContactoEmergencia2 as 'TelefonoContacto2',
			a.id_seguro as 'Seguro',
			a.AfiliacionMedica as 'Afiliacion',
			a.EscuelaProcedencia as 'Procedencia',
			a.RazonParaEstudiar as 'Razones',
			a.FechaEntregaDocumento as 'EntregaDoctos',
			a.ActualizacionEntregaDocumento as 'ActualizaDoctos',
			a.ActaNacimiento as 'Acta',
			a.CertificadoPrimaria as 'CertificadoPri',
			a.CertificadoSecundaria as 'CertificadoSec',
			a.DocumentoCURP as 'DocCURP',
			a.CartaConducta as 'Carta',
			a.Fotografias as 'Fotos',
			a.BoletasPrimaria as 'BoletaPri',
			a.BoletasSecundaria as 'BoletaSe',
			a.BoletasSecundariaOriginal as 'BoletaSeOr',
			a.BoletasSecundariaCopia as 'BoletaSeCo',
			a.ConstanciadeEER1 as 'Constancia1',
			a.ConstanciadeEER2 as 'Constancia2',
			a.ConstanciadeEER3 as 'Constancia3',
			a.ConstanciaEstudios as 'ConstanciaEst',
			a.SolicitudInscripcion as 'Solicitud',
			a.ActaNacimientoTutor as 'ActaT',
			a.CredencialTutor as 'CredencialT',
			a.NotaDocumentos as 'NotaDoctos',
			a.Saldo as 'Saldo',
			a.Abono as 'Abono',
			a.Beca as 'Beca',
			a.BecaSEP as 'BecaSep',
			a.PorcentajeBeca as 'PorcenBeca',
			s.Seccion as 'Seccion',
			g.Grado as 'Grado',
			f.Grupo as 'Grupo',
			e.Estado as 'Estado',
			m.nombre as 'Municipio',
			r.religion as 'Religion',
			u.email as 'Email'
			FROM alumnos a
			join secciones s on s.idSeccion = a.idSeccion
			join grados g on g.idGrado = a.idGrado
			join grupos f on f.idGrupo = a.idGrupo
			join estados e on e.idEstados = a.idEstado 
			join municipios m on m.id = a.idCiudad 
			join religiones r on r.id = a.id_religion
			join users u on u.id = a.id_usuario
			WHERE idAlumno > ? $x1 $x2 $x3";

			$alumnos = DB::select($query,[$id]);
			$data['alumnos'] = $alumnos;
			return $data;
		}

		// busqueda para imprimir becarios
		function imprimirB($id) {

			// Verificamos los datos obtenidos por GET, si no existen, les damos un valor NULL.
			$idnivel = isset($_GET['nivel'])? $_GET['nivel'] : "";
			$idgrado = isset($_GET['grado'])? $_GET['grado'] : "";
			$idgrupo = isset($_GET['grupo'])? $_GET['grupo'] : "";

			// Verificamos el valor de las variables, si es mayor a NULL, agregamos la condición a la consulta.
			$x1 = ($idnivel > "" ? " AND s.Seccion = '".$idnivel."'" : "");
			$x2 = ($idgrado > "" ? " AND g.Grado = '".$idgrado."'" : "");
			$x3 = ($idgrupo > "" ? " AND f.Grupo = '".$idgrupo."'" : "");

			//dd($idnivel, $idgrado, $idgrupo);

			// Consulta MySQL.
			$query = "SELECT a.idAlumno as 'id',
			a.imagenPerfil as 'Ruta',
			a.Nombres as 'Nombre',
			a.ApellidoPaterno as 'Paterno',
			a.ApellidoMaterno as 'Materno',
			
			a.Saldo as 'Saldo',
			a.Abono as 'Abono',
			a.Beca as 'Beca',
			a.BecaSEP as 'BecaSep',
			a.PorcentajeBeca as 'PorcenBeca',
			s.idSeccion as 'idSeccion',
			s.Seccion as 'Seccion',
			g.Grado as 'Grado',
			f.Grupo as 'Grupo',
			u.email as 'Email'
			FROM alumnos a
			join secciones s on s.idSeccion = a.idSeccion
			join grados g on g.idGrado = a.idGrado
			join grupos f on f.idGrupo = a.idGrupo
			join users u on u.id = a.id_usuario
			WHERE idAlumno > ? $x1 $x2 $x3 AND a.Beca = '1' || a.BecaSEP = '1'";

			$alumnos = DB::select($query,[$id]);
			$data['alumnos'] = $alumnos;
			return $data;
		}

	// INICIO --> PDF's COLABORADORES.
		// Función para visualizar el archivo de formulario.
		function perfilColabo($id) {

			// Obtenemos los datos de la consulta de la función.
			$data = $this->consulta($id);

			// Verificamos que exista un arreglo.
			if(isset($data['colaboradores'][0])) {
				return PDFS::loadView("SIAO/pdf-perfil-x2", $data)->stream('archivo.pdf');
				//return view("SIAO/prueba", $data);
			} else {
				return 'No se halló información suficiente para generar el reporte';
			}
		}

		// Obtenemos información de los colaboradores en base su $id.
		function consulta($id) {

			// Consulta MySQL.
			$query = "SELECT c.idColaborador as 'id',
			c.Nombre as 'Nombre',
			c.Apellido as 'Apellidos',
			c.Telefono as 'Tel',
			c.ImagenPerfil as 'Ruta',
			c.Domicilio as 'Domicilio',
			r.Rol as 'Rol',
			u.email as 'Email'
			FROM colaboradores c
			join roles r on r.idRol = c.idRol
			join users u on u.id = c.id_usuario
			WHERE idColaborador = ?";

			$colaboradores = DB::select($query,[$id]);
			$data['colaboradores'] = $colaboradores;
			return $data;
		}
	// FIN --> PDF's COLABORADORES.

	// INICIO --> PDF's TUTORES
		// Función para visualizar el archivo de listado.
		function vistaTutorLista($id) {
			// Obtenemos los datos de la consulta de la función.
			if($id == 0){
			    $data = $this->tutorado($id);
			// Verificamos que exista un arreglo.
    			if(isset($data['tutores'][0])) {
    				return PDFS::loadView("sistema/colaboradores/pdf-tutor-lista", $data)->stream('archivo.pdf');
    			} else {
    				return 'No se halló información suficiente para generar el reporte';
    			}
			}
			else{
			    $data = $this->tutorespdf($id);
			    $datos = $this->tutoralumno($id);
			if(isset($data['tutores'][0])) {
    				return PDFS::loadView("sistema/colaboradores/pdf-tutor", $data, $datos)->stream('archivo.pdf');
    			} else {
    				return 'No se halló información suficiente para generar el reporte';
    			}
			}

		}

		// Obtenemos información de los alumnos en base su $id, $nivel, $grado y $grupo.
		function tutorado($id) {

		// Consulta MySQL.
			$query = "SELECT t.idTutor as 'id',
			t.Nombres as 'Nombres',
			t.Apellidos as 'Apellidos',
			t.Email as 'Correo',
			t.TelefonoCasa as 'TelCas',
			t.TelefonoMovil as 'TelMo',
			t.FechaNacimiento as 'Fecha',
			t.Ocupacion as 'Trabajo',
			t.CodigoPostal as 'Codigo',
			t.Colonia as 'Colonia',
			t.Calle as 'Calle',
			t.NumeroExterior as 'Ext',
			t.NumeroInterior as 'Int',
			t.idEstadoCivil as 'Civil',
			t.Distrito as 'Distrito',
			e.Estado as 'Estado',
			u.ImagenPerfil as 'Ruta',
			al.Nombres as 'NombreAlumno'
			FROM tutores t
			left join estados e on e.idEstados = t.idEstado 
			left join users u on u.id = t.id_usuario
			left join tutoresalumnos ta on ta.idTutor = t.idTutor
			left join alumnos al on ta.idAlumno = al.idAlumno
			WHERE t.idTutor > ?";


			$tutores = DB::select($query,[$id]);
			$data['tutores'] = $tutores;
			return $data;
		}
		function tutorespdf($id) {

		// Consulta MySQL.
			$query = "SELECT t.idTutor as 'id',
			t.Nombres as 'Nombres',
			t.Apellidos as 'Apellidos',
			t.Email as 'Correo',
			t.TelefonoCasa as 'TelCas',
			t.TelefonoMovil as 'TelMo',
			t.FechaNacimiento as 'Fecha',
			t.Ocupacion as 'Trabajo',
			t.CodigoPostal as 'Codigo',
			t.Colonia as 'Colonia',
			t.Calle as 'Calle',
			t.NumeroExterior as 'Ext',
			t.NumeroInterior as 'Int',
			t.idEstadoCivil as 'Civil',
			t.Distrito as 'Distrito',
			e.Estado as 'Estado',
			u.ImagenPerfil as 'Ruta'
			FROM tutores t
			left join estados e on e.idEstados = t.idEstado 
			left join users u on u.id = t.id_usuario
			WHERE t.idTutor = ?";


			$tutores = DB::select($query,[$id]);
			$data['tutores'] = $tutores;
			return $data;
		}
		function tutoralumno($id){
			$query = "SELECT al.Nombres as 'NombreAlumno',
			al.ApellidoPaterno as 'App',
			al.ApellidoMaterno as 'Apm'
			FROM tutores t
			join tutoresalumnos ta on ta.idTutor = t.idTutor
			join alumnos al on ta.idAlumno = al.idAlumno
			WHERE t.idTutor = ?";


			$tutoresalumnos = DB::select($query,[$id]);
			$datos['tutoresalumnos'] = $tutoresalumnos;
			return $datos;
		}
	// FIN --> PDF's TUTORES

		// Función para visualizar el archivo de formulario.
		function vistaAlumnoForm1($id) {
			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir2($id);
			
			if(isset($data['alumnos'][0])) {
				$pdf = PDFS::loadView('sistema.alumnos.imprimir_detalle_alumno', $data);
				return $pdf->inline('invoice.pdf');
			}else{
				return '<h3>Verifique que los datos del alumno estén completos ya que no se podrá visualizar el PDF.</h3>';
			}
		}

		function vistaPTForm($id) {

	        $alumnos = Alumnos::where('idAlumno', '=', $id)->get();
	        $alumnos->each(function($alumnos) {
	            $alumnos->seccion;
	        });
	        
	        $colegiaturas = CostoColegiaturas::orderBy('id_costo_colegiatura', 'ASC')->where('tipo_operacion', '=', '1')->get();

	        $pagos = PeriodosPagos::orderBy('idPeriodoPago', 'ASC')->get();
	        $pagos->each(function($pagos) {
	            $pagos->periodo;
	        });
			
			$pdf = PDFS::loadView('sistema.tutores.imprimir_pagos_tutor', ['alumnos' => $alumnos, 'colegiaturas' => $colegiaturas, 'pagos' => $pagos]);
			return $pdf->inline('invoice.pdf');
		}

		function vistaBoletaForm($id,$idper) {

	        $alumnos = Alumnos::where('idGrupo',$id)
	        ->select('idAlumno','Nombres','ApellidoPaterno','ApellidoMaterno','idGrado')
	        ->get();


	        $relaciones = GruposMaterias::where('id_grupo', '=', $id)->pluck('id_materia');


	        $materias = Materias::whereIn('materias.idMateria', $relaciones)
	        ->select('idMateria','Nombre') ->get();
	          
	        
	        $periodo = periodosevaluacion::find($idper);
	          
	            
	        $calificaciones = DB::select('SELECT idMateria,idPeriodoEvaluacion,idAlumno,CalificacionFinal 
	            FROM `calificaciones` 
	            WHERE `idGrupo`= ? 
	            AND `idPeriodoEvaluacion`= ?',
	        [$id,$idper]);

			$pdf = PDFS::loadView('sistema.colaboradores.imprimir_boleta', ['alumnos' => $alumnos,'materias' => $materias,'periodo'=>$periodo,'calificaciones'=>$calificaciones]);
			$pdf->setPaper('A4','landscape');
			return $pdf->inline('invoice.pdf');
		}

		function vistaPerfilForm($id) {

	        $alumnos = Alumnos::where('idAlumno', '=', $id)->get();
	        $alumnos->each(function($alumnos) {
	            $alumnos->periodo;
	            $alumnos->seccion;
	            $alumnos->grado;
	            $alumnos->grupo;
	        });

	        $alumnosx2 = Alumnos::where('idAlumno', '=', $id)->pluck('idGrupo');
	        $relaciones = GruposMaterias::where('id_grupo', '=', $alumnosx2)->pluck('id_materia');
	        $materias = Materias::whereIn('materias.idMateria', $relaciones)
	        //->join('calificaciones', 'materias.idMateria', '=', 'calificaciones.idMateria')
	        ->get();




	        $profesor = GrupoMateriaProfesor::orderBy('id', 'ASC')->get();
	        $profesor->each(function($profesor) {
	            $profesor->profesor;
	        });
	        $relacionmaterias = GruposMaterias::whereIn('id_materia', $materias)->get();

	        $calificaciones = Calificaciones::orderBy('idCalificacion', 'ASC')->get(); 

	        $consultas = DB::select('SELECT materias.Nombre,calificaciones.CalificacionFinal,calificaciones.idPeriodoEvaluacion FROM materias INNER JOIN calificaciones ON materias.idMateria = calificaciones.idMateria WHERE idAlumno = ?', [$id]);


	        /*OBTENER PERIODOS DE EVALUACIÓN*/
	        //dd($alumnos);*/
	          $materias_grupo = GruposMaterias::find($relaciones);
	          $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
	          $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)

	          ->get();

	          
	          $alumno = Alumnos::where('idAlumno',$id)->first();
              $caliFinal = [];
            foreach ($materias as $mat) {
                foreach ($periodosEvaluacion as $per) {
                    array_push($caliFinal, DB::select('SELECT SUM(CalificacionFinal)"CaliFinal",idMateria,idPeriodoEvaluacion FROM `calificaciones` WHERE idAlumno = ? AND idMateria = ? AND idPeriodoEvaluacion =?',[$id,$mat->idMateria,$per->idPeriodoEvaluacion]));

                }
            }
			//dd($caliFinal);
			$pdf = PDFS::loadView('sistema.colaboradores.imprimir_perfil_academico', ['alumnos' => $alumnos, 'materias' => $materias,'periodosEvaluacion'=>$periodosEvaluacion,"calificaciones" => $calificaciones,"consultas" =>$consultas,"alumno"=>$alumno,"Final"=>$caliFinal]);
			$pdf->setPaper('A4','portrait');
			return $pdf->inline('invoice.pdf');
		}

		function vistaCalAForm($id) {

	        $materias_grupo = GruposMaterias::find($id);
	        $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
	        $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)->get();
			
			$pdf = PDFS::loadView('sistema.profesores.imprimir_calificaciones_alumno', compact("periodosEvaluacion","materias_grupo"));
			$pdf->setPaper('A4','landscape');
			return $pdf->inline('invoice.pdf');
		}

		function vistaCalGForm($id) {

	        $materias_grupo = GruposMaterias::find($id);
	        $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
	        //dd($materias_grupo);
	        $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)->get();
			
			$pdf = PDFS::loadView('sistema.colaboradores.imprimir_calificaciones_gr', compact("periodosEvaluacion","materias_grupo"));
			$pdf->setPaper('A4','landscape');
			return $pdf->inline('invoice.pdf');
		}

		function vistaDepForm($id) {
			$alumnos = Alumnos::where('idAlumno', '=', $id)
	        ->join("secciones as x1","alumnos.idSeccion","=","x1.idSeccion")
	        ->join("grados as x2","alumnos.idGrado","=","x2.idGrado")
	        ->join("grupos as x3","alumnos.idGrupo","=","x3.idGrupo")
	        ->join("periodosescolares as x4","alumnos.idSeccion","=","x4.idSeccion")
	        ->groupBy('idAlumno')
	        ->get();

	        //Obtien el alumno
	        $alumno = Alumnos::where('idAlumno',$id)->first();
	        
	        //Guarda la seccion del Alumno en una variable
	        $seccion = $alumno->idSeccion;

	        //Guardamos el valor del id del alumno en una variable
	        $idAlu = $alumno->idAlumno;

	        //Obtenmos el periodo escolar del Alumno
	        $periodo = periodosescolares::where('idSeccion',$seccion)->first();

	        //GUarda el id del Periodo en una variable
	        $idperiodo = $periodo->idPeriodo;

	        //Obtenemos un periodo del alumno
	        $periodo = periodospagos::where('idPeriodoEscolar',$idperiodo)->first();


	        //Guardamos el total de Pagos en una variable
	        $totalpagos= $periodo->TotalPagos;

	       
	        $fechas2 = Colegiaturas::where('idAlumno','=',$idAlu)
	        ->join("periodospagos","periodospagos.idPeriodoPago","=","colegiaturas.idPeriodoPago")
	        ->join("costo_colegiaturas","costo_colegiaturas.id_costo_colegiatura","=","periodospagos.idCosto")
	        ->first();
	        if ($fechas2 != null || $fechas2 != '') {
	            $numpagos = $fechas2->total_pagos;    # code...
	              //Obtenemos todas las fechas del alumno
	                if ($numpagos != '' || $numpagos!= null) 
	                {
	                   $fechas = periodospagos::where('idPeriodoEscolar',$idperiodo)
	                    ->where('TotalPagos', $numpagos)
	                    ->get();
	                    
	                }
	                
	        }
	        else
	        {
	             $fechas = periodospagos::where('totalpagos',$totalpagos)
	                 ->where('idPeriodoEscolar',$idperiodo)
	                ->get();   
	        }
	        


	        //Join de PeriodosPago con Colegiatura
	        $colegiaturas = Colegiaturas::where('idAlumno',$idAlu)
	        ->get();

	        

	        /******* CONSULTAS DEL CONTROLADOR **********/


	                //obtengo la seccion del alumno seleccionado
	        $plucked = $alumnos->pluck('idSeccion');

	        //obtener periodoescolar del alumno
	        $periodosescolares = periodosescolares::whereIn('idSeccion', $plucked)->where('Activo', '=', 1)
	        ->pluck('idPeriodo');

	        //obtener periodospago del alumno           
	        $periodospagos = periodospagos::whereIn('idPeriodoEscolar', $periodosescolares)->get();
	        $periodospagos2 = periodospagos::whereIn('idPeriodoEscolar', $periodosescolares)->pluck('idPeriodoPago');

	        //obtener colegiaturas ya pagadas del alumno    
	        $colegiaturas = colegiaturas::where('idAlumno', '=', $id)->whereIn('idPeriodoPago', $periodospagos2)->get();
	        $colegiaturas2 = colegiaturas::where('idAlumno', '=', $id)->whereIn('idPeriodoPago', $periodospagos2)->pluck('idPeriodoPago');
	            
	        $periodospagos3 = periodospagos::whereNotIn('idPeriodoPago', $colegiaturas2)->get();
	        
	         // dd($colegiaturas2);
	       /************************************ */
	        

			$pdf = PDFS::loadView('sistema.caja.imprimir_detalle_pago', ['alumnos' => $alumnos,'fechas' =>$fechas,'pagos'=> $fechas2,'colegiaturas'=>$colegiaturas,'periodospagos3'=>$periodospagos3]);
			return $pdf->inline('invoice.pdf');
		
		}

		function vistaHoraForm($periodo,$seccion) {
			$per = $periodo;
			$sec = $seccion;
			$horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios) {
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        // dd($horarios->niveles);

        });


        $estructura = DB::table('horarios_estructura')
        ->select(
            'horarios_estructura.idHorario',
            'periodo_escolar',
            'nivel_educativo',
            'horaInicio',
            'horaFinal',
            'receso',
            'comentario'
        )
        ->orderBy('idHorario', 'ASC')
        ->get();
      

        $materias = DB::table('materias')
        ->select(
            'materias.idMateria',
            'Nombre',
            'idSeccion',
            'idGrado',
            'idGrupo'
        )
        ->orderBy('idMateria', 'ASC')
        ->get();

        $periodos = DB::table('periodosescolares')
        ->select(
            'periodosescolares.idPeriodo',
            'Periodo'
        )
        ->orderBy('idPeriodo', 'ASC')
        ->get();

    
        $secciones = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->orderBy('idSeccion', 'ASC')
        ->get();

        $grados = DB::table('grados')
        ->select(
            'grados.idGrado',
            'Grado'
        )
        ->orderBy('idGrado', 'ASC')
        ->get();

        $grupos = DB::table('grupos')
        ->select(
            'grupos.idGrupo',
            'Grupo'
        )
        ->orderBy('idGrupo', 'ASC')
        ->get();

        $relacionmaterias = DB::table('materia_profesor_grupo')
        ->select(
            'materia_profesor_grupo.id',
            'id_materia_grupo',
            'id_profesor'
        )
        ->orderBy('id', 'ASC')
        ->get();
          // dd($relacionmaterias);

        $grupillos = DB::table('grupos_materias')
        ->select(
            'grupos_materias.id',
            'id_grupo',
            'id_materia'
        )
        ->orderBy('id', 'ASC')
        ->get(); 

         $hora_mat=DB::table('horarios_materias')
                 
                    ->get();
            $horaLunes=DB::table('horarios_materias')
                ->join('horarios_estructura','horarios_materias.horario','=','horarios_estructura.horaInicio')
                ->join('materias','horarios_materias.diaLunes','=','materias.idMateria')
                ->join('materia_profesor_grupo','horarios_materias.diaLunes','=','id_materia_grupo')
                ->where('materia_profesor_grupo.id_profesor','=',Auth::user()->id)
                ->get();
                $horaMartes=DB::table('horarios_materias')
                ->join('horarios_estructura','horarios_materias.horario','=','horarios_estructura.horaInicio')
                ->join('materias','horarios_materias.diaMartes','=','materias.idMateria')
                ->join('materia_profesor_grupo','horarios_materias.diaLunes','=','id_materia_grupo')
                ->where('materia_profesor_grupo.id_profesor','=',Auth::user()->id)
                ->get();
            
                $hora=DB::table('horarios_materias')->where('idperiodo','=',$periodo)->get();
                
			$pdf = PDFS::loadView('sistema.profesores.imprimir_horario_profesor', ['horaLunes'=>$horaLunes,'horaMartes'=>$horaMartes,'horarios' => $horarios, 'estructura' => $estructura, 'materias' => $materias, 'periodos' => $periodos, 'secciones' => $secciones, 'grados' => $grados, 'grupos' => $grupos, 'relacionmaterias' => $relacionmaterias, 'grupillos' => $grupillos,'periodo'=>$per,'seccion'=>$sec]);
			return $pdf->inline('invoice.pdf');
		}

		function vistaCalForm($id) {

	        $alumnos = Alumnos::where('idAlumno', '=', $id)->get();
	        $alumnos->each(function($alumnos) {
	            $alumnos->periodo;
	            $alumnos->seccion;
	            $alumnos->grado;
	            $alumnos->grupo;
	        });

	        $alumnosx2 = Alumnos::where('idAlumno', '=', $id)->pluck('idGrupo');
	        $relaciones = GruposMaterias::where('id_grupo', '=', $alumnosx2)->pluck('id_materia');
	        $materias = Materias::whereIn('materias.idMateria', $relaciones)
	        //->join('calificaciones', 'materias.idMateria', '=', 'calificaciones.idMateria')
	        ->get();




	        $profesor = GrupoMateriaProfesor::orderBy('id', 'ASC')->get();
	        $profesor->each(function($profesor) {
	            $profesor->profesor;
	        });
	        $relacionmaterias = GruposMaterias::whereIn('id_materia', $materias)->get();

	        $calificaciones = Calificaciones::orderBy('idCalificacion', 'ASC')->get(); 

	        $consultas = DB::select('SELECT materias.Nombre,calificaciones.CalificacionFinal,calificaciones.idPeriodoEvaluacion FROM materias INNER JOIN calificaciones ON materias.idMateria = calificaciones.idMateria WHERE idAlumno = ?', [$id]);


	        /*OBTENER PERIODOS DE EVALUACIÓN*/
	        //dd($alumnos);*/
	          $materias_grupo = GruposMaterias::find($relaciones);
	          $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
	          $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)

	          ->get();

	          
	          $alumno = Alumnos::where('idAlumno',$id)->first();

				$pdf = PDFS::loadView('sistema.tutores.imprimir_calificaciones_hijo', ['alumnos' => $alumnos, 'materias' => $materias,'periodosEvaluacion'=>$periodosEvaluacion,"calificaciones" => $calificaciones,"consultas" =>$consultas,"alumno"=>$alumno]);
				$pdf->setPaper('A4','landscape');
				return $pdf->inline('invoice.pdf');
			
		}

		function imprimirCH($id)
		{

			// Consulta MySQL.
			$query = "SELECT (SELECT concat(alumnos.Nombres,' ',alumnos.ApellidoPaterno,' ',alumnos.ApellidoMaterno) FROM alumnos WHERE calificaciones.idAlumno = alumnos.idAlumno) AS Nombre,(SELECT periodosevaluacion.Descripcion FROM periodosevaluacion WHERE periodosevaluacion.idPeriodoEvaluacion = calificaciones.idPeriodoEvaluacion) AS Periodo,(SELECT periodosescolares.Periodo FROM periodosescolares WHERE periodosescolares.idPeriodo = calificaciones.id_ciclo) as PeriodoE,(SELECT materias.Nombre from materias WHERE materias.idMateria = calificaciones.idMateria) AS Materia,(SELECT grados.Grado FROM grados WHERE grados.idGrado = alumnos.idGrado) AS Grado,(SELECT grupos.Grupo FROM grupos WHERE grupos.idGrupo = calificaciones.idGrupo) as Grupo,calificaciones.CalificacionFinal FROM calificaciones INNER JOIN alumnos on calificaciones.idAlumno = alumnos.idAlumno WHERE calificaciones.idAlumno = ?";

			$cal = DB::select($query,[$id]);
			$data['calificaciones'] = $cal;
			return $data;

		}

		function vistaHorarioAForm($id) {
			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimirHA($id);
			
			
			if(isset($data['horarios_estructura'][0])) {
				$pdf = PDFS::loadView('sistema.tutores.imprimir_horario_alumno', $data);
				$pdf->setPaper('A4','landscape');
				return $pdf->inline('invoice.pdf');
			}else{
				return '<h3>Verifique que los datos del alumno estén completos ya que no se podrá visualizar el PDF.</h3>';
			}
		}

		function imprimirHA($id)
		{
			// Consulta MySQL.
			$query = "SELECT horainicio,horaFinal,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaLunes) as Lunes,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaMartes) as Martes,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaMiercoles) as Miercoles,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaJueves) as Jueves,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaViernes) as Viernes,(SELECT Periodo FROM periodosescolares WHERE periodosescolares.idPeriodo = horarios_materias.idperiodo) as Periodo,(SELECT seccion FROM secciones WHERE secciones.idSeccion = horarios_materias.idseccion) as Seccion,(SELECT Grado FROM grados WHERE grados.idGrado = horarios_materias.idgrado) as Grado,(SELECT Grupo FROM grupos WHERE grupos.idGrupo = horarios_materias.idgrado) as Grupo,(SELECT concat(alumnos.Nombres,' ',alumnos.ApellidoPaterno,' ',alumnos.ApellidoMaterno) FROM alumnos where alumnos.idAlumno = ? ) AS Nombre FROM horarios_estructura INNER JOIN horarios_materias on horarios_estructura.horaInicio = horarios_materias.horario AND horarios_estructura.periodo_escolar = horarios_materias.idperiodo AND horarios_materias.idseccion = horarios_estructura.nivel_educativo INNER JOIN alumnos on alumnos.idSeccion = horarios_materias.idseccion WHERE horarios_estructura.periodo_escolar = (SELECT periodosescolares.idPeriodo FROM periodosescolares WHERE periodosescolares.idSeccion = alumnos.idSeccion and alumnos.idAlumno = ?) and horarios_estructura.nivel_educativo = (SELECT secciones.idSeccion FROM secciones WHERE secciones.idSeccion = alumnos.idSeccion AND alumnos.idAlumno = ?) and horarios_materias.idgrado = (SELECT grados.idGrado FROM grados,secciones WHERE grados.idGrado = alumnos.idGrado and grados.idSeccion = secciones.idSeccion and alumnos.idAlumno = ?)";

			$horarioal = DB::select($query,[$id,$id,$id,$id]);
			$data['horarios_estructura'] = $horarioal;
			return $data;

		}

		function vistaHorarioForm($periodo,$seccion,$grado,$grupo) {
			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir3($periodo,$seccion,$grado,$grupo);
			//dd($data);
			
			if(!empty($data['horarios_estructura'])) {
				$pdf = PDFS::loadView('sistema.colaboradores.imprimir_horario', $data);
				$pdf->setPaper('A4', 'landscape');
				return $pdf->inline('invoice.pdf');
			}else{
				return '<h3>Verifique que los datos del horario estén completos ya que no se podrá visualizar el PDF.</h3>';
			}
		}

		function imprimir3($periodo,$seccion,$grado,$grupo)
		{
			// Consulta MySQL.
			$query = "SELECT horainicio,horaFinal,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaLunes) as Lunes,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaMartes) as Martes,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaMiercoles) as Miercoles,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaJueves) as Jueves,(SELECT nombre FROM materias WHERE materias.idMateria = horarios_materias.diaViernes) as Viernes,(SELECT Periodo FROM periodosescolares WHERE periodosescolares.idPeriodo = horarios_materias.idperiodo) as Periodo,(SELECT seccion FROM secciones WHERE secciones.idSeccion = horarios_materias.idseccion) as Seccion,(SELECT Grado FROM grados WHERE grados.idGrado = horarios_materias.idgrado) as Grado,(SELECT Grupo FROM grupos WHERE grupos.idGrupo = horarios_materias.idgrado) as Grupo FROM horarios_estructura INNER JOIN horarios_materias on horarios_estructura.horaInicio = horarios_materias.horario AND horarios_estructura.periodo_escolar = horarios_materias.idperiodo AND horarios_materias.idseccion = horarios_estructura.nivel_educativo WHERE horarios_estructura.periodo_escolar = ? AND horarios_estructura.nivel_educativo = ? AND horarios_materias.idgrado = ? AND horarios_materias.idgrupo = ?";

			$horarioc = DB::select($query,[$periodo,$seccion,$grado,$grupo]);
			$data['horarios_estructura'] = $horarioc;
			return $data;

		}
		
			// Funcion para generar pdf de credenciales
		function vistacredenciales($id) {
			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir2($id);
			
			if(isset($data['alumnos'][0])) {
				$pdf = PDFS::loadView('sistema.alumnos.credenciales', $data);
				$pdf->setOption('page-height','58');
				$pdf->setOption('page-width','90');
				$pdf->setOption('margin-top',0);
				$pdf->setOption('margin-right',0);
				$pdf->setOption('margin-left',0);
				$pdf->setOption('margin-bottom',0);
				return $pdf->inline('invoice.pdf');
				// return view ('sistema/alumnos/credenciales',['alumnos'=>$data['alumnos']]);
			}else{
				return 'Debe llenar toda la información requerida';
			}
		}
		// Funcion para generar pdf de credenciales
		function vistacredenciales1($id) {
			// Obtenemos los datos de la consulta de la función.
			$data = $this->imprimir2($id);
			
			if(isset($data['alumnos'][0])) {
				// $pdf = PDFS::loadView('sistema.alumnos.credenciales', $data);
				// $pdf->setOption('page-height','58');
				// $pdf->setOption('page-width','86');
				// $pdf->setOption('margin-top',0);
				// $pdf->setOption('margin-right',0);
				// $pdf->setOption('margin-left',0);
				// $pdf->setOption('margin-bottom',0);
				// return $pdf->inline('invoice.pdf');
				return view ('sistema/alumnos/credenciales',['alumnos'=>$data['alumnos']]);
			}else{
				return 'Debe llenar toda la información requerida';
			}
		}

		// Obtenemos información de los alumnos en base su $id.
		function imprimir2($id) {

			// Consulta MySQL.
			$query = "SELECT a.idAlumno as 'id',
			a.imagenPerfil as 'Ruta',
			a.Nombres as 'Nombre',
			a.ApellidoPaterno as 'Paterno',
			a.ApellidoMaterno as 'Materno',
			a.FechaNacimiento as 'Fecha',
			a.CURP as 'Curp',
			a.CodigoPostal as 'Codigo',
			a.Colonia as 'Colonia',
			a.Calle as 'Calle',
			a.NumeroExterior as 'Ext',
			a.NumeroInterior as 'Int',
			a.Telefono as 'Tel',
			a.Enfermedad as 'Enfermedad',
			a.DetalleEnfermedad as 'DetalleEnf',
			a.alergias as 'Alergias',
			a.detalle_alergia as 'DetalleAler',
			a.NombreContactoEmergencia as 'NombreContacto',
			a.TelefonoContactoEmergencia1 as 'TelefonoContacto1',
			a.TelefonoContactoEmergencia2 as 'TelefonoContacto2',
			a.id_seguro as 'Seguro',
			a.AfiliacionMedica as 'Afiliacion',
			s.Seccion as 'Seccion',
			g.Grado as 'Grado',
			f.Grupo as 'Grupo',
			u.email as 'Email',
			b.estado as 'estado',
			i.nombre as 'municipio',
			a.IglesiaAsistida as 'iglesia',
			a.Distrito as Distrito,
			a.bautizado as Bautizado,
			c.seguro as seguro,
			a.EscuelaProcedencia as EscuelaProcedencia,
			a.RazonParaEstudiar as RazonParaEstudiar,
			a.ActaNacimiento as ActaNacimiento,
			a.CertificadoPrimaria as CertificadoPrimaria,
			a.CertificadoSecundaria as CertificadoSecundaria,
			a.DocumentoCURP as DocumentoCURP,
			a.CartaConducta as	CartaConducta,
			a.Fotografias as Fotografias,
			a.BoletasPrimaria as BoletasPrimaria,
			a.BoletasSecundaria as BoletasSecundaria,
			a.BoletasSecundariaOriginal as BoletasSecundariaOriginal,
			a.ConstanciadeEER1 as ConstanciadeEER1,
			a.ConstanciadeEER2 as ConstanciadeEER2,
			a.ConstanciadeEER3 as ConstanciadeEER3,
			a.BoletasSecundariaCopia as BoletasSecundariaCopia,
			a.ConstanciaEstudios as ConstanciaEstudios,
			a.SolicitudInscripcion as SolicitudInscripcion,
			a.NotaDocumentos as NotaDocumentos,
			a.Beca as Beca,
			a.PorcentajeBeca as PorcentajeBeca
			FROM alumnos a
			join secciones s on s.idSeccion = a.idSeccion
			join grados g on g.idGrado = a.idGrado
			join grupos f on f.idGrupo = a.idGrupo
			join users u on u.id = a.id_usuario
			join estados b on b.idEstados = a.idEstado
			join municipios i on i.id = a.idCiudad
			join seguros c on c.id = a.id_seguro
			WHERE idAlumno = ?";

			$alumnos = DB::select($query,[$id]);

			$data['alumnos'] = $alumnos;
			return $data;

		}
	// FIN --> PDF's ALUMNOS.
	function vistacredencialesC($id) {
			// Obtenemos los datos de la consulta de la función.
			$data = $this->consulta($id);
			
			if(isset($data['colaboradores'][0])) {
				$pdf = PDFS::loadView('sistema.alumnos.credencialesC', $data);
				$pdf->setOption('page-height','58');
				$pdf->setOption('page-width','90');
				$pdf->setOption('margin-top',0);
				$pdf->setOption('margin-right',0);
				$pdf->setOption('margin-left',0);
				$pdf->setOption('margin-bottom',0);
				return $pdf->inline('invoice.pdf');
				// return view ('sistema/alumnos/credenciales',['alumnos'=>$data['alumnos']]);
			}else{
				return 'Debe llenar toda la información requerida';
			}
		}
}
