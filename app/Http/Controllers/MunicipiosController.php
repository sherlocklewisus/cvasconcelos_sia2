<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Municipios;

class MunicipiosController extends Controller
{
    public function getMunicipios($id_estado)
    {
    	$municipios = Municipios::whereRaw("estado_id = ?",[$id_estado])->get();
    	return view("sistema.municipios.cargarMunicipios",compact("municipios"));
    }
}
