<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\ForosLista;
use App\Grados;
use App\Grupos;
use App\GruposMaterias;
use App\GrupoMateriaProfesor;
use DB;

class GenerarForoController extends Controller {

	public function index() {
		$secciones = DB::table('secciones')
		->select (
			'secciones.idSeccion',
			'Seccion'
		)
		->get();

    	return view('sistema/profesores/generar-foro', ['secciones' => $secciones]);
    }

    public function store(Request $request) {
 		$this->validate($request, [
            "seccional"=>"required",
            "gradual"=>"required",
            "grupal"=>"required",
            "material"=>"required",
            "profesional"=>"required",
            "titular"=>"required",
            "status"=>"required",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            
        DB::beginTransaction();

        $foros = new ForosLista();
        $foros ->idseccion = $request->seccional;
        $foros ->idgrado = $request->gradual;
        $foros ->idgrupo = $request->grupal;
        $foros ->idmateria = $request->material;
        $foros ->idprofesor = $request->profesional;
        $foros ->titulo_foro = $request->titular;
        $foros ->descripcion = $request->comentarial;
        $foros ->estado = $request->status;
        $SaveForos = $foros->save();

        if(!$SaveForos) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El foro se ha creado correctamente.";
        }

        if(!$request->ajax()) {
        	Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("generar-foro");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El foro se ha creado correctamente."]);
 	}
}
