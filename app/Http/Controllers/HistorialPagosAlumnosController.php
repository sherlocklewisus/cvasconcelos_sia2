<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\HistorialPagos;
use App\tutoresalumnos;
use DB;

class HistorialPagosAlumnosController extends Controller
{
    public function index() {

    	$historial = HistorialPagos::orderBy('idHistorico', 'ASC')->get();
    	$tutalum = tutoresalumnos::get();

    	return view ('SIAO/historial-pagos', ['historial' => $historial, 'tutalum' => $tutalum]);
    }
}

