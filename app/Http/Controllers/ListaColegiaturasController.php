<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Colegiaturas;
use App\Alumnos;
use App\CostoPagos;
use App\CostoColegiaturas;
use App\PeriodosPagos;
use App\periodosescolares;
use DateTime;
use DB;

class ListaColegiaturasController extends Controller
{
    public function index(Request $request) {
         $mes = date('Y-m-d');
        $mesStart = date('Y-m-01');
        $mesEnd = date('Y-m-31');

        $alumnos = Alumnos::orderBy('idAlumno', 'DESC')
        ->join("secciones as x1","alumnos.idSeccion","=","x1.idSeccion")
        ->join("grados as x2","alumnos.idGrado","=","x2.idGrado")
        ->join("grupos as x3","alumnos.idGrupo","=","x3.idGrupo")
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? and x1.Seccion LIKE ? and x2.Grado LIKE ? and x3.Grupo LIKE ?",["%".$request->Nombre."%","%".$request->nivel."%","%".$request->grado."%","%".$request->grupo."%"])
        ->paginate(15);

        $colegiaturas = Colegiaturas::where([
            ['tipodepago', '>=', '182'],
            ['tipodepago', '<=', '186'],
        ])->get();

        $periodoescolar = periodosescolares::where('Activo', '=', 1)->get();
        $periodopago = PeriodosPagos::where([
            ['FechaPago', '>=', $mesStart],
            ['FechaPago', '<=', $mesEnd],
        ])->get();


        $costocolegiaturas = CostoColegiaturas::where('tipo_operacion', '=', '1')->get();

    	return view ('sistema/colaboradores/listado-colegiaturas', ['alumnos'=>$alumnos,'colegiaturas'=>$colegiaturas, 'periodopago' => $periodopago,'periodoescolar' => $periodoescolar,'costocolegiaturas'=>$costocolegiaturas]);
    }

    public function store(Request $request) {
    	$this->validate($request, [
            "usuario"=>"required",
            "fechaapagar"=>"required",
        ]);

        $server = "localhost";
                    $user = "adminani_ipjroot";
                    $pass = "animatiomx2017";
                    $bd = "adminani_ipjv";

		$var = $request->fechaapagar;
		$nuevafecha = date("Y-m-d", strtotime($var));

		// Creamos la conexión
		$conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexión de la base de datos.");

		// Generamos la consulta
		$sql = "UPDATE colegiaturas SET Prorroga = 1, FechaProrroga = '".$nuevafecha."' WHERE idReferencia = ". $request->usuario;

		// Formato de datos utf8
		mysqli_set_charset($conexion, "utf8");

		//Guardamos en una variable los datos de consulta & conexión
		if(!$result = mysqli_query($conexion, $sql)) die("Ha sucedido un error.");

        if(!$result) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! La prorroga se ha agregado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista-colegiaturas");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! La prorroga se ha agregado correctamente."]);
    }
}
