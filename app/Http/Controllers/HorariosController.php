<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Horarios;
use App\Secciones;
use DB;

class HorariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
         $seccion = Secciones::select('secciones')
            ->select('secciones.idSeccion',
                'Seccion')
            ->get();
        

        $detalle = DB::table('detalle_horario')
        ->select('detalle_horario.idSeccion',
            'idDetalleHorario',
            'HoraInicial',
            'HoraFinal',
            'idSeccion',
            'deleted_at'
            )
        ->where('idSeccion','=', $request->Seccion)
        ->orderBy('idDetalleHorario', 'ASC')
        ->get();

        return view ('SIAO/horarios',['seccion' => $seccion,'detalle' => $detalle]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
       
       $this->validate($request,[       
            "seccion"=>"required"
        ]);

       $tipo_mensaje = "mensaje-success";
       $texto_mensaje = "";
       DB::beginTransaction();
        $i = 0;
        $variable2 = $request->HF;

         if (count($request->HI) > 0 ) {
            foreach ($request->HI as $HI) {
                $messages = [
            'HI.*.date_format' => 'ERROR!! La Hora Inicial no coinciden con el formato',
             'HF.*.date_format' => 'ERROR!! La Hora Final no coinciden con el formato',
             'HI.*.required' => 'ERROR!! La Hora Inicial es requerida',
             'HF.*.required' => 'ERROR!! La Hora Final es requerida',
                ];
             $this->validate($request,[
            "seccion"=> "required",
            "HI.*"=>"required|date_format:H:i",
            "HF.*"=>"required|date_format:H:i"
            ],$messages);  
               
            $horarios = new Horarios();
            $horarios->HoraInicial = $HI;
            $horarios->HoraFinal = $variable2[$i];
            $horarios->idSeccion = $request->seccion;
            $horarios ->save(); 
            $i++;
        }
        }else{

        }
        if(!$horarios) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "Agregado";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "Se agrego con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect(url()->previous());
            }

        return Response()->json(["mensaje"=>"Se borro con exito!"]);

        return redirect(url()->previous());       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
         $horario = Horarios::findOrFail($id);
        
        
        $ho = $horario->Forcedelete();
        if(!$ho) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se puede borrar";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "Borrado con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect(url()->previous());
            }

        return Response()->json(["mensaje"=>"Se borro con exito!"]);

        return redirect(url()->previous());
    }


    public function imp_horario(Request $request){
        
        $horario = DB::table('detalle_horario')
        ->select(
            'detalle_horario.idSeccion',
            'idDetalleHorario',
            'HoraInicial',
            'HoraFinal','deleted_at')
        ->where('idSeccion','=',$request ->Seccion)
        ->get();
   
         $pdf = \PDF::loadView("SIAO.imp_horario",compact("horario"));
        return $pdf->stream("horario.pdf");
    }
}
