<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Grados;
use App\Grupos;
use App\Alumnos;
use App\Reportes;
use DB;

class AgregarReporteController extends Controller {

	public function index() {
    	$secciones = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->where('idSeccion', '<>' , '0')
        ->get();

        $alumnos = DB::table('alumnos')
        ->select(
            'alumnos.idAlumno',
            'Nombres',
            'ApellidoPaterno',
            'ApellidoMaterno'
        )
        ->where('idSeccion', '<>' , '0')
        ->get();

    	return view('SIAO/agregar-reporte', ['secciones' => $secciones, 'alumnos' => $alumnos]);
    }

    public function getGrade(Request $request, $id) {
        if($request->ajax()){
            $grade = Grados::grade($id);
            return response()->json($grade);
        }
    }

    public function getGroup(Request $request, $id) {
        if($request->ajax()){
            $group = Grupos::group($id);
            return response()->json($group);
        }
    }
 
 	public function store(Request $request) {
 		$this->validate($request, [
            "idAlumno"=>"required",
            "idTutor"=>"required",
            "fechaActual"=>"required",
            "asunto"=>"required",
            "descripcion"=>"required",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            
        DB::beginTransaction();

        $reportes = new Reportes();
        $reportes ->IdAlumno = $request->idAlumno;
        $reportes ->IdTutor = $request->idTutor;
        $reportes ->Fecha = $request->fechaActual;
        $reportes ->Asunto = $request->asunto;
        $reportes ->Descripcion = nl2br($request->descripcion);
        $reportes ->Tiporeporte = $request->tiporeporte;
        $reportes ->status = 0;
        $saveReportes = $reportes->save();

        if(!$saveReportes) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El reporte se ha enviado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("generar-reporte?value=11");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El reporte se ha enviado correctamente."]);
 	}
}
