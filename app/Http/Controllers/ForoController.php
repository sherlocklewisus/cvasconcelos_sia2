<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\ForosLista;
use App\Foro;
use DB;

class ForoController extends Controller {

	public function index() {
    	$foros = ForosLista::orderBy('idForo', 'ASC')->get();
        $foros->each(function($foros) {
            $foros->seccional;
            $foros->gradual;
            $foros->grupal;
            $foros->material;
        });

        $comentarios = Foro::get();

        $alumnos = DB::table('alumnos')
        ->select (
            'alumnos.idAlumno',
            'Nombres',
            'ApellidoPaterno',
            'ApellidoMaterno',
            'id_usuario',
            'imagenPerfil'
        )
        ->get();

        $colaboradores = DB::table('colaboradores')
        ->select (
            'colaboradores.idColaborador',
            'Nombre',
            'Apellido',
            'id_usuario',
            'ImagenPerfil'
        )
        ->get();

    	return view('sistema/profesores/foro', ['foros' => $foros, 'comentarios' => $comentarios, 'alumnos' => $alumnos, 'colaboradores' => $colaboradores]);
    }
}
