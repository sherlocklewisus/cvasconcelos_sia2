<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlumnosRequest;
use App\Http\Requests\AlumnosUpdateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Route;
use App\Alumnos;
use App\User;
use App\Religiones;
use App\Seguros;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Estados;
use App\Municipios;
use App\Archivos;
use DB;
use Auth;
use App\Historico;
use App\CiclosEscolares;
use App\Calificaciones;
use App\Colaboradores;
use PDFS;
use App\DocumentosAlumnos;
use Exception;
use App\tutoresalumnos;





class ColegiaturasControlController extends Controller {

    public function index(Request $request) 

    {

    }

 public function colegiaturas(Request $request)
    {
       $mes = date('Y-m-d');
        $mesStart = date('Y-m-01');
        $mesEnd = date('Y-m-31');

       $secciones = Secciones::get();
        if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('idSeccion = '.$request->nivel)
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
               
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.')')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
            });
        }

        elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.')')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        }
        elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('idSeccion = '.$request->nivel)
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.')')
            ->orderBy("idAlumno","DESC")->paginate(20);
            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            ->where('Nombres', 'like', "%$request->alumno%")
            ->whereRaw('(idSeccion = '.$request->nivel.' and idGrado = '.$request->grado.' and idGrupo ='.$request->grupo.')')
            ->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

         else {
            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("ApellidoPaterno","ASC")->paginate(20);


            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

        return view ('sistema/colaboradores/colegiaturascontrol', ['alumnos'=>$estudiantes,'secciones'=>$secciones]);
    }
  



     


}

