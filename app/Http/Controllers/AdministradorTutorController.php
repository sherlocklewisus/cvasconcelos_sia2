<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Mensajes;
use App\tutoresalumnos;
use App\Alumnos;

use DB;
use Auth;

class AdministradorTutorController extends Controller
{
    public function index() {

        $tutor = Auth::user()->tutor->idTutor;

    	$mensajesRecibidos = Mensajes::orderBy('idMensaje', 'ASC')->get();

        $hijos = tutoresalumnos::where('idTutor', '=', $tutor)->get();
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $estudiantes = Alumnos::orderBy('idAlumno', 'ASC')->get();
        $estudiantes->each(function($estudiantes) {
            $estudiantes->seccion;
            $estudiantes->grado;
            $estudiantes->grupo;
        });

    	return view ('sistema/tutores/admin-tutor', ['mensajesRecibidos' => $mensajesRecibidos, 'hijos' => $hijos, 'estudiantes' => $estudiantes]);
    }
}
