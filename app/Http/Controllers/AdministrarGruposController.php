<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GruposCreateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Alumnos;
use DB;
use Carbon\Carbon;
use Exception;
use App\user;
use App\Colaboradores;
use Auth;
class AdministrarGruposController extends Controller
{
    public function index() {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }
          }
          else{
            $secciones = Secciones::get();

          }

        return view('sistema/colaboradores/formulario-admin-grupos', ['secciones' => $secciones]);
    }

    public function getGradual(Request $request, $id) {
        if($request->ajax()){
            $grado = Grados::grado($id);
            return response()->json($grado);
        }
    }

    public function getGrupos(Request $request, $id) {
        if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function getAlumnos(Request $request, $id) {
        if($request->ajax()){
            $alumno = Alumnos::alumno($id);
            return response()->json($alumno);
        }
    }

    public function changeAlumnosGrupo(Request $request) {
        $mensaje_text = "ok";
        $status = 200;

        try {
            //Log::info("Verificando el total de alumnos");
            if(count($request->alumnos) > 0) {
                DB::beginTransaction();
                //Log::info("Iniciando transacción");
                $i = 0;
                $cad = "";

                foreach ($request->alumnos as $id_alumno) {
                    //Log::info("Identificador del alumno: ..".$id_alumno);
                    $alumno = Alumnos::findOrFail($id_alumno);
                    $alumno->idGrupo = $request->idNewGrupo;
                    $al = $alumno->save();
                    $i=$i+1;
                    $cad.=$alumno->Nombres.", ";
                    //Log::info("Identificador del alumno: ".$id_alumno);

                    if(!$al) {
                        throw new Exception("Parece que ocurrio un error, intentelo de nuevo.");  
                    }
                }

                DB::commit();

                if($i == 1) {
                    $mensaje_text = "Se ha realizado el cambio de grupo correctamente.";
                } else {
                    $mensaje_text = "Se ha realizado el cambio de grupo correctamente.";
                }
                $status = 200;
            } else {
                $mensaje_text = "Debe seleccionar un mínimo alumno, intentelo de nuevo.";
                $status = 500;
            }
        } catch(Exception $ex) {
            DB::rollback();
                $mensaje_text = "Parece que ocurrio un error, intentelo de nuevo.";
                $status = 500;
                //Log::info("aplicando rollback");
        }

        return Response()->json(["mensaje"=>$mensaje_text],$status);
    }

    public function destroy($id) {
        $mensaje_text = "";
        $status = 200;//ok

        $total_alumnos_grupo = DB::table("grupos as x1")
            ->join("alumnos as x2","x1.idGrupo","=","x2.idGrupo")
            ->where("x2.idGrupo","=",$id)
            ->count();

        if($total_alumnos_grupo == 0) {
            $grupo = Grupos::findOrFail($id);

            if($grupo->delete()) {
                $mensaje_text = "¡En hora buena! El grupo se ha eliminado correctamente.";
                $status = 200;
            } else {
                $mensaje_text = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
                $status = 500;
            }
        } else {
            $mensaje_text = "El grupo no se puede eliminar porque tiene inscritos ".$total_alumnos_grupo." alumnos , por favor  verifica que el grupo no tenga alumnos";
            $status = 500;
        }

        return Response()->json(["mensaje"=>$mensaje_text],$status);
    }

    public function save_alumnos_grupos(Request $request) {
        dd($request->alumnos);
    }
}
