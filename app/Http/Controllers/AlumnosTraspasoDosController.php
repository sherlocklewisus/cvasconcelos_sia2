<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use App\Http\Requests\SaveAlumnosRequest;
// use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\User;
use App\Alumnos;
use App\periodosescolares;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Historico;

use DB;

class AlumnosTraspasoDosController extends Controller
{
	    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

    	// Estructura de control -> La sentencia de busqueda contiene todos los parametros.
    	if($request->nivel != "" && $request->grado != "" && $request->grupo != "") {
        	$estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
        		['idSeccion', '=', $request->nivel],
        		['idGrado', '=', $request->grado],
        		['idGrupo', '=', $request->grupo]
        	])->orderBy("idAlumno","DESC")->paginate(25);

        	$estudiantes->each(function($estudiantes) {
	            $estudiantes->seccion;
	            $estudiantes->grado;
	            $estudiantes->grupo;
	        });

        // Estructura de control -> La sentencia de busqueda contiene todos solo dos de los parametros.
        } elseif ($request->nivel != "" && $request->grado != "") {
        	$estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
        		['idSeccion', '=', $request->nivel],
        		['idGrado', '=', $request->grado]
        	])->orderBy("idAlumno","DESC")->paginate(25);

        	$estudiantes->each(function($estudiantes) {
	            $estudiantes->seccion;
	            $estudiantes->grado;
	            $estudiantes->grupo;
	        });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        } else {
        	$estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where('idSeccion', '=', $request->nivel)->orderBy("idAlumno","DESC")->paginate(25);

        	$estudiantes->each(function($estudiantes) {
	            $estudiantes->seccion;
	            $estudiantes->grado;
	            $estudiantes->grupo;
	        });
        }

        // Obtenemos la fecha actual del servidor.
        $fecha = date('Y-m-d');
		// Variables de consultas a BD.	        
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $cuarto = User::get();
        $secciones = Secciones::get();
        $periodos = periodosescolares::where('Activo','=','2')->get();
        $periodoescolar = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', 4]])->get();

        // Estructura de control -> Busqueda de ciclos por medio de las secciones elegidas
        $nivelado = isset($_GET['nivel'])? $_GET['nivel'] : "";
        if($nivelado == "" || $nivelado == null) {
        	$ciclosescolares = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', 0]])->get();
        } else {
        	$ciclosescolares = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', $nivelado]])->get();
        }

        return view ('sistema/colaboradores/lista-alumnos-traspaso2', ['estudiantes' => $estudiantes, 'secciones' => $secciones, 'cuarto' => $cuarto, 'ciclosescolares' => $ciclosescolares, 'periodoescolar' => $periodoescolar, 'periodos' => $periodos]);
	}
		   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
	}
    public function getPostGrado(Request $request, $id) {
        if($request->ajax()){
        	$fecha = date('Y-m-d');

        	if($id == 4) {
        		$busquedaPeriodo = periodosescolares::where([['idSeccion', '=', $id],['FechaFin', '<', $fecha],['Activo', '=', 1]])->pluck('idPeriodo');
        		$busquedaGrupo = Grupos::whereIn('id_ciclo', $busquedaPeriodo)->pluck('idGrado');
        		$busquedaGrado = Grados::whereIn('idGrado', $busquedaGrupo)->get();

        		return response()->json($busquedaGrado);
        	} else {
        		$gradoValue = Grados::grade($id);
            	return response()->json($gradoValue);
        	}
        }
    }

    public function getPostGrupo(Request $request, $id) {
        if($request->ajax()){
            $grupoValue = Grupos::group($id);
            return response()->json($grupoValue);
        }
    }

    public function getPostSeccion(Request $request, $id) {
        if($request->ajax()){
            $seccionValue = periodosescolares::where('idPeriodo', '=', $id)->get();

	        foreach($seccionValue as $niv) {
	            $value = $niv->idSeccion;

	            if($request->ajax()){
	                $evaluacion = Secciones::periodos($value);
	                return response()->json($evaluacion);
	            }
	        }
        }
    }

    public function getPostGrade(Request $request, $id) {
        if($request->ajax()){
        	$fecha = date('Y-m-d');

        	if($id == 4) {
        		$busquedaPeriodo = periodosescolares::where([['idSeccion', '=', $id],['FechaFin', '<', $fecha],['Activo', '=', 1]])->pluck('idPeriodo');
        		$busquedaGrupo = Grupos::whereIn('id_ciclo', $busquedaPeriodo)->pluck('idGrado');
        		$busquedaGrado = Grados::whereIn('idGrado', $busquedaGrupo)->get();

        		return response()->json($busquedaGrado);
        	} else {
        		$gradoValue = Grados::grade($id);
            	return response()->json($gradoValue);
        	}
        }
    }

    public function getPostGroup(Request $request, $id) {
        if($request->ajax()){
            $grupoValue = Grupos::where('idGrado', '=', $id)->orderBy('Grupo', 'ASC')->get();
            return response()->json($grupoValue);
        }
    }

	public function searchAll(Request $request){
			$alumnos = Alumnos::where('alumnos.idSeccion','=',$request->nivel)
			->where('alumnos.idGrado','=',$request->grado)
			->where('alumnos.idGrupo','=',$request->grupo)
			->join("Secciones","Secciones.idSeccion","=","alumnos.idSeccion")
			->join("Grados","alumnos.idGrado","=","grados.idGrado")
			->join("Grupos","alumnos.idGrupo","=","grupos.idGrupo")
			->get();
			return Response()->json($alumnos);
	}

	public function datos(Request $request){
		if($request->grado == 3)
		{
			$grado = Grados::where('grados.idGrado','>',$request->grado)
			->where('grados.idSeccion','=',2)
			->where('periodosescolares.Activo',"=",2)
			->leftjoin('secciones','secciones.idSeccion','=','grados.idSeccion')
			->leftjoin('periodosescolares','periodosescolares.idSeccion','=','grados.idSeccion')
			->leftjoin('grupos','grupos.idGrado','=','grados.idGrado')
			->groupby('grupos.Grupo')
			->groupby('secciones.Seccion')
			->get();
		}
		else if($request->grado == 9){
			$grado = Grados::where('grados.idGrado','>',$request->grado)
			->where('grados.idSeccion','=',3)
			->where('periodosescolares.Activo',"=",2)
			->leftjoin('periodosescolares','periodosescolares.idSeccion','=','grados.idSeccion')
			->leftjoin('secciones','secciones.idSeccion','=','grados.idSeccion')
			->leftjoin('grupos','grupos.idGrado','=','grados.idGrado')
			->groupby('grupos.Grupo')
			->groupby('secciones.Seccion')
			->get();
		}
		else if($request->grado == 12){
			$grado = Grados::where('grados.idGrado','>',$request->grado)
			->where('grados.idSeccion','=',4)
			->where('periodosescolares.Activo',"=",2)
			->leftjoin('periodosescolares','periodosescolares.idSeccion','=','grados.idSeccion')
			->leftjoin('secciones','secciones.idSeccion','=','grados.idSeccion')
			->leftjoin('grupos','grupos.idGrado','=','grados.idGrado')
			->groupby('grupos.Grupo')
			->groupby('secciones.Seccion')
			->get();
		}
		else{
		$grado = Grados::where('grados.idGrado','>',$request->grado)
		->where('grados.idSeccion','=',$request->seccion)
		->where('periodosescolares.Activo',"=",2)
		->leftjoin('periodosescolares','periodosescolares.idSeccion','=','grados.idSeccion')
		->leftjoin('secciones','secciones.idSeccion','=','grados.idSeccion')
		->leftjoin('grupos','grupos.idGrado','=','grados.idGrado')
		->groupby('grupos.Grupo')
		->groupby('secciones.Seccion')
		->get();
		}
		return Response()->json($grado);
	}
	function SaveAlumnos(Request $request){
		if($request->nivel==null)
		{
			$rules=[
				'grado'=>'required',
				'grupo'=>'required',
				'grupo2'=>'required',
			];
			$messages=[
				"grado.required"=>"El Grado es requerido",
				"grupo.required"=>"El Grupo es requerido",
				"grupo2.required"=>"El nuevo grado es requerido",
			];
			$this->validate($request,$rules,$messages);
			$tipo_mensaje = "mensaje-success";
			$texto_mensaje = "";
			$status = 200;
			dd($request);
		}

	}

	function changeofcourse(){
		$data = Input::get('checkbox');

	}
}
