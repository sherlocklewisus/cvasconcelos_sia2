<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveTareaRequest;
use Illuminate\Support\Facades\Session;

use App\Mensajes;
use App\Tareas;
use App\TareasAlumnos;
use App\ArchivosTarea;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use App\Grupos;
use App\Grados;
use App\Secciones;
use App\Materias;
use App\Alumnos; 
use App\tutoresalumnos;
use App\Tutores;

use Exception;
use DB;
use Auth;

class TareasController extends Controller
{
    public function index() {
        $profesor = Auth::user()->colaborador->idColaborador;
        $profemate = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
        $grupomate = GruposMaterias::whereIn('id', $profemate)->groupBy('id_grupo')->pluck('id_grupo');
        $grupomatex2 = GruposMaterias::whereIn('id', $profemate)->pluck('id_materia');
        $grupo = Grupos::whereIn('idGrupo', $grupomate)->groupBy('idGrado')->pluck('idGrado');
        $grado = Grados::whereIn('idGrado', $grupo)->pluck('idSeccion');
        $secciones = Secciones::whereIn('idSeccion', $grado)->get(); 

        return view('sistema/profesores/agregar-tareas', ['secciones' => $secciones]);
    }

    public function getGradoTarea(Request $request, $id) {
        if($request->ajax()){
            $profesor = Auth::user()->colaborador->idColaborador;
            $profemate = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
            $grupomate = GruposMaterias::whereIn('id', $profemate)->groupBy('id_grupo')->pluck('id_grupo');
            $grupomatex2 = GruposMaterias::whereIn('id', $profemate)->pluck('id_materia');
            $grupo = Grupos::whereIn('idGrupo', $grupomate)->groupBy('idGrado')->pluck('idGrado');
            $grados = Grados::whereIn('idGrado', $grupo)->where('idSeccion', '=', $id)->get();
            return response()->json($grados);
        }
    }

    public function getGrupoTarea(Request $request, $id) {
        if($request->ajax()){
            $profesor = Auth::user()->colaborador->idColaborador;
            $profemate = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
            $grupomate = GruposMaterias::whereIn('id', $profemate)->groupBy('id_grupo')->pluck('id_grupo');
            $grupos = Grupos::whereIn('idGrupo', $grupomate)->where('idGrado', '=', $id)->get();
            return response()->json($grupos);
        }
    }

    public function getMateriaTarea(Request $request, $id) {
        if($request->ajax()){
            $profesor = Auth::user()->colaborador->idColaborador;
            $profemate = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
            $grupomatex2 = GruposMaterias::whereIn('id', $profemate)->where('id_grupo', '=', $id)->pluck('id_materia');
            $materias = Materias::whereIn('idMateria', $grupomatex2)->get();
            return response()->json($materias);
        }
    }

    public function store(SaveTareaRequest $request) {
        DB::beginTransaction();

        $materiagrupo = GruposMaterias::where([
            ['id_grupo', '=', $request->grupotarea],
            ['id_materia', '=', $request->idMateria],
        ])->get();

        foreach($materiagrupo as $mg) {
            $relacion = $mg->id;
        }

        $tarea = new Tareas();
        $tarea->idMateria = $request->idMateria;
        $tarea->Descripcion = $request->tarea;
        $tarea->DetalleTarea = $request->Descripcion;
        $tarea->FechaEntrega = $request->FechaEntrega;
        $tarea->id_materia_grupo = $relacion;
        $savetarea = $tarea->save(); 


        $estudiantes = Alumnos::where('idGrupo', '=', $request->grupotarea)->get();
        foreach($estudiantes as $alumno) {
            
            $tutores=tutoresalumnos::where("idAlumno", "=",$alumno->idAlumno)->get();

            foreach ($tutores as $tutor) {
                $correos = tutores::where("idTutor","=",$tutor->idTutor)->get();
                foreach ($correos as $correo) {
                    $mensajes = new Mensajes();
                    $mensajes ->idUsuarioEnvia = Auth::user()->colaborador->idColaborador;
                    $mensajes ->idUsuarioRecibe = $correo->Email;

                    $mensajes ->Asunto = "Tarea: " . $request->tarea;
                    $mensajes ->Relevancia =2;
                    $mensajes ->Mensaje = nl2br($request->Descripcion);
                    $mensajes ->fechaenvio = date('d/m/Y');
                    $msj = $mensajes->save();
                }
            }

        }
        
        
        

        /*

        foreach($estudiantes as $alumnos) {
            
            $mensajes = new Mensajes();
            $mensajes ->idUsuarioEnvia = Auth::user()->colaborador->idColaborador;

            
            //$tutor = tutoresalumnos::where("idAlumno", "=",$alumnos->idAlumno)->pluck("idTutor");
            //$user_tutor= tutores::where("idTutor","=",$tutor)->pluck("username");
            //$mensajes ->idUsuarioRecibe = $tutor;

            $mensajes ->Asunto = "Tarea: " . $request->tarea;
            $mensajes ->Relevancia =2;
            $mensajes ->Mensaje = nl2br($request->Descripcion);
            $mensajes ->fechaenvio = date('d/m/Y');
            $msj = $mensajes->save();
            
        } */


        

        if(count($request->filetareas) > 0) {
            foreach ($request->filetareas as $filetarea) {
                $arch = new ArchivosTarea();
                $arch->idTarea = $tarea->idTarea;
                $arch->archivo = $filetarea;
                $savearchivo = $arch->save();
            }
        }

        if(!$savetarea && !$savearchivo) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            $estudiantes = Alumnos::where('idGrupo', '=', $request->grupotarea)->get();

        foreach($estudiantes as $alumnos) {
                $relaciontarea = new TareasAlumnos();
                $relaciontarea->idAlumno = $alumnos->idAlumno;
                $relaciontarea->idMateria = $request->idMateria;
                $relaciontarea->idTarea = $tarea->idTarea;
                $relaciontarea->Entregada = 0;
                $final = $relaciontarea->save();
            }



            if(!$final) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "La tarea se ha guardado correctamente.";

            }
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("agregar-tareas");
        }

        return Response()->json(["mensaje"=>"La tarea se ha guardado correctamente."]);

}

    public function enviarMensajeTarea(){

    }



    public function ver_tareas_alumno()
    {
        $grupos_materias = Auth::user()->alumno->grupo->grupos_materias()->get();

        $relaciones = GrupoMateriaProfesor::get();
        $relaciones->each(function($relaciones) {
            $relaciones->profesor;
        });

        return view('sistema/tareas/ver_tareas_alumno', ['grupos_materias' => $grupos_materias, 'relaciones' => $relaciones]);
    }

    /*
        funcion para listar tareas
    */
    public function tareas($id_materia_grupo)
    {
        $user = Auth::user()->alumno->idAlumno;
        $tareas = Tareas::whereRaw("id_materia_grupo = ?",[$id_materia_grupo])->get();
        $tareasalumno = TareasAlumnos::where('idAlumno', '=', $user)->get();

        return view('sistema/tareas/tareas', ['tareas' => $tareas, 'tareasalumno' => $tareasalumno]);
    }

    // public function detalle_tarea($id_tarea)
    // {
    //     $tarea = Tareas::findOrFail($id_tarea);
    //     return view("sistema/tareas/detalle_tarea",compact("tarea"));
    // }
    public function listado_tareas()
    {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $profe = Auth::User()->colaborador->idColaborador;
        $grupomateria = GrupoMateriaProfesor::select('id_materia_grupo')->where('id_profesor', '=', $profe)->get();
        $tareas = Tareas::join("materias","materias.idMateria","=","tareas.idMateria")->get();
        
       return view('sistema.profesores.listado-tareas', ['tareas' => $tareas, 'grupomateria' => $grupomateria]);

    }
    public function detalle_tarea_profesor($id_tarea)
    {
        $tarea = Tareas::findOrFail($id_tarea);
        $archivos  = ArchivosTarea::where("idTarea","=",$tarea->idTarea)->get();
        return view("sistema.profesores.detalle_tarea_profesor",["tarea"=>$tarea,"archivos"=>$archivos]);
    }  
}
