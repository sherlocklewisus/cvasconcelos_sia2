<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\HorarioEst;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;

use DB;
use Auth;

class ListaHorariosProfesor extends Controller
{
    public function index(Request $request) {
    	$id=$request->id;
    	$profesor = $id;
    	$profemate = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
    	$grupomate = GruposMaterias::whereIn('id', $profemate)->pluck('id_materia');
    	$horarios = HorarioEst::groupBy('idperiodo')->whereIn('diaLunes', $grupomate)->get();
    	$horarios->each(function($horarios) {
    		$horarios->periodo;
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });
 
    	$horariosx2 = HorarioEst::groupBy('idperiodo')->whereIn('diaMartes', $grupomate)->get();
    	$horariosx3 = HorarioEst::groupBy('idperiodo')->whereIn('diaMiercoles', $grupomate)->get();
    	$horariosx4 = HorarioEst::groupBy('idperiodo')->whereIn('diaJueves', $grupomate)->get();
    	$horariosx5 = HorarioEst::groupBy('idperiodo')->whereIn('diaViernes', $grupomate)->get();

        return view('sistema.colaboradores.listahorarios', ['horarios' => $horarios, 'horariosx2' => $horariosx2, 'horariosx3' => $horariosx3, 'horariosx4' => $horariosx4, 'horariosx5' => $horariosx5]);
    }
}
