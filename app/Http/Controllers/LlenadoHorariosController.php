<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\GruposMaterias;
use App\Grupos;
use App\HorarioEst;
use DB;

class LlenadoHorariosController extends Controller
{
    public function index() {
    	$dates = DB::table('horarios_estructura')
    	->select(
    		'horarios_estructura.idHorario',
            'nivel_educativo'
    	)
    	->orderBy('nivel_educativo', 'ASC')
    	->get();

        $horarios = DB::table('horarios_estructura')
        ->select(
            'horarios_estructura.idHorario',
            'periodo_escolar',
            'nivel_educativo',
            'horaInicio',
            'horaFinal',
            'receso',
            'comentario'
        )
        ->orderBy('idHorario', 'ASC')
        ->get();

    	$secciones = DB::table('secciones')
    	->select(
    		'secciones.idSeccion',
    		'Seccion'
    	)
    	->orderBy('idSeccion', 'ASC')
    	->get();

    	$grado = DB::table('grados')
    	->select(
    		'grados.idGrado',
            'idSeccion',
    		'Grado'
    	)
    	->orderBy('idGrado', 'ASC')
    	->get();

        $materiaGrupo = GruposMaterias::orderBy('id', 'ASC')->get();
        $materiaGrupo->each(function($materiaGrupo) {
            $materiaGrupo->materiasdisponibles;
        });

    	return view ('SIAO/llenado-horarios', ['dates' => $dates, 'horarios' => $horarios, 'secciones' => $secciones, 'grado' => $grado, 'materiaGrupo' => $materiaGrupo]);
    }

    public function getGroup(Request $request, $id) {
                if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function getGroupos(Request $request, $id) {

        if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function store(Request $request) {
        $cont = $request->filas;
        DB::beginTransaction();

        for($i = 1; $i <= $cont; $i++) {

            $this->validate($request, [
                "idseccion"=>"required",
                "idgrado"=>"required",
                "idgrupo"=>"required",
                "hora".$i=>"required",
            ]);

            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";

            $horarios = new HorarioEst();
            $horarios ->idperiodo = $request->periodoEscolar;
            $horarios ->idseccion = $request->idseccion;
            $horarios ->idgrado = $request->idgrado;
            $horarios ->idgrupo = $request->idgrupo;
            $horarios ->horario = $request['hora'.$i];

            if (!$horarios ->diaLunes = $request['lunes'.$i]) {
                $horarios ->diaLunes = 0;
            } else {
                $horarios ->diaLunes = $request['lunes'.$i];
            }

            if (!$horarios ->diaMartes = $request['martes'.$i]) {
                $horarios ->diaMartes = 0;
            } else {
                $horarios ->diaMartes = $request['martes'.$i];
            }

            if (!$horarios ->diaMiercoles = $request['miercoles'.$i]) {
                $horarios ->diaMiercoles = 0;
            } else {
                $horarios ->diaMiercoles = $request['miercoles'.$i];
            }

            if (!$horarios ->diaJueves = $request['jueves'.$i]) {
                $horarios ->diaJueves = 0;
            } else {
                $horarios ->diaJueves = $request['jueves'.$i];
            }

            if (!$horarios ->diaViernes = $request['viernes'.$i]) {
                $horarios ->diaViernes = 0;
            } else {
                $horarios ->diaViernes = $request['viernes'.$i];
            }

            $hrs = $horarios->save();
        }

        if(!$hrs) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            $server = "localhost";
            $user = "adminani_ipjroot";
            $pass = "animatiomx2017";
            $bd = "adminani_ipjv";

            $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inexperado en la conexion de la base de datos");
            
            $sql = "INSERT INTO grupos_horario(idperiodo, idseccion, idgrado, idgrupo) VALUES ('".$request->periodoEscolar."','".$request->idseccion."', '".$request->idgrado."', '".$request->idgrupo."')";

            mysqli_set_charset($conexion, "utf8");

            $double = mysqli_query($conexion, $sql);
            if(!$double) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "¡En hora buena! El horario se ha guardado correctamente.";
            }
        }
        if(!$request->ajax()) {
            return redirect("lista-horarios?value=8");
        } else {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("llenado-horarios?value=8&nivel=".$request->idseccion);
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El horario se ha guardado correctamente."]);
    }
}
