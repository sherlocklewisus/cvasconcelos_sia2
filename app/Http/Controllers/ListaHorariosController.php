<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\HorarioEst;
use App\HorarioGrupo;
use DB;
use App\Secciones;
use App\Colaboradores;
use App\User;
use Auth;

class ListaHorariosController extends Controller
{
    public function index(Request $request) {
          $tipodeus = Auth::user()->rol->idRol == 9;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
                $horariosGrupos = HorarioGrupo::where('idSeccion','=',$secciones2->idSeccion)->orderBy('id', 'ASC')->paginate(200);
                $horariosGrupos->each(function($horariosGrupos) {
                    $horariosGrupos->periodos;
                    $horariosGrupos->niveles;
                    $horariosGrupos->grados;
                    $horariosGrupos->grupos;
                });

                $secciones = DB::table('secciones')
                ->select(
                    'secciones.idSeccion',
                    'Seccion'
                )
                ->where('idSeccion', '=', $secciones2->idSeccion)
                ->orderBy('idSeccion', 'ASC')
                ->get();

          }
          else{
            $horariosGrupos = HorarioGrupo::orderBy('id', 'ASC')->paginate(200);
            $horariosGrupos->each(function($horariosGrupos) {
                $horariosGrupos->periodos;
                $horariosGrupos->niveles;
                $horariosGrupos->grados;
                $horariosGrupos->grupos;
            });

            $secciones = DB::table('secciones')
            ->select(
                'secciones.idSeccion',
                'Seccion'
            )
            ->where('idSeccion', '<>', '0')
            ->orderBy('idSeccion', 'ASC')
            ->get();
          }


        // dd($horariosGrupos);

        return view('sistema/colaboradores/listado-horarios', ['horariosGrupos' => $horariosGrupos, 'secciones' => $secciones]);
    }

    public function store(Request $request) {
        $server = "localhost";
        $user = "adminani_ipjroot";
        $pass = "animatiomx2017";
        $bd = "adminani_ipjv";

        $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexion de la base de datos");

        $sql = "DELETE FROM grupos_horario WHERE id = '". $request->horariolist ."'";

        mysqli_set_charset($conexion, "utf8");

        $partOne = mysqli_query($conexion, $sql);

        $query = "DELETE FROM horarios_materias WHERE idperiodo = ". $request->periodolist ." AND idseccion = ". $request->nivellist ." AND idgrupo = ". $request->grupolist;

        mysqli_set_charset($conexion, "utf8");

        $partTwo = mysqli_query($conexion, $query);

        if(!$partOne && !$partTwo) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El horario se ha eliminado correctamente.";
        }

        if(!$request->ajax()) {
            return redirect("lista-horarios");
        }
        
        return Response()->json(["mensaje"=>"¡En hora buena! El horario se ha eliminado correctamente."]);
    }

    public function destroy($id) {
        $horarios = HorarioGrupo::findOrFail($id);
        $delete = $horarios->forceDelete();

        return back();
    }
}
