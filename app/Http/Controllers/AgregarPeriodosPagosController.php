<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\periodosescolares;
use App\Secciones;
use App\Alumnos;

use App\Clave;

use App\PeriodosPagos;
use App\Colegiaturas;
use App\CostoPagos;

use DB;

class AgregarPeriodosPagosController extends Controller
{
    public function index() {
    	if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
    	$periodos = periodosescolares::where('Activo','=',1)->orderBy('idPeriodo', 'ASC')->get();
    	$secciones = Secciones::orderBy('idSeccion', 'ASC')->get();
    	$alumnos = Alumnos::orderBy('idAlumno', 'ASC')->get();

    	return view ('sistema/caja/agregar-periodopago', ['periodos' => $periodos, 'secciones' => $secciones, 'alumnos' => $alumnos]);
    }

    public function getClave(Request $request, $id) {
        if($request->ajax()){
            $ciclo = periodosescolares::where('idSeccion', '=', $id)->get();
            return response()->json($ciclo);
        }
    }
    public function getcuenta(Request $request) {
        if($request->ajax()){
            $ciclo = secciones::join('periodosescolares', 'periodosescolares.idSeccion', '=', 'secciones.idSeccion')
            ->where('periodosescolares.idPeriodo', '=', $request->idCiclo)->get();
            return response()->json($ciclo);
        }
    }
    public function store(Request $request) {
 	$this->validate($request, [
            "idCiclo"=>"required",
            "tipo_pago"=>"required",
            "idseccion"=>"required",
            "monto_pago"=>"required",
            // "total_pagos"=>"required",
            "fecha_pago"=>"required",
            // "monto_total"=>"required",
            
        ]);

        // $verify = CostoPagos::join('periodospagos', 'costo_colegiaturas.idPeriodoPago', '=', 'periodospagos.idPeriodoPago')
        // ->join('periodosescolares', 'periodospagos.idPeriodoEscolar', '=', 'periodosescolares.idPeriodo')
        // ->where([
        // 	['idTipoOperacion', '=', $request->idTipoOperacion],
        // 	['periodosescolares.idPeriodo', '=', $request->idCiclo]
        // ])->get();
 		$verify = CostoPagos::where([
        	['idTipoOperacion', '=', $request->idTipoOperacion],
        	//['idPeriodoEscolar', '=', $request->idCiclo]
        ])->get();

        // dd($verify);

        if(count($verify) > 0) {
        	DB::rollback();
		    $tipo_mensaje = "mensaje-danger";
		    $texto_mensaje = "Los criterios ya han sido ingresados anteriormente, intentelo nuevamente.";
		} else {
	        $tipo_mensaje = "mensaje-success";
	        $texto_mensaje = "";
	            
	        DB::beginTransaction();

	        $var = $request->fecha_pago;
			$nuevafecha = date("Y-m-d", strtotime($var));
			$value = $request->tipo_pago;
			$secciones = $request->idseccion;
			$TipoOperacion = "";
			if ($value == '4101-0002') {
		    	$value2 = 4101-0002;
				if($secciones == 'PREESCOLAR'){
					$TipoOperacion = 183;
				}
				else if ($secciones == 'PRIMARIA'){
					$TipoOperacion = 184;
				}
				else if ($secciones == 'SECUNDARIA'){
					$TipoOperacion = 185;
				}
				else{
					$TipoOperacion = 186;
				}
			}
			if ($value == '4101-0001') {
		    	$value2 = 4101-0001;
				if($secciones == 'PREESCOLAR'){
					$TipoOperacion = 178;
				}
				else if ($secciones == 'PRIMARIA'){
					$TipoOperacion = 179;
				}
				else if ($secciones == 'SECUNDARIA'){
					$TipoOperacion = 180;
				}
				else{
					$TipoOperacion = 181;
				}

		    	
		    } elseif ($value == '2145-0001') {
		    	$value = 3;
		    }


		    if($value == '4101-0001'){
		    	$costopagos = new CostoPagos();
		    	$costopagos ->idTipoOperacion = $TipoOperacion;
		    	// $costopagos ->idPeriodoPago = $pagos->idPeriodoPago;
		    	$costopagos ->tipo_operacion = $value2;
		    	$costopagos ->idSeccion = $request->idseccion;
		    	$costopagos ->idSeccion2 = $request->idseccionx2;
		    	$costopagos ->costo = $request->monto_pago;
		    	$costopagos ->costo_adventista = $request->monto_pago_adv;
		    	$costopagos ->total_pagos = 1;
		    	$costopagos ->total_costo = $request->monto_pago;
		    	$costopagos ->total_costo_adventista = $request->monto_total_adv;
		    	$Salvarcostos = $costopagos->save();
		    }
		    else{
		    	$costopagos = new CostoPagos();
		    	$costopagos ->idTipoOperacion = $TipoOperacion;
		    	// $costopagos ->idPeriodoPago = $pagos->idPeriodoPago;
		    	$costopagos ->tipo_operacion = $value;
		    	$costopagos ->idSeccion = $request->idseccion;
		    	$costopagos ->idSeccion2 = $request->idseccionx2;
		    	$costopagos ->costo = $request->monto_pago;
		    	$costopagos ->costo_adventista = $request->monto_pago_adv;
		    	$costopagos ->total_pagos = 10;
		    	$resultadoF = ($request->monto_pago * 12)/10;
		    	$costopagos ->total_costo = $resultadoF;
		    	$costopagos ->total_costo_adventista = $request->monto_total_adv;

		    	$Salvarcostos = $costopagos->save();

		    	$costopagos1 = new CostoPagos();
		    	$costopagos1 ->idTipoOperacion = $TipoOperacion;
		    	// $costopagos ->idPeriodoPago = $pagos->idPeriodoPago;
		    	$costopagos1 ->tipo_operacion = $value;
		    	$costopagos1 ->idSeccion = $request->idseccion;
		    	$costopagos1 ->idSeccion2 = $request->idseccionx2;
		    	$costopagos1 ->costo = $request->monto_pago;
		    	$costopagos1 ->costo_adventista = $request->monto_pago_adv;
		    	$costopagos1 ->total_pagos = 12;
		    	$costopagos1 ->total_costo = $request->monto_pago;
		    	$costopagos1 ->total_costo_adventista = $request->monto_total_adv;

		    	$Salvarcosto1 = $costopagos1->save();

		    	$costopagos2 = new CostoPagos();
		    	$costopagos2 ->idTipoOperacion = $TipoOperacion;
		    	$costopagos2 ->tipo_operacion = $value;
		    	$costopagos2 ->idSeccion = $request->idseccion;
		    	$costopagos2 ->idSeccion2 = $request->idseccionx2;
		    	$costopagos2 ->costo = $request->monto_pago;
		    	$costopagos2 ->costo_adventista = $request->monto_pago_adv;
		    	$costopagos2 ->total_pagos = 1;
		    	$resultadoF2 = ($request->monto_pago * 10);
		    	$costopagos2 ->total_costo = $resultadoF2;
		    	$costopagos2 ->total_costo_adventista = $request->monto_total_adv;
		    	$Salvarcostos2 = $costopagos2->save();
		    }
		    	
		    	if(!$Salvarcostos) {
		            DB::rollback();
		            $tipo_mensaje = "mensaje-danger";
		            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
		        } else {
		        	DB::commit();
					$tipo_mensaje = "mensaje-success";
					$texto_mensaje = "El periodo de pago se ha guardado correctamente.";
				}
				if($value == '4101-0001'){
				    for($cont = 0; $cont < 1; $cont++) {
					    $pagos = new PeriodosPagos();
					    $pagos ->idCosto = $costopagos->id_costo_colegiatura;
					    $pagos ->idPeriodoEscolar = $request->idCiclo;

						$fechado = strtotime ( '+'.($cont).' month' , strtotime ( $nuevafecha ) );
						$fechado = date ( 'Y-m-j' , $fechado );
						
						$pagos ->FechaPago = $fechado;
						$pagos->PagoNum = ($cont+1);
						$pagos->TotalPagos = 1;
						$SalvarPagos = $pagos->save();
				    }
				}
				else{
				    for($cont = 0; $cont < 10; $cont++) {
					    $pagos = new PeriodosPagos();
					    $pagos ->idCosto = $costopagos->id_costo_colegiatura;
					    $pagos ->idPeriodoEscolar = $request->idCiclo;

						$fechado = strtotime ( '+'.($cont).' month' , strtotime ( $nuevafecha ) );
						$fechado = date ( 'Y-m-j' , $fechado );
						
						$pagos ->FechaPago = $fechado;
						$pagos->PagoNum = ($cont+1);
						$pagos->TotalPagos = 10;
						$SalvarPagos = $pagos->save();
				    }

				    for($cont = 0; $cont < 12; $cont++) {
					    $pagos = new PeriodosPagos();
					    $pagos ->idCosto = $costopagos1->id_costo_colegiatura;
					    $pagos ->idPeriodoEscolar = $request->idCiclo;

						$fechado = strtotime ( '+'.($cont).' month' , strtotime ( $nuevafecha ) );
						$fechado = date ( 'Y-m-j' , $fechado );
						
						$pagos ->FechaPago = $fechado;
						$pagos->PagoNum = ($cont+1);
						$pagos->TotalPagos = 12;
						$SalvarPagos = $pagos->save();
				    }

				    for($cont = 0; $cont < 1; $cont++) {
					    $pagos = new PeriodosPagos();
					    $pagos ->idCosto = $costopagos2->id_costo_colegiatura;
					    $pagos ->idPeriodoEscolar = $request->idCiclo;

						$fechado = strtotime ( '+'.($cont).' month' , strtotime ( $nuevafecha ) );
						$fechado = date ( 'Y-m-j' , $fechado );
						
						$pagos ->FechaPago = $fechado;
						$pagos->PagoNum = ($cont+1);
						$pagos->TotalPagos = 1;
						$SalvarPagos = $pagos->save();
				    }
				}

	        if(!$SalvarPagos) {
	            DB::rollback();
	            $tipo_mensaje = "mensaje-danger";
	            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
	        } 	    

	    }
        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("agregar-periodopago?value=1");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El periodo de pago se ha guardado correctamente."]);
 	}
}
