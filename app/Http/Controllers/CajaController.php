<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SaveEntradasSalidasRequest;
use App\Http\Requests\SaveColegiaturasRequest;
use App\Http\Requests\saveConveniosRequest;
use App\Http\Requests\SaveArqueosRequest;
use Illuminate\Support\Facades\Session;
use App\TipoPagos;
use App\TipoOperacionesEfectivo;
use App\TipoOperaciones;
use App\TipoDenominaciones;
use App\Denominaciones;
use App\EntradasSalidas;
use App\DetalleEntradasSalidas;
use App\Alumnos;
use App\Colegiaturas;
use App\CostoColegiaturas;
use App\PeriodosPagos;
use App\Convenios;
use App\Prorrogas;
use App\Arqueos;
use App\TipoResoluciones;
use App\TipoDependencias;
use App\DetalleArqueo;
use App\Colaboradores;
use App\Facturacion;
use App\User;
use App\Secciones;
use App\Grupos;
use App\Salidas;
use App\Grados;
use Carbon\Carbon;
use DomPDF;
use Exception;
use DB;
use PDFS;

class CajaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipoPagos = TipoPagos::all();
        $tipoOperacionesEfectivo = TipoOperacionesEfectivo::all();
        $tipoOperaciones = TipoOperaciones::all();
        $tipoDenominaciones = TipoDenominaciones::all();
        $denominaciones = Denominaciones::all();
        $periodosDePago = PeriodosPagos::orderBy("FechaPago","DESC");
        return view("sistema.caja.lista-entradas-caja",compact("tipoPagos","tipoOperacionesEfectivo","tipoOperaciones","tipoDenominaciones","denominaciones","periodosDePago"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTipoOperaciones(Request $request)
    {
        $tipoOperaciones = TipoOperaciones::whereRaw("Cuenta LIKE ? AND Descripcion LIKE ?",["%".$request->codigo."%","%".$request->operacion."%"])->get();
        return Response()->json($tipoOperaciones);
    }

    public function obtenertipooperacion(Request $request)
    {
        $operacion = TipoOperaciones::where("idTipoOperacion","=",$request->rd_alumno)
            ->first();
        return Response()->json($operacion);
    }

    public function getDenominaciones(Request $request)
    {
        $denominaciones = Denominaciones::whereRaw("idTipoDenominacion = ?",[$request->idTipoDenominacion])->get();
        return Response()->json($denominaciones);
    }

    public function saveSalidas(Request $request)
    {
        $rules =[
                'Monto'=>'required | numeric',
                'Cantidad' => 'required | numeric',
                'PagadoA'=>'required | max:45 ',
                'conope'=>'required',
                'idTipoPago'=>'required',

            ];
            $messages = [
                "conope.required"=>"El campo Concepto de operación es requerido",
                "Monto.required"=>"El campo Cantidad es requerido",
                "Monto.numeric"=>"El campo Cantidad debe ser numerico",
                "Cantidad.required"=>"El campo Cantidad entregada es requerido",
                "Cantidad.numeric"=>"El campo Cantidad debe ser numerico",
                "idTipoPago.required"=>"El campo Tipo de pago es requerido",
                "PagadoA.required"=>"El campo pagado a es requerido",
                
            ];
            $this->validate($request, $rules,$messages);

        $texto_mensaje = "";
        $tipo_mensaje = "";
        $status = 200;

        DB::beginTransaction();

        try {

                $sal = new Salidas();;
                $sal->FechaOperacion = $request->FechaOperacion;
                $sal->idTipoOperacion = $request->conope;
                $sal->tipoOperacionEfectivo=2;
                $sal->Monto = $request->Monto;
                $sal->Cantidad = $request->Cantidad;
                $sal->Cambio = $request->Cambio;
                $sal->idTipoPago = $request->idTipoPago;
                $sal->Referencia = $request->tipo_folio;//true o false
                $sal->PagadoA = $request->PagadoA;
                $sal->DescripcionConcepto = $request->DescripcionConcepto;
                $s = $sal->save();

            DB::commit();
            $texto_mensaje = "Retiro de caja exictosamente";
            $tipo_mensaje = "mensaje-success";
            $status = 200;

        } catch(Exception $ex)
        {
            DB::rollback();
            $texto_mensaje = $ex->getMessage();
            $tipo_mensaje = "mensaje-danger";
            $status = 500;
        }
        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("altas_salidas");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);

    }

    
   public function saveEntradasSalidas(Request $request)
    {
       
          if ($request->idTipoOperacionEfectivoES == 2){

            $rules=[
                'colegi' => 'required | numeric ',
                'Cantidad' => 'required | numeric | ',
                'DescripcionConcepto'=>'required | max:197',
                'PagadoA'=>'required | max:45 ',
                'fechadepago'=>'required',
                'PorcentageBeca'=>'required',
            ];
            $messages = [
                "colegi.required"=>"El campo Monto es requerido",
                "colegi.numeric"=>"El campo Monto debe ser numerico",
                "Cantidad.required"=>"El campo Cantidad es requerido",
                "Cantidad.numeric"=>"El campo Cantidad debe ser numerico",
                "DescripcionConcepto.max"=>"El campo Descripcion Concepto debe contener 45 caracteres como maximo",
                "DescripcionConcepto.required"=>"El campo Descripcion Concepto es requerido",
                "PagadoA.required"=>"El campo pagado a es requerido",
                "PagadoA.max"=>"El campo pagado a debe contener 45 caracteres como maximo",
                "fechadepago.required"=>"Debe seleccionar el tipo de pago",
            ];
            $this->validate($request, $rules, $messages);
         }else
         {
           $rules =[
                'idTipoNivel' => 'required',
                'TotalPago'=>'required | numeric',
                'colegi' => 'required | numeric',
                'FechaOperacion' => 'required',
                // 'idTipoOperacionEfectivoES' => 'required',
                'PagadoA'=>'required | max:45 ',
                'colegi' => 'required | numeric  ',
                'Cantidad' => 'required | numeric  ',
                'fechadepago'=>'required',
                'PorcentageBeca'=>'required',

            ];
             $messages = [
                "FechaOperacion.required"=>"El campo fecha operación es requerido",
                "idTipoOperacionEfectivoES.required"=>"El campo Tipo operacion efectivo es requerido",
                "idTipoNivel.required"=>"Debes seleccionar la operación a realizar",
                "colegi.required"=>"El campo costo es requerido",
                "colegi.numeric"=>"El campo costo debe ser numerico",
                "Monto.required"=>"El campo Monto es requerido",
                "Monto.numeric"=>"El campo Monto debe ser numerico",
                "Cantidad.required"=>"El campo Cantidad es requerido",
                "Cantidad.numeric"=>"El campo Cantidad debe ser numerico",
                "TotalPago.required"=>"El campo Total a pagar es requerido",
                "TotalPago.numeric"=>"El campo Total a pagar debe ser numerico",
                "DescripcionConcepto"=>"El campo Descripcion Concepto debe contener 45 caracteres como maximo",
                "DescripcionConcepto.required"=>"El campo Descripcion Concepto es requerido",
                "PagadoA.required"=>"El campo pagado a es requerido",
                "PagadoA.max"=>"El campo pagado a debe contener 45 caracteres como maximo",
                "fechadepago.required"=>"Debe seleccionar el tipo de pago",

            ];
            $this->validate($request, $rules, $messages);

         }



        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
        $status = 200;
        $i = 0;
        $costo_colegiatura = 0;

        //dd($request);
        /*$val = ($request->idTipoOperacionEfectivoES==2)?$request->rd_tipo_operacion:$request->idTipoNivel;
        dd($val);*/

        DB::beginTransaction();

        try
        {
            if($request->idTipo != 0)
            {

                if(!$request->rd_alumno)
                {
                    throw new Exception("Debes seleccionar un alumno");
                }

                $al = Alumnos::find($request->rd_alumno);
                
                $periodo_pago = PeriodosPagos::where("idPeriodoPago" ,"=" , $request->fechadepago)->first();
                // dd($periodo_pago);
                $conv = Convenios::where("idAlumno","=",$al->idAlumno)->first();
                $pro = Prorrogas::where("idAlumno","=",$al->idAlumno)->first();
                $retraso = false;
                $pagoPorRetraso = 0;
                //verificamos si el alumno tiene una prorroga
                if($pro)
                {
                    if(Carbon::parse($pro->nuevaFecha)->addDay() < Carbon::now())
                    {
                        $retraso = true;
                    }
                }
                else
                {
                    if(Carbon::parse($periodo_pago->FechaPago)->addDay() < Carbon::now())
                    {
                        $retraso = true;
                    }
                }
                //$pagoPorRetraso = $request->RecargoPorPagoRetrasado;

                $costo = CostoColegiaturas::whereRaw("idTipoOperacion = ?",[$request->idTipoNivel])->first();
                //dd($costo);

                //si el alumno es adventista entonces se tomara en cuenta el costo adventista
                if($al->id_religion==4)
                {
                     $costo_colegiatura = $costo->costo_adventista;
                }
                else
                {
                     $costo_colegiatura = 1000;
                }
                if($conv)
                {
                    if($conv->aplicaPara == $request->idTipoNivel)
                    {
                        $costo_colegiatura = $conv->costoAcordado;
                    }
                }
                $cool = Colegiaturas::whereRaw("idAlumno = ? AND idPeriodoPago = ?",[$al->idAlumno,$request->idTipoNivel])->first();
                //dd($cool);
                if($cool != null)
                {
                    throw new Exception("Ya se ha realizado el pago de la inscripción");
                }
                $cool2 = Colegiaturas::whereRaw("idAlumno = ? AND idPeriodoPago= ?",[$al->idAlumno,$request->fechadepago])->first();
                if($cool2!=null)
                {
                    throw new Exception("Ya se ha realizado el pago de la colegiatura");
                }
                if($retraso)
                {
                    $recargo = ($costo_colegiatura * $request->recargo)/100;
                    $pagoPorRetraso = $recargo;
                }  
                $col = new Colegiaturas();
                $col->idAlumno = $al->idAlumno;
                $col->idPeriodoPago = $request->fechadepago;
                $col->colegiatura = $request->colegi;
                $col->fechadepago = $request->FechaOperacion;
                $col->PorcentajeBeca = $request->PorcentageBeca;
                $col->descAdventista = $costo_colegiatura;
                $col->TotalApagar = $request->TotalPago;
                $col->comisionB = $request->comisionB;
                $col->RecargoPorPagoRetrasado = $request->RecargoPorPagoRetrasado;
                $col->Pagado = 1;//true o false
                $col->Prorroga = $request->Prorroga;
                $col->FechaProrroga = $request->FechaProrroga;
                $col->tipodepago = $request->idTipoNivel;
                $c = $col->save();
            }
            $entradaSalida = new EntradasSalidas();
            $entradaSalida->FechaOperacion = $request->FechaOperacion;
            $val1 = ($request->idTipoOperacionEfectivoES==2)?$request->rd_tipo_operacion:1;
            $entradaSalida->tipoOperacionEfectivo = $val1;

            // if ($request->UsarSaldo ==1) {
            //   $entradaSalida->Monto
            // }
            $entradaSalida->Monto = $request->TotalPago;
            $entradaSalida->Cantidad = $request->Cantidad;
            $entradaSalida->Cambio = $request->Cambio;
            $val = ($request->idTipoOperacionEfectivoES==2)?$request->rd_tipo_operacion:$request->idTipoNivel;
            $entradaSalida->idTipoOperacion = $val;
            $entradaSalida->DescripcionConcepto = $request->DescripcionConcepto;

            
            if($request->idTipo != 0)
            {
                $entradaSalida->idReferencia = $col->idReferencia;
            } 
            if($request->idTipoOperacionEfectivoES==1)
            {

            }
            $entradaSalida->idTipoPago = $request->idTipoPago;
            $entradaSalida->PagadoA = $request->PagadoA;
            $entradaSalida->RubroPresupuestal = $request->RubroPresupuestal;
            if($request->tipo_folio)
            {$entradaSalida->tipo_folio = $request->tipo_folio;}
            $e_s = $entradaSalida->save();
            
            if ($request->UsarSaldo==1) {
                if ($saldo>$request->TotalPago) {
                    $resta=$TotalPago-$saldo;


                }else{
                $resta=0;
            }
            $al->saldo=$resta;
            $al->save();
        }
            


            if(!$e_s)
            {
                throw new Exception("No se pudo registrar la entrada / salida");
            }

            DB::commit();
            $texto_mensaje = "La operación se realizo con exito!";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = $ex->getMessage()." ".$ex->getLine();
            $status = 500;
            //dd($ex);
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/caja");
        }

        $token = csrf_token();
        //dd($token);

        return Response()->json(["mensaje"=>$texto_mensaje,"token"=>$token],$status);
    }



    public function saldos(Request $request)
    {
       if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
                ['idSeccion', '=', $request->nivel], ['Saldo','!=' ,'0']
            ])->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
               
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
                ['idSeccion', '=', $request->nivel],
                ['idGrado', '=', $request->grado], 
                ['Saldo','!=' ,'0']
            ])->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
            });
        }

        elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
                ['idSeccion', '=', $request->nivel],
                ['idGrado', '=', $request->grado],
                ['idGrupo', '=', $request->grupo],
                ['Saldo','!=' ,'0']
            ])->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        }
        elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
                ['Nombres', 'like', "%$request->alumno%"],
                ['idSeccion', '=', $request->nivel],
                ['Saldo','!=' ,'0']
            ])->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
                ['Nombres', 'like', "%$request->alumno%"],
                ['idSeccion', '=', $request->nivel],
                ['idGrado', '=', $request->grado],
                ['Saldo','!=' ,'0']
            ])->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                
            });
        }
        elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
                ['Nombres', 'like', "%$request->alumno%"],
                ['idSeccion', '=', $request->nivel],
                ['idGrado', '=', $request->grado],
                ['idGrupo', '=', $request->grupo],
                ['Saldo','!=' ,'0']
            ])->orderBy("idAlumno","DESC")->paginate(20);

            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

         else {
            $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')
            // ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);
            ->where([
                ['Saldo','!=' ,'0'],                
            ])->orderBy("idAlumno","DESC")->paginate(20);


            $estudiantes->each(function($estudiantes) {
                $estudiantes->seccion;
                $estudiantes->grado;
                $estudiantes->grupo;
            });
        }

        $secciones = Secciones::get();
        return view("sistema.caja.saldos",compact("estudiantes","secciones"));
    }

    public function lista_entradas_salidas(Request $request)
    {
        // dd($request->metodo);
        $lista_entradas_salidas = null;
        // condicionales solo para los filtro, se validan si estan llenas o vacios
        if(is_null($request->FechaInicial) || $request->FechaInicial == ""  && is_null($request->FechaFinal) || $request->FechaFinal == "" && is_null($request->metodo))
        {
            $lista_entradas_salidas = EntradasSalidas::orderBy('idFolio','desc')->paginate(20);
        }
        else if($request->metodo == 1)
        {
             $lista_entradas_salidas = EntradasSalidas::where('tipoOperacionEfectivo','=',$request->metodo)->orderBy('idFolio','desc')->paginate(20);
        }
        else if($request->metodo == 2)
        {
            $lista_entradas_salidas = Salidas::where('tipoOperacionEfectivo','=',$request->metodo)->orderBy('idFolio','desc')->paginate(20);
        }
        else if($request->FechaInicial!=""&&$request->FechaFinal=="")
        {
            $lista_entradas_salidas = EntradasSalidas::where("FechaOperacion",">",$request->FechaInicial)->orderBy('idFolio','desc')
                                                  ->paginate(20);
        }
        else if($request->FechaInicial==""&&$request->FechaFinal!="")
        {
            $lista_entradas_salidas = EntradasSalidas::where("FechaOperacion",'<',$request->FechaFinal)->orderBy('idFolio','desc')
                                                  ->paginate(20);
        }
        else
        {
         $lista_entradas_salidas = EntradasSalidas::whereRaw("FechaOperacion BETWEEN ? AND ?",[$request->FechaInicial,$request->FechaFinal])->orderBy('idFolio','desc')
                                                  ->paginate(20);
        }


        return view("sistema.caja.lista_entradas_salidas",compact("lista_entradas_salidas"));
    }
    

    public function cobro_recurrente()
    {
        $texto_mensaje = "";
        $tipo_mensaje = "";
        $status = 200;
        $saldo = 0;

        $periodo_pago = 5;

        try
        {
            //si el dia del mes es entre el 5 y el 10 se ejecutara el cobro
            if(Carbon::now()->format("d") >= 5 && Carbon::now()->format("d") <= 10)
            {
                //echo "todo Ok";
                $alumnos = Alumnos::whereRaw("cobro_recurrente = ?",[true])->get();
                //dd($alumnos);

                foreach($alumnos as $al)
                {
                    $pagoPorRetraso = 0;
                    $costo = CostoColegiaturas::whereRaw("idSeccion = ?",[$al->idSeccion])->first();
                    $costo_colegiatura = $costo->costo;

                    $cool = Colegiaturas::whereRaw("idAlumno = ? AND idPeriodoPago = ?",[$al->idAlumno,$periodo_pago])->first();

                    if($cool)
                    {
                        continue;
                    }

                    if($al->Beca)
                    {
                        if($al->Saldo >= (($costo_colegiatura - (($costo_colegiatura*$al->PorcentajeBeca)/100))+$pagoPorRetraso))
                        {
                            $col = new Colegiaturas();
                            $col->idAlumno = $al->idAlumno;
                            $col->idPeriodoPago = $periodo_pago;
                            $col->colegiatura = $costo_colegiatura;
                            $col->PorcentajeBeca = $al->PorcentajeBeca;
                            $col->TotalApagar = ($costo_colegiatura - ($costo_colegiatura*$al->PorcentajeBeca)/100)+$pagoPorRetraso;
                            $col->RecargoPorPagoRetrasado = $pagoPorRetraso;
                            $col->Pagado = 1;
                            $col->Prorroga = 0;
                            $col->FechaProrroga = null;
                            //se descuenta el costo de la colegiatura menos el porcentage de la beca a el saldo del alumno
                            $al->Saldo = $al->Saldo - (($costo_colegiatura - (($costo_colegiatura*$al->PorcentajeBeca)/100))+$pagoPorRetraso);
                            
                            $al->save();
                            $col->save();
                            echo $al->Saldo;
                        }
                        else
                        {
                            //no tienes fondos suficientes
                            echo "No tienes fondos suficientes";
                        }
                    }
                    else
                    {
                        if($al->Saldo >= ($costo_colegiatura+$pagoPorRetraso))
                        {
                            $col = new Colegiaturas();
                            $col->idAlumno = $al->idAlumno;
                            $col->idPeriodoPago = $periodo_pago;
                            $col->colegiatura = $costo_colegiatura;
                            $col->PorcentajeBeca = $al->PorcentajeBeca;
                            $col->TotalApagar = $costo_colegiatura + $pagoPorRetraso;
                            $col->RecargoPorPagoRetrasado = $pagoPorRetraso;
                            $col->Pagado = 1;
                            $col->Prorroga = 0;
                            $col->FechaProrroga = null;
                            //se descuenta el costo de la colegiatura menos el porcentage de la beca a el saldo del alumno
                            $al->Saldo = $al->Saldo - ($costo_colegiatura+$pagoPorRetraso);
                            
                            $al->save();
                            $col->save();
                            echo $al->Saldo;
                        }
                        else
                        {
                            //algo
                        }
                    }
                    

                    echo "nothing";
                    

                }
            }
            else
            {
                echo Carbon::now()->format("d");
            }

            //$alumnos = Alumnos::whereRaw("Cobro_recurrente = ? ",[true])->get();
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage();
        }
    }

    public function lista_costo_colegiaturas()
    {
        $costo_colegiaturas = CostoColegiaturas::paginate(20);
        return view("sistema.caja.lista_costo_colegiaturas",compact("costo_colegiaturas"));
    }

    public function alta_convenios()
    {
        return view("sistema.caja.alta_convenios");
    }

   public function save_convenios(saveConveniosRequest $request)
    {

        $texto_mensaje = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            $conv = new Convenios();
            $conv->num_convenio = $request->numero_convenio;
            $conv->Descripcion = $request->Descripcionconv;
            $conv->idAlumno = $request->rd_alumno;
            $conv->fechaInicio = $request->FechaInicio;
            $conv->fechaVencimiento = $request->FechaVencimiento;
            if($request->changeCostoColeg == 0) {
                $conv->changeCostoColeg = NULL;
                $conv->costoAcordado = NULL;
                $conv->aplicaPara = NULL;
            } else {
                $conv->changeCostoColeg = 1;
                $conv->costoAcordado = $request->CostoAcordado;
                $conv->aplicaPara = $request->idTipoNivel;
            }
            if($request->ckFechaPago == 0) {
                $conv->ckFechaPago = NULL;
                $conv->FechaPago = NULL;
            } else {
                $conv->ckFechaPago = 1;
                $conv->FechaPago = $request->FechaPago;
            }

            $c = $conv->save();

            if(!$c)
            {
                throw new Exception("No se pudo registrar el convenio");
            }

            DB::commit();
            $texto_mensaje = "El convenio se registro con exito!";
            $status = 200;            
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $texto_mensaje = $ex->getMessage();
            $status = 500;
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

    /*
        funcion para saber si un alumno tiene un convenio acivo
    */
    public function verificarConvenio(Request $request){
        $variable = date('Y-m-d');
        $convenio = Convenios::where("idAlumno","=",$request->rd_alumno)
            ->join("tipooperaciones","tipooperaciones.idTipoOperacion","=","convenios.aplicaPara")
            ->first();
        return Response()->json($convenio);
    }
    public function verificarColegiatura(Request $request){
        
        $pivo = Alumnos::where("alumnos.idAlumno","=",$request->rd_alumno)->where("colegiaturas.tipodepago","=",$request->tipopago)
        ->join("costo_colegiaturas","costo_colegiaturas.idSeccion2","=","alumnos.idSeccion")
        ->join("colegiaturas","colegiaturas.tipodepago","=","costo_colegiaturas.idTipoOperacion")
        ->join("tipooperaciones","tipooperaciones.idTipoOperacion","=","costo_colegiaturas.idTipoOperacion")
        ->first(); 
        
        return Response()->json($pivo);
    }
    public function groupbyperiodo(Request $request){
        $group= Colegiaturas::where('colegiaturas.idAlumno','=',$request->Alumno)
        ->where('colegiaturas.tipodepago','=',$request->tipo)
        ->where('costo_colegiaturas.total_pagos','=',$request->pago)
        ->join("periodospagos","periodospagos.idPeriodoPago","=","colegiaturas.idPeriodoPago")
        ->join("costo_colegiaturas","costo_colegiaturas.id_costo_colegiatura","=","periodospagos.idCosto")
        ->orderBy('colegiaturas.idReferencia','desc')
        ->first();
        return Response()->json($group);
    }
    public function getoperation(Request $request){
        $operation = CostoColegiaturas::where('costo_colegiaturas.idTipoOperacion','=',$request->opera)
        ->join('periodospagos','periodospagos.idCosto','=','costo_colegiaturas.id_costo_colegiatura')
        ->first();
        return Response()->json($operation);
    }
    public function obcolegiatura(Request $request){
        $obtecolegiatura = Colegiaturas::where('colegiaturas.idAlumno','=',$request->rd_alumno)
        ->where('colegiaturas.idPeriodoPago','=',$request->fechadepago)
        ->first();
        return Response()->json($obtecolegiatura);
    }
    public function modificarcolegiatura(Request $request){
        $texto_mensaje = "";
        $tipo_mensaje = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            Colegiaturas::where('colegiaturas.idReferencia','=',$request->rd_alumno)
        ->update(['colegiaturas.PorcentajeBeca'=>$request->porcentage,
            'colegiaturas.TotalApagar'=>$request->TotalApagar,
            'colegiaturas.RecargoPorPagoRetrasado'=>$request->RecargoPorPagoRetrasado,
            'colegiaturas.comisionB'=>$request->comisionB,
            'colegiaturas.fechadepago'=>$request->fechadepago]);
        EntradasSalidas::where('entradassalidas.idFolio','=',$request->rd_alumno)
        ->update(['entradassalidas.Cantidad'=>$request->Cantidad,
            'entradassalidas.Monto'=>$request->TotalApagar,
            'entradassalidas.idTipoPago'=>$request->idTipoPago,
            'entradassalidas.Cambio'=>$request->Cambio]);

            DB::commit();
            $texto_mensaje = "La colegiatura se pago exitosamente!";
            $tipo_mensaje = "mensaje-success";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $texto_mensaje = $ex->getMessage();
            $tipo_mensaje = "mensaje-danger";
            $status = 500;
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista_costo_colegiaturas");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
        
    }
    public function limitdate(Request $request){
        $alumno = Alumnos::where('alumnos.idAlumno','=',$request->rd_alumno)
        ->where("costo_colegiaturas.total_pagos","=",$request->pagos)
        ->where('costo_colegiaturas.idTipoOperacion','=',$request->tipo)
        ->join("costo_colegiaturas","costo_colegiaturas.idSeccion2","=","alumnos.idSeccion")
        // ->join("colegiaturas","colegiaturas.idAlumno","=","alumnos.idAlumno")
        ->join("periodospagos","periodospagos.idCosto","=","costo_colegiaturas.id_costo_colegiatura")
        ->get();
        return Response()->json($alumno);
    }
    public function obtFechasdePago(Request $request){
        
        $alumno = Alumnos::where('alumnos.idAlumno','=',$request->rd_alumno)
        ->where("costo_colegiaturas.total_pagos","=",$request->pagos)
        ->join("costo_colegiaturas","costo_colegiaturas.idSeccion2","=","alumnos.idSeccion")
        ->join("periodospagos","periodospagos.idCosto","=","costo_colegiaturas.id_costo_colegiatura")
        ->orderBy('periodospagos.idPeriodoPago','DESC')
        ->take(1)  
        ->get();
        return Response()->json($alumno);
    }
    public function searchDate(Request $request){
        $alumno = Colegiaturas::where('colegiaturas.idAlumno','=',$request->idAlumno)
        ->where('colegiaturas.tipodepago','=',$request->tipo)
        ->join("periodospagos","periodospagos.idPeriodoPago","=","colegiaturas.idPeriodoPago")
        ->join("costo_colegiaturas","costo_colegiaturas.id_costo_colegiatura","=","periodospagos.idCosto")
        ->first();
        return Response()->json($alumno);
    }
    public function obtTipoOperacionByCategoria(Request $request){
        $alumnoSeccion = Alumnos::where('idAlumno','=',$request->idAlumno)
        //->first();
        ->pluck('alumnos.idSeccion');
        $tipooperacion = "";
        if($request->codigo == '4101-0001'){
            if($alumnoSeccion[0] == 1){
                $tipooperacio = 178;
            }else if($alumnoSeccion[0] == 2){
                $tipooperacio = 179;
            }
            else if($alumnoSeccion[0] == 3){
                $tipooperacio = 180;
            }
            else{
                $tipooperacio = 181;
            }
        }else if($request->codigo == '4101-0002'){
            if($alumnoSeccion[0] == 1){
                $tipooperacio = 183;
            }else if($alumnoSeccion[0] == 2){
                $tipooperacio = 184;
            }
            else if($alumnoSeccion[0] == 3){
                $tipooperacio = 185;
            }
            else{
                $tipooperacio = 186;
            }
        }
        else{
            if($alumnoSeccion[0] == 1){
                $tipooperacio = 188;
            }else if($alumnoSeccion[0] == 2){
                $tipooperacio = 189;
            }
            else if($alumnoSeccion[0] == 3){
                $tipooperacio = 190;
            }
            else{
                $tipooperacio = 191;
            }
        }
        $operaciones = TipoOperaciones::where('idTipoOperacion','=',$tipooperacio)->whereRaw("cuenta LIKE ?",["%".$request->codigo."%"])->get();
        return Response()->json($operaciones);
        //return Response()->json($alumnoSeccion);
    }
    public function obtCostoColegiatura(Request $request)
    {
        $pivote = Colegiaturas::where("idAlumno","=",$request->rd_alumno)        
        ->join("costo_colegiaturas","costo_colegiaturas.idPeriodoPago","=","colegiaturas.idPeriodoPago")
        ->join("periodospagos","colegiaturas.idPeriodoPago","=","periodospagos.idPeriodoPago")
        ->first();
    }   
    public function verificarProrroga(Request $request){
        /*$col = Colegiaturas::where("idAlumno","=",$request->rd_alumno)
                                ->where("Prorroga","=",true)
                                ->first();*/
        $pro = Prorrogas::where("idAlumno","=",$request->rd_alumno)->first();

        return Response()->json($pro);
    }
    public function resumen_colegiaturas(){
        return view("sistema.caja.resumen_colegiaturas");
    }
    public function findCosto($id_costo){
        $costo_colegiatura = CostoColegiaturas::select("costo_colegiaturas.id_costo_colegiatura as id_costo","x2.Descripcion","costo_colegiaturas.costo","costo_colegiaturas.costo_adventista","costo_colegiaturas.total_pagos","x1.Seccion")
                                ->join("secciones as x1","costo_colegiaturas.idSeccion2","=","x1.idSeccion")
                                ->join("tipooperaciones as x2","costo_colegiaturas.idTipoOperacion","=","x2.idTipoOperacion")
                                ->whereRaw("id_costo_colegiatura = ?",[$id_costo])->first();
        return Response()->json($costo_colegiatura);
    }
    public function updateCosto(Request $request){
        $texto_mensaje = "";
        $tipo_mensaje = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            $costo = CostoColegiaturas::find($request->id_costo);
            $costo->costo = $request->costo;
            $costo->costo_adventista = $request->CostoAdventistaModal;
            $c = $costo->save();

            if(!$c)
            {
                throw new Exception("No se pudo actualizar");
            }

            DB::commit();
            $texto_mensaje = "El costo se modifico con exito!";
            $tipo_mensaje = "mensaje-success";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $texto_mensaje = $ex->getMessage();
            $tipo_mensaje = "mensaje-danger";
            $status = 500;
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista_costo_colegiaturas");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }
    public function generarNumConvenio()
    {
        return Response()->json(["num_convenio"=>Convenios::generarNumConvenio()]);
    }
    public function generarNumArqueo()
    {
        return Response()->json(["codigo"=>Arqueos::generarNumArqueo()]);
    }
    public function imprimir_entradas_salidas(Request $request)
    {
        // dd($request);
        $lista_entradas_salidas = null;
        $titulo = null;

        if(is_null($request->FechaInicial) || $request->FechaInicial == ""  && is_null($request->FechaFinal) || $request->FechaFinal == "")
        {
            $lista_entradas_salidas = EntradasSalidas::paginate(10);
        }
        elseif($request->FechaInicial != ""  &&  $request->FechaFinal != "" && $request->metodo == '')
        {
             $lista_entradas_salidas = EntradasSalidas::whereRaw("FechaOperacion BETWEEN ? AND ?",[$request->FechaInicial,$request->FechaFinal])
                                                      ->paginate(10);
        }
        // Se valida que el select metodo venga con datos y que (entradas = 1 && salidas = 2)
       else if($request->metodo == 1)
        {
             $lista_entradas_salidas = EntradasSalidas::where('tipoOperacionEfectivo','=',$request->metodo)->paginate(10);
             $titulo = 'Entradas';
        }
        else if($request->metodo == 2)
        {
            $lista_entradas_salidas = EntradasSalidas::where('tipoOperacionEfectivo','=',$request->metodo)->paginate(10);
            $titulo = 'Salidas';
        }

        $pdf = PDFS::loadView("sistema.caja.imprimir_entradas_salidas",compact(["lista_entradas_salidas",'titulo']));
        $pdf->setPaper("letter","landscape");
        return $pdf->inline("entradas_salidas.pdf");
    }

    public function imprimir_entrada_salida_individual(Request $request)
    {
        $pdf = PDFS::loadView("sistema.caja.imprimir_entrada_salida_individual");
        //$pdf->setPaper("letter","landscape");
        return $pdf->inline("entradas_salidas.pdf");
    }

    public function arqueos()
    {
        $denominaciones = Denominaciones::all();
        $tipoDenominaciones = TipoDenominaciones::all();
        $tipoOperacionesEfectivo = TipoOperacionesEfectivo::all();
       
        //6 == caja
        $colaboradores = Colaboradores::where("idRol","=",6)->get();

        return view("sistema.caja.arqueos",compact("denominaciones","tipoDenominaciones","tipoOperacionesEfectivo","colaboradores"));
    }

    public function generarCorte(Request $request)
    {
        //dd($request->FechaCorte);
        $in_out = EntradasSalidas::select("entradassalidas.*","x1.Descripcion as tipoPago")
                                 ->join("tipopagos as x1","x1.idTipoPago","=","entradassalidas.idTipoPago")
                                 ->where("FechaOperacion","=",$request->FechaCorte)
                                 ->get();//where("FechaOperacion","=",$request->FechaCorte)->get();
        //dd($in_out);
        return Response()->json($in_out);
    }

    public function saveArqueos(SaveArqueosRequest $request)
    {
        $texto_mensaje = "";
        $tipo_mensaje = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            $arq = new Arqueos();
            $arq->FechaOperacion = $request->FechaCorte;
            $arq->Codigo = $request->codigo;
            $arq->TotalBilletesEfectivo = $request->total_billetes;
            $arq->TotalMonedasEfectivo = $request->total_monedas;
            $arq->idColaborador = $request->id_funcionario_manejo;
            $arq->idFuncionarioControl = $request->FuncionarioControl;
            $arq->hora_elab_arqueo = $request->hora_elab_arqueo;
            $arq->fecha_elab_arqueo = $request->fecha_elab_arqueo;
            $arq->hora_conteo = $request->hora_conteo;
            $arq->fecha_conteo = $request->fecha_conteo;
            $arq->TotalCaja = $request->total_caja;
            $arq->TotalEfectivo = $request->total_efectivo;
            //dd($request->hora_elab_arqueo);
            //$arq->TotalCaja = 
            $arq->Observaciones = $request->Observaciones;
            $ar = $arq->save();

            if(!$ar)
            {
                throw new Exception("No se pudo registrar el arqueo");
            }

            $i = 0;

            if(count($request->idDenominacion)>0)
            {
                foreach($request->idDenominacion as $idDe)
                {
                    $det_arq = new DetalleArqueo();
                    $det_arq->idTipoDenominacion = $idDe;
                    $det_arq->idDenominacion = $request->denominacion[$i];
                    $det_arq->CantidadUnidades = $request->CantidadUnidades[$i];
                    $det_arq->idTipoOperacionEfectivo = $request->idTipoOperacionEfectivo[$i];
                    $det_arq->id_arqueo = $arq->idArqueo;
                    $d_a = $det_arq->save();

                    $i=$i+1;

                    if(!$d_a)
                    {
                        throw new Exception("No se pudo guardar el detalle del arqueo");
                    }
                }
            }

            $status = 200;
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El arqueo se registro con exito!";
            DB::commit();
        }
        catch(Exception $ex)
        {   
            $status = 500;
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = $ex->getMessage();
            DB::rollback();
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("arqueos");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

    public function imprimir_arqueo(Request $request,$codigo)
    {
         $arqueo = Arqueos::whereRaw("Codigo = ?",[$codigo])->first();
        $in_out = EntradasSalidas::select("entradassalidas.*","x1.Descripcion as tipoPago")
                                 ->join("tipopagos as x1","x1.idTipoPago","=","entradassalidas.idTipoPago")
                                 ->where("FechaOperacion","=",$arqueo->FechaCorte)
                                 ->get();

        $pdf = PDFS::loadView("sistema.caja.imprimir_arqueo",compact("arqueo","in_out"));
        return $pdf->inline("arqueo_de_caja.pdf");
       
    }

    public function lista_arqueos(Request $request)
    {
        // condicionales para los filtro de la vista se valida que si estan vacios o no 
        if($request->fec_ini!="" && $request->fec_fin!="")
        {


               $arqueos = Arqueos::orderBy("FechaOperacion",'DESC')
       ->whereBetween('FechaOperacion', [$request->fec_ini, $request->fec_fin])
       ->paginate(20);

        $arqueos->each(function ($arqueos){
            $arqueos->colaborador;
        });
        return view("sistema.caja.lista_arqueos",compact("arqueos"));

        }
        else if($request->fec_ini=="" && $request->fec_fin!="")
        {
             $arqueos = Arqueos::where('FechaOperacion','<',$request->fec_fin)->orderBy("FechaOperacion","DESC")->paginate(20);
        $arqueos->each(function ($arqueos){
            $arqueos->colaborador;
        });
        return view("sistema.caja.lista_arqueos",compact("arqueos"));
        }
        else if($request->fec_ini!="" && $request->fec_fin=="")
        {
             $arqueos = Arqueos::where('FechaOperacion','>',$request->fec_ini)->orderBy("FechaOperacion","DESC")->paginate(20);
        $arqueos->each(function ($arqueos){
            $arqueos->colaborador;
        });
        return view("sistema.caja.lista_arqueos",compact("arqueos"));
        }
        else
        {
            $arqueos = Arqueos::orderBy("FechaOperacion","DESC")->paginate(20);
        $arqueos->each(function ($arqueos){
            $arqueos->colaborador;
        });
        return view("sistema.caja.lista_arqueos",compact("arqueos"));
        }

    }

    public function alta_becas()
    {
        return view("sistema.caja.alta_becas");
    }

    public function save_becas(Request $request)
    {
        $texto_mensaje = "";
        $tipo_mensaje = "";
        $status = 200;

        DB::beginTransaction();

        try
        {
            $alumno = Alumnos::find($request->rd_alumno);
            // Se verifica que el checkbox de beca sep no venga vacio
            if($request->BecaSEP != null)
                $alumno->BecaSEP = $request->BecaSEP;
            else
                $alumno->Beca = 1;
            $alumno->PorcentajeBeca = $request->PorcentajeBeca;

            $al = $alumno->save();

            if(!$al)
            {
                throw new Exception("se se pudo actualizar la beca");
            }

            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "La beca se asigno con exito!";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = $ex->getMessage();
            $status = 500;
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("alta_becas");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

    public function lista_salidas(Request $request)
    {
        //dd($request->FechaInicial);
        $lista_entradas_salidas = null;

        if(is_null($request->FechaInicial) || $request->FechaInicial == ""  && is_null($request->FechaFinal) || $request->FechaFinal == "")
        {
            $lista_entradas_salidas = EntradasSalidas::where("tipoOperacionEfectivo","=",2)->paginate(20);
        }
        else
        {
             $lista_entradas_salidas = EntradasSalidas::whereRaw("tipoOperacionEfectivo = 2 AND FechaOperacion BETWEEN ? AND ?",[$request->FechaInicial,$request->FechaFinal])
                                                      ->paginate(20);
        }


        return view("sistema.caja.lista_salidas",compact("lista_entradas_salidas"));
    }

    public function lista_entradas(Request $request)
    {
        //dd($request->FechaInicial);
        $lista_entradas_salidas = null;

        if(is_null($request->FechaInicial) || $request->FechaInicial == ""  && is_null($request->FechaFinal) || $request->FechaFinal == "")
        {
            $lista_entradas_salidas = EntradasSalidas::where("tipoOperacionEfectivo","=",1)->paginate(20);
        }
        else
        {
            $lista_entradas_salidas = EntradasSalidas::whereRaw("tipoOperacionEfectivo = 1 AND FechaOperacion BETWEEN ? AND ?",[$request->FechaInicial,$request->FechaFinal])
                                                      ->paginate(20);
        }

        return view("sistema.caja.lista_entradas",compact("lista_entradas_salidas"));
    }
    /**=================================================================================================================***/
    public function lista_convenios(Request $request) {


        if($request->nombres!="")
        {
            if($request->Nivel != ""){
                if($request->Nivel == 1){
                    $b1 = 183;
                    $b2 = 178;
                }
                else if($request->Nivel == 2){
                    $b1 = 184;
                    $b2 = 179;
                }
                else if($request->Nivel == 3){
                    $b1 = 185;
                    $b2 = 180;
                }
                else{
                    $b1 = 181;
                    $b2 = 186;
                }
            }
            else{
                $b1 ="";
                $b2 ="";
            }

            $convenios = Convenios::where([['Nombres','like',"%$request->nombres%"]])
            ->orWhere('aplicaPara','=',$b1)
            ->orWhere('aplicaPara','=',$b2)
            ->join('tipooperaciones as tip', "convenios.aplicaPara","=", "tip.idTipoOperacion")
             ->join("alumnos","alumnos.idAlumno","=","convenios.idAlumno")
             ->orderBy('id_convenio', 'DESC')->paginate(20, array('convenios.*','tip.Descripcion as DescripcionTipo','tip.Cuenta as Cuentas','alumnos.*'));
            $convenios->each(function($convenios) {
                $convenios->alumnos;
            });
            
            return view ('sistema/caja/listado-convenios', ['convenios' => $convenios]);

        }
        else{
                if($request->Nivel != ""){
                    if($request->Nivel == 1){
                        $b1 = 183;
                        $b2 = 178;
                    }
                    else if($request->Nivel == 2){
                        $b1 = 184;
                        $b2 = 179;
                    }
                    else if($request->Nivel == 3){
                        $b1 = 185;
                        $b2 = 180;
                    }
                    else{
                        $b1 = 181;
                        $b2 = 186;
                    }
                }
                else{
                    $b1 ="";
                    $b2 ="";
                }
            $c = Convenios::where([['Nombres','like',"%$request->nombres%"]])
            ->Where('aplicaPara','=',$b1)
            ->orWhere('aplicaPara','=',$b2)
            ->join('tipooperaciones as tip', "convenios.aplicaPara","=", "tip.idTipoOperacion")
            ->join("alumnos","alumnos.idAlumno","=","convenios.idAlumno")
            ->orderBy('id_convenio', 'DESC')->paginate(20, array('convenios.*','tip.Descripcion as DescripcionTipo','tip.Cuenta as Cuentas','alumnos.*'));
            if(count($c) == 0){
                $convenios = Convenios::where([['Nombres','like',"%$request->nombres%"]])
                ->join('tipooperaciones as tip', "convenios.aplicaPara","=", "tip.idTipoOperacion")
                ->join("alumnos","alumnos.idAlumno","=","convenios.idAlumno")
                ->orderBy('id_convenio', 'DESC')->paginate(20, array('convenios.*','tip.Descripcion as DescripcionTipo','tip.Cuenta as Cuentas','alumnos.*'));
            }
            else{
                $convenios = Convenios::where([['Nombres','like',"%$request->nombres%"]])
            ->Where('aplicaPara','=',$b1)
            ->orWhere('aplicaPara','=',$b2)
            ->join('tipooperaciones as tip', "convenios.aplicaPara","=", "tip.idTipoOperacion")
            ->join("alumnos","alumnos.idAlumno","=","convenios.idAlumno")
            ->orderBy('id_convenio', 'DESC')->paginate(20, array('convenios.*','tip.Descripcion as DescripcionTipo','tip.Cuenta as Cuentas','alumnos.*'));
            }
            $convenios->each(function($convenios) {
                $convenios->alumnos;
            });
            return view ('sistema/caja/listado-convenios', ['convenios' => $convenios]);
        }
    }
    //------------------------------MODIFICACIONES COMPLETAS DE EDGAR EN CAJA------------------//
    public function altasalidas (Request $request)
    {
        // Las salidas se pusieron aparte ya que se requeria de dos vistas diferentes, pero al guardar se quedo en la misma funcion
        $tipoPagos = TipoPagos::all();
        $tipoOperacionesEfectivo = TipoOperacionesEfectivo::all();
        $tipoOperaciones = TipoOperaciones::all();
        $tipoDenominaciones = TipoDenominaciones::all();
        $denominaciones = Denominaciones::all();
        $colaboradores = Colaboradores::where('idRol','=',4)->get();
        $periodosDePago = PeriodosPagos::orderBy("FechaPago","DESC");
        return view("sistema.caja.altasalidas",compact("tipoPagos","tipoOperacionesEfectivo","colaboradores","tipoOperaciones","tipoDenominaciones","denominaciones","periodosDePago"));
    }
    public function facturasentregadas(Request $request)
    {
        // Se buscan las facturas entregadas y se validan los filtros 
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
       $alumno = Alumnos::pluck("idAlumno");
        if($request->nombres!=""&&$request->fec_ini==null&&$request->fec_fin==null)
        {
           

        $facturacion = Facturacion::where('status',1)
        ->orderBy("alumnos.Nombres",'DESC')
        ->where([['Nombres','like',"%$request->nombres%"]])
      ->join("alumnos","alumnos.idAlumno","=","facturas.idAlumno")
      ->orderBy('idFactura', 'DESC')->paginate(15);
      $users = User::orderBy('id', 'ASC')->get();
       return view ('sistema/caja/facturasentregadas', ['facturacion' => $facturacion, 'users' => $users]);

        } 
        elseif($request->nombres==null&&$request->fec_ini!=""&&$request->fec_fin!=""){   

        $facturacion = Facturacion::where('status',1)
        ->orderBy("alumnos.Nombres",'DESC')
       ->whereBetween('fecha', [$request->fec_ini, $request->fec_fin])
        ->join("alumnos","alumnos.idAlumno","=","facturas.idAlumno")
        ->paginate(15);
       $users = User::orderBy('id', 'ASC')->get();
       return view ('sistema/caja/facturasentregadas', ['facturacion' => $facturacion, 'users' => $users]);

        }
        elseif($request->nombres==null&&$request->fec_ini!=""&&$request->fec_fin==NULL){   

        $facturacion = Facturacion::where('status',1)
        ->orderBy("alumnos.Nombres",'DESC')
       ->whereRaw('fecha >= "'.$request->fec_ini.'"')
        ->join("alumnos","alumnos.idAlumno","=","facturas.idAlumno")
        ->paginate(15);
       $users = User::orderBy('id', 'ASC')->get();
       return view ('sistema/caja/facturasentregadas', ['facturacion' => $facturacion, 'users' => $users]);
        }
        elseif($request->nombres==null&&$request->fec_ini==null&&$request->fec_fin!=""){   

        $facturacion = Facturacion::where('status',1)
        ->orderBy("alumnos.Nombres",'DESC')
       ->whereRaw('fecha < "'.$request->fec_fin.'"')
        ->join("alumnos","alumnos.idAlumno","=","facturas.idAlumno")
        ->paginate(15);
       $users = User::orderBy('id', 'ASC')->get();
       return view ('sistema/caja/facturasentregadas', ['facturacion' => $facturacion, 'users' => $users]);
        }

        else{
    
             $facturacion = Facturacion::where('status',1)
            ->orderBy("alumnos.Nombres",'DESC')
             ->join("alumnos","alumnos.idAlumno","=","facturas.idAlumno")
              ->orderBy('idFactura', 'DESC')->paginate(15);
    
          $facturacion->each(function($facturacion) {
                $facturacion->alumnos;
                $facturacion->parentescos;
            });
    
    
            $users = User::orderBy('id', 'ASC')->get();
              return view ('sistema/caja/facturasentregadas', ['facturacion' => $facturacion, 'users' => $users]);
    
        }
    }
    public function getGrados($id_seccion)
    {
        # code...
        $grupos = Grados::whereRaw("idSeccion = ?",[$id_seccion])->orderBy("idGrado","ASC")->get();
        return Response()->json($grupos);
        
    }
    public function getGrupos($id_grado)
    {
        # code...
        $grupos = Grupos::whereRaw("idGrado = ?",[$id_grado])->orderBy("grupo","ASC")->get();
        return Response()->json($grupos);
        
    }
    public function imprimir_ticket($id)
    {
         
          // Obtenemos los datos de la consulta de la función.
            $data = $this->imprimir($id);
            // Verificamos que exista un arreglo.
            if(isset($data['colegiaturas'][0])) {
            $pdf = PDFS::loadView("sistema.caja.ticket", $data);
                $pdf->setOption('page-height','290');
                $pdf->setOption('page-width','80');
                $pdf->setOption('margin-top',5);
                $pdf->setOption('margin-right',5);
                $pdf->setOption('margin-left',5);
                $pdf->setOption('margin-bottom',0);
                return $pdf->inline("ticket.pdf");

            } else {
                return 'No se halló información suficiente para generar el reporte';
            }
       
    }

     public function imprimir_recibo($id)
    {
        
          // Obtenemos los datos de la consulta de la función.
            $data = $this->imprimir($id);
            // Verificamos que exista un arreglo.
            if(isset($data['colegiaturas'][0])) {
            $pdf = PDFS::loadView("sistema.caja.recibo", $data)->setOrientation('portrait');
                return $pdf->inline("recibo.pdf");

            } else {
                return 'No se halló información suficiente para generar el reporte';
            }
       
    }
    
     // Obtenemos información de los alumnos en base su $id.
        function imprimir($id) {

            // Consulta MySQL.
            $query = "SELECT * FROM colegiaturas
            join alumnos on alumnos.idAlumno = colegiaturas.idAlumno
            join secciones s on s.idSeccion = alumnos.idSeccion
            join grados g on g.idGrado = alumnos.idGrado
            join grupos f on f.idGrupo = alumnos.idGrupo
            join periodospagos on periodospagos.idperiodoPago = colegiaturas.idPeriodoPago
            join tipooperaciones on tipooperaciones.idTipoOperacion = colegiaturas.tipodepago
            join entradassalidas on entradassalidas.idReferencia = colegiaturas.idReferencia
            WHERE colegiaturas.idAlumno = ? ORDER BY colegiaturas.idReferencia DESC LIMIT 1";

            $colegiaturas = DB::select($query,[$id]);
            $data['colegiaturas'] = $colegiaturas;
            return $data;
        }
        public function imprimir_reciboES($id)
    {          
          // Obtenemos los datos de la consulta de la función.
            $data = $this->imprimirES($id);

            // Verificamos que exista un arreglo.
            if(isset($data['entradassalidas'][0])) {
            $pdf = PDFS::loadView("sistema.caja.reciboES", $data);
             $pdf->setOption('page-height','290');
                $pdf->setOption('page-width','80');
                $pdf->setOption('margin-top',5);
                $pdf->setOption('margin-right',5);
                $pdf->setOption('margin-left',5);
                $pdf->setOption('margin-bottom',0);
                return $pdf->inline("reciboES.pdf");

            } else {
                return 'No se halló información suficiente para generar el reporte';
            }
       
    }
    
     // Obtenemos información de los alumnos en base su $id.
        function imprimirES($id) {

            // Consulta MySQL.
            $query = "SELECT entradassalidas.idFolio,entradassalidas.FechaOperacion,entradassalidas.tipoOperacionEfectivo,entradassalidas.Monto,entradassalidas.Cantidad,entradassalidas.CantidadRecibida,entradassalidas.CantidadEntregada,entradassalidas.Cambio,entradassalidas.idTipoOperacion,
entradassalidas.idReferencia,entradassalidas.idTipoPago,entradassalidas.PagadoA,entradassalidas.DescripcionConcepto,entradassalidas.RubroPresupuestal,entradassalidas.tipo_folio,entradassalidas.created_at,entradassalidas.updated_at,entradassalidas.deleted_at,
colegiaturas.idReferencia,colegiaturas.idAlumno,colegiaturas.tipo_operacion,colegiaturas.tipodepago,colegiaturas.idPeriodoPago,colegiaturas.colegiatura,colegiaturas.PorcentajeBeca,colegiaturas.descAdventista,colegiaturas.TotalApagar,colegiaturas.Pagado,colegiaturas.Prorroga,colegiaturas.FechaProrroga,
colegiaturas.RecargoPorPagoRetrasado,colegiaturas.comisionB,colegiaturas.fechadepago,colegiaturas.created_at,colegiaturas.updated_at,colegiaturas.deleted_at,
alumnos.idAlumno,alumnos.imagenPerfil,alumnos.Username,alumnos.Password,alumnos.extranjero,alumnos.Nombres,alumnos.ApellidoPaterno,alumnos.ApellidoMaterno,alumnos.FechaNacimiento,alumnos.CURP,alumnos.NIA,alumnos.idSeccion,alumnos.idGrado,alumnos.idGrupo,alumnos.idEstado,alumnos.idCiudad,
alumnos.CodigoPostal,alumnos.Colonia,alumnos.Calle,alumnos.NumeroExterior,alumnos.NumeroInterior,alumnos.Telefono,alumnos.id_religion,alumnos.Bautizado,alumnos.IglesiaAsistida,alumnos.Distrito,alumnos.Enfermedad,alumnos.DetalleEnfermedad,alumnos.alergias,alumnos.detalle_alergia,alumnos.ejercicio,
alumnos.detalle_ejercicio,alumnos.NombreContactoEmergencia,alumnos.TelefonoContactoEmergencia1,alumnos.TelefonoContactoEmergencia2,alumnos.id_seguro,alumnos.AfiliacionMedica,alumnos.EscuelaProcedencia,alumnos.RazonParaEstudiar,alumnos.FechaEntregaDocumento,alumnos.ActualizacionEntregaDocumento,alumnos.ActaNacimiento,
alumnos.CertificadoPrimaria,alumnos.CertificadoSecundaria,alumnos.DocumentoCURP,alumnos.CartaConducta,alumnos.Fotografias,alumnos.CertificadoPreescolar,alumnos.CartaTutor,alumnos.CopiaActa,alumnos.ExamenMedico,alumnos.CopiaExamen,alumnos.BoletaReporte,alumnos.Kardex,alumnos.BoletasPrimaria,alumnos.BoletasSecundaria,
alumnos.BoletasSecundariaOriginal,alumnos.BoletasSecundariaCopia,alumnos.ConstanciadeEER1,alumnos.ConstanciadeEER2,alumnos.ConstanciadeEER3,alumnos.ConstanciaEstudios,alumnos.Certificadoparcialestudio,alumnos.equivalenciaparcialestudio,alumnos.CURPPadre,alumnos.SolicitudInscripcion,alumnos.ActaNacimientoTutor,
alumnos.CredencialTutor,alumnos.TipoSangrePadre,alumnos.TipoSangrePadre,alumnos.NotaDocumentos,alumnos.Activo,alumnos.Saldo,alumnos.Abono,alumnos.BecaSEP,alumnos.cobro_recurrente,alumnos.EliminadoPor,alumnos.MotivoEliminar,alumnos.status_actual,alumnos.id_usuario,alumnos.created_at,alumnos.updated_at,
alumnos.deleted_at, s.idSeccion,s.Seccion,s.ClaveSEP,s.idColegio,g.idGrado,g.idSeccion,g.Grado,g.ordenCampos,f.idGrupo,f.idGrado,f.Grupo,f.id_ciclo,f.GrupoActual,f.ordenCampos,f.created_at,f.updated_at,f.deleted_at,periodospagos.idPeriodoPago,
periodospagos.idCosto,periodospagos.idPeriodoEscolar,periodospagos.FechaPago,periodospagos.PagoNum,periodospagos.TotalPagos,tipooperaciones.idTipoOperacion,tipooperaciones.Cuenta,tipooperaciones.Descripcion,tipooperaciones.U,tipooperaciones.N,tipooperaciones.RBR,tipooperaciones.AI,tipooperaciones.IA,tipooperaciones.BI,
tipooperaciones.CP,tipooperaciones.DED,tipooperaciones.Activo,tipooperaciones.DescripcionCorta
FROM entradassalidas  
            join colegiaturas on colegiaturas.idReferencia = entradassalidas.idReferencia
            join alumnos on alumnos.idAlumno = colegiaturas.idAlumno
            join secciones s on s.idSeccion = alumnos.idSeccion
            join grados g on g.idGrado = alumnos.idGrado
            join grupos f on f.idGrupo = alumnos.idGrupo
            join periodospagos on periodospagos.idperiodoPago = colegiaturas.idPeriodoPago
            join tipooperaciones on tipooperaciones.idTipoOperacion = colegiaturas.tipodepago
            WHERE entradassalidas.idReferencia = ? ORDER BY entradassalidas.idReferencia DESC LIMIT 1";

            $entradassalidas = DB::select($query,[$id]);
            $data['entradassalidas'] = $entradassalidas;
            return $data;

        }
        public function imprimir_recibosalidas($id)
    {          
          // Obtenemos los datos de la consulta de la función.
            $data = $this->imprimirsalidas($id);

            // Verificamos que exista un arreglo.
            if(isset($data['entradassalidas'][0])) {
            $pdf = PDFS::loadView("sistema.caja.recibosalidas", $data)->setOrientation('portrait');
                return $pdf->inline("recibosalidas.pdf");

            } else {
                return 'No se halló información suficiente para generar el reporte';
            }
       
    }
    
     // Obtenemos información de los alumnos en base su $id.
        function imprimirsalidas($id) {

            // Consulta MySQL.
            $query = "SELECT * FROM entradassalidas
            
            join colegiaturas on colegiaturas.idReferencia = entradassalidas.idReferencia
             join alumnos on alumnos.idAlumno = colegiaturas.idAlumno
             join periodospagos on periodospagos.idperiodoPago = colegiaturas.idPeriodoPago
            join tipooperaciones on tipooperaciones.idTipoOperacion = colegiaturas.tipodepago
            WHERE entradassalidas.idReferencia = ? ORDER BY entradassalidas.idReferencia DESC LIMIT 1";

            $entradassalidas = DB::select($query,[$id]);
            $data['entradassalidas'] = $entradassalidas;
            return $data;

        }


}
