<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Prorrogas;
use DB;

class ListaProrrogasCajaController extends Controller
{
    public function index() {

    	$prorrogas = Prorrogas::orderBy('idProrroga', 'ASC')->paginate(10);
        $prorrogas->each(function($prorrogas) {
            $prorrogas->alumnos;
        });

    	return view ('sistema/caja/listado-prorrogas', ['prorrogas' => $prorrogas]);
    }
}
