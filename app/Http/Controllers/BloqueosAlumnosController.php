<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Alumnos;
use DB;

class BloqueosAlumnosController extends Controller
{
    public function index(Request $request) {
    	$alumnos = Alumnos::whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ?",["%".$request->nameAlumnos."%"])->orderBy('idAlumno', 'ASC')->get();

    	return view ('SIAO/bloqueos-alumnos', ['alumnos' => $alumnos]);
    }
}