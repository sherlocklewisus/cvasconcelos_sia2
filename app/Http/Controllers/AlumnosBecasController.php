<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Alumnos;
use DB;

class AlumnosBecasController extends Controller
{
    public function index(Request $request) {
    	$alumnos = Alumnos::whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ?",["%".$request->nameAlumnos."%"])->orderBy('idAlumno', 'DESC')->where('Beca', '=', 1)->paginate(10);
    	$alumnos->each(function($alumnos) {
            $alumnos->seccion;
            $alumnos->grado;
            $alumnos->grupo;
        });

    	return view ('SIAO/becas-alumnos', ['alumnos' => $alumnos]);
    }
}
