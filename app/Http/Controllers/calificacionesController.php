<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SaveCalificacionesRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
Use App\periodosevaluacion;
use App\Materias;
use App\Alumnos;
use App\Calificaciones;
use App\CiclosEscolares;
use Carbon\Carbon;
use DB;
use Exception;
use App\GruposMaterias;

class calificacionesController extends Controller
{

     use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $materias = DB::table('materias')
            ->select(
                'materias.idMateria',
                'Nombre',
                'idColaborador',
                'idSeccion',
                'idGrado',
                'idGrupo',
                'deleted_at'
            )
            ->where('idColaborador','=',Auth::user()->colaborador->idColaborador)
            ->get();

            $seccion = DB::table('secciones')
            ->select(
              'secciones.idSeccion',
              'Seccion'
              )->get();
              $grado = DB::table('grados')
              ->select('grados.idGrado','Grado')->get();
              $grupo = DB::table('grupos')
              ->select('grupos.idGrupo','Grupo')->get();
        return view('SIAO/calificacionesfn',['materias' => $materias,'seccion' => $seccion,'grado' => $grado,'grupo' => $grupo]);
        }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->seccioncalifica;
      $date = Carbon::now();
      $dateF= $date->format('d/m/Y');

      $fechaEV = DB::table('periodosevaluacion')
      ->select('periodosevaluacion.idPeriodoEvaluacion','idSeccion',
      'FechaEvaluacionInicial','FechaEvaluacionFinal')
      ->where('idSeccion','=',$request->seccioncalifica)
      ->where('FechaEvaluacionInicial','<=',$dateF)
      ->where('FechaEvaluacionFinal','>=',$dateF)
      ->get();
      //dd($fechaEV);
      //return ($fechaEV);

      if(count($fechaEV) == 0) {
        //dd('No puede agregar nada');
      } else {
        //dd('Puede agregar');
        $this->validate($request, [
          "seccioncalifica"=>"required",
        ]);
        //var_dump($request->Calificacion);
        $i = 0;
        dd($request->Alumnos);
        foreach($request->Calificacion as $cal ) {
          $calificaciones = new Calificaciones();
          $calificaciones->idMateria = $request->Materia;
          $calificaciones->idAlumno = $request->Alumnos[$i];
          $calificaciones->idPeriodoEvaluacion = $request->seccioncalifica;
          $calificaciones->CalificacionFinal = $request->cal;
          $rere = $calificaciones->save();
          $i=$i+1;

          if(!$rere) {
            dd('Todo mal');
          } else {
            dd('Todo OK');
          }
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPeriodo(Request $request){
        $vr = periodosevaluacion::join('materias','materias.idMateria','=','periodosevaluacion.idMateria')
        ->whereRaw("periodosevaluacion.idMateria like ?", ["%".$request ->nombre."%"])->get();
        return Response()->json($vr);
    }

    public function getAlumnos(Request $request)
    {
      $alumno = Alumnos::join('secciones','secciones.idSeccion','=','alumnos.idSeccion')
          ->join('grados','grados.idGrado','=','alumnos.idGrado')
          ->join('grupos','grupos.idGrupo','=','alumnos.idGrupo')
      ->whereRaw('alumnos.idSeccion = ? and alumnos.idGrado = ? and alumnos.idGrupo = ? ',[$request->NSeccion,$request->NGrado,$request->NGrupo])
      ->get();
         return Response()->json($alumno);

    }

    /* 
      vista calificacion alumnos
    */
      public function vista_calificar_alumnos($id_materia_grupo)
      {
          /*$datos_grupo = DB::table("grupos_materias as x1")
                              ->join("grupos as x2","x1.id_grupo","=","x2.idGrupo")
                              ->join("materias as x3","x1.id_materia","=","x3.idMateria")
                              ->whereRaw("x1.id = ?",[$id_materia_grupo])
                              ->first();

          $alumnos = DB::table("materia_profesor_grupo as x1")
            ->select("x4.idAlumno","x4.Nombres","x4.ApellidoPaterno","x4.ApellidoMaterno","x5.Nombre as materia")
               ->join("grupos_materias as x2","x1.id_materia_grupo","=","x2.id")
               ->join("grupos as x3","x2.id_grupo","=","x3.idGrupo")
               ->join("alumnos as x4","x3.idGrupo","=","x4.idGrupo")
               ->join("materias as x5","x2.id_materia","=","x5.idMateria")
               ->whereRaw("x2.id = ?",[$id_materia_grupo])
               ->orderBy("x4.ApellidoPaterno")
               ->get();

          //dd($alumnos);*/
          $materias_grupo = GruposMaterias::find($id_materia_grupo);
          $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
          $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)->get();

          //$periodosEvaluacion = periodosevaluacion::where("idSeccion","=",$materias_grupo->grupo->grado->seccion->idSeccion)->get();
          //dd($periodosEvaluacion);
          //dd($materias_grupo->grupo->grado->seccion->idSeccion);

          return view("sistema.profesores.calificar_alumnos",compact("periodosEvaluacion","materias_grupo"));
      }

      public function calificar_alumnos(SaveCalificacionesRequest $request)
      {

          /*$request->alumnos;
          $request->calificaciones;*/
          //dd($request);
          $mensaje_tipo = "";
          $mensaje_texto = "";
          $status = 200;
          $i=0;
          
          try
          {
            foreach ($request->alumnos as $alumno)
            {
          
              foreach($request->idPeriodosEvalion as $idPeriodo)
              {
                $per = PeriodosEvaluacion::find($idPeriodo);

                /*
                  validamos que el periodo este en fecha para ser calificado
                  o tenga el campo calificar en true para calificarlo
                  fuera de la fecha normal
                */

                if($per->FechaEvaluacionInicial < Carbon::now() && $per->FechaEvaluacionFinal > Carbon::now() || $per->ActivarHastaFecha != null && $per->ActivarHastaFecha >= Carbon::now())
                {
                   //nothin
                }
                else
                {
                  continue;
                }

                $al = Alumnos::find($alumno);
                $calificacion = Calificaciones::firstOrNew([
                    "idAlumno"=>$alumno,
                    /*"CalificacionFinal"=>$request->CalificacionParcial[$alumno][$idPeriodo],*/
                    "idMateria"=>$request->idMateria,
                    "idPeriodoEvaluacion"=>$idPeriodo,
                    "id_ciclo"=>$per->idCicloEscolar
                  ]);
                //dd($cal);
                $calificacion->id_ciclo = $per->idCicloEscolar;
                $calificacion->idGrupo = $al->idGrupo;
                $calificacion->CalificacionFinal = $request->CalificacionParcial[$alumno][$idPeriodo];
                $cal = $calificacion->save();
                //dd($calificacion);
                /*
                $calificacion = new Calificaciones();
                $calificacion->idAlumno = $alumno;
                $calificacion->CalificacionFinal = $request->CalificacionParcial[$alumno][$idPeriodo];
                $calificacion->idMateria = $request->idMateria;
                $calificacion->idPeriodoEvaluacion = $idPeriodo;
                $cal = $calificacion->save();
                $i=$i+1;
                */
                if(!$cal)
                {
                  throw new Exception("No se pudo calificar el alumno");
                }
              }

              $mensaje_tipo = "mensaje-success";
              $mensaje_texto = "La calificacion se agrego con exito!";
              $status = 200;
            }
          }
          catch(Exception $ex)
          {
              $mensaje_tipo = "mensaje-danger";
              $mensaje_texto = "No se pudieron agregar las calificaciones";
              $status = 500;
          }

          if(!$request->ajax())
          {
              Session::flash($mensaje_tipo,$mensaje_texto);
              return redirect("vista_calificar_alumnos/".$request->id_materia_grupo);
          }

          return Response()->json(["mensaje"=>$mensaje_texto],$status);
      }
}
