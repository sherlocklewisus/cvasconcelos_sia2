<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Tutores;
use App\tutoresalumnos;
use App\Parentescos;
use App\Alumnos;
use DB;

class insAlumTutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $tutores = Tutores::get();
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $alumnos = Alumnos::get();
        $alumnos->each(function($alumnos) {
            $alumnos->seccion;
            $alumnos->grado;
            $alumnos->grupo;
        });

        $parentesco = DB::table('parentescos')
            ->select('parentescos.idParentescos',
            'Parentesco')
            ->where('idParentescos','<', 8)
            ->get();
            $tutores=Tutores::whereRaw("tutores.Nombres LIKE ?",["%".$request->Nombre."%"])->paginate(15);

      return view('sistema/colaboradores/relAlumTuto',['parentesco' => $parentesco, 'tutores' => $tutores, 'alumnos' => $alumnos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'unique' => 'ERROR!! El :attribute ya tiene un tutor asignado'
        ];
      
        $this->validate($request, [
                    "Alumno"=>"required|unique:tutoresalumnos,idAlumno",
                    "parentesco"=>"required",
                    "Tutor"=>"required"
        ],$messages);
    
        if($request->parentesco > 7)
        {
            $parentesco = new Parentescos();
            $parentesco->Parentesco = $request->detalle_p;
            $parentesco->save();
        }
        if (count($request->Alumno) > 0 ) {
            foreach ($request->Alumno as $Al) {
            $tutoal = new tutoresalumnos();
            $tutoal->idAlumno = $Al;
            $tutoal->idParentesco = $request->parentesco;
            $tutoal->idTutor = $request->Tutor;
            $tutoal ->save();
        }
        }else{

        }

        if(!$tutoal) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "Se registro correctamente";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "Registro con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("relAlumTuto?value=7");
            }

        return Response()->json(["mensaje"=>"Registro con exito!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTutores(Request $request){
        $var = Tutores::whereRaw("concat(Nombres,Apellidos) like ?", ["%".$request ->nombre."%"])->get();
        return Response()->json($var);
    }
    public function getAlumnorel(Request $request){
        $var = Alumnos::whereRaw("concat(Nombres,ApellidoPaterno,ApellidoMaterno) like ?", ["%".$request ->NombreAlumno."%"])->get();
                $var->each(function($var) {
            $var->seccion;
            $var->grado;
            $var->grupo;
        });
        return Response()->json($var);
    }
}

