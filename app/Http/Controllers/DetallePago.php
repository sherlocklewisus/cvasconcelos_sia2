<?php


namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Colegiaturas;
use App\Alumnos;
use App\CostoPagos;
use App\CostoColegiaturas;
use App\PeriodosPagos;
use App\periodosescolares;
use DateTime;
use DB;
use App\Secciones;
use PDFS;

class DetallePago extends Controller
{
    


    public function pagos_detalle($id){
        //Consulta de Alumnos
        $alumnos = Alumnos::where('idAlumno', '=', $id)
        ->join("secciones as x1","alumnos.idSeccion","=","x1.idSeccion")
        ->join("grados as x2","alumnos.idGrado","=","x2.idGrado")
        ->join("grupos as x3","alumnos.idGrupo","=","x3.idGrupo")
        ->join("periodosescolares as x4","alumnos.idSeccion","=","x4.idSeccion")
        ->groupBy('idAlumno')
        ->get();

        //Obtien el alumno
        $alumno = Alumnos::where('idAlumno',$id)->first();
        
        //Guarda la seccion del Alumno en una variable
        $seccion = $alumno->idSeccion;

        //Guardamos el valor del id del alumno en una variable
        $idAlu = $alumno->idAlumno;

        //Obtenmos el periodo escolar del Alumno
        $periodo = periodosescolares::where('idSeccion',$seccion)->first();

        //GUarda el id del Periodo en una variable
        $idperiodo = $periodo->idPeriodo;

        //Obtenemos un periodo del alumno
        $periodo = periodospagos::where('idPeriodoEscolar',$idperiodo)->first();


        //Guardamos el total de Pagos en una variable
        $totalpagos= $periodo->TotalPagos;

       
        $fechas2 = Colegiaturas::where('colegiaturas.idAlumno','=',$idAlu)
        ->join("periodospagos","periodospagos.idPeriodoPago","=","colegiaturas.idPeriodoPago")
        ->join("costo_colegiaturas","costo_colegiaturas.id_costo_colegiatura","=","periodospagos.idCosto")
        ->first();
        if ($fechas2 != null || $fechas2 != '') {
            $numpagos = $fechas2->total_pagos;    # code...
              //Obtenemos todas las fechas del alumno
                if ($numpagos != '' || $numpagos!= null) 
                {
                   $fechas = periodospagos::where('idPeriodoEscolar',$idperiodo)
                    ->where('TotalPagos', $numpagos)
                    ->get();

                    $inscripcion = Colegiaturas::where( 'idAlumno',$idAlu)
                    ->whereIn('colegiaturas.tipodepago',["179","180","181","182"])
                    ->get();
                     // dd($inscripcion);
                   
                }
                
        }
        else
        {
             $fechas = periodospagos::where('totalpagos',$totalpagos)
                 ->where('idPeriodoEscolar',$idperiodo)
                ->get();
                $inscripcion = Colegiaturas::where( 'idAlumno',$idAlu)
                    ->whereIn('colegiaturas.tipodepago',["179","180","181","182"])
                    ->get();   
        }
        


        //Join de PeriodosPago con Colegiatura
        $colegiaturas = Colegiaturas::where('idAlumno',$idAlu)
        ->get();

        

        /******* CONSULTAS DEL CONTROLADOR **********/


                //obtengo la seccion del alumno seleccionado
        $plucked = $alumnos->pluck('idSeccion');

        //obtener periodoescolar del alumno
        $periodosescolares = periodosescolares::whereIn('idSeccion', $plucked)->where('Activo', '=', 1)
        ->pluck('idPeriodo');

        //obtener periodospago del alumno           
        $periodospagos = periodospagos::whereIn('idPeriodoEscolar', $periodosescolares)->get();
        $periodospagos2 = periodospagos::whereIn('idPeriodoEscolar', $periodosescolares)->pluck('idPeriodoPago');

        //obtener colegiaturas ya pagadas del alumno    
        $colegiaturas = colegiaturas::where('idAlumno', '=', $id)->whereIn('idPeriodoPago', $periodospagos2)->get();
        $colegiaturas2 = colegiaturas::where('idAlumno', '=', $id)->whereIn('idPeriodoPago', $periodospagos2)->pluck('idPeriodoPago');
            
        $periodospagos3 = periodospagos::whereNotIn('idPeriodoPago', $colegiaturas2)->get();
        
         // dd($colegiaturas2);
       /************************************ */
        
        return view ('sistema/caja/detalle_pago_alumno', ['alumnos' => $alumnos,'fechas' =>$fechas,'pagos'=> $fechas2,'colegiaturas'=>$colegiaturas,'periodospagos3'=>$periodospagos3,'inscripcion'=>$inscripcion]);
    }
}
