<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\periodosevaluacion;
use App\CiclosEscolares;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use Carbon\Carbon;
use Exception;
use App\GruposMaterias;
use App\MateriasReprobadas;
use App\Materias;
use App\Alumnos;
use App\Secciones;
use App\Calificaciones;
use App\periodosescolares;
use Auth;
use DB;

class ReprobadasMateriaController extends Controller
{
    public function index($seccion,$grado) {    

    	$materias =DB::select('SELECT * FROM `materias` WHERE `idSeccion`= ? AND `idGrado`= ?', [$seccion,$grado]);

        $periodosevaluacion = DB::select('SELECT * FROM `periodosevaluacion` WHERE `idSeccion` = ?',[$seccion]);

        $materiascalifiacion = DB::select('SELECT * FROM calificaciones INNER JOIN materias On calificaciones.idMateria = materias.idMateria');

        
 
        
        return view('sistema/colaboradores/materias-repro-materia', ['materias' => $materias,'periodosevaluacion' => $periodosevaluacion ,'materiascalifiacion' =>$materiascalifiacion ]);
    }
}
