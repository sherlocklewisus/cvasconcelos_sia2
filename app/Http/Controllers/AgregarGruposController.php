<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GruposCreateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use App\Secciones;
use App\Grupos;
use App\Grados;
use App\Alumnos;
use DB;
use Carbon\Carbon;
use Exception;
use App\user;
use App\Colaboradores;
use Auth;

class AgregarGruposController extends Controller
{
	public function index() {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }
          }
          else{
            $secciones = Secciones::get();

          }
    	return view("sistema.colaboradores.formulario-grupos",compact("secciones"));
	}

    public function getSeccion(Request $request, $id) {
        if($request->ajax()){
            $seccion = Grados::gradosNivel($id);
            return response()->json($seccion);
        }
    }

    public function getGrados(Request $request, $id) {
        if($request->ajax()){
            $grados = Grupos::grupoGrado($id);
            return response()->json($grados);
        }
    }

	public function show($seccion) {
        $secciones = Secciones::whereRaw("Seccion = ?",[$seccion])->paginate(5);
        return view("sistema.colaboradores.listado-grupos",compact("secciones"));
    }

    public function getGrupos($id_grado)
    {
    	$grupos = Grupos::whereRaw("idGrado = ?",[$id_grado])->get();
    	return view("sistema.grupos.getGrupos",compact("grupos"));
    }

    public function store(GruposCreateRequest $request) {
    	$grupo = new Grupos();
    	$grupo->idGrado = $request->idGrado;
    	$grupo->grupo = $request->grupo;
    	$group = $grupo->save();

    	if(!$group) {
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
            $status = 500; //Internal Error
        } else {
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El grupo se ha guardado correctamente";
            $status = 200; //ok
        }

        
        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("agregar-grupos");
        }
        
        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

    public function getGruposJson($id_grado)
    {
    	$grupos = Grupos::whereRaw("idGrado = ?",[$id_grado])->orderBy("grupo","ASC")->get();
    	return Response()->json($grupos);
    }

    public function admin_grupos()
    {
        $secciones = Secciones::all()->pluck("Seccion","idSeccion");
        $alumnos = Alumnos::all();
        return view("sistema.grupos.admin_grupos",compact("alumnos","secciones"));
    }

    public function changeAlumnosGrupo(Request $request)
    {
        /*$user = DB::table("users")->whereDate("created_at",Carbon::now())->toSql();
        dd($user);*/
        $mensaje_text = "ok";
        $status = 200;
        try
        {
            Log::info("verificando total alumnos");
            if(count($request->alumnos) > 0)
            {
                DB::beginTransaction();
                    Log::info("Iniciando transaccion");
                    $i=0;
                    $cad = "";
                    foreach ($request->alumnos as $id_alumno) 
                    {
                        Log::info("ide de alumno: ..".$id_alumno);
                        $alumno = Alumnos::findOrFail($id_alumno);
                        /*$alumno->idSeccion = $request->idSeccion;
                        $alumno->idGrado = $request->idGrado;*/
                        $alumno->idGrupo = $request->idNewGrupo;
                        //$alumno->idGrupo = 2;
                        $al = $alumno->save();
                        $i=$i+1;
                        $cad.=$alumno->Nombres.", ";
                        Log::info("ide de alumno: ".$id_alumno);
                        //$al=false;
                        if(!$al)
                        {
                            throw new Exception("¡Ups! Parece que ocurrio un error, intentelo de nuevo.");  
                        }
                    }

                DB::commit();
                if($i == 1)
                {
                    $mensaje_text = "El alumno ".$cad." se cambio de grupo con exito!";
                }
                else
                {
                    $mensaje_text = "Los alumnos ".$cad." se cambiaron de grupo con exito!";
                }
                $status = 200;
            }
            else
            {
                $mensaje_text = "debes seleccionar al menos un alumno";
                $status = 500;
            }
        }
        catch(Exception $ex)
        {
            DB::rollback();
                $mensaje_text = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
                $status = 500;
                Log::info("aplicando rollback");
        }

        return Response()->json(["mensaje"=>$mensaje_text],$status);
    }

    public function destroy($id)
    {
        $mensaje_text = "";
        $status = 200;//ok

        $total_alumnos_grupo = DB::table("grupos as x1")
            ->join("alumnos as x2","x1.idGrupo","=","x2.idGrupo")
            ->where("x2.idGrupo","=",$id)
            ->count();

        if($total_alumnos_grupo == 0)
        {
            $grupo = Grupos::findOrFail($id);
            if($grupo->forceDelete())
            {
                $mensaje_text = "¡En hora buena! El grupo se ha eliminado correctamente.";
                $status = 200;
            }
            else
            {
                $mensaje_text = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
                $status = 500;
            }
          
        }
        else
        {
            $mensaje_text = "El grupo no se puede eliminar porque tiene inscritos ".$total_alumnos_grupo." alumnos , por favor  verifica que el grupo no tenga alumnos";
            $status = 500;
        }

         return Response()->json(["mensaje"=>$mensaje_text],$status);

    }


    /*
        autor:Omar Garcia
        descripcion: funcion para asignar alumnos a un grupo
        method: post
        return: response
    */
    public function save_alumnos_grupos(Request $request)
    {
        dd($request->alumnos);
    }

}
