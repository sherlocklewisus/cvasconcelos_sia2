<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Requests\AlumnosRequest;
use Illuminate\Support\ServiceProvider;

use App\Alumnos;
use App\Municipios;
use App\User;
use App\Secciones;
use App\Grados;
use App\Grupos;
use DB;

class SavealumnosController extends Controller {
    public function index() {
	    $estados = DB::table('estados')
	       	->select(
	            'estados.idEstados', 
	            'Estado'
	        )
	       	->get();

	    $secciones = DB::table('secciones')
	       	->select(
	            'secciones.idSeccion', 
	            'Seccion'
	        )
	       	->get();

	    //dd($listTutores);
	    //return $listTutores;

	    return view('SIAO/agregar-alumnos', ['estados' => $estados, 'secciones' => $secciones]);
    }

    public function store(AlumnosRequest $request) {
        DB::beginTransaction();

            $user = new User();
            $user->name = $request->Email;
            $user ->email = $request->Email;
            $user ->password= bcrypt ($request['Email']);

            $user ->typeUser = 'Alumnos';
            $us = $user ->save();

            // ---------------------------------------------------->

            $alumno =  new Alumnos();
            $alumno->imagenPerfil = $request->imagenPerfil;
            dd($request->imagenPerfil);
            $alumno->Username = $request->Email;
            $alumno->Password = bcrypt ($request['Email']);
            $alumno->Nombres = $request->Nombres;
            $alumno->ApellidoPaterno = $request->ApellidoPaterno;
            $alumno->ApellidoMaterno = $request->ApellidoMaterno;
            $alumno->FechaNacimiento = $request->FechaNacimiento;
            $alumno->CURP = $request->CURP;
            $alumno->idSeccion = $request->idSeccion;
            $alumno->idGrado = $request->idGrado;
            $alumno->idGrupo = $request->idGrupo;
            $alumno->idEstado = $request->idEstado;
            $alumno->idCiudad = $request->idCiudad;
            $alumno->CodigoPostal = $request->CodigoPostal;
            $alumno->Colonia = $request->Colonia;
            $alumno->Calle = $request->Calle;
            $alumno->NumeroExterior = $request->NumeroExterior;
            $alumno->NumeroInterior = $request->NumeroInterior;
            $alumno->Telefono = $request->Telefono;
            $alumno->Religion = $request->Religion;
            $alumno->Bautizado = $request->Bautizado;
            $alumno->IglesiaAsistida = $request->IglesiaAsistida;
            $alumno->Distrito = $request->Distrito;
            $alumno->Enfermedad = $request->Enfermedad;
            $alumno->DetalleEnfermedad = $request->DetalleEnfermedad;
            $alumno->NombreContactoEmergencia = $request->NombreContactoEmergencia;
            $alumno->TelefonoContactoEmergencia1 = $request->TelefonoContactoEmergencia1;
            $alumno->TelefonoContactoEmergencia2 = $request->TelefonoContactoEmergencia2;
            $alumno->AfiliacionMedica = $request->AfiliacionMedica;
            $alumno->EscuelaProcedencia = $request->EscuelaProcedencia;
            $alumno->RazonParaEstudiar = $request->RazonParaEstudiar;
            $alumno->FechaEntregaDocumento = $request->FechaEntregaDocumento;
            $alumno->ActualizacionEntregaDocumento = $request->ActualizacionEntregaDocumento;
            $alumno->ActaNacimiento = $request->ActaNacimiento;
            $alumno->CertificadoPrimaria = $request->CertificadoPrimaria;
            $alumno->CertificadoSecundaria = $request->CertificadoSecundaria;
            $alumno->DocumentoCURP = $request->DocumentoCURP;
            $alumno->CartaConducta = $request->CartaConducta;
            $alumno->Fotografias = $request->Fotografias;
            $alumno->BoletasPrimaria = $request->BoletasPrimaria;
            $alumno->BoletasSecundaria = $request->BoletasSecundaria;
            $alumno->BoletasSecundariaOriginal = $request->BoletasSecundariaOriginal;
            $alumno->BoletasSecundariaCopia = $request->BoletasSecundariaCopia;
            $alumno->ConstanciadeEER1 = $request->ConstanciadeEER1;
            $alumno->ConstanciadeEER2 = $request->ConstanciadeEER2;
            $alumno->ConstanciadeEER3 = $request->ConstanciadeEER3;
            $alumno->ConstanciaEstudios = $request->ConstanciaEstudios;
            $alumno->SolicitudInscripcion = $request->SolicitudInscripcion;
            $alumno->ActaNacimientoTutor = $request->ActaNacimientoTutor;
            $alumno->CredencialTutor = $request->CredencialTutor;
            $alumno->NotaDocumentos = $request->NotaDocumentos;
            $alumno->Activo = 1;
            $alumno->Saldo = $request->Saldo;
            $alumno->Abono = $request->Abono;
            $alumno->Beca = $request->Beca;
            $alumno->PorcentajeBeca = $request->PorcentajeBeca;
            $alumno->id_usuario = $user->id;
            $tu = $alumno->save();

            if(!$tu || !$us) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El Alumno no se pudo registrar";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El Alumno se registro con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("agregar-alumnos");
            }

        return Response()->json(["mensaje"=>"El Alumno se registro con exito!"]);
    }
}
