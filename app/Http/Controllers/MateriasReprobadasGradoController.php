<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\periodosevaluacion;
use App\CiclosEscolares;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use Carbon\Carbon;
use DB;
use Exception;
use App\GruposMaterias;
use App\MateriasReprobadas;
use App\Materias;
use App\Alumnos;
use App\Secciones;
use App\periodosescolares;

class MateriasReprobadasGradoController extends Controller
{
    //
    public function index(Request $request) {



        // Estructura de control -> La sentencia de busqueda contiene todos solo dos de los parametros.
         if ($request->nivel != "" && $request->grado != "") {
          $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where([
            ['idSeccion', '=', $request->nivel],
            ['idGrado', '=', $request->grado]
          ])->orderBy("idAlumno","DESC")->paginate(25);

          $estudiantes->each(function($estudiantes) {
              $estudiantes->seccion;
              $estudiantes->grado;
              $estudiantes->grupo;
          });

        // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
        } else {
          $estudiantes = Alumnos::orderBy('idAlumno', 'DESC')->where('idSeccion', '=', $request->nivel)->orderBy("idAlumno","DESC")->paginate(25);

          $estudiantes->each(function($estudiantes) {
              $estudiantes->seccion;
              $estudiantes->grado;
              $estudiantes->grupo;
          });
        }

         $alumnos= DB::table('alumnos')
          ->select(
            'alumnos.idAlumno','Nombres','ApellidoPaterno','ApellidoMaterno','idSeccion', 'idGrado')->get();
        // Obtenemos la fecha actual del servidor.
        $fecha = date('Y-m-d');

        // Variables de consultas a BD.
        
        $secciones = Secciones::get();
        $periodosevaluacion = DB::table('periodosevaluacion')
            ->select('periodosevaluacion.idPeriodoEvaluacion','FechaEvaluacionInicial','FechaEvaluacionFinal', 'Descripcion')->get(); 
        $periodoescolar = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', 4]])->get();

        // Estructura de control -> Busqueda de ciclos por medio de las secciones elegidas
        $nivelado = isset($_GET['nivel'])? $_GET['nivel'] : "";
        if($nivelado == "" || $nivelado == null) {
          $ciclosescolares = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', 0]])->get();
        } else {
          $ciclosescolares = periodosescolares::where([['FechaFin', '<', $fecha],['Activo', '=', 1],['idSeccion', '=', $nivelado]])->get();
        }

        return view ('sistema/colaboradores/materias-reprobadas-grado', ['estudiantes' => $estudiantes, 'secciones' => $secciones, 'ciclosescolares' => $ciclosescolares, 'periodoescolar' => $periodoescolar, 'periodosevaluacion' => $periodosevaluacion,'alumnos'=>$alumnos]);
    }
    public function reprobados_grado($seccion,$grado){ 
      // Estructura de control -> La sentencia de busqueda contiene todos solo dos de los parametros.
         
          $estudiantes = Alumnos::orderBy('alumnos.idAlumno', 'DESC')
          //->join("calificaciones","alumnos.idAlumno","=","calificaciones.idAlumno")
          ->where([
            ['idSeccion', '=', $seccion],
            ['idGrado', '=', $grado]
          ])
          ->orderBy("alumnos.idAlumno","DESC")->get();

          $periodosevaluacion = periodosevaluacion::where('idSeccion',$seccion)
          ->get();

         $calificaciones =DB::select('SELECT * FROM `alumnos` INNER JOIN calificaciones ON alumnos.idAlumno = calificaciones.idAlumno WHERE alumnos.idSeccion = ? AND alumnos.idGrado = ?', [$seccion,$grado]);

          
          

        return view ('sistema/colaboradores/materias-reprobadas-grado', ['estudiantes' => $estudiantes,'periodosevaluacion' => $periodosevaluacion,'calificaciones' => $calificaciones]);
    }

    public function store(Request $request)
    {
      DB::beginTransaction();
      $i=0;
      $mala = new MateriasReprobadas;
      $mala->idAlumno = $request->idAlumno[$i];
      $mala->idCiclo = $request->idPeriodosEvaluacion[$i];
      $mala->idSeccion = $request->idSeccion;
      $mala->idGrado = $request->idGrado; 
      $mala->MatRepXCiclo = $request->suma;
      $mala->TotalMatRep = $request->sumaFinal;
      $final = $mala->save();
        $i=$i+1;

      if(!$final)
          {
            DB::rollback();
            $tipo_mensaje="mensaje-danger";
            $texto_mensaje="Parece que ocurrio un error, intentelo de nuevo";
          }
          else
          {
            DB::commit();
            $tipo_mensaje="mensaje-success";
            $texto_mensaje="¡En hora buena! Se han guardado correctamente los datos";
          }

      if(!$request->ajax())
        {
          Session::flash($tipo_mensaje,$texto_mensaje);
          return redirect("materias-reprobadas-grado?seccion={$request->idSeccion}&grado={$request->idGrado}");
        }    

      return Response()->json(["mensaje"=>"perfect eres crack"]);   

    }
}
