<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\HorarioEst;
use DB;

class VerHorariosController extends Controller
{
    public function index() {
        $horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios) {
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });

        $estructura = DB::table('horarios_estructura')
        ->select(
            'horarios_estructura.idHorario',
            'periodo_escolar',
            'nivel_educativo',
            'horaInicio',
            'horaFinal',
            'receso',
            'comentario'
        )
        ->orderBy('idHorario', 'ASC')
        ->get();

    	$materias = DB::table('materias')
    	->select(
    		'materias.idMateria',
    		'Nombre',
    		'idSeccion',
    		'idGrado',
    		'idGrupo'
    	)
    	->orderBy('idMateria', 'ASC')
    	->get();

        $periodos = DB::table('periodosescolares')
        ->select(
            'periodosescolares.idPeriodo',
            'Periodo'
        )
        ->orderBy('idPeriodo', 'ASC')
        ->get();

        $secciones = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->orderBy('idSeccion', 'ASC')
        ->get();

        $grados = DB::table('grados')
        ->select(
            'grados.idGrado',
            'Grado'
        )
        ->orderBy('idGrado', 'ASC')
        ->get();

        $grupos = DB::table('grupos')
        ->select(
            'grupos.idGrupo',
            'Grupo'
        )
        ->orderBy('idGrupo', 'ASC')
        ->get();


    	return view ('sistema/colaboradores/vista-horarios', ['horarios' => $horarios, 'estructura' => $estructura, 'materias' => $materias, 'periodos' => $periodos, 'secciones' => $secciones, 'grados' => $grados, 'grupos' => $grupos]);
    }
}
