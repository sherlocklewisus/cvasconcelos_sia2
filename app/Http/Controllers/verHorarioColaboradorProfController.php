<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use App\Http\Requests\ActividadRequest;
use App\Horarios;
use App\HorarioEst;
use App\periodosescolares;
use App\Grados;
use App\Grupos;
use App\GruposMaterias;
use App\actividadesextra;
use DB;


class verHorarioColaboradorProfController extends Controller
{
    //
	 public function index() {   
            return view('sistema/colaboradores/ver-horario-colaborador-prof'); }



    public function store(ActividadRequest $request)
    {
    //     $this->validate($request,[
    //         "id"=>"required",
    //         "actividad"=>"required",
    //         "FechaInicio"=>"required",
    //         "detalles"=>"required"
    //     ]);
    //     $tipo_mensaje="mensaje-succes";
    //     $texto_mensaje="";

        DB::beginTransaction();

        $actividad = new actividadesextra();
        $actividad->idProfesor=$request->id;
        $actividad->Nombre=$request->actividad;
        $actividad->Fecha=$request->FechaInicio;
        $actividad->Detalles=$request->detalles;
        
        $final=$actividad->save();
        if(!$final)
            {
                DB::rollback();
                $tipo_mensaje="mensaje-danger";
                $texto_mensaje= "Parece que ocurrio un error, intentelo de nuevo";
            }
            else
            {
                DB::commit();
                $tipo_mensaje="mensaje-success";
                $texto_mensaje="¡En hora buena! Se ha guardado correctamente la actividad";
            }
        if(!$request->ajax())
            {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("ver-horario-colaborador-prof?periodo={$request->per}&seccion={$request->sec}&id2={$request->id}&nombre2={$request->nombre}&apellido2={$request->apellido}");
            }
        return Response()->json(["mensaje"=>"¡En hora buena! se ha guardado correctamente la actividad]"]);
    }
 
}
