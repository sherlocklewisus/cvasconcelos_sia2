<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\HorarioEst;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use App\ForosLista;
use App\Tareas;
use App\Mensajes;

use DB;
use Auth;

class AdministradorProfesorController extends Controller
{
    public function index() {
        $horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios){
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });

        $estructura = Horarios::orderBy('idHorario', 'ASC')->get();
        $mensajesRecibidos = Mensajes::orderBy('idMensaje', 'ASC')->get();
        $profesor = Auth::user()->colaborador->idColaborador;
        $materiaprofesor = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
        $materiasrelacion = GruposMaterias::whereIn('id', $materiaprofesor)->get();
        $foros = ForosLista::where('idprofesor', '=', $profesor)->get();
        $materiasrelacionx2 = GruposMaterias::whereIn('id', $materiaprofesor)->pluck('id');
        $tareas = Tareas::whereIn('id_materia_grupo', $materiasrelacionx2)->get();

    	return view('sistema/profesores/admin-profesor', ['horarios' => $horarios, 'estructura' => $estructura, 'mensajesRecibidos' => $mensajesRecibidos, 'materiasrelacion' => $materiasrelacion, 'foros' => $foros, 'tareas' => $tareas]);
    }
}
