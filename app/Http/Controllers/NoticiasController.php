<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Noticias;
use DB;

class NoticiasController extends Controller
{
    public function index() {
    	return view('sistema/colaboradores/formulario-noticias', []);
    }

    public function store(Request $request) {
        $this->validate($request, [
            "titulo_noticia"=>"required",
            "copete_noticia"=>"required",
            "fecha_noticia"=>"required",
            "cuerpo_noticia"=>"required",
            "categoria_noticia"=>"required",
            "autor_noticia"=>"required",
            "ImagenPerfil"=>"file|image",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            
        DB::beginTransaction();

        $var = $request->fecha_noticia;
		$nuevafecha = date("Y-m-d", strtotime($var));

        $noticias = new Noticias();
        $noticias ->titulo = $request->titulo_noticia;
        $noticias ->copete = $request->copete_noticia;
        $noticias ->fecha = $nuevafecha;
        $noticias ->cuerpo = nl2br($request->cuerpo_noticia);
        $noticias ->categoria = $request->categoria_noticia;
        $noticias ->link = $request->link_noticia;
        $noticias ->autor = $request->autor_noticia;
            
	        if(count($request->file('imagenNoticia')) > 0) {

	            $file = $request->file('imagenNoticia');
	            $nombre = $file->getClientOriginalName();
	            \Storage::disk('imagenNoticias')->put($nombre, \File::get($file));

	            $noticias ->portada = "assets/noticias/".$nombre;
	        } else {
	            $noticias ->portada = "";
	        }

        $noti = $noticias->save();

        if(!$noti) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! La noticia se ha guardado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("agregar-noticias?value=8");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! La noticia se ha guardado correctamente."]);
    }
}
