<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ColaboradoresRequest;
use App\Http\Requests\ColaboradoresUpdateRequest;
use App\Http\Requests\SustitutoRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use App\Colaboradores;
use App\Roles;
use App\User;
use Auth;
use Exception;
use App\GrupoMateriaProfesor;
use Carbon\Carbon;

/**
     * Store a newly created resource in storage.
     *
     * @author Edgar Saavedra -- 10/07/2018
     *
     */

/*Se creo un nuevo controlador para los profesores que sustituyen a otros ya que se necesita agregar un nuevo colaborador y por el mismo modo, se ha de cambiar las materias del profesor antiguo al nuevo */
class SustitutoController extends Controller
{
    public function index($id)
    {
    	//METODO SIN OCUPAR PROVISIONALMENTE--- POSIBLE USO EN EL FUTURO
    	$susID = DB::table('colaboradores')
            ->leftjoin('materia_profesor_grupo',function($j){$j->on('colaboradores.idColaborador', '=', 'materia_profesor_grupo.id_profesor');})
            ->leftjoin('grupos_materias',function($j){$j->on('materia_profesor_grupo.id_materia_grupo', '=', 'grupos_materias.id');})
            ->leftjoin('materias','grupos_materias.id_materia','=','materias.idMateria')
            ->leftjoin('periodosescolares',function($j){$j->on('materias.idPeriodo','=','periodosescolares.idPeriodo');})
            ->select('Periodo','materias.Nombre','materias.idMateria','username')
            ->where('colaboradores.idColaborador',$id)
            ->get();
        $name = DB::table('colaboradores')->select('Nombre','Apellido')->where('idColaborador',$id)->get();
        $name = str_replace('[{"Nombre":"', '', $name);$name = str_replace('","Apellido":"', ' ', $name);
        $name = str_replace('"}]', '', $name);
        // dd($susID);
        $colaboradores = DB::table('colaboradores')
        ->select(
            'colaboradores.idColaborador',
            'Nombre',
            'Apellido',
            'idRol',
            'Email'
        )->where([['idColaborador', '<>' ,'0'],['idRol','=','4']])->orderBy('idColaborador', 'ASC')->get();
        $idC = DB::table('colaboradores')->select('idColaborador')->where('idColaborador',$id)->get();
        $idC = str_replace('[{"idColaborador":"','', $idC);
        $idC = str_replace('"}]', '', $idC);
    	return view('sistema/colaboradores/sustituto',compact('susID','name','colaboradores','idC'));
    }
    public function create()
    {
        //dejamos por defecto a 1 para que se ignore en la validación
        //si el usuario logueado es master tomara valor de cero para que pueda apaarecer en su lista 
        //1 -- master
        $val = "Master";
        if(Auth::check())
        {
            if(Auth::user()->rol->idRol == 1)
            {
               
                $val = "";
            }
        }
        $roles = Roles::whereRaw("Rol <> ? and Rol <> ? and Rol <> ?",["Alumno","Tutor",$val])->get();
        return view('sistema/colaboradores/sustituto',compact("roles"));
    }
    public function store(ColaboradoresRequest $request)
    {
    	// dd($request);
		DB::beginTransaction();
            $user = new User();
            $user->name = $request->Email;
            $user->password = $request->Email;
            $user->email = $request->Email;
            $user->typeUser = 4;
            $us = $user->save();
            $colaborador = new Colaboradores();
            $colaborador->username = $request->Email;
            $colaborador->Password = $request->Email;
            $colaborador->Nombre = $request->Nombre; 
        	$colaborador->Apellido = $request->Apellido;
        	$colaborador->idRol = 4;
        	$colaborador->Email = $request->Email;
        	$colaborador->Telefono = $request->Telefono;
        	$colaborador->Domicilio = $request->Domicilio;
        	$colaborador->ImagenPerfil = $request->imagenPerfil;
        	//dd($request->imagenPerfil);
        	$colaborador->id_usuario = $user->id;
        	$col = $colaborador->save();
        //MISMO METODO QUE EL DE COLABORADORESCONTROLLER, SOLO SE AGREGO UNAS NUEVAS LINEAS DE CODIGO A CONTINUACION PARA PODER HACER EL UPDATE EN LAS MATERIAS Y CAMBIAR EL PROFESOR
        	$materia_profesor_grupo = new GrupoMateriaProfesor();
        	$idC = $request->idC;
        	$c = Colaboradores::find($colaborador->idColaborador);
        //SE REALIZA UNA CONSULTA PARA BUSCAR LAS MATERIAS DEL PROFESOR QUE ESTAN ACTIVADAS EN EL CICLO ESCOLAR Y ASI SOLO MODIFICAR LAS MISMAS, DEJANDO LAS DEL CICLO PASADO CON EL PROFESOR ANTERIOR.
	        $now = Carbon::now();
	        $fecha = $now->format('Y-m-d'); 
	        $materiasperiodo = DB::table('periodosescolares as p')->leftjoin('materias as m','p.idPeriodo','=','m.idPeriodo')
        	->leftjoin('grupos_materias as gm','m.idMateria','=','gm.id_materia')->leftjoin('materia_profesor_grupo as mpg','gm.id','=','mpg.id_materia_grupo')->select('Periodo','id_profesor','gm.id')
        	->whereRaw('("'.$fecha.'" BETWEEN FechaInicio AND FechaFin) AND id_profesor = '.$idC)->get();
	        foreach ($materiasperiodo as $value) 
	        {
	        	$result = DB::table('materia_profesor_grupo')->where('id_profesor',$idC)->where('id_materia_grupo',$value->id)->update(['id_profesor'=>$c->idColaborador]);
	        }
	    //FIN DEL CAMBIO Y DE LA ACTUALIZACION DE DATOS EN LA TABLA MATERIAS_GRUPO_PROFESOR

        if(!$col || !$us || !$result)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "No se pudo sustituir el profesor";
            $status = 200;//ok
        }
        else
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "Se sustituyo el colaborador con exito!";
            $status = 500;//error interno
        }

    	if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/sustitucion/create?value=2");
        }
        return Response()->json(["mensaje"=>"El colaborador se sustituyo con exito!"],$status);
    }
    public function update(SustitutoRequest $request)
    {
    	//dd($request);
    	DB::beginTransaction();
    		$materia_profesor_grupo = new GrupoMateriaProfesor();
	    	$idR = $request->idR;
	    	$c = Colaboradores::find($request->idP);
	    	$result="";
	    //SE REALIZA UNA CONSULTA PARA BUSCAR LAS MATERIAS DEL PROFESOR QUE ESTAN ACTIVADAS EN EL CICLO ESCOLAR Y ASI SOLO MODIFICAR LAS MISMAS, DEJANDO LAS DEL CICLO PASADO CON EL PROFESOR ANTERIOR.
	        $now = Carbon::now();
	        $fecha = $now->format('Y-m-d'); 
	        $materiasperiodo = DB::table('periodosescolares as p')->leftjoin('materias as m','p.idPeriodo','=','m.idPeriodo')
        	->leftjoin('grupos_materias as gm','m.idMateria','=','gm.id_materia')->leftjoin('materia_profesor_grupo as mpg','gm.id','=','mpg.id_materia_grupo')->select('Periodo','id_profesor','gm.id')
        	->whereRaw('("'.$fecha.'" BETWEEN FechaInicio AND FechaFin) AND id_profesor = '.$idR)->get();
        	if($idR != $request->idP){
    	        foreach ($materiasperiodo as $value)
    	        {
    	        	$result = DB::table('materia_profesor_grupo')->where('id_profesor',$idR)->where('id_materia_grupo',$value->id)->update(['id_profesor'=>$c->idColaborador]);
    	        }
        	}
	        if(!$result)
            {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se pudo sustituir el profesor";
                $status = 200;//ok
            }
            else
            {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "Se sustituyo el colaborador con exito!";
                $status = 500;//error interno
            }

    	if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/sustitucion/create?value=2");
        }
        return Response()->json(["mensaje"=>"El colaborador se sustituyo con exito!"],$status);
    }
}