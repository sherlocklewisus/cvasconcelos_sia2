<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use Illuminate\Validation\Rule;
use App\Tutores;
use App\Municipios;
use App\Estados;
use App\User;
use App\tutoresalumnos;
use Carbon\Carbon;
use DB;
use App\Religiones;

class EditartutoresController extends Controller
{
    
    public function index() {

    }

    public function getCiudad(Request $request, $id) {
        if($request->ajax()){
            $ciudad = Municipios::ciudad($id);
            return response()->json($ciudad);
        }
    }

    public function show($id) {

         
        $tutor = Tutores::find($id);
        $user = User::where("id","=",$tutor->id_usuario)->first();

          
            $estados = Estados::all()->pluck("Estado","idEstados");
              
               $ciudad = DB::table('municipios')
               ->select(
                        'municipios.id',
                        'nombre'
                        )
               ->get();
        $parentesco = DB::table('parentescos')
            ->select('parentescos.idParentescos',
            'Parentesco')
            ->get();

        $relacion = DB::table('tutoresalumnos')
        ->select('tutoresalumnos.idTutoresAlumnos',
             'idTutoresAlumnos',
             'idTutor',
             'idAlumno',
             'idParentesco')
        ->where('idTutor','=', $id)
        ->get();

        $alumno = DB::table('alumnos')
        ->select('alumnos.idAlumno',
             'Nombres',
             'ApellidoPaterno',
             'ApellidoMaterno')
        ->where('idAlumno','<>', '0')
        ->get();

        $religiones = Religiones::where("id","<=",6)->pluck("Religion","id");


       return view('sistema/colaboradores/editar-tutores', ['tutor' => $tutor,'user'=> $user, 'ciudad' => $ciudad, 'estados' => $estados,'parentesco'=>$parentesco,'alumno'=>$alumno, 'relacion' => $relacion,"religiones"=>$religiones]);

    }

    public function update(Request $request, $id){
                
                $user = null;
                if($request->id_alumno != $id)

                $tuto = Tutores::find($id);
                $user = User::where("id","=",$tuto->id_usuario)->first();

                if(is_null($user) || $user == "")
                {
                    $user = new User();
                } else {
                     $user = User::find($tuto->id_usuario);
                }

               
            
            $this->validate($request, [
            "username"=>"required",
            "password" => "min:8|confirmed",
            "Nombres"=>"required|regex:/^[\pL\s\-]+$/u",
            "Apellidos"=>"required|regex:/^[\pL\s\-]+$/u",
            "Email"=>  Rule::unique('tutores')->ignore($tuto->Email,"Email"),
            // "TelefonoCasa"=>"required|min:7",
            "TelefonoMovil"=>"min:7",
            "Ocupacion"=>"required",
            "Estados"=>"required",
            // "idCiudad"=>"required",
            "CodigoPostal"=>"required",
            "Colonia"=>"required",
            "Calle"=>"required",
            "NumeroExterior"=>"required",
            "EstadoCivil"=>"required",
            
            /*"Bautizado"=>"required",
            "IglesiaAsistida"=>"required",
            "Distrito"=>"required",*/
        ]);
         $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";
            $tt = true;
        DB::beginTransaction();


            $user->name = $request->username;
            $user->email = $request->Email;
            $user->password=$request->password;
            $user->typeUser = 7;
            $user->ImagenPerfil = $request->imagenPerfil;
            $us = $user ->save();


            $tuto = Tutores::find($id);
            $tuto->username = $request->username;
            $tuto->password = $request->password;
            $tuto->Nombres = $request->Nombres;
            $tuto->Apellidos = $request->Apellidos;
            $tuto->Email = $request->Email;
            $tuto->rfc = $request->rfc;
            $tuto->TelefonoCasa = $request->TelefonoCasa;
            $tuto->TelefonoMovil = $request->TelefonoMovil;
            $tuto->TelefonoExtra1 = $request->TelefonoExtra1;
            $tuto->TelefonoExtra2 = $request->TelefonoExtra2;
            $tuto->FechaNacimiento = $request->FechaNacimiento;
            $tuto->Ocupacion = $request->Ocupacion;
            $tuto->idEstado = $request->Estados;
            $tuto->idCiudad = $request->idCiudad;
            $tuto->CodigoPostal = $request->CodigoPostal;
            $tuto->Colonia = $request->Colonia;
            $tuto->Calle = $request->Calle;
            $tuto->NumeroExterior = $request->NumeroExterior;
            $tuto->NumeroInterior = $request->NumeroInterior;
            $tuto->idEstadoCivil = $request->EstadoCivil;
            $tuto->Comentario = $request->Comentario;
            //$tuto->Religion = $request->Religion;
            if($request->id_religion == 6)
            {
                if($request->original_id_religion <= 6)
                {
                    $religion = new Religiones();
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    $tuto->Religion = $religion->id;
                }
                else
                {
                    $religion = Religiones::find($request->original_id_religion);
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    $tuto->Religion = $religion->id;
                }
            }
            else
            {
                $tuto->Religion = $request->id_religion;
            }
            $tuto->Bautizado = $request->Bautizado;
            $tuto->IglesiaAsistida = $request->IglesiaAsistida;
            $tuto->Distrito = $request->Distrito;
            $tuto->id_usuario = $user->id;
            $tu = $tuto->save(); 

            if($request->Alumno == NULL || $request->parentesco == NULL)
            {

            }
            else{
                 $messages = [
            'unique' => 'ERROR!! El :attribute ya tiene un tutor asignado'
                ];
      
        $this->validate($request, [
                    "Alumno"=>"required|unique:tutoresalumnos,idAlumno",
                    "parentesco"=>"required",
                    /*"Tutor"=>"required"*/
        ],$messages);
                if (count($request->Alumno) > 0 ) {
            foreach ($request->Alumno as $Al) {
            $tutoal = new tutoresalumnos();
            $tutoal->idAlumno = $Al;
            $tutoal->idParentesco = $request->parentesco;
            $tutoal->idTutor = $tuto->idTutor;
            $tt = $tutoal ->save();
        }
        }else{

        }
            }
        
            if(!$tu || !$us || !$tt) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El Tutor no se pudo actualizar";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El Tutor se actualizo con exito!";
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("/lista-tutor");
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect('editar-tutores/'.$tuto->idTutor.'?value=7');
            }
           return Response()->json(["mensaje"=>"El Tutor se actualizo con exito!"]);
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/lista-tutor");  
      
    }
    
}
