<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlumnosRequest;
use App\Http\Requests\AlumnosUpdateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Route;

use App\Reportes;
use DB;

class VerReportesController extends Controller
{
    public function index() {
    	$id = isset($_GET['idreporte'])? $_GET['idreporte'] : 1;

        $reportes = DB::table('reportes')
        ->select(
            'reportes.IdReporte',
            'IdAlumno',
            'IdTutor',
            'Fecha',
            'Asunto',
            'Descripcion',
            'Tiporeporte',
            'Status'
        )
        ->orderBy('IdReporte', 'DESC')
        ->Where('IdReporte', '=', $id)
        ->get();

        $alumnos = DB::table('alumnos')
        ->select(
            'alumnos.idAlumno',
            'Nombres',
            'ApellidoPaterno',
            'ApellidoMaterno'
        )
        ->orderBy('idAlumno', 'ASC')
        ->get();

        $tutores = DB::table('tutores')
        ->select(
            'tutores.idTutor',
            'Nombres',
            'Apellidos'
        )
        ->orderBy('idTutor', 'ASC')
        ->get();

	   	return view('SIAO/ver-reportes', ['reportes' => $reportes, 'alumnos' => $alumnos, 'tutores' => $tutores]);
    }
}
