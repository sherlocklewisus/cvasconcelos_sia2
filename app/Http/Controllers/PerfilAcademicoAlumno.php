<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlumnosRequest;
use App\Http\Requests\AlumnosUpdateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Route;
use App\Alumnos;
use App\User;
use App\Religiones;
use App\Seguros;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Estados;
use App\Municipios;
use App\Archivos;
use App\Materias;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use DB;
use Auth;
use App\Historico;
use App\CiclosEscolares;
use App\Calificaciones;
use App\Colaboradores;
use PDFS;
use App\DocumentosAlumnos;
use Exception;
use App\periodosevaluacion;


class PerfilAcademicoAlumno extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
         $tipodeus = Auth::user()->rol->idRol;
          if($tipodeus == '10')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }
                $cuarto = User::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                      if($secciones2->idSeccion == '2'){
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orWhere('idSeccion','=',1)->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                      }
                      else{
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orWhere('idSeccion','=',4)->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                      }
                }


          }

          // si es usuario 9
           elseif($tipodeus == '9')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
                
                $cuarto = User::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                      
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                                      }


          }
          // fin de condicion para usuario 9
          else{
            if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                }
                $cuarto = User::get();
                $secciones = Secciones::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                    ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }
          }
       

        $relacionalt = DB::table('tutoresalumnos')
        ->select('tutoresalumnos.idTutoresAlumnos',
             'idTutoresAlumnos',
             'idTutor',
             'idAlumno',
             'idParentesco')
        ->get();
        $Roles = Auth::user()->rol->idRol;

        return view ('sistema/colaboradores/lista-alumnos-academico', ['estudiantes' => $estudiantes, 'secciones' => $secciones, 'cuarto' => $cuarto, 'relacionalt'=>$relacionalt,'roles'=>$Roles]);
    }

    public function listar_materia_calificacion($id){


        $alumnos = Alumnos::where('idAlumno', '=', $id)->get();
        $alumnos->each(function($alumnos) {
            $alumnos->periodo;
            $alumnos->seccion;
            $alumnos->grado;
            $alumnos->grupo;
        });

        $alumnosx2 = Alumnos::where('idAlumno', '=', $id)->pluck('idGrupo');
        $relaciones = GruposMaterias::where('id_grupo', '=', $alumnosx2)->pluck('id_materia');
        $materias = Materias::whereIn('materias.idMateria', $relaciones)
        //->join('calificaciones', 'materias.idMateria', '=', 'calificaciones.idMateria')
        ->get();




        $profesor = GrupoMateriaProfesor::orderBy('id', 'ASC')->get();
        $profesor->each(function($profesor) {
            $profesor->profesor;
        });
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $relacionmaterias = GruposMaterias::whereIn('id_materia', $materias)->get();
        $calificaciones = Calificaciones::orderBy('idCalificacion', 'ASC')->get(); 
        $consultas = DB::select('SELECT materias.Nombre,calificaciones.CalificacionFinal,calificaciones.idPeriodoEvaluacion FROM materias INNER JOIN calificaciones ON materias.idMateria = calificaciones.idMateria WHERE idAlumno = ?', [$id]);


        /*OBTENER PERIODOS DE EVALUACIÓN*/
        //dd($alumnos);*/
          $materias_grupo = GruposMaterias::find($relaciones);
          $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
          $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)

          ->get();

          
          $alumno = Alumnos::where('idAlumno',$id)->first();




        return view('sistema/colaboradores/perfil_academico_alumno', ['alumnos' => $alumnos, 'materias' => $materias,'periodosEvaluacion'=>$periodosEvaluacion,"calificaciones" => $calificaciones,"consultas" =>$consultas,"alumno"=>$alumno]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lista_grupos(Request $request){

         $tipodeus = Auth::user()->rol->idRol;
          if($tipodeus == '10')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
                   $grado = Grados::where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
                    $grado->each(function($grado) {
                        $grado->seccion;
                    });
                    $grupos = Grupos::join('grados','grupos.idGrado','=','grados.idGrado')->where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
                    $grupos->each(function($grupos) {
                        $grupos->grado;
                    });
                    $grupos= Grupos::join('grados','grupos.idGrado','=','grados.idGrado')->where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
                   $grado = Grados::where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
                    $grado->each(function($grado) {
                        $grado->seccion;
                    });
                    $grupos = Grupos::join('grados','grupos.idGrado','=','grados.idGrado')->where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
                    $grupos->each(function($grupos) {
                        $grupos->grado;
                    });
                    $grupos= Grupos::join('grados','grupos.idGrado','=','grados.idGrado')->where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }

          }
          elseif ($tipodeus == '9') {
             $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
                $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
               $grado = Grados::where('idSeccion','=',$usuario->id_nivel)->get();
                $grado->each(function($grado) {
                    $grado->seccion;
                });
                $grupos = Grupos::join('grados','grupos.idGrado','=','grados.idGrado')->where('idSeccion','=',$usuario->id_nivel)->get();
                $grupos->each(function($grupos) {
                    $grupos->grado;
                });
                $grupos= Grupos::join('grados','grupos.idGrado','=','grados.idGrado')->where('idSeccion','=',$usuario->id_nivel)->get();
          }
          else{
            if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                }
            $secciones = Secciones::get();
            $grupos = Grupos::get();
            $grupos->each(function($grupos) {
                $grupos->grado;
            });
            $grupos= Grupos:: paginate(15);
               $grado = Grados::get();
                $grado->each(function($grado) {
                    $grado->seccion;
                });

          }
 
        return view('sistema/colaboradores/lista-grupos', ['secciones' => $secciones, 'grupos' => $grupos, 'grado' => $grado]);
 
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function calificaciones_grupo($id,$idSeccion)
    {
        $alumnos = Alumnos::where('idGrupo',$id)
        ->get();

        $relacion = GruposMaterias::where('id_grupo', $id)->pluck('id_materia');
        $materias = Materias::whereIn('materias.idMateria', $relacion)
        ->get();

 

        return view('sistema/colaboradores/calificaciones_grupal', ['alumnos' => $alumnos,'materias' => $materias]);
    }
    public function calificaciones_boleta($id,$idperiodo){ 
        $alumnos = Alumnos::where('idGrupo',$id)
        ->select('idAlumno','Nombres','ApellidoPaterno','ApellidoMaterno')
        ->get();


        $relaciones = GruposMaterias::where('id_grupo', '=', $id)->pluck('id_materia');


        $materias = Materias::whereIn('materias.idMateria', $relaciones)
        ->select('idMateria','Nombre') ->get();
          
        
        $periodo = periodosevaluacion::find($idperiodo);
          
            
        $calificaciones = DB::select('SELECT idMateria,idPeriodoEvaluacion,idAlumno,CalificacionFinal 
            FROM `calificaciones` 
            WHERE `idGrupo`= ? 
            AND `idPeriodoEvaluacion`= ?',
        [$id,$idperiodo]);
            

         // return redirect('boleta_grupo/1/3');
            return view('sistema/colaboradores/boleta_grupo', ['alumnos' => $alumnos,'materias' => $materias,'periodo'=>$periodo,'calificaciones'=>$calificaciones]);
    }

  public function imprimirboleta($id,$idperiodo)
        {
         $alumnos = Alumnos::where('idGrupo',$id)
        ->select('idAlumno','Nombres','ApellidoPaterno','ApellidoMaterno')
        ->get();


        $relaciones = GruposMaterias::where('id_grupo', '=', $id)->pluck('id_materia');


        $materias = Materias::whereIn('materias.idMateria', $relaciones)
        ->select('idMateria','Nombre') ->get();
          
        
        $periodo = periodosevaluacion::find($idperiodo);
          
            
        $calificaciones = DB::select('SELECT idMateria,idPeriodoEvaluacion,idAlumno,CalificacionFinal 
            FROM `calificaciones` 
            WHERE `idGrupo`= ? 
            AND `idPeriodoEvaluacion`= ?',
        [$id,$idperiodo]);
            
 $pdf = PDFS::loadView('sistema.colaboradores/imprimir_boleta',['alumnos' => $alumnos,'materias' => $materias,'periodo'=>$periodo,'calificaciones'=>$calificaciones]);
    $pdf->setPaper("letter","landscape");
                return $pdf->inline('reporte.pdf');
            
        }

    public function lista_periodosevaluacion($id){

        $relaciones = GruposMaterias::where('id_grupo', '=', $id)->pluck('id_materia');
        
        $materias = Materias::whereIn('materias.idMateria', $relaciones)
        //->join('calificaciones', 'materias.idMateria', '=', 'calificaciones.idMateria')
        ->get();
        
        if(count($materias)>0)
        {
        

        $profesor = GrupoMateriaProfesor::orderBy('id', 'ASC')->get();
        $profesor->each(function($profesor) {
            $profesor->profesor;
        });
        $relacionmaterias = GruposMaterias::whereIn('id_materia', $materias)->get();

        //$calificaciones = Calificaciones::orderBy('idCalificacion', 'ASC')->get(); 

        /*OBTENER PERIODOS DE EVALUACIÓN*/
        //dd($alumnos);*/
          $materias_grupo = GruposMaterias::find($relaciones);
          $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
          $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)
          ->get();

        return view('sistema/colaboradores/lista_periodos_evaluacion',['periodosEvaluacion'=>$periodosEvaluacion,'id'=>$id]);

        }
        else
        {   
            return view("sistema/colaboradores/grupo_inactivo");
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
