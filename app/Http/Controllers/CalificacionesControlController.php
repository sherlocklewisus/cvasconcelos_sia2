<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Route;
use App\Alumnos;
use App\User;
use App\Religiones;
use App\Seguros;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Estados;
use App\Municipios;
use App\Archivos;
use DB;
use Auth;
use App\Historico;
use App\CiclosEscolares;
use App\Calificaciones;
use PDFS;
use App\DocumentosAlumnos;
use Exception;
use App\Colaboradores;
use App\GruposMaterias;
use App\periodosevaluacion;
use App\Materias;

class CalificacionesControlController extends Controller
{
    //
    public function index(Request $request)
    {
     $tipodeus = Auth::user()->rol->idRol;
       
          if($tipodeus == '9')
          {
            $tipo = Auth::user()->id;
             $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
             $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
             $GruposMaterias = Materias::orderBy('idMateria', 'DESC')
              ->join('grupos_materias','materias.idMateria','=','grupos_materias.id_materia')
              ->whereRaw("Nombre LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ?",["%".$request->Nombre."%","%".$usuario->id_nivel."%","%".$request->grado."%"])->paginate(20);
                    $GruposMaterias->each(function($GruposMaterias) {
                        $GruposMaterias->seccion;
                        $GruposMaterias->grado;
                        $GruposMaterias->grupo;

                    });
              // $secciones = Secciones::get();
          }
          else{
             $GruposMaterias = Materias::orderBy('idMateria', 'DESC')
              ->join('grupos_materias','materias.idMateria','=','grupos_materias.id_materia')
              ->whereRaw("Nombre LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ?",["%".$request->Nombre."%","%".$request->Seccion."%","%".$request->grado."%"])->paginate(20);
                    $GruposMaterias->each(function($GruposMaterias) {
                        $GruposMaterias->seccion;
                        $GruposMaterias->grado;
                        $GruposMaterias->grupo;

                    });
             
              $secciones = Secciones::get();
          }
     

        return view ('sistema/colaboradores/principal_profesores_control', ['GruposMaterias' => $GruposMaterias ,'secciones' => $secciones]);
    }

      public function vista_calificar_alumnos_control($id_materia_grupo)
      {
         
          //dd($alumnos);*/
          $materias_grupo = GruposMaterias::find($id_materia_grupo);
          $cicloActual = CiclosEscolares::whereRaw("idSeccion = ? AND activo = true",[$materias_grupo->grupo->grado->seccion->idSeccion])->first();
          $periodosEvaluacion = periodosevaluacion::where("idCicloEscolar","=",$cicloActual->idPeriodo)->get();

          //$periodosEvaluacion = periodosevaluacion::where("idSeccion","=",$materias_grupo->grupo->grado->seccion->idSeccion)->get();
          //dd($periodosEvaluacion);
          //dd($materias_grupo->grupo->grado->seccion->idSeccion);

          return view("sistema.colaboradores.calificaciones_alumnos_control",compact("periodosEvaluacion","materias_grupo"));
      }
}
