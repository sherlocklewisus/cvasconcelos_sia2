<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\PeriodosEvaluacion;
use DB;

class SeguimientoHorariosController extends Controller
{
    public function index() {
        $estructuras = DB::table('horarios_estructura')
        ->get();

        $periodos = DB::table('periodosescolares')
        ->get();

        $niveles = DB::table('secciones')
        ->get();

        return view ('SIAO/seguimiento-horarios', ['estructuras' => $estructuras, 'periodos' => $periodos, 'niveles' => $niveles]);
    }
}
