<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Alumnos;
use DateTime;
use DB;

class ListaBloqueosControlController extends Controller
{
    public function index(Request $request) {
    	$alumnos = Alumnos::whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ?",["%".$request->nameAlumnos."%"])->where('status_actual', '=', 0)->orderBy('idAlumno', 'DESC')->paginate(10);
        $alumnosx2 = Alumnos::where('status_actual', '=', 0)->orderBy('Nombres', 'DESC')->paginate(20);
    	$mes = date('Y-m');
        $minima = $mes."-01";
        $minima = DateTime::createFromFormat('Y-m-d', $minima);
        $maxima = $mes."-30";
        $maxima = DateTime::createFromFormat('Y-m-d', $maxima);

        $colegiaturas = DB::table('colegiaturas')
        ->join('alumnos', 'colegiaturas.idAlumno', '=', 'alumnos.idAlumno')
        ->join('periodospagos', 'colegiaturas.idPeriodoPago', '=', 'periodospagos.idPeriodoPago')
        ->select('colegiaturas.*', 'alumnos.*', 'periodospagos.*')
        ->where([
            ['periodospagos.FechaPago', '>=', $minima],
            ['periodospagos.FechaPago', '<=', $maxima],
            ['alumnos.status_actual', '>', '0'],
        ])
        ->orderBy('colegiaturas.idReferencia', 'ASC')
        ->get();

    	return view ('sistema/colaboradores/listado-bloqueos', ['alumnos' => $alumnos, 'colegiaturas' => $colegiaturas,'al' => $alumnosx2]);
    }

     public function bloqueos(Request $request) {
        $alumnos = Alumnos::where('status_actual', '!=', 0)->orderBy('Nombres', 'DESC')->paginate(20);

        $mes = date('Y-m');
        $minima = $mes."-01";
        $minima = DateTime::createFromFormat('Y-m-d', $minima);
        $maxima = $mes."-30";
        $maxima = DateTime::createFromFormat('Y-m-d', $maxima);

        $colegiaturas = DB::table('colegiaturas')
        ->join('alumnos', 'colegiaturas.idAlumno', '=', 'alumnos.idAlumno')
        ->join('periodospagos', 'colegiaturas.idPeriodoPago', '=', 'periodospagos.idPeriodoPago')
        ->select('colegiaturas.*', 'alumnos.*', 'periodospagos.*')
        ->where([
            ['periodospagos.FechaPago', '>=', $minima],
            ['periodospagos.FechaPago', '<=', $maxima],
            ['alumnos.status_actual', '>', '0'],
        ])
        ->orderBy('alumnos.Nombres', 'ASC')
        ->get();

        return view ('sistema/colaboradores/listado-bloqueos', ['alumnos' => $alumnos, 'colegiaturas' => $colegiaturas]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            "idalumnado"=>"required",
            "opciones"=>"required",
        ]);

        $server = "localhost";
                    $user = "adminani_ipjroot";
                    $pass = "animatiomx2017";
                    $bd = "adminani_ipjv";

        // Creamos la conexión
        $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexión de la base de datos. HOLA");

        if($request->opciones == 1) {
            // Generamos la consulta
            $sql = "UPDATE alumnos SET status_actual = 1 WHERE idAlumno = ". $request->idalumnado;

        } else {
            // Generamos la consulta
            $sql = "UPDATE alumnos SET status_actual = 0 WHERE idAlumno = ". $request->idalumnado;
        }

        // Formato de datos utf8
        mysqli_set_charset($conexion, "utf8");

        //Guardamos en una variable los datos de consulta & conexión
        if(!$result = mysqli_query($conexion, $sql)) die("Ha sucedido un error.");

        if(!$result) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {

            $query = "SELECT * FROM tutoresalumnos WHERE idAlumno = ". $request->idalumnado;

            mysqli_set_charset($conexion, "utf8");

            if(!$consulta = mysqli_query($conexion, $query)) die ("Se ha generado un error.");

            $filas = mysqli_num_rows($consulta);

            if ($filas === 0) {
                $consult = true;
            } else {
                while($row = mysqli_fetch_array($consulta)) {
                    $padre = $row['idTutor'];

                    if($request->opciones == 1) {
                        // Generamos la consulta
                        $query = "UPDATE tutores SET status_actual = 1 WHERE idTutor = ". $padre;
                    } else {
                        // Generamos la consulta
                        $query = "UPDATE tutores SET status_actual = 0 WHERE idTutor = ". $padre;
                    }

                    // Formato de datos utf8
                    mysqli_set_charset($conexion, "utf8");

                    //Guardamos en una variable los datos de consulta & conexión
                    if(!$consult = mysqli_query($conexion, $query)) die("Ha sucedido un error.");
                }
            }

            if(!$consult) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "¡En hora buena! El alumno sufrio los cambios correctamente.";
            }
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("listado-bloqueos?value=1");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El alumno sufrio los cambios correctamente."]);
    }
}
