<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Requests\ActividadRequest;
use App\Http\Controllers\View;
use App\Horarios;
use App\HorarioEst;
use App\periodosescolares;
use App\Grados;
use App\Grupos;
use App\GruposMaterias;
use App\actividadesextra;
use DB;
use Auth;

class VerHorarioProfesorController extends Controller
{
  public function vista()
  {

    
    return view('sistema/colaboradores/ver-horario-profesor');
  }
    public function index() {

          /*horarios estructur tiene relacion con
            * niveles
            * grados
            * grupos
            * periodos
            /*
              return view ('sistema/profesores/ver-horario-profesor', ['horarios' => $horarios, 'estructura' => $estructura, 'materias' => $materias, 'periodos' => $periodos, 'secciones' => $secciones, 'grados' => $grados, 'grupos' => $grupos, 'relacionmaterias' => $relacionmaterias, 'grupillos' => $grupillos]);

            */      
            return view('sistema/profesores/ver-horario-profesor'); 

    }
        public function getRequest(Request $request)
    {   
        // dd($request)

          /*$prueba = HorarioEst::join('periodosescolares', 'periodosescolares.idPeriodo', '=', 'horarios_estructura.periodo_escolar')     
            ->selectRaw('horarios_estructura.idHorario','periodosescolares.idPeriodo','horarios_estructura.nivel_educativo')
            ->get(); 
            return response($prueba);

            dd($prueba);*/
        $horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios) {
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        // dd($horarios->niveles);

        });


        $estructura = DB::table('horarios_estructura')
        ->select(
            'horarios_estructura.idHorario',
            'periodo_escolar',
            'nivel_educativo',
            'horaInicio',
            'horaFinal',
            'receso',
            'comentario'
        )
        ->orderBy('idHorario', 'ASC')
        ->get();
      

        $materias = DB::table('materias')
        ->select(
            'materias.idMateria',
            'Nombre',
            'idSeccion',
            'idGrado',
            'idGrupo'
        )
        ->orderBy('idMateria', 'ASC')
        ->get();

        $periodos = DB::table('periodosescolares')
        ->select(
            'periodosescolares.idPeriodo',
            'Periodo'
        )
        ->orderBy('idPeriodo', 'ASC')
        ->get();

    
        $secciones = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->orderBy('idSeccion', 'ASC')
        ->get();

        $grados = DB::table('grados')
        ->select(
            'grados.idGrado',
            'Grado'
        )
        ->orderBy('idGrado', 'ASC')
        ->get();

        $grupos = DB::table('grupos')
        ->select(
            'grupos.idGrupo',
            'Grupo'
        )
        ->orderBy('idGrupo', 'ASC')
        ->get();

        $relacionmaterias = DB::table('materia_profesor_grupo')
        ->select(
            'materia_profesor_grupo.id',
            'id_materia_grupo',
            'id_profesor'
        )
        ->orderBy('id', 'ASC')
        ->get();
          // dd($relacionmaterias);

        $grupillos = DB::table('grupos_materias')
        ->select(
            'grupos_materias.id',
            'id_grupo',
            'id_materia'
        )
        ->orderBy('id', 'ASC')
        ->get(); 

         $hora_mat=DB::table('horarios_materias')
                 
                    ->get();
         // dd($hora_mat);
          /*


            $prueba = DB::table('horarios_estructura')
            ->join('periodosescolares', 'periodosescolares.idPeriodo', '=', 'horarios_estructura.periodo_escolar')
            ->join('secciones','secciones.idSeccion','=','horarios_estructura.nivel_educativo')
            ->join('')

            ->select('horarios_estructura.idHorario','periodosescolares.idPeriodo','horarios_estructura.nivel_educativo','secciones.idSeccion','secciones.Seccion')
            ->where('horarios_estructura.idHorario','=','34')
            ->get(); 
          */
            /*
            return response()->json($prueba,$pruebado);*/
// dd($estructura);
           // return response::json($prueba);
            $horaLunes=DB::table('horarios_materias')
                ->join('horarios_estructura','horarios_materias.horario','=','horarios_estructura.horaInicio')
                ->join('materias','horarios_materias.diaLunes','=','materias.idMateria')
                ->join('materia_profesor_grupo','horarios_materias.diaLunes','=','id_materia_grupo')
                ->where('materia_profesor_grupo.id_profesor','=',Auth::user()->id)
                ->get();
                $horaMartes=DB::table('horarios_materias')
                ->join('horarios_estructura','horarios_materias.horario','=','horarios_estructura.horaInicio')
                ->join('materias','horarios_materias.diaMartes','=','materias.idMateria')
                ->join('materia_profesor_grupo','horarios_materias.diaLunes','=','id_materia_grupo')
                ->where('materia_profesor_grupo.id_profesor','=',Auth::user()->id)
                ->get();
            // dd($request);

                $hora=DB::table('horarios_materias')->where('idperiodo','=',$request->periodo)->get();
                // dd($hora);
// dd($grupillos);

        return response()->json(['horaLunes'=>$horaLunes,'horaMartes'=>$horaMartes,'horarios' => $horarios, 'estructura' => $estructura, 'materias' => $materias, 'periodos' => $periodos, 'secciones' => $secciones, 'grados' => $grados, 'grupos' => $grupos, 'relacionmaterias' => $relacionmaterias, 'grupillos' => $grupillos]);


    }
     public function store(ActividadRequest $request)
    {
   
        DB::beginTransaction();

        $actividad = new actividadesextra();
        $actividad->idProfesor=$request->id;
        $actividad->Nombre=$request->actividad;
        $actividad->Fecha=$request->FechaInicio;
        $actividad->Detalles=$request->detalles;
        
        $final=$actividad->save();
        if(!$final)
            {
                DB::rollback();
                $tipo_mensaje="mensaje-danger";
                $texto_mensaje= "Parece que ocurrio un error, intentelo de nuevo";
            }
            else
            {
                DB::commit();
                $tipo_mensaje="mensaje-success";
                $texto_mensaje="¡En hora buena! Se ha guardado correctamente la actividad";
            }
        if(!$request->ajax())
            {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("ver-horario-profesor?periodo={$request->per}&seccion={$request->sec}");
            }
        return Response()->json(["mensaje"=>"¡En hora buena! se ha guardado correctamente la actividad]"]);
    }
}
