<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Noticias;
use DB;

class ListaNoticiasController extends Controller
{
    public function index() {
    	$noticias = Noticias::orderBy('idNoticia', 'DESC')->get();

    	return view('sistema/colaboradores/listado-noticias', ['noticias' => $noticias]);
    }

    public function store(Request $request) {
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";

    $server = "localhost";
    $user = "adminani_ipjroot";
    $pass = "animatiomx2017";
    $bd = "adminani_ipjv";

        $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexion de la base de datos");

        $sql = "DELETE FROM `noticias` WHERE idNoticia = ".$request->noticia;

        mysqli_set_charset($conexion, "utf8");

        $response = mysqli_query($conexion, $sql);

        if(!$response) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! La noticia se ha eliminado exitosamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista-noticias?value=8");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! La noticia se ha eliminado correctamente."]);
	}
}
