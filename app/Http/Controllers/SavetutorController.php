<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;
use App\Religiones;
use App\Tutores;
use App\Municipios;
use App\User;
use App\Estados;
use App\tutoresalumnos;
use DB;

use Carbon\Carbon;

class SavetutorController extends Controller {
    public function index() {
       
        $estados = DB::table('estados')
            ->select(
                'estados.idEstados', 
                'Estado'
            )
            ->get();
            $seccion = DB::table('secciones')
            ->select('secciones.idSeccion',
                'Seccion')
            ->get();
            $parentesco = DB::table('parentescos')
            ->select('parentescos.idParentescos',
            'Parentesco')
            ->get();
                                                

              $religiones = Religiones::where("id","<=",6)->pluck("religion","id");
        return view('sistema/colaboradores/formulario-tutor', ['estados' => $estados, 'seccion' => $seccion,'parentesco' => $parentesco, 'religiones' => $religiones]);
    }

    public function getCiudad(Request $request, $id) {
        if($request->ajax()){
            $ciudad = Municipios::ciudad($id);
            return response()->json($ciudad);
        }
    }

    public function store(Request $request) {

        $this->validate($request, [
            "Nombres"=>"required",
            "Apellidos"=>"required",
            // "Email"=> "required|email|unique:tutores",
            // "TelefonoCasa"=>"required",
            "Ocupacion"=>"required",
            "Estados"=>"required",
            "Ciudad"=>"required",
            "CodigoPostal"=>"required",
            "Colonia"=>"required",
            "Calle"=>"required",
            "NumeroExterior"=>"required",
            "EstadoCivil"=>"required",
            // "id_religion"=>"required",
            /*"Bautizado"=>"required",
            "IglesiaAsistida"=>"required",
            "Distrito"=>"required",*/

        
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
        $tt = true;
        DB::beginTransaction();
        $email1 = $request->Nombres."".$request->Apellidos."@ipjvasconcelos.edu.mx";
        $email = preg_replace('[\s+]','', $email1);
            $user = new User();
            $user->name = $email;
            $user ->email = $email;
            $user ->password= $email;
            $user ->typeUser = 7;
            $user->ImagenPerfil = $request->imagenPerfil;
            //dd($request->imagenPerfil);
            $us = $user ->save();         

            // ---------------------------------------------------->

            $tutor =  new Tutores();
            $tutor->username = $email;
            $tutor->password = $email;
            $tutor->Nombres = $request->Nombres;
            $tutor->Apellidos = $request->Apellidos;
            $tutor->Email = $email;
            $tutor->rfc = $request->rfc;
            $tutor->TelefonoCasa = $request->TelefonoCasa;
            $tutor->TelefonoMovil = $request->TelefonoMovil;
            $tutor->TelefonoExtra1 = $request->TelefonoExtra1;
            $tutor->TelefonoExtra2 = $request->TelefonoExtra2;
            $tutor->FechaNacimiento = $request->FechaNacimiento;
            $tutor->Ocupacion = $request->Ocupacion;
            $tutor->idEstado = $request->Estados;
            $tutor->idCiudad = $request->Ciudad;
            $tutor->CodigoPostal = $request->CodigoPostal;
            $tutor->Colonia = $request->Colonia;
            $tutor->Calle = $request->Calle;
            $tutor->NumeroExterior = $request->NumeroExterior;
            $tutor->NumeroInterior = $request->NumeroInterior;
            $tutor->idEstadoCivil = $request->EstadoCivil;
            $tutor->Comentario = $request->Comentario;
            //$request->Religion trae el id de religio
            if($request->id_religion == 6)
            {
                $religion = new Religiones();
                $religion->religion = $request->religion_otros;
                $rel = $religion->save();
                $tutor->Religion = $religion->id;
            }
            else
            {
                $tutor->Religion = $request->id_religion;
            }

            $tutor->Bautizado = $request->Bautizado;
            $tutor->IglesiaAsistida = $request->IglesiaAsistida;
            $tutor->Distrito = $request->Distrito;
            $tutor->id_usuario = $user->id;
            $tu = $tutor->save(); 

            //-------------------------------------
            if($request->Alumno == NULL || $request->parentesco == NULL)
            {

            }
            else{
                 $messages = [
            'unique' => 'ERROR!! El :attribute ya tiene un tutor asignado'
                ];
      
        $this->validate($request, [
                    "Alumno"=>"required|unique:tutoresalumnos,idAlumno",
                    "parentesco"=>"required",
                    /*"Tutor"=>"required"*/
        ],$messages);
                if (count($request->Alumno) > 0 ) {
            foreach ($request->Alumno as $Al) {
            $tutoal = new tutoresalumnos();
            $tutoal->idAlumno = $Al;
            $tutoal->idParentesco = $request->parentesco;
            $tutoal->idTutor = $tutor->idTutor;
            $tt = $tutoal ->save();
        }
        }else{

        }
            }
        


            if(!$tu || !$us || !$tt) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El Tutor no se pudo registrar";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El Tutor se registro con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("agregar-tutor?value=7");
            }

        return Response()->json(["mensaje"=>"El Tutor se registro con exito!"]);
    }

    

}

