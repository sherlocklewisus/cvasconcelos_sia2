<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Convenios;
use DB;

class ListaConveniosController extends Controller
{
	public function index() {
	    $convenios = Convenios::orderBy('id_convenio', 'DESC')->paginate(15);
	    $convenios->each(function($convenios) {
            $convenios->alumnos;
        });
	    
	    return view ('sistema/caja/listado-convenios', ['convenios' => $convenios]);
	}
}
 