<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\HistorialPagos;
use DB;

class ListaDeudoresController extends Controller
{
    public function index() {
    	$colegiaturas = HistorialPagos::where('statusfinal', '=', 0)->orderBy('idHistorico', 'DESC')->paginate(20);
    	$colegiaturas->each(function($colegiaturas) {
            $colegiaturas->alumnos;
        });

    	return view ('sistema/colaboradores/listado-deudores', ['colegiaturas' => $colegiaturas]);
    }
    public function caja() {
    	$colegiaturas = HistorialPagos::where('statusfinal', '=', 0)->orderBy('idHistorico', 'DESC')->paginate(20);
    	$colegiaturas->each(function($colegiaturas) {
            $colegiaturas->alumnos;
        });

    	return view ('sistema/caja/listado-deudores', ['colegiaturas' => $colegiaturas]);
    }
}
