<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Grados;
use App\Grupos;
use App\Secciones;
use App\Materias;
use App\Colaboradores;
use App\periodosescolares;
use App\GruposMaterias;
use App\GrupoMateriaProfesor;
use DB;
use Exception;
use App\Http\Requests\SaveMateriasProfesorGrupoRequest;
use App\User;
use Auth;

class MateriasController extends Controller
{
   public function index() {
      $tipodeus = Auth::user()->rol->idRol == 9;
      if($tipodeus == 'true'){
          $tipo = Auth::user()->id;
          $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
         $periodo = DB::table('periodosescolares')
                ->select('periodosescolares.idPeriodo',
                    'Periodo')
                ->where('idSeccion','=',$usuario->id_nivel)
                ->get();

         $seccion = DB::table('secciones')
                ->select('secciones.idSeccion',
                    'Seccion')
                ->where('idSeccion','=',$usuario->id_nivel)
                ->get();

        $colaborador = DB::table('colaboradores')
                ->select('colaboradores.idRol',
                    'idColaborador',
                    'Nombre',
                    'Apellido'
                    )
                 ->where('idRol', '=' ,'4')
                 ->get();   
      }
      else{
     $periodo = DB::table('periodosescolares')
            ->select('periodosescolares.idPeriodo',
                'Periodo')
            ->get();

     $seccion = DB::table('secciones')
            ->select('secciones.idSeccion',
                'Seccion')
            ->get();

    $colaborador = DB::table('colaboradores')
            ->select('colaboradores.idRol',
                'idColaborador',
                'Nombre',
                'Apellido'
                )
             ->where('idRol', '=' ,'4')
             ->get();   
      }
   

        return view('sistema/colaboradores/materias', ['periodo' => $periodo, 'secciones' => $seccion, 'colaborador' => $colaborador]);
    }

    public function getSeccion(Request $request, $id) {
        if($request->ajax()){
            $periodo = periodosescolares::where('idPeriodo', '=', $id)->first();
            $seccion = Secciones::where('idSeccion', '=', $periodo->idSeccion)->get();
            return response()->json($seccion);
        }
    }

    public function getGrado(Request $request, $id) {
        if($request->ajax()){
            $grado = Grados::grado($id);
            return response()->json($grado);
        }
    }

    public function getGrupo(Request $request, $id) {
        if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function store(Request $request) {
        //dd($request);
    	 $this->validate($request,[
    	 		"Nombre"=>"required",
    	 		"Periodo"=>"required",
				"idSeccion"=>"required",
				"idGrado"=>"required",
				/*"Grupo"=>"required",*/
				/*"Docente"=>"required",*/
				// "DescripcionHorario"=>"required|max:45",
				// "DescripcionCurso"=>"required|max:300",
				// "ObjetivosGenerales"=>"required|max:45",
				// "ObjetivosEspecificos"=>"required|max:45",
				// "Actividades"=>"required|max:500",
				// "Bibliografia"=>"required|max:45",
                "grupos"=>"required_if:check_asignar_materias,1",
                "grupos.*"=>"exists:grupos,idGrupo"
				],[
                    "grupos.required_if"=>"Debes seleccionar al menos un grupo",
                    "grupos.*.exists"=>"El grupo seleccionado no es valido",
                ]);

    	        $tipo_mensaje = "mensaje-success";
       	        $texto_mensaje = "";

                try
                {
       	            $materia =  new Materias();
                    $materia->Nombre = $request->Nombre;
                    $materia->idPeriodo = $request->Periodo;
                    $materia->idSeccion = $request->idSeccion;
                    $materia->idGrado = $request->idGrado;
                    /*$materia->idGrupo = $request->Grupo;
                    $materia->Semestre = $request->Semestre;*/
                    /*$materia->idColaborador = $request->Docente;*/
                    $materia->DescripcionHorario = $request->DescripcionHorario;
                    $materia->DescripcionCurso = $request->DescripcionCurso;
                    $materia->ObjetivosGenerales = $request->ObjetivosGenerales;
                    $materia->ObjetivosEspecificos = $request->ObjetivosEspecificos;
                    $materia->Actividades = $request->Actividades;
                    $materia->Bibliografia = $request->Bibliografia;
                    $materia->save();

                    if(!$materia) {
                        throw new Exception("No se pudo registrar la materia");        
                    } 

                    //verificamos si presionaron el checkbox para asignar grupo
                    if($request->check_asignar_materias == 1)
                    {
                        $grupos = $request->grupos;

                        if(count($grupos)==0)
                        {
                            throw new Exception("necesitas seleccionar al menos un grupo");
                            
                        }

                        foreach($grupos as $id_grupo)
                        {
                            $id_materia_grupo = DB::table("grupos_materias")
                            ->insertGetId([
                              "id_grupo"=>$id_grupo,
                              "id_materia"=>$materia->idMateria
                            ]);

                        if(!$id_materia_grupo)
                        {
                            throw new Exception("No se pudo insertar los datos en la tabla grupos_materias");
                        }
                    }

                    if($request->check_asignar_materias == 1 && $request->check_asignar_profesor == 1)
                    {
                        $profesor = $request->profesor;

                        $ma_prof_group = DB::table("materia_profesor_grupo")
                                    ->insert([
                                        "id_materia_grupo"=>$id_materia_grupo,
                                        "id_profesor"=>$profesor
                                        ]);

                        if(!$ma_prof_group)
                        {
                            throw new Exception("No se pudo insertar los datos en la tabla materias_profesor_grupo ");
                        }

                    }
                }

                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "La materia ha sido guardada correctamente.";
                $status = 200;
            }
            catch(Exception $ex)
            {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
                $status = 500;         
            }   

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("materias?value=9");
            }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);

    }


    public function materias_profesores(Request $request)
    {

          $tipodeus = Auth::user()->rol->idRol == 9;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->pluck("Seccion","idSeccion");
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
                    $materias = Materias::whereRaw("idSeccion = ? AND idGrado = ?",[$secciones2->idSeccion,$request->idGrado])->get();
                    $profesores = Colaboradores::obtProfesores()->get();
                    $profesores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                                        ->join("roles","roles.idRol","=","users.typeUser")
                                        ->whereRaw("colaboradores.Nombre LIKE ? AND colaboradores.Apellido LIKE ?",["%".$request->Nombre."%", "%".$request->Apellido."%"])->orderBy("idColaborador","DESC")->paginate(7);
                    $colaboradores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                                        ->join("roles","roles.idRol","=","users.typeUser")
                                        ->whereRaw("colaboradores.Nombre LIKE ? AND colaboradores.Apellido LIKE ?",["%".$request->Nombre."%", "%".$request->Apellido."%"])->orderBy("idColaborador","DESC")->paginate(200);

          }
          else{
            if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                }
            $secciones = Secciones::all()->pluck("Seccion","idSeccion");
            $materias = Materias::whereRaw("idSeccion = ? AND idGrado = ?",[$request->idSeccion,$request->idGrado])->get();
            $profesores = Colaboradores::obtProfesores()->get();
            $profesores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                                ->join("roles","roles.idRol","=","users.typeUser")
                                ->whereRaw("colaboradores.Nombre LIKE ? AND colaboradores.Apellido LIKE ?",["%".$request->Nombre."%", "%".$request->Apellido."%"])->orderBy("idColaborador","DESC")->paginate(7);
            $colaboradores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                                        ->join("roles","roles.idRol","=","users.typeUser")
                                        ->whereRaw("colaboradores.Nombre LIKE ? AND colaboradores.Apellido LIKE ?",["%".$request->Nombre."%", "%".$request->Apellido."%"])->orderBy("idColaborador","DESC")->paginate(200);
          }
        return view("sistema.materias.materias_profesores",compact("secciones","materias","profesores","colaboradores"));
    }

    public function materias_grupo(Request $request)
    {
          $tipodeus = Auth::user()->rol->idRol == 9;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
                $materias = Materias::whereRaw("idSeccion = ? AND idGrado = ?",[$secciones2->idSeccion,$request->idGrado])->get();
                $profesores = Colaboradores::obtProfesores()->get();

          }
          else{
            if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                }
            $secciones = Secciones::get();
            $materias = Materias::whereRaw("idSeccion = ? AND idGrado = ?",[$request->idSeccion,$request->idGrado])->get();
            $profesores = Colaboradores::obtProfesores()->get();
          }

        return view("sistema.materias.materias_grupo",compact("secciones","materias","profesores"));
    }

       public function lista_profesores(Request $request)
    {
        $profesores = Colaboradores::obtProfesores()->whereRaw("concat(colaboradores.Nombre,colaboradores.Apellido) LIKE ? ",["%".$request->nombre."%"])->paginate(15);
        return view("sistema.materias.lista_profesores",compact("profesores"));
    }

    public function saveMateriasGrupo(Request $request) {
        DB::beginTransaction();

        $request->idSeccion;
        $request->idGrado;

        $validar = 0;
        $materias = $request->materias;
        $grupos = $request->grupos;

        if(count($grupos) == 0) {  
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Debe seleccionar un grupo para continuar.";
            $validar = 1;
        }

        if(count($materias) == 0) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Debe seleccionar una materia para continuar.";
            $validar = 1;
        }

        if($validar == 0) {
            foreach($grupos as $id_grupo) {
                foreach($materias as $id_materia) {
                    $select = GruposMaterias::where([
                        ['id_grupo', '=', $id_grupo],
                        ['id_materia', '=', $id_materia]
                    ])->get();

                    if(count($select) > 0) {
                        DB::rollback();
                        $tipo_mensaje = "mensaje-danger";
                        $texto_mensaje = "La materia ya se encuentra registrada, intentelo nuevamente.";
                    } else {
                        $mg = new GruposMaterias();
                        $mg->id_grupo = $id_grupo;
                        $mg->id_materia = $id_materia;
                        $save = $mg->save();

                        if(!$save) {
                            DB::rollback();
                            $tipo_mensaje = "mensaje-danger";
                            $texto_mensaje = "Parece que ocurrio un error, intentelo nuevamente.";
                        } else {
                            DB::commit();
                            $tipo_mensaje = "mensaje-success";
                            $texto_mensaje = "Los cambios se han guardado correctamente.";
                        }
                    }
                }
            }
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("materias_grupo")->withInput();
        }

        return Response()->json(["mensaje"=>"Los cambios se han guardado correctamente."]);
    }

    public function getGrade(Request $request, $id) {
        if($request->ajax()){
            $grade = Grados::grade($id);
            return response()->json($grade);
        }
    }

    public function getGroup(Request $request, $id) {
        if($request->ajax()){
            $group = Grupos::group($id);
            return response()->json($group);
        }
    }

    public function getMatter(Request $request, $id) {
        if($request->ajax()){
            $matter = Materias::materias($id);
            return response()->json($matter);
        }
    }

    public function search_materias(Request $request)
    {
        $materias = Materias::whereRaw("idSeccion = ? AND idGrado = ?",[$request->idSeccion,$request->idGrado])->get();

        return Response()->json($materias);
    }

    public function search_materias_grupos(Request $request)
    {
        $materias_grupo = DB::table("grupos_materias as x1")
                          ->select("x1.id as id_materia_grupo","x1.id","x3.Grupo","x2.Nombre","x6.id_profesor","x7.Nombre as maestro", "x7.Apellido")
                          ->join("materias as x2","x1.id_materia","=","x2.idMateria")
                          ->join("grupos as x3","x1.id_grupo","=","x3.idGrupo")
                          ->join("grados as x4","x3.idGrado","=","x4.idGrado")
                          ->join("secciones as x5","x4.idSeccion","=","x5.idSeccion")
                          ->leftjoin("materia_profesor_grupo as x6","x1.id","=","x6.id_materia_grupo")
                          ->leftjoin("colaboradores as x7","x6.id_profesor","=","x7.idColaborador")
                          ->whereRaw("x5.idSeccion = ? AND x4.idGrado = ?",[$request->idSeccion,$request->idGrado])
                          ->orderBy("x3.Grupo")
                          ->get();

        return Response()->json($materias_grupo);
    }

     public function update_profesor_grupo(Request $request)
    {

        //$param['id'] = $this->input->post('mid');
        // $descripcion['maestro'] = $this->input->post('mmaestro');

        // echo $this->mpersona->update_profesor_grupo($param);

        
        $texto_mensaje = "";
        $status = 200;
        try
        {
            DB::beginTransaction();


             // $descripcion = $request->descripcion;
                $ma_prof_group = DB::table("materia_profesor_grupo")
                ->update([
                            'id_profesor'=>$_POST["nuevom"]
                        ])->where('id_profesor',$_POST["maestroa"]);

                if(!$ma_prof_group)
                {
                    throw new Exception("No se pudo insertar los datos en la tabla materias_profesor_grupo");
                }
           
            DB::commit();
            $texto_mensaje = "Los datos se registraron con exito!";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $texto_mensaje = $ex->getMessage();
            $status = 500;
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);

    }

    public function saveMaterias_profesor_grupo(SaveMateriasProfesorGrupoRequest $request)
    {
        
        $texto_mensaje = "";
        $status = 200;
        //dd($request->materias_grupo);
        try
        {
            DB::beginTransaction();

            if(count($request->materias_grupo) <= 0)
            {
                throw new Exception("Debes seleccionar una materia");
                
            }

            foreach ($request->materias_grupo as $id_materia_grupo) 
            {
                $ma_prof_group = DB::table("materia_profesor_grupo")
                ->insert([
                            "id_materia_grupo"=>$id_materia_grupo,
                            "id_profesor"=>$request->id_profesor
                        ]);

                if(!$ma_prof_group)
                {
                    throw new Exception("No se pudo insertar los datos en la tabla materias_profesor_grupo");
                }
            }
           
            DB::commit();
            $texto_mensaje = "Los datos se registraron con exito!";
            $status = 200;
        }
        catch(Exception $ex)
        {
            DB::rollback();
            $texto_mensaje = $ex->getMessage();
            $status = 500;
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }
    public function listadop(Request $request)
    {
        # code...
        $profes = Colaboradores::join('users','colaboradores.id_usuario','=','users.id')->orderBy('users.id', 'desc')->get();
        $profesM = GrupoMateriaProfesor::get();
        $materiasg= GruposMaterias::get();
        $materias = Materias::get();

        return Response()->json(["profesores"=>$profes, "materiasgrupo"=>$materiasg, "materias"=> $materias, "grupomateriaprofesor"=> $profesM]);
    }
}
