<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\HorarioEst;
use App\GruposMaterias;
use App\ForosLista;
use App\Tareas;
use App\TareasAlumnos;

use Auth;
use DB;

class AdministradorAlumnosController extends Controller
{
    public function index() {
        $user = Auth::user()->alumno->idGrupo;
        $userx2 = Auth::user()->alumno->idAlumno;

        $relacion = GruposMaterias::where('id_grupo', '=', $user)->get();
        $foros = ForosLista::where('idgrupo', '=', $user)->orderBy('idForo', 'ASC')->get();

        $horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios) {
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });

        $material = GruposMaterias::where('id_grupo', '=', $user)->pluck('id_materia');
        $tareas = Tareas::whereIn('idMateria', $material)->orderBy('idTarea', 'DESC')->get();
        $tareasalumnos = TareasAlumnos::where('idAlumno', '=', $userx2)->get();
        //dd($tareasalumnos);

        $estructura = DB::table('horarios_estructura')
        ->select(
            'horarios_estructura.idHorario',
            'periodo_escolar',
            'nivel_educativo',
            'horaInicio',
            'horaFinal',
            'receso',
            'comentario'
        )
        ->orderBy('idHorario', 'ASC')
        ->get();

    	$materias = DB::table('materias')
    	->select(
    		'materias.idMateria',
    		'Nombre',
    		'idSeccion',
    		'idGrado',
    		'idGrupo'
    	)
    	->orderBy('idMateria', 'ASC')
    	->get();

        $periodos = DB::table('periodosescolares')
        ->select(
            'periodosescolares.idPeriodo',
            'Periodo',
            'FechaInicio',
            'FechaFin',
            'Activo'
        )
        ->orderBy('idPeriodo', 'ASC')
        ->get();

        $secciones = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->orderBy('idSeccion', 'ASC')
        ->get();

        $grados = DB::table('grados')
        ->select(
            'grados.idGrado',
            'Grado'
        )
        ->orderBy('idGrado', 'ASC')
        ->get();

        $grupos = DB::table('grupos')
        ->select(
            'grupos.idGrupo',
            'Grupo'
        )
        ->orderBy('idGrupo', 'ASC')
        ->get();

        $mensajesRecibidos = DB::table('mensajes')
        ->select(
            'mensajes.idMensaje',
            'idUsuarioRecibe',
            'status'
        )
        ->orderBy('idMensaje', 'ASC')
        ->get();

        $grupillos = DB::table('grupos_materias')
        ->select(
            'grupos_materias.id',
            'id_grupo',
            'id_materia'
        )
        ->orderBy('id', 'ASC')
        ->get();

    	return view ('sistema/alumnos/admin-alumno', ['horarios' => $horarios, 'estructura' => $estructura, 'materias' => $materias, 'periodos' => $periodos, 'secciones' => $secciones, 'grados' => $grados, 'grupos' => $grupos, 'mensajesRecibidos' => $mensajesRecibidos, 'grupillos' => $grupillos, 'relacion' => $relacion, 'foros' => $foros, 'tareas' => $tareas, 'tareasalumnos' => $tareasalumnos]);
    }
}
