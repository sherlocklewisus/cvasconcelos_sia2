<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;


use App\ArchivosTareasAlumno;
use App\GruposMaterias;
use App\ArchivosTarea;
use App\Alumnos;
use DB;

class lista_Tareas_EntregadasController extends Controller
{
     public function index(Request $request)
    {
    	
    	$tareasAlumno = DB::table('archivos_tarea_alumno')
    	->select(
    		'archivos_tarea_alumno.id',
    		'archivo',
    		'idTarea',
    		'idAlumno') 
    	->orderBy('id', 'ASC')
        ->get();
    	
		$alumno = DB::table('alumnos')
			    	   ->select('idAlumno',
			    	   	'Nombres',
			    	   	'ApellidoPaterno',
			    	   	'ApellidoMaterno'
			    		)
			    	   ->get();

    	return view('sistema/profesores/entregadas-tareas', ['tareasAlumno'=> $tareasAlumno], ['alumno'=>$alumno]);

    }
}
	