<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Noticias;
use DB;

class VistaNoticiasController extends Controller
{
    public function index() {
    	$noticias = Noticias::orderBy('idNoticia', 'ASC')->limit('6')->get();
    	//$noticiasx2 = Noticias::orderBy('idNoticia', 'ASC')->limit('8')->get();

    	return view('pagina/noticias', ['noticias' => $noticias]);
    }
}
