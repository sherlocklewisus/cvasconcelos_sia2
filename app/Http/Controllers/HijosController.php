<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;

class HijosController extends Controller
{
    public function index() {
        if(!Session::has("hijo")) {
            if(Auth::user()->rol->Rol == 'Tutor') {
                $hijos = Auth::user()->tutor->alumnos_hijos();
                $hijo = (count($hijos->get())?$hijos->first():0);
                Session::put("hijo",(($hijo)?$hijo->idAlumno:0));
            }
        }

        $hijo = DB::table('tutoresalumnos')
        ->select(
            'tutoresalumnos.idTutoresAlumnos',
            'idTutor',
            'tutoresalumnos.idAlumno',
            'idParentesco',
            'tutoresalumnos.deleted_at',
            'alumnos.Nombres',
            'alumnos.ApellidoPaterno',
            'alumnos.ApellidoMaterno',
            'secciones.Seccion',
            'grados.Grado',
            'grupos.Grupo'
        )
        ->join('alumnos','alumnos.idAlumno','=', 'tutoresalumnos.idAlumno')
        ->join('secciones','secciones.idSeccion','=', 'alumnos.idSeccion')
        ->join('grados','grados.idGrado', '=', 'alumnos.idGrado')
        ->join('grupos', 'grupos.idGrupo','=', 'alumnos.idGrupo')
        ->where('idTutor', '=' , Auth::User()->tutor->idTutor)
        ->get();
        
        return view('sistema/tutores/hijos', ['hijo' => $hijo]);
    }

    public function show() {
        $id = $_GET['id'];
        
        $documentos = DB::table('tutoresalumnos')
        ->select(
            'tutoresalumnos.idTutoresAlumnos',
            'idTutor',
            'tutoresalumnos.idAlumno',
            'idParentesco',
            'tutoresalumnos.deleted_at',
            'alumnos.Nombres',
            'alumnos.FechaEntregaDocumento',
            'alumnos.ActualizacionEntregaDocumento',
            'alumnos.ApellidoPaterno',
            'alumnos.ApellidoMaterno',
            'alumnos.ActaNacimiento',
            'alumnos.CertificadoPrimaria',
            'alumnos.CertificadoSecundaria',
            'alumnos.DocumentoCURP',
            'alumnos.CartaConducta',
            'alumnos.Fotografias',
            'alumnos.BoletasPrimaria',
            'alumnos.BoletasSecundaria',
            'alumnos.BoletasSecundariaOriginal',
            'alumnos.BoletasSecundariaCopia',
            'alumnos.ConstanciadeEER1',
            'alumnos.ConstanciadeEER2',
            'alumnos.ConstanciadeEER3',
            'alumnos.ConstanciaEstudios',
            'alumnos.SolicitudInscripcion',
            'alumnos.ActaNacimientoTutor',
            'alumnos.CredencialTutor'
        )
        ->join('alumnos','alumnos.idAlumno','=', 'tutoresalumnos.idAlumno')
        ->where('alumnos.idAlumno', '=' , $id)
        ->where('idTutor', '=' , Auth::User()->tutor->idTutor)
        ->get();

        return view('sistema/tutores/lista-documentos', ['documentos' => $documentos]);
    }
}