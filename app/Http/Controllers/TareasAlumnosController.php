<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Route;
use App\Http\Requests\TareaRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Tareas;

use App\ArchivosTareasAlumno;
use App\ArchivosTarea;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use App\Grupos;
use App\Grados;
use App\Secciones;
use App\Materias;
use App\Alumnos;
use App\TareasAlumnos;

use Exception;
use DB;
use Auth;

class TareasAlumnosController extends Controller
{

	public function index() {
		
	    return view('sistema/tareas/tareas');
	}

/*
    public function subirTarea(Request $request) {
    $tipo_mensaje = "mensaje-success";
    $texto_mensaje = "";

    DB::beginTransaction();
    
    if(count($request->file('files_add1'))  > 0) {
      $file = $request->file('files_add1');
      $nombre = $file->getClientOriginalName();
      \Storage::disk('documentos')->put($nombre, \File::get($file));

      $archivos = new ArchivosTareaAlumno();
      $archivos ->nombreDocto = $nombre;
      $archivos ->idTarea = $request->idTa;
      $archivos ->idTareaAlumno = $request->userAlu;
      $archiv = $archivos->save();
    }

   
    if(!$request->ajax()) {
      Session::flash($tipo_mensaje,$texto_mensaje);
      return redirect("ver_tareas");
    }

    return Response()->json(["mensaje"=>"¡En hora buena! la tarea se ha guardado correctamente."]);
  }*/

  public function store(TareaRequest $request) {
    $tipo_mensaje = "mensaje-success";
    $texto_mensaje = "";

    DB::beginTransaction();

      /*if(count($request->file_tareas) > 0) {
            foreach ($request->file_tareas as $filetarea) {
                $arch = new ArchivosTareasAlumno();
                $arch->archivo = $filetarea;
                $arch ->idTarea = $request->idTa;
                $arch ->idTareaAlumno = $request->userAlu;
                $savearchivo = $arch->save();
            }
        }

*/
    
    if(count($request->file('files_add1'))  > 0) {
      $file = $request->file('files_add1');
      $nombre = $file->getClientOriginalName();
      \Storage::disk('documentos2')->put($nombre, \File::get($file));
      $archivos = new ArchivosTareasAlumno();
      $archivos ->archivo = $nombre;
      $archivos ->idTarea = $request->idTa;
      $archivos ->idAlumno = $request->userAlu;
      $archiv = $archivos->save();
    }

    if(!$archiv) {
      DB::rollback();
      $tipo_mensaje = "mensaje-danger";
      $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
    } else {
             $tareas = TareasAlumnos::where('idTarea', '=', $request->idTa)->get();
             foreach($tareas as $tarea) {
                  
                TareasAlumnos::find($tarea->idTarea);
                
                $tarea->Entregada = 1;
                $final = $tarea->save();
            }

    if(!$final) {
        DB::rollback();
        $tipo_mensaje = "mensaje-danger";
        $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
        } else{
      DB::commit();
      $tipo_mensaje = "mensaje-success";
      $texto_mensaje = "¡En hora buena! La tarea se ha guardado correctamente.";
    }
  }
     
    if(!$request->ajax()) {
      Session::flash($tipo_mensaje,$texto_mensaje);
      return redirect("ver_tareas/20");
    }

    return Response()->json(["mensaje"=>"¡En hora buena! La tarea se ha guardado correctamente."]);
  }

}
