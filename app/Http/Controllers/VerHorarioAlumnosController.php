<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\HorarioEst;
use App\periodosescolares;
use App\Materias;
use App\Secciones;
use App\Grados;
use App\Grupos;
use Auth;
use DB;

class VerHorarioAlumnosController extends Controller
{
    public function index() {
        $userx1 = Auth::user()->alumno->idSeccion;
        $userx2 = Auth::user()->alumno->idGrado;
        $userx3 = Auth::user()->alumno->idGrupo;

        $periodos = periodosescolares::where([
            ["Activo", "=", 1],
            ["idSeccion", "=", $userx1],
        ])->get();

        $horarios = HorarioEst::orderBy('idHorario', 'ASC')->get();
        $horarios->each(function($horarios) {
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });

        $estructura = Horarios::get();
    	$materias = Materias::get();
        
        $secciones = Secciones::get();
        $grados = Grados::get();
        $grupos = Grupos::get();


    	return view ('sistema/alumnos/ver-horario-alumno', ['horarios' => $horarios, 'estructura' => $estructura, 'materias' => $materias, 'periodos' => $periodos, 'secciones' => $secciones, 'grados' => $grados, 'grupos' => $grupos]);
    }
}
