<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\PeriodosEvaluacion;
use App\Periodos;
use App\Secciones;
use DB;

class EditarHorariosController extends Controller
{
    public function index() {
    	$periodo = isset($_GET['periodo'])? $_GET['periodo'] : "";
    	$nivel = isset($_GET['nivel'])? $_GET['nivel'] : "";

        $periodos = Periodos::where('idPeriodo', '=', $periodo)->orderBy('idPeriodo', 'ASC')->get();
        $seccion = Secciones::where('idSeccion', '=', $nivel)->orderBy('idSeccion', 'ASC')->get();
        $estructura = Horarios::where([['periodo_escolar', '=', $periodo],['nivel_educativo', '=', $nivel]])->orderBy('idHorario', 'ASC')->get();

        return view ('sistema/colaboradores/editar-estructura', ['seccion' => $seccion, 'periodos' => $periodos, 'estructura' => $estructura]);
    }

    public function store(Request $request) {
        $count = $request->cantidades;
        
        DB::beginTransaction();

        for($cont = 1; $cont <= $count; $cont++) {

            $this->validate($request, [
                "ini_x".$cont=>"required",
                "fin_x".$cont=>"required",
            ]);

            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";

            $horarios = Horarios::findOrFail($request['idtabla_'.$cont]);
			$horarios ->horaInicio = $request['ini_x'.$cont];
			$horarios ->horaFinal = $request['fin_x'.$cont];

			if(!$horarios ->receso = $request['receso_x'.$cont]) {
				$horarios ->receso = 0;
			} else {
				$horarios ->receso = 1;
			}

			$horariosSave = $horarios->save();

			if(!$horariosSave) {
	            DB::rollback();
	            $tipo_mensaje = "mensaje-danger";
	            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
	        } else {
	            DB::commit();
	            $tipo_mensaje = "mensaje-success";
	            $texto_mensaje = "El horario se ha modificado correctamente.";
	        }
        }

        if(!$request->ajax()) {

            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("editar-estructura?value=8&nivel=".$request->nivel_educativo."&periodo=".$request->periodo_escolar);
        }

        return Response()->json(["mensaje"=>"El horario se ha modificado correctamente."]);
    }
}
