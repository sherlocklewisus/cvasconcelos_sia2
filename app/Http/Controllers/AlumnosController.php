<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlumnosRequest;
use App\Http\Requests\AlumnosUpdateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Route;
use App\Alumnos;
use App\User;
use App\Religiones;
use App\Seguros;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Estados;
use App\Municipios;
use App\Archivos;
use DB;
use Auth;
use App\Historico;
use App\CiclosEscolares;
use App\Calificaciones;
use App\Colaboradores;
use PDFS;
use App\DocumentosAlumnos;
use Exception;


class AlumnosController extends Controller {


    // public function index(Request $request) {
    //     $secciones = Secciones::all()->pluck("Seccion","Seccion");
    //     $listAlumnos = Alumnos::leftjoin("secciones","secciones.idSeccion","=","alumnos.idSeccion")
    //     ->leftjoin("grados","alumnos.idGrado","=","grados.idGrado")
    //     ->leftjoin("grupos","alumnos.idGrupo","=","grupos.idGrupo")
    //     ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno,secciones.seccion,grados.Grado) LIKE ? AND grupos.Grupo LIKE ?",["%".$request->alumno."".$request->nivel."".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")//->toSql();
    //     ->paginate(10);
       // return view('sistema/colaboradores/lista-alumnos', compact("listAlumnos","secciones"));
    // }

    public function index(Request $request) 
    {
          $tipodeus = Auth::user()->rol->idRol;
          if($tipodeus == '10')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }
                $cuarto = User::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);
                    if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
                    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
                    }
                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                      if($secciones2->idSeccion == '2'){
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orWhere('idSeccion','=',1)->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                      }
                      else{
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orWhere('idSeccion','=',4)->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                      }
                }


          }

          // si es usuario 9
           elseif($tipodeus == '9')
          {
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
                
                $cuarto = User::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                      
                            $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                            ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?" ,["%".$request->alumno."%","%".$secciones2->idSeccion."%","%".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")->paginate(20);

                            $estudiantes->each(function($estudiantes) {
                                $estudiantes->seccion;
                                $estudiantes->grado;
                                $estudiantes->grupo;
                            });
                                      }


          }
          // fin de condicion para usuario 9
          else{
                $cuarto = User::get();
                $secciones = Secciones::get();
                if ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                       
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno == null) {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                    });
                }

                elseif($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno == null) {

                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });

                // Estructura de control -> La sentencia de busqueda contiene solo uno de los parametros.
                }
                elseif ($request->nivel != "" && $request->grado == null && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo == null && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        
                    });
                }
                elseif ($request->nivel != "" && $request->grado != "" && $request->grupo != "" && $request->alumno !="") {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')->where([
                        ['Nombres', 'like', "%$request->alumno%"],
                        ['idSeccion', '=', $request->nivel],
                        ['idGrado', '=', $request->grado],
                        ['idGrupo', '=', $request->grupo]
                    ])->orderBy("idAlumno","DESC")->paginate(20);

                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }

                 else {
                    $estudiantes = Alumnos::orderBy('ApellidoPaterno', 'ASC')
                    ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE? AND concat(idSeccion,idGrado,idGrupo)" ,["%".$request->alumno."%","".$request->nivel."".$request->grado."".$request->grupo.""])->orderBy("idAlumno","DESC")->paginate(20);


                    $estudiantes->each(function($estudiantes) {
                        $estudiantes->seccion;
                        $estudiantes->grado;
                        $estudiantes->grupo;
                    });
                }
          }
       

        $relacionalt = DB::table('tutoresalumnos')
        ->select('tutoresalumnos.idTutoresAlumnos',
             'idTutoresAlumnos',
             'idTutor',
             'idAlumno',
             'idParentesco')
        ->get();
        $Roles = Auth::user()->rol->idRol;

        return view ('sistema/colaboradores/lista-alumnos', ['estudiantes' => $estudiantes, 'secciones' => $secciones, 'cuarto' => $cuarto, 'relacionalt'=>$relacionalt,'roles'=>$Roles]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        $alumnos = Alumnos::paginate(5);
        return view("alumnos.index",compact("alumnos"));
        /*$listAlumnos = DB::table('alumnos')
            ->select(
                'alumnos.idAlumno',
                'Nombres',
                'ApellidoPaterno',
                'ApellidoMaterno',
                'idSeccion',
                'idGrado',
                'idGrupo'
            )
            ->where('idAlumno', '<>' ,'0')
            ->orderBy('idAlumno', 'ASC')
            ->get();

            //dd($listAlumnos);
            //return $listAlumnos;

            return view('SIAO/lista-alumnos', ['listAlumnos' => $listAlumnos]);
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $religiones = Religiones::where("id","<=",6)->pluck("religion","id");
        // $seguros = Seguros::where("id","<=",7)->pluck("seguro","id");
        // $secciones = Secciones::all()->pluck("Seccion","idSeccion");
        // $estados = Estados::all()->pluck("Estado","idEstados");
        // return view("sistema.colaboradores.formulario-alumnos",compact("estados","secciones","religiones","seguros"));

        $religiones = Religiones::where('id', '<=', 6)->get();
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }

          }
          else{
            $secciones = Secciones::get();
          }
        $seguros = Seguros::where('id', '<=', 7)->get();
        $estados = Estados::get();

        return view ('sistema/colaboradores/formulario-alumnos', ['religiones' => $religiones, 'secciones' => $secciones, 'seguros' => $seguros, 'estados' => $estados]);
    }

    public function getGrade(Request $request, $id) {
        if($request->ajax()){
            $grade = Grados::grade($id);
            return response()->json($grade);
        }
    }

    public function getGroup(Request $request, $id) {
        if($request->ajax()){
            $group = Grupos::group($id);
            return response()->json($group);
        }
    }

    public function getCity(Request $request, $id) {
        if($request->ajax()){
            $city = Municipios::city($id);
            return response()->json($city);
        }
    }

    public function getGraduazo(Request $request, $id) {
        if($request->ajax()){
            $grade = Grados::grade($id);
            return response()->json($grade);
        }
    }

    public function getGrupazo(Request $request, $id) {
        if($request->ajax()){
            $group = Grupos::group($id);
            return response()->json($group);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlumnosRequest $request)
    {

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
        $status = 200;
        $rel = true;
        $seg = true;

        DB::beginTransaction();

        try
        {
            //necesitamos crear el historico desde que el alumno se da de alta
            //obtenemos el ciclo actual
            $ciclo = CiclosEscolares::where("idSeccion","=",$request->idSeccion)
                          ->where("Activo","=",1)
                          ->first();
            
            //sinos devuelve un ciclo null
            //le indicamos al encargado de control que tiene que registrar un ciclo
            //y ponerlo activo
            if(is_null($ciclo))
            {
                //throw new Exception("Parece que ocurrio un error por favor de de alta un ciclo escolar e intentelo de nuevo.");
                DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "El Ciclo no esta Activo";
            $status = 500;//error interno
            Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("/alumnos/create?value=1");
            }
            else{
                    $user = new User();
                    $user->name = $request->Email;
                    $user->password = bcrypt ($request['Email']);
                    $user->email = $request->Email;
                    $user->typeUser = 8; //en la db es type alumno
                    $us = $user->save();

                    if(!$us)
                    {
                       // throw new Exception("Parece que ocurrio un error, intentelo de nuevo.");
                        DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se pudo guardar como usuario";
                $status = 500;//error interno
                Session::flash($tipo_mensaje,$texto_mensaje);
                    return redirect("/alumnos/create?value=1");
                    }

                $alumno =  new Alumnos();

                if($request->extranjero != "")
                {
                    $alumno->extranjero = $request->extranjero;
                }

                $alumno->Username = $request->Email;
                $alumno->Password = $request->Email;
                $alumno->Nombres = $request->Nombres;
                $alumno->ApellidoPaterno = $request->ApellidoPaterno;
                $alumno->ApellidoMaterno = $request->ApellidoMaterno;
                $alumno->FechaNacimiento = $request->FechaNacimiento;
                $alumno->CURP = $request->CURP;
                $alumno->NIA = $request->NIA;
                $alumno->idSeccion = $request->idSeccion;
                $alumno->idGrado = $request->idGrado;
                $alumno->idGrupo = $request->idGrupo;
                $alumno->idEstado = $request->idEstado;
                $alumno->idCiudad = $request->Ciudad;
                $alumno->CodigoPostal = $request->CodigoPostal;
                $alumno->Colonia = $request->Colonia;
                $alumno->Calle = $request->Calle;
                $alumno->NumeroExterior = $request->NumeroExterior;
                $alumno->NumeroInterior = $request->NumeroInterior;
                $alumno->Telefono = $request->Telefono;

                if($request->id_religion == 6)
                {
                    $religion = new Religiones();
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    
                    if(!$rel)
                    {
                        //throw new Exception("Parece que ocurrio un error en el campo religión, intentelo de nuevo.");
                        DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se pudo guardar el campo religion.";
                $status = 500;//error interno
                Session::flash($tipo_mensaje,$texto_mensaje);
                    return redirect("/alumnos/create?value=1");
                    }

                    $alumno->id_religion = $religion->id;
                }
                else
                {
                    $alumno->id_religion = $request->id_religion;
                }

                $alumno->Bautizado = $request->Bautizado;
                $alumno->IglesiaAsistida = $request->IglesiaAsistida;
                $alumno->Distrito = $request->Distrito;
                $alumno->Enfermedad = $request->Enfermedad;
                $alumno->DetalleEnfermedad = $request->DetalleEnfermedad;
                $alumno->ejercicio = $request->ejercicio;
                $alumno->detalle_ejercicio = $request->detalle_ejercicio;
                $alumno->NombreContactoEmergencia = $request->NombreContactoEmergencia;
                $alumno->TelefonoContactoEmergencia1 = $request->TelefonoContactoEmergencia1;
                $alumno->TelefonoContactoEmergencia2 = $request->TelefonoContactoEmergencia2;

                if($request->id_seguro == 7)
                {
                    $seguro = new Seguros();
                    $seguro->seguro = $request->seguro_otros;
                    $seg = $seguro->save();
                    
                    if(!$seg)
                    {
                        //throw new Exception("Parece que ocurrio un error en el campo seguro, intentelo de nuevo.");
                        DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se pudo guardar el campo seguro";
                $status = 500;//error interno
                Session::flash($tipo_mensaje,$texto_mensaje);
                    return redirect("/alumnos/create?value=1");
                    }

                    $alumno->id_seguro = $seguro->id;
                }
                else
                {
                    $alumno->id_seguro = $request->id_seguro;
                }


                $alumno->AfiliacionMedica = $request->AfiliacionMedica;
                $alumno->EscuelaProcedencia = $request->EscuelaProcedencia;
                $alumno->RazonParaEstudiar = $request->RazonParaEstudiar;
                $alumno->FechaEntregaDocumento = $request->FechaEntregaDocumento;
                /*$alumno->ActualizacionEntregaDocumento = $request->ActualizacionEntregaDocumento;*/
                
                 $alumno->ActaNacimiento = $request->ActaNacimiento;                
                $alumno->CertificadoPrimaria = $request->CertificadoPrimaria;
                $alumno->CertificadoSecundaria = $request->CertificadoSecundaria;                
                $alumno->Fotografias = $request->fotografías;
                $alumno->CredencialTutor = $request->copiasIdentificacion;
                $alumno->NotaDocumentos = $request->NotaDocumentos;
                $alumno->CURPPadre = $request->copiasCurp;

                $alumno->CertificadoPreescolar = $request->CertificadoPreescolar;
                $alumno->CartaTutor = $request->cartaTutor;
                $alumno->CopiaActa = $request->actaCopias;
                $alumno->ExamenMedico = $request->examenMedico;
                $alumno->CopiaExamen = $request->medicoCopias;
                $alumno->BoletaReporte = $request->boletaReporte;
                $alumno->Kardex = $request->kardex;
                $alumno->CertificadoPreescolar = $request->CertificadoPreescolar;
                $alumno->CartaTutor = $request->cartaTutor;
                $alumno->CopiaActa = $request->actaCopias;
                $alumno->ExamenMedico = $request->examenMedico;
                $alumno->CopiaExamen = $request->medicoCopias;
                $alumno->BoletaReporte = $request->boletaReporte;
                $alumno->Kardex = $request->kardex;
                

                $alumno->Activo = 1;
                $alumno->Saldo = $request->Saldo;
                $alumno->Abono = $request->Abono;
                $alumno->Beca = $request->Beca;
                $alumno->BecaSEP = $request->BecaSEP;
                $alumno->PorcentajeBeca = $request->PorcentajeBeca;
                $alumno->imagenPerfil = $request->imagenP;
                //dd($request->imagenPerfil);
                $alumno->id_usuario = $user->id;
                $al = $alumno->save();

                if(!$al)
                {
                   // throw new Exception("Parece que ocurrio un error, intentelo de nuevo.");
                    DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El alumno no se pudo guardar, verifique sus campos";
                $status = 500;//error interno
                Session::flash($tipo_mensaje,$texto_mensaje);
                    return redirect("/alumnos/create?value=1");
                }

                if(count($request->file('files_add1'))  > 0) {
                    $file = $request->file('files_add1');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add2'))  > 0) {
                    $file = $request->file('files_add2');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add3'))  > 0) {
                    $file = $request->file('files_add3');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add4'))  > 0) {
                    $file = $request->file('files_add4');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add5'))  > 0) {
                    $file = $request->file('files_add5');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add6'))  > 0) {
                    $file = $request->file('files_add6');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add7'))  > 0) {
                    $file = $request->file('files_add7');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add8'))  > 0) {
                    $file = $request->file('files_add8');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add9'))  > 0) {
                    $file = $request->file('files_add9');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }

                if(count($request->file('files_add10'))  > 0) {
                    $file = $request->file('files_add10');
                    $nombre = $file->getClientOriginalName();
                    \Storage::disk('documentos')->put($nombre, \File::get($file));

                    $archivos = new Archivos();
                    $archivos ->idAlumno = $user->id;
                    $archivos ->nombreDocto = $nombre;
                    $archivos ->status = 0;
                    $archiv = $archivos->save();
                }




                //registramos al alumno en el historico
                $historico = new Historico();
                $historico->idAlumno = $alumno->idAlumno;
                $historico->idSeccion = $alumno->idSeccion;
                $historico->idGrado = $alumno->idGrado;
                $historico->idGrupo = $alumno->idGrupo;
                $historico->idCiclo = $ciclo->idPeriodo;
                $h = $historico->save();

                if(!$h)
                {
                    //throw new Exception("Parece que ocurrio un error al generar el historico del alumno, intentelo de nuevo.");
                    DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "no se pudo guardar en historico";
                $status = 500;//error interno
                Session::flash($tipo_mensaje,$texto_mensaje);
                    return redirect("/alumnos/create?value=1");
                }
                

                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El alumno se ha guardado correctamente.";
                $status = 200;//ok
            }


        }catch(Exception $ex)
        {
            //dd($ex);
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
            $status = 500;//error interno
            Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("/alumnos/create?value=1");
        }

        
        if(!$request->ajax())
        {
            if ($request->btn_enviar == 1) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("/lista-alumnos?value=1");
            } elseif ($request->btn_enviar == 2) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("/alumnos/create?value=1");
            } else {
                Session::flash($tipo_mensaje,$texto_mensaje);
                //return redirect("/PDFAlumno/".$alumno->idAlumno);
                $alumnado = $alumno->idAlumno;
                return redirect("/alumnos/create?value=1&perfil=".$alumnado);
            }
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $religiones = Religiones::where("id","<=",6)->pluck("religion","id");
        $seguros = Seguros::where("id","<=",7)->pluck("seguro","id");
        $secciones = Secciones::all()->pluck("Seccion","idSeccion");
        $estados = Estados::all()->pluck("Estado","idEstados");
        $alumno = Alumnos::findOrFail($id);
        $archivos = Archivos::where("idAlumno", "=", $alumno->id_usuario)->get();
        $tipodeus = Auth::user()->rol->idRol;

        return view("sistema/colaboradores/editar-alumnos",compact("alumno","secciones","estados","religiones","seguros","archivos","tipodeus"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(AlumnosUpdateRequest $request, $id)
    {
        ini_set('memory_limit','256M');
        //dd($request);
        // dd($request->imagenPerfil);
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
        $rel = true;
        $seg = true;
        $user = null;

        if($request->id_alumno != $id)
            return redirect(404); 

        $alumno = Alumnos::findOrFail($id);
        $user = User::where("id","=",$alumno->id_usuario)->first();

        //verificamos si la seccion se modifica o no
        //esta variable nos servira para ver si cambiamos
        //de seccion de cilo al alumno en el historico
        $modificarSeccion = ($alumno->idSeccion != $request->idSeccion?true:false);

        if(is_null($user))
        {
            $user = new User();
        }

        DB::beginTransaction();
        
        try {
            $user->name = $request->input("name");
            $user->password = $request->input("Password");
            $user->email = $request->input("Email");
            $user->typeUser = 8; //en la db es type alumno
            $us = $user->save();
            $alumno->extranjero = $request->extranjero;
            $alumno->Username = $request->Email;
            $alumno->Password = $request->Email;
            $alumno->Nombres = $request->Nombres;
            $alumno->ApellidoPaterno = $request->ApellidoPaterno;
            $alumno->ApellidoMaterno = $request->ApellidoMaterno;
            $alumno->FechaNacimiento = $request->FechaNacimiento;
            $alumno->CURP = $request->CURP;
            $alumno->NIA = $request->NIA;
            $alumno->idSeccion = $request->idSeccion;
            $alumno->idGrado = $request->idGrado;
            $alumno->idGrupo = $request->idGrupo;
            $alumno->idEstado = $request->idEstado;
            $alumno->idCiudad = $request->idCiudad;
            $alumno->CodigoPostal = $request->CodigoPostal;
            $alumno->Colonia = $request->Colonia;
            $alumno->Calle = $request->Calle;
            $alumno->NumeroExterior = $request->NumeroExterior;
            $alumno->NumeroInterior = $request->NumeroInterior;
            $alumno->Telefono = $request->Telefono;
            
            if($request->id_religion == 6)
            {
                if($request->original_id_religion <= 6)
                {
                    $religion = new Religiones();
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    $alumno->id_religion = $religion->id;
                }
                else
                {
                    $religion = Religiones::find($request->original_id_religion);
                    $religion->religion = $request->religion_otros;
                    $rel = $religion->save();
                    $alumno->id_religion = $religion->id;
                }
            }
            else
            {
                $alumno->id_religion = $request->id_religion;
            }

            $alumno->Bautizado = $request->Bautizado;
            $alumno->IglesiaAsistida = $request->IglesiaAsistida;
            $alumno->Distrito = $request->Distrito;
            $alumno->Enfermedad = $request->Enfermedad;
            $alumno->DetalleEnfermedad = $request->DetalleEnfermedad;
            $alumno->alergias = $request->alergias;
            $alumno->detalle_alergia= $request->detalle_alergia;
            $alumno->ejercicio = $request->ejercicio;
            $alumno->detalle_ejercicio = $request->detalle_ejercicio;
            $alumno->NombreContactoEmergencia = $request->NombreContactoEmergencia;
            $alumno->TelefonoContactoEmergencia1 = $request->TelefonoContactoEmergencia1;
            $alumno->TelefonoContactoEmergencia2 = $request->TelefonoContactoEmergencia2;


            if($request->id_seguro == 7)
            {
                if($request->original_id_seguro <= 7)
                {
                    $seguro = new Seguros();
                    $seguro->seguro = $request->seguro_otros;
                    $seg = $seguro->save();
                    $alumno->id_seguro = $seguro->id;
                }
                else
                {
                    $seguro = Seguros::find($request->original_id_seguro);
                    $seguro->seguro = $request->seguro_otros;
                    $seg = $seguro->save();
                    $alumno->id_seguro = $seguro->id;
                }
            }
            else
            {
                $alumno->id_seguro = $request->id_seguro;
            }

            $alumno->AfiliacionMedica = $request->AfiliacionMedica;
            $alumno->EscuelaProcedencia = $request->EscuelaProcedencia;
            $alumno->RazonParaEstudiar = $request->RazonParaEstudiar;
            /*$alumno->FechaEntregaDocumento = $request->FechaEntregaDocumento;*/
            $alumno->ActualizacionEntregaDocumento = $request->ActualizacionEntregaDocumento;
            
             $alumno->ActaNacimiento = $request->ActaNacimiento;                
                $alumno->CertificadoPrimaria = $request->CertificadoPrimaria;
                $alumno->CertificadoSecundaria = $request->CertificadoSecundaria;                
                $alumno->Fotografias = $request->fotografías;
                $alumno->CredencialTutor = $request->copiasIdentificacion;
                $alumno->NotaDocumentos = $request->NotaDocumentos;
                $alumno->CURPPadre = $request->copiasCurp;

                $alumno->CertificadoPreescolar = $request->CertificadoPreescolar;
                $alumno->CartaTutor = $request->cartaTutor;
                $alumno->CopiaActa = $request->actaCopias;
                $alumno->ExamenMedico = $request->examenMedico;
                $alumno->CopiaExamen = $request->medicoCopias;
                $alumno->BoletaReporte = $request->boletaReporte;
                $alumno->Kardex = $request->kardex;
            
            $alumno->Activo = 1;
            $alumno->Saldo = $request->Saldo;
            $alumno->Abono = $request->Abono;
            $alumno->Beca = $request->Beca;
            $alumno->BecaSEP = $request->BecaSEP;
            $alumno->PorcentajeBeca = $request->PorcentajeBeca;
            $alumno->imagenPerfil = $request->imagenP;
            $alumno->id_usuario = $user->id;
            $al = $alumno->save();

            if(count($request->file('files_add1'))  > 0) {
                $file = $request->file('files_add1');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add2'))  > 0) {
                $file = $request->file('files_add2');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add3'))  > 0) {
                $file = $request->file('files_add3');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add4'))  > 0) {
                $file = $request->file('files_add4');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add5'))  > 0) {
                $file = $request->file('files_add5');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add6'))  > 0) {
                $file = $request->file('files_add6');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add7'))  > 0) {
                $file = $request->file('files_add7');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add8'))  > 0) {
                $file = $request->file('files_add8');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add9'))  > 0) {
                $file = $request->file('files_add9');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            if(count($request->file('files_add10'))  > 0) {
                $file = $request->file('files_add10');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('documentos')->put($nombre, \File::get($file));

                $archivos = new Archivos();
                $archivos ->idAlumno = $user->id;
                $archivos ->nombreDocto = $nombre;
                $archivos ->status = 0;
                $archiv = $archivos->save();
            }

            //obtenemos el ultimo registro historico del alumno y lo modificamos
            $historico = Historico::where("idAlumno","=",$alumno->idAlumno)
                     ->get()
                     ->last();
            
            $ciclo = null;

            if($modificarSeccion)
            {
                $ciclo = CiclosEscolares::where("idSeccion","=",$request->idSeccion)
                          ->where("Activo","=",1)
                          ->first();
            }

            
            //si no exixte en el historico entonces creare un registro
            if(!$historico)
            {
                //necesitamos crear el historico desde que el alumno se da de alta
                //obtenemos el ciclo actual
                $ciclo = CiclosEscolares::where("idSeccion","=",$request->idSeccion)
                          ->where("Activo","=",1)
                          ->first();
            
                //sinos devuelve un ciclo null
                //le indicamos al encargado de control que tiene que registrar un ciclo
                //y ponerlo activo
                if(!$ciclo)
                {
                    throw new Exception("Necesitar dar de alta un ciclo enn la seccion: ".$request->idSeccion);
                }


                //registramos al alumno en el historico
                $historico = new Historico();
            }

            //se creara o actualizara el registro depenendiento del resulta
            //de la busqueda
            $historico->idAlumno = $alumno->idAlumno;
            $historico->idSeccion = $alumno->idSeccion;
            $historico->idGrado = $alumno->idGrado;
            $historico->idGrupo = $alumno->idGrupo;

            if($ciclo)
            {
                $historico->idCiclo = $ciclo->idPeriodo;
            }
            $h = $historico->save();

            if(!$h)
            {
                throw new Exception("hubo un problema al agregar al historico al alumno");
            }
            


            if(!$al || !$us || !$rel || !$seg)
            {
                throw new Exception("El alumno no se pudo modificar");
            }

            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El alumno se modificó con éxito!";
            $status = 200;//ok

        }
        catch(Exception $ex)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = $ex->getMessage()." Line:".$ex->getLine();
            $status = 500;//error interno
        }
        
        if(!$request->ajax()){
            Session::flash($tipo_mensaje,$texto_mensaje);

            if($request->elecciones == 1) {
                return redirect("/alumnos/".$alumno->idAlumno."/edit?value=1");
            } else {
                return redirect("/alumnos/".$alumno->idAlumno."/edit?value=1&perfil=".$alumno->idAlumno);
            }
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";

        $alumno = Alumnos::findOrFail($id);
        $user = User::findOrFail($alumno->id_usuario);

            DB::beginTransaction();

            $alumno->EliminadoPor = $request->elimino;
            $alumno->MotivoEliminar = $request->MotivoA;
            $alumno->save();
            $al = $alumno->delete();
            $us = $user->delete();

        if(!$al || !$us)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Ocurrió un error, inténtelo de nuevo.";
            $status = 500;//Internal Error
        }
        else
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El alumno se ha eliminado correctamente.";
            $status = 200;//ok
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/lista-alumnos");
        }
        return Response()->json(["mensaje"=>$texto_mensaje],$status);

    }


    public function historico(Request $request)
    {
        $listAlumnos  = Alumnos::onlyTrashed()->leftjoin("secciones","secciones.idSeccion","=","alumnos.idSeccion")
        ->leftjoin("grados","alumnos.idGrado","=","grados.idGrado")
        ->leftjoin("grupos","alumnos.idGrupo","=","grupos.idGrupo")
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND concat(secciones.seccion,grados.Grado) LIKE ? AND grupos.Grupo LIKE ?",["%".$request->Nombre."%","%".$request->Seccion."".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")//->toSql();
        ->paginate(10);
        return view("sistema.colaboradores.historico_alumnos",compact("listAlumnos"));
    }


    public function restore(Request $request, $id)
    {
        $al = $alumno = Alumnos::withTrashed()->where("idAlumno","=",$id)->first();
        //dd($col);
        //echo $col->id_usuario;
        $user = User::withTrashed()->where("id","=",$al->id_usuario)->first();
        //dd($user);
        
        DB::beginTransaction();
            $al = $alumno->restore();
            $us = $user->restore();

        if(!$al || !$us)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "El alumno no se pudo restablecer";
            $status = 500;//Internal Error
        }
        else
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El alumno se restableci�� con ��xito!";
            $status = 200;//ok
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            //return redirect("/historico_alumnos");
            return redirect("/alumnos/".$alumno->idAlumno."/edit");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }


    public function reporte_alumnos(Request $request)
    {
        //$alumnos = Alumnos::orderBy("Nombres","DESC")->get();
        $alumnos = Alumnos::leftjoin("secciones","secciones.idSeccion","=","alumnos.idSeccion")
        ->leftjoin("grados","alumnos.idGrado","=","grados.idGrado")
        ->leftjoin("grupos","alumnos.idGrupo","=","grupos.idGrupo")
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno,secciones.seccion,grados.Grado) LIKE ? AND grupos.Grupo LIKE ?",["%".$request->alumno."".$request->nivel."".$request->grado."%","%".$request->grupo."%"])
        ->orderBy("ApellidoPaterno","ASC")//->toSql();
        ->orderBy("ApellidoMaterno","ASC")
        ->orderBy("Nombres","ASC")
        ->get();
        //dd($alumnos);
        $pdf = DomPDF::loadView("SIAO.alumnos.reporte_alumnos",compact("alumnos"));
        return $pdf->stream("alumnos.pdf");
    }

    /*
        funcion para ver el detalle de inicios de session del alumno
    */
    public function acceso_historico_alumnos(Request $request)
    {
        $listAlumnos = Alumnos::orderBy('idAlumno', 'DESC')->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? AND idSeccion LIKE ? AND idGrado LIKE ? AND idGrupo LIKE ?",["%".$request->Nombre."%","%".$request->Seccion."%","%".$request->grado."%","%".$request->grupo."%"])->paginate(20);
        $listAlumnos->each(function($listAlumnos) {
            $listAlumnos->seccion;
            $listAlumnos->grado;
            $listAlumnos->grupo;
            $listAlumnos->pages;
        });

          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',1)->get();
              }
              else{
                  $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->orWhere('idSeccion','=',4)->get();
              }

          }
          else{
            $secciones = Secciones::get();
          }

        return view ('sistema/colaboradores/acceso_historico_alumnos', ['listAlumnos' => $listAlumnos, 'secciones' => $secciones]);
    }



    public function search_alumnos(Request $request)
    {
        $listAlumnos = Alumnos::leftjoin("secciones","secciones.idSeccion","=","alumnos.idSeccion")
        ->leftjoin("grados","alumnos.idGrado","=","grados.idGrado")
        ->leftjoin("grupos","alumnos.idGrupo","=","grupos.idGrupo")
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno,secciones.seccion,grados.Grado) LIKE ? AND grupos.Grupo LIKE ?",["%".$request->alumno."".$request->nivel."".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")->get();
        //dd($listAlumnos);
        return Response()->json($listAlumnos);
    }


     public function getAlumnoByGrupo(Request $request){
        $var = Alumnos::select("alumnos.*","secciones.Seccion","grados.Grado","grupos.Grupo")
            ->join('secciones','secciones.idSeccion','=','alumnos.idSeccion')
            ->join('grados','grados.idGrado','=','alumnos.idGrado')
            ->join('grupos','grupos.idGrupo','=','alumnos.idGrupo')
            ->whereNotIn("alumnos.idAlumno",
                Alumnos::select("alumnos.idAlumno")->join("tutoresalumnos","alumnos.idAlumno","=","tutoresalumnos.idAlumno")->get()
                )
            ->whereRaw("concat(Nombres,ApellidoPaterno,ApellidoMaterno) like ?", ["%".$request ->nombre."%"])->get();
        return Response()->json($var);
    }

    public function calificaciones_alumno()
    {
        $calificaciones = [];
        if(Auth::check() && Auth::user()->typeUser == 8)//8 -- alumno
        {
            /*$calificaciones = DB::table('calificaciones as x1')
            ->select("x2.Nombres as alumno","x3.Nombre as materia","x1.calificacionFinal as calificacion")
            ->join("alumnos as x2","x1.idAlumno","=","x2.idAlumno")
            ->join("materias as x3","x1.idMateria","=","x3.idMateria")
            ->where("x2.idAlumno","=",Auth::user()->alumno->idAlumno)
            ->get();*/

            $calificaciones =  Calificaciones::where("idAlumno","=",Auth::user()->alumno->idAlumno)->get();
        }

        return view("sistema.alumnos.calificaciones_alumnos",compact("calificaciones"));
    }

    public function historial_academico()
    {

        $calificaciones = [];
        if(Auth::check() && Auth::user()->typeUser == 8)//8 -- alumno
        {
            $calificaciones =  Calificaciones::where("idAlumno","=",Auth::user()->alumno->idAlumno)->get();
        }

        return view("sistema.alumnos.historial_academico",compact("calificaciones"));
    }


    public function imprimir_Calificaciones()
    {
        return view("SIAO.alumnos.imprimir_Calificaciones");
    }

    public function obtAlumno($idAlumno)
    {
        $alumno = Alumnos::find($idAlumno);
        return Response()->json($alumno);
    }

    public function aviso_privacidad($idAlumno)
    {
        $alumno = Alumnos::find($idAlumno);
        $pdf = PDFS::loadView("sistema.alumnos.aviso_privacidad_alumnos",compact("alumno"));
        return $pdf->stream("aviso_privacidad.pdf");
    }

}
