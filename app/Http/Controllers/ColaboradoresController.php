<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ColaboradoresRequest;
use App\Http\Requests\ColaboradoresUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use App\Colaboradores;
use App\Roles;
use App\User;
use Auth;
use Exception;
use App\Secciones;
use App\GrupoMateriaProfesor;
use App\GruposMaterias;
use App\HorarioEst;
class ColaboradoresController extends Controller {
    
    public function index(Request $request) {
	    //$listColaboradores = DB::table('colaboradores')
          /*$tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $rol = "Profesor";

              

              $listColaboradores = Colaboradores::join("roles","roles.idRol","=","colaboradores.idRol")
                            ->join("users","users.id","=","colaboradores.id_usuario")
                            ->whereRaw("colaboradores.Nombre LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$rol."%"])
                            ->orderBy("idColaborador","DESC")->paginate(10);

                $lista_correos = Colaboradores::join("users","users.id","=","colaboradores.id_usuario")
                ->select('users.email')
                ->get();
          }
          else{
            $listColaboradores = Colaboradores::join("roles","roles.idRol","=","colaboradores.idRol")
                            ->join("users","users.id","=","colaboradores.id_usuario")
                            ->whereRaw("colaboradores.Nombre LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$request->rol."%"])
                            ->orderBy("idColaborador","DESC")->paginate(10);
            
            
          }*/
          $listColaboradores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                            ->join("roles","roles.idRol","=","users.typeUser")
                            ->whereRaw("colaboradores.Nombre LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$request->rol."%"])
                            ->orderBy("idColaborador","DESC")->paginate(10000);

                          


         /*Obtener la imagen de perfil  de los colaboradores*/
        $imagenColaborador=DB::table('colaboradores')
        ->select(
            'id_usuario','idColaborador','Nombre','Apellido','ImagenPerfil')
        ->orderBy('idColaborador', 'ASC')
        ->get();

       //dd($listColaboradores);
            /*->select(
              'colaboradores.idColaborador',
              'Nombre',
              'Apellido',
              'idRol'
          )
          ->where('idColaborador', '<>' ,'0')
          ->orderBy('idColaborador', 'ASC')
          ->get();*/

          //dd($listColaboradores);
          //return $listColaboradores;

          return view('sistema/colaboradores/listado-colaboradores', ['listColaboradores' => $listColaboradores],['imagenColaborador'=>$imagenColaborador]);
    }
    
    // funcion para mostrar el listado de profesores por nivel
      public function listaProfesor(Request $request) {


              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
                
                
                  $listColaboradores = Colaboradores::
                  join("users","users.id","=","colaboradores.id_usuario")
                  ->join("materia_profesor_grupo","materia_profesor_grupo.id_profesor","=","colaboradores.id_usuario")
                  ->join("grupos_materias","grupos_materias.id","=","materia_profesor_grupo.id_materia_grupo")
                  ->join("grupos","grupos.idGrupo","=","grupos_materias.id_grupo")
                  ->join("grados","grados.idGrado","=","grupos.idGrado")
                  ->join("secciones","secciones.idSeccion","=","grados.idSeccion")
                  ->where("idRol","=",4)
                  ->where("secciones.idSeccion","=",$usuario->id_nivel)

                  ->whereRaw("concat(colaboradores.Nombre,colaboradores.Apellido) LIKE ? ",["%".$request->nombre."%","%".$request->rol."%"])
                    ->groupBy("colaboradores.id_usuario")->paginate(20);

      $profesor1 =  $listColaboradores->toJson();
      foreach ($listColaboradores as $idp) {
       $profesor = $idp->idColaborador;
      $profemate = GrupoMateriaProfesor::where('id_profesor', '=', $profesor)->pluck('id_materia_grupo');
      $grupomate = GruposMaterias::whereIn('id', $profemate)->pluck('id_materia');
      $horarios = HorarioEst::groupBy('idperiodo')->whereIn('diaLunes', $grupomate)->get();
      $horarios->each(function($horarios) {
        $horarios->periodo;
            $horarios->niveles;
            $horarios->grados;
            $horarios->grupos;
        });
 
      $horariosx2 = HorarioEst::groupBy('idperiodo')->whereIn('diaMartes', $grupomate)->get();
      $horariosx3 = HorarioEst::groupBy('idperiodo')->whereIn('diaMiercoles', $grupomate)->get();
      $horariosx4 = HorarioEst::groupBy('idperiodo')->whereIn('diaJueves', $grupomate)->get();
      $horariosx5 = HorarioEst::groupBy('idperiodo')->whereIn('diaViernes', $grupomate)->get(); 
      return view('sistema/colaboradores/listado-profesores', ['listColaboradores' => $listColaboradores,'horarios' => $horarios, 'horariosx2' => $horariosx2, 'horariosx3' => $horariosx3, 'horariosx4' => $horariosx4, 'horariosx5' => $horariosx5]);

      }
      
                  

            
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        $colaboradores = Colaboradores::paginate(5);
        return view("colaboradores.index",compact("colaboradores"));
    }
*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dejamos por defecto a 1 para que se ignore en la validación
        //si el usuario logueado es master tomara valor de cero para que pueda apaarecer en su lista 
        //1 -- master
        $val = "Master";
        if(Auth::check())
        {
            if(Auth::user()->rol->idRol == 1)
            {
               
                $val = "";
            }
        }
        $roles = Roles::whereRaw("Rol <> ? and Rol <> ? and Rol <> ?",["Alumno","Tutor",$val])->get();
        return view('sistema/colaboradores/formulario-colaboradores',compact("roles"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColaboradoresRequest $request)
    {
        /*print_r($request->ImagenPerfil);
        echo var_dump($request->ImagenPerfil->getClientOriginalName());
        dd($request->ImagenPerfil);*/
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            DB::beginTransaction();
                $user = new User();
                $user->name = $request->Email;
                $user->password = $request->Email;
                $user->email = $request->Email;
                $user->typeUser = $request->idRol;
                $us = $user->save();
                $colaborador = new Colaboradores();
                $colaborador->username = $request->Email;
                $colaborador->Password = $request->Email;
                $colaborador->Nombre = $request->Nombre; 
            	$colaborador->Apellido = $request->Apellido;
            	$colaborador->idRol = $request->idRol;
            	$colaborador->Email = $request->Email;
            	$colaborador->Telefono = $request->Telefono;
            	$colaborador->Domicilio = $request->Domicilio;
            	$colaborador->ImagenPerfil = $request->imagenPerfil;
            	//dd($request->imagenPerfil);
            	$colaborador->id_usuario = $user->id;
            	$col = $colaborador->save();

            if(!$col || !$us)
            {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El colaborador no se pudo registrar";
                $status = 200;//ok
            }
            else
            {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El colaborador se registro con exito!";
                $status = 500;//error interno
            }

        
        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/colaboradores/create?value=2");
        }
        return Response()->json(["mensaje"=>"El colaborador se registro con exito!"],$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //dejamos por defecto a 1 para que se ignore en la validación
        //si el usuario logueado es master tomara valor de cero para que pueda apaarecer en su lista 
        //1 -- master
        $val = "Master";
        if(Auth::check())
        {
            if(Auth::user()->rol->idRol == 1)
            {
               
                $val = "";
            }
        }
        $colaborador = Colaboradores::findOrFail($id);

        $usuario = User::findOrFail($colaborador->id_usuario);

        

        $roles = Roles::whereRaw("Rol <> ? and Rol <> ? and Rol <> ?",["Alumno","Tutor",$val])->get()->pluck("Rol","idRol");
        return view("sistema/colaboradores/editar-colaboradores",compact("colaborador","roles","usuario"));
    }

    /**     *
     * Update the specified resource in storage.

     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColaboradoresUpdateRequest $request, $id)
    {
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";

        $colaborador = Colaboradores::find($id);
        $user = User::find($colaborador->id_usuario);

        DB::beginTransaction();
            $user->name = $request->username;
            $user->email = $request->Email;
            $user->password = $request->Password;
            // $user->typeUser = $request->idRol;
            $us = $user->save();
            $colaborador->Nombre = $request->Nombre; 
            $colaborador->Apellido = $request->Apellido; 
            $colaborador->Telefono = $request->Telefono;
            $colaborador->Domicilio = $request->Domicilio;
            $colaborador->ImagenPerfil = $request->imagenPerfil;
            // $colaborador->idRol = $request->idRol;
            $col = $colaborador->save();

        if(!$col || !$us)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "El colaborador no se pudo actualizar";
        }
        else
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El colaborador se actualizo con exito!";
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/colaboradores/".$colaborador->idColaborador."/edit?value=2");
        }
        return Response()->json(["mensaje"=>"El colaborador se actualizo con exito!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";

        $colaborador = Colaboradores::findOrFail($id);
        $user = User::find($colaborador->id_usuario);

        DB::beginTransaction();

        try
        {
            /*rol 4 profesor*/
            if($colaborador->user->rol->idRol ==4)
            {
                $delete_mat = DB::table("materia_profesor_grupo")->where("id_profesor","=",$colaborador->idColaborador)->delete();

                if(!$delete_mat)
                {
                    DB::rollback(); 
                    $tipo_mensaje = "mensaje-danger";
                    $texto_mensaje = "No se puede eliminar por su relacion con materias";
                    $status = 500; 
                }
            }
           
            $colaborador->EliminadoPor = Auth::user()->colaborador->Nombre." ".Auth::User()->colaborador->Apellido;
            $colaborador->MotivoEliminar = $request->Motivo;
            $colaborador->save();
            
            $col = $colaborador->delete();
            $us = $user->delete();

            if(!$col || !$us)
            {
                throw new Exception("¡Ups! Parece que ocurrió un error, inténtelo de nuevo.");
            }

            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El colaborador se ha eliminado correctamente.";
            $status = 200;//ok

        }catch(Exception $ex)
        {
            DB::rollback(); 
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "No se puede eliminar por su relacion con materias";
            $status = 500; 
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/colaboradores");
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historico(Request $request)
    {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $rol = "Profesor";
        $listColaboradores  = Colaboradores::onlyTrashed()
                                            ->join("roles","roles.idRol","=","colaboradores.idRol")
                                            ->whereRaw("concat(colaboradores.Nombre,colaboradores.Apellido) LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$rol."%"])
                                            ->paginate(10);
          }
          else{
        $listColaboradores  = Colaboradores::onlyTrashed()
                                            ->join("roles","roles.idRol","=","colaboradores.idRol")
                                            ->whereRaw("concat(colaboradores.Nombre,colaboradores.Apellido) LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$request->rol."%"])
                                            ->paginate(10);
          }

        return view("sistema.colaboradores.historico_colaboradores",compact("listColaboradores"));
    }


    public function restore(Request $request, $id)
    {
        $col = $colaborador = Colaboradores::withTrashed()->where("idColaborador","=",$id)->first();
        //dd($col);
        //echo $col->id_usuario;
        $user = User::withTrashed()->where("id","=",$col->id_usuario)->first();
        //dd($user);
        
        DB::beginTransaction();
            $col = $colaborador->restore();
            $us = $user->restore();

        if(!$col || !$us)
        {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrió un error, inténtelo de nuevo.";
            $status = 500;//Internal Error
        }
        else
        {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El colaborador se restableció correctamente.";
            $status = 200;//ok
        }

        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/historico_colaboradores?value=2");
        }
        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }


    public function acceso_historico_colaboradores(Request $request)
    {
          $tipodeus = Auth::user()->rol->idRol == 10;
          if($tipodeus == 'true'){
              $rol = "Profesor";
        $listColaboradores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                            ->join("roles","roles.idRol","=","users.typeUser")
                            ->whereRaw("colaboradores.Nombre LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$rol."%"])
                            ->orderBy("idColaborador","DESC")->paginate(10);
          }
          else{
        $listColaboradores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                            ->join("roles","roles.idRol","=","users.typeUser")
                            ->whereRaw("colaboradores.Nombre LIKE ? AND roles.Rol LIKE ?",["%".$request->Nombre."%","%".$request->rol."%"])
                            ->orderBy("idColaborador","DESC")->paginate(10);
          }

        return view("sistema.colaboradores.acceso_historico_colaboradores",compact("listColaboradores"));
    }
    
    public function lista_materias_profesores()
        {
            $colaborador = [];

            if(Auth::check() && Auth::user()->typeUser == 4) //4 -- profesor
            {
                $colaborador = Colaboradores::find(Auth::user()->colaborador->idColaborador);
            }

            return view("sistema.profesores.lista-materias_profesores",compact("colaborador"));
        }


    public function principal_profesores()
    {
        $colaborador = [];

        if(Auth::check() && Auth::user()->typeUser == 4) //4 -- profesor
        {
            $colaborador = Colaboradores::find(Auth::user()->colaborador->idColaborador);
            $colaboradores = GrupoMateriaProfesor::join('grupos_materias','materia_profesor_grupo.id_materia_grupo','=','grupos_materias.id')
            ->join('materias','grupos_materias.id_materia','=','materias.idMateria')
            ->join('grupos','grupos.idGrupo','=','grupos_materias.id_grupo')
    	    ->join('grados','grados.idGrado','=','grupos.idGrado')
    	    ->join('secciones','secciones.idSeccion','=','grados.idSeccion')
    	    ->where('id_profesor','=',Auth::user()->colaborador->idColaborador)
    	    ->where('materias.Nombre','!=','EXPERIMENTOS')
    	    ->where('materias.Nombre','!=','NIVELACIÓN ACADÉMICA')
    	    ->where('materias.Nombre','!=','ARTES')
    	    ->where('materias.Nombre','!=','CLUB DE ROBOTICA')
            ->get();
        }

        return view("sistema.profesores.principal_profesores",compact("colaboradores","colaborador"));
    }

    public function lista_profesores(Request $request)
    {
        $profesores = Colaboradores::obtProfesores()->whereRaw("concat(colaboradores.Nombre,colaboradores.Apellido) LIKE ? ",["%".$request->nombre."%"])->paginate(15);
        return view("SIAO.profesores.lista_profesores",compact("profesores"));
    }
    public function lista_caja(Request $request)
    {
        $caja = Colaboradores::obtCajas()->whereRaw("concat(colaboradores.Nombre,colaboradores.Apellido) LIKE ? ",["%".$request->Nombre."%"])->paginate(15);
        return view("SIAO.listacaja",compact("caja"));
    }
    public function lista_control(Request $request)
    {
        $control = Colaboradores::obtControles()->whereRaw("colaboradores.Nombre LIKE ?",["%".$request->Nombre."%"])->paginate(15);
        return view("SIAO.lista-control",compact("control"));
    }
    public function lista_directivo(Request $request)
    {
        $directivo = Colaboradores::obtDirectivos()->whereRaw("colaboradores.Nombre LIKE ?",["%".$request->Nombre."%"])->paginate(15);
        return view("SIAO.lista-directivos",compact("directivo"));
    }

    public function lista_director(Request $request)
    {
        $director = Colaboradores::obtDirector()->whereRaw("colaboradores.Nombre LIKE ?",["%".$request->Nombre."%"])->paginate(15);
        return view("SIAO.lista-director",compact("director"));
    }
    


    public function registro_historico_profesores()
    {
        $profesores = Colaboradores::obtProfesores()->onlyTrashed()->paginate(15);
        return view("SIAO.profesores.registro_historico_profesores",compact("profesores"));
    }

    public function restablecer_profesor($id)
    {
        $mensaje_tipo = "";
        $mensaje_texto = "";
        $status = 200;
        //dd($id);
        $profesor = Colaboradores::withTrashed()->findOrFail($id);
        //dd($profesor);
        $user = User::withTrashed()->findOrFail($profesor->id_usuario);

        DB::beginTransaction();

        $prof = $profesor->restore();
        $us = $user->restore();

        if(!$prof || !$us)
        {
            DB::rollback();
            $mensaje_tipo = "alert-danger";
            $mensaje_texto = "El profesor no se pudo restablecer";
            $status = 500;
        }
        else
        {
            DB::commit();
            $mensaje_tipo = "alert-success";
            $mensaje_texto = "El profesor se restablecio con exito!";
            $status = 200;
        }

        Session::flash($mensaje_tipo,$mensaje_texto);

        return redirect("lista_profesores?value=6");
    }

    public function create_profesor()
    {
        return view('SIAO.profesores.agregar-profesor');
    }

    public function agregar_profesor(Request $request)
    {
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            DB::beginTransaction();
                $user = new User();
                $user->name = $request->Email;
                $user->password = $request->Email;
                $user->email = $request->Email;
                $user->typeUser = 4;
                $us = $user->save();
                $colaborador = new Colaboradores();
                $colaborador->username = $request->Email;
                $colaborador->Password = $request->Email;
                $colaborador->Nombre = $request->Nombre; 
                $colaborador->Apellido = $request->Apellido;
                $colaborador->idRol = 4;//profesor en la base de datos
                $colaborador->Email = $request->Email;
                $colaborador->Telefono = $request->Telefono;
                $colaborador->Domicilio = $request->Domicilio;
                $colaborador->ImagenPerfil = $request->ImagenPerfil;
                $colaborador->id_usuario = $user->id;
                $col = $colaborador->save();

            if(!$col || !$us)
            {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "El profesor no se pudo registrar";
                $status = 200;//ok
            }
            else
            {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "El profesor se registro con exito!";
                $status = 500;//error interno
            }

        
        if(!$request->ajax())
        {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("/colaboradores/create");
        }
        return Response()->json(["mensaje"=>$texto_mensaje],$status);
    }

     public function getCaja(Request $request){
        $caja = Colaboradores::whereRaw("concat(Nombres,Apellidos) like ? and idRol=6", ["%".$request ->nombre."%"])->get();
        return Response()->json($caja);
    }

}
