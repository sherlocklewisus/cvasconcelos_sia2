<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\Materias;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\GruposMaterias;
use DB;
use App\Colaboradores;
use App\User;
use Auth;

class MateriaslistGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
      $tipodeus = Auth::user()->rol->idRol == 9;
      if($tipodeus == 'true'){
          $tipo = Auth::user()->id;
          $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
          $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
          $materias = Materias::where('idSeccion','=',$usuario->id_nivel)->get();
          $relacion = GruposMaterias::get();
          $grados = Grados::get();
      }
      else{
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
        $secciones = Secciones::get();
        $materias = Materias::get();
        $relacion = GruposMaterias::get();
        $grados = Grados::get();
      }


        /*$listMaterias = Materias::select("materias.*")
            ->orderBy('idMateria', 'DESC')
            ->whereRaw("materias.Nombre LIKE ? and concat(x2.Seccion,x3.Grado) LIKE ?",["%".$request->Nombre."%","%".$request->Seccion."".$request->grado."%"])->paginate(10);*/

        $grupos2 = Grupos::orderBy('idGrupo', 'DESC')->get();
        $grupos = Grupos::orderBy('idGrupo', 'DESC')->where("idGrado","=",$request->grado)->get();
        
        return view('sistema/colaboradores/lista-materias-grupo', ['secciones' => $secciones, 'grupos' => $grupos, 'relacion' => $relacion, 'materias' => $materias, 'grados' => $grados, 'grupos2' => $grupos2]);
    }

    public function getSeccion(Request $request, $id) {
        if($request->ajax()){
            $seccion = Grados::gradosNivel($id);
            return response()->json($seccion);
        }
    }


    public function destroy(Request $request, $id)
    {
       
        $materia = Materias::findOrFail($id);
        
        
        $ma = $materia->delete();
        if(!$ma) {
                DB::rollback();
                $tipo_mensaje = "mensaje-danger";
                $texto_mensaje = "No se puede borrar materia";
            } else {
                DB::commit();
                $tipo_mensaje = "mensaje-success";
                $texto_mensaje = "Se elimino con exito!";
            }

            if(!$request->ajax()) {
                Session::flash($tipo_mensaje,$texto_mensaje);
                return redirect("lista-materias?value=9");
            }

        return Response()->json(["mensaje"=>"La materia se borro con exito!"]);

        return redirect('lista-materias');

    }
}
