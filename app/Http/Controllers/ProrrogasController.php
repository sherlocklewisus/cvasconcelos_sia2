<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Prorrogas;
use App\Alumnos;
use DB;

class ProrrogasController extends Controller
{
    public function index(Request $request) {
        $alumnos = Alumnos::orderBy('idAlumno', 'DESC')
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno,idSeccion,idGrado,idGrupo) LIKE ?",["%".$request->namePro."".$request->nivel."".$request->grado."%","%".$request->grupo."%"])->orderBy("idAlumno","DESC")->get();
        $alumnos->each(function($estudiantes) {
            $estudiantes->seccion;
            $estudiantes->grado;
            $estudiantes->grupo;

        });

    	$secciones = Secciones::orderBy('idSeccion', 'ASC')->get();
    	$cuentaatras = Prorrogas::orderBy('idProrroga', 'DESC')->limit(1)->get();
    	return view ('sistema/caja/formulario-prorrogas', ['alumnos' => $alumnos, 'secciones' => $secciones, 'cuentaatras' => $cuentaatras]);
    }

    public function getGrado(Request $request, $id) {
        if($request->ajax()){
            $grado = Grados::grado($id);
            return response()->json($grado);
        }
    }

    public function getGrupo(Request $request, $id) {
        if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function store(Request $request) {
        $this->validate($request, [
            "Alumno"=>"required",
            "fechaNueva"=>"required",
            "noPago"=>"required"
        ],[
            "Alumno.required"=>"El alumno es requerido",
            "fechaNueva.required"=>"La fecha es requerida"]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            
        DB::beginTransaction();

        $prorroga = new Prorrogas();
        $prorroga ->num_prorroga = $request->noPago;
        $prorroga ->idAlumno = $request->Alumno;
        $prorroga ->nuevaFecha = $request->fechaNueva;
        $SaveProrroga = $prorroga->save();

        if(!$SaveProrroga) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! La prorroga se ha creado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("formulario-prorrogas");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! La prorroga se ha creado correctamente."]);
 	}
}
