<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Calificaciones;
use App\periodosevaluacion;
use App\Alumnos;
use App\Tutores;
use App\Colaboradores;
use App\Materias;
use DB;
use Auth;
use App\Roles;
use App\User;
use App\Grados;
use App\Secciones;

class AdministradorControlController extends Controller
{
	public function index() {
	    
	    $contenido = isset($_GET['return'])? $_GET['return'] : "0";

        if($contenido != '0') {
            $usuario = Auth::user()->id;
            $decode = base64_decode($_GET['return']);
            $busqueda = Roles::where('Rol', '=', $decode)->pluck('idRol');
            $busquedax2 = Roles::where('Rol', '=', $decode)->get();
            $compatible = User::where('id', '=', $usuario)->whereIn('data_float', $busqueda)->get();

          
            if($compatible <> '0') {

                foreach($busquedax2 as $search) {
                    header("access-control-allow-origin: *");

                    $server = "localhost";
                    $user = "adminani_ipjroot";
                    $pass = "animatiomx2017";
                    $bd = "adminani_ipjv";

                    $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexi贸n de la base de datos.");
                    $sql = "UPDATE users SET data_float = ".$search->idRol." WHERE id = ". $usuario;
                    mysqli_set_charset($conexion, "utf8");
                    if(!$result = mysqli_query($conexion, $sql)) die("Ha sucedido un error.");
                    $close = mysqli_close($conexion) or die("Ha sucedido un error inesperado en la desconexi贸n de la base de datos.");

                    return redirect("inicio-control-escolar");
                }
            }
        }
        
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
		$calificaciones = Calificaciones::get();
		$periodos = periodosevaluacion::get();
        $alumnos = Alumnos::where('deleted_at', '=', NULL)->get();
        $tutores = Tutores::get();
        $profesor = Colaboradores::where('idRol', '=', 4)->get();
        $materias = Materias::get();


		$tutorA = DB::table('tutores')
            ->select(
                'tutores.idTutor','Religion')
            ->where('Religion', '=' ,4)
            ->count();

        $tutorC = DB::table('tutores')
        ->select(
            'tutores.idTutor','Religion')
        ->where('Religion', '=' ,1)
        ->count();

        $tutorCr = DB::table('tutores')
        ->select(
            'tutores.idTutor','Religion')
        ->where('Religion', '=' ,2)
        ->count();

        $tutorE = DB::table('tutores')
        ->select(
            'tutores.idTutor','Religion')
        ->where('Religion', '=' ,3)
        ->count();
          $tipodeus = Auth::user()->rol->idRol == 10;
          $tipodeus1 = Auth::user()->rol->idRol == 9;
          $tipodeus2 = Auth::user()->rol->idRol == 5;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              if($secciones2->idSeccion == '2'){
                  $grados = Grados::where('idSeccion','=',$usuario->id_nivel)->orwhere('idSeccion','=',1)->get();
              }
              else{
                 $grados = Grados::where('idSeccion','=',$usuario->id_nivel)->orwhere('idSeccion','=',4)->get();
              }

          }
          else if($tipodeus1 == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
              $grados = Grados::where('idSeccion','=',$usuario->id_nivel)->get();
              $alumnos = Alumnos::where('idSeccion','=',$usuario->id_nivel)->get();
              $materias = Materias::where('idSeccion','=',$usuario->id_nivel)->get();
          }
          else{
           $grados = Grados::get();   
          }
          ini_set('memory_limit', '-1');

    	return view ('sistema/colaboradores/admin-control-escolar', ['calificaciones' => $calificaciones, 'tutorA' => $tutorA,  'tutorC' => $tutorC, 'tutorCr' => $tutorCr,  'tutorE' => $tutorE, 'periodos' => $periodos, 'alumnos' => $alumnos, 'tutores' => $tutores, 'profesor' => $profesor, 'materias' => $materias, 'Grados' => $grados]);
    }
}
