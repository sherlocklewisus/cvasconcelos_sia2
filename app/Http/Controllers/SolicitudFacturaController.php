<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Alumnos;
use App\Parentescos;
use App\Facturacion;
use DB;

class SolicitudFacturaController extends Controller
{
    public function index(Request $request) {
    	$alumnos = Alumnos::whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ?",["%".$request->nameAlumnos."%"])->orderBy('idAlumno', 'ASC')->get();
    	$parentesco = Parentescos::orderBy('idParentescos', 'ASC')
    	 ->where('idParentescos', '<=', 7)
    	 ->get();

    	return view ('sistema/caja/solicitud-factura', ['alumnos' => $alumnos, 'parentesco' => $parentesco]);
    }

    public function store(Request $request) {
    	$this->validate($request, [
            "alumnado"=>"required",
            "solicitud"=>"required",
            "fechado"=>"required",
            "mes"=>"required",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            
        DB::beginTransaction();

        $solicitudes = new Facturacion();
        $solicitudes ->idAlumno = $request->alumnado;
        $solicitudes ->solicito = $request->solicitud;
        $solicitudes ->fecha = $request->fechado;
        $solicitudes ->Mes = $request->mes;
        $saved = $solicitudes->save();

        if(!$saved) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! La solicitud a sido registrada correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("solicitud-factura?value=6");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! La solicitud a sido registrada correctamente."]);
    }
}
