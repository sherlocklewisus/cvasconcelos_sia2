<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CriteriosEvaluacion extends Model
{
    protected $table = 'criteriosevaluacion';
    public $timestamps = false;
}
