<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\HistorialPagos;
use App\Alumnos;
use DB;

class ListaDeudoresCajaController extends Controller
{
    public function index(Request $request) {
    // dd('hola');
    // 	$colegiaturas = HistorialPagos::where('statusfinal', '=', 0)->orderBy('idHistorico', 'DESC')->paginate(20);
    // 	$colegiaturas->each(function($colegiaturas) {
    //         $colegiaturas->alumnos;
    //     });

    // 	return view ('sistema/caja/listado-deudores', ['colegiaturas' => $colegiaturas]);
    // }

    	// $colegiaturas = HistorialPagos::where('statusfinal', '=', 0)->orderBy('idHistorico', 'DESC')->paginate(20);
    	// $colegiaturas->each(function($colegiaturas) {
     //        $colegiaturas->alumnos;
     //    });

    	$alumnos = Alumnos::orderBy('idAlumno', 'DESC')
        ->join("secciones as x1","alumnos.idSeccion","=","x1.idSeccion")
        ->join("grados as x2","alumnos.idGrado","=","x2.idGrado")
        ->join("grupos as x3","alumnos.idGrupo","=","x3.idGrupo")
        ->whereRaw("concat(Nombres,' ',ApellidoPaterno,' ',ApellidoMaterno) LIKE ? and x1.Seccion LIKE ? and x2.Grado LIKE ? and x3.Grupo LIKE ?",["%".$request->Nombre."%","%".$request->nivel."%","%".$request->grado."%","%".$request->grupo."%"])
        ->paginate(15);

        // dd($alumnos);

    	return view ('sistema/caja/listado-deudores', ['alumnos' => $alumnos]);
    }

    public function detalles($id){

    	//Consulta de Alumnos
    	$alumnos = Alumnos::where('idAlumno', '=', $id)
    	->join("secciones as x1","alumnos.idSeccion","=","x1.idSeccion")
    	->join("grados as x2","alumnos.idGrado","=","x2.idGrado")
        ->join("grupos as x3","alumnos.idGrupo","=","x3.idGrupo")
        ->get();

        //obtengo la seccion del alumno seleccionado
       	$plucked = $alumnos->pluck('idSeccion');
        
        // dd($plucked);
        

       	//obtener periodoescolar del alumno
       	$periodosescolares = periodosescolares::whereIn('idSeccion', $plucked)->where('Activo', '=', 1)->get();
       	foreach ($periodosescolares as $key) {
       		$periodoses=$key->idPeriodo;
       	}
       	// dd($periodoses);
       	// ->pluck('idPeriodo');

       	//obtener periodospago del alumno       	
       	// $periodospagos = PeriodosPagos::whereIn('idPeriodoEscolar', $periodosescolares)->get();
       	// $periodospagos = PeriodosPagos::join('costo_colegiaturas','periodospagos.idCosto','=','costo_colegiaturas.id_costo_colegiatura')->where('costo_colegiaturas.tipo_operacion','=',1)->whereIn('periodospagos.idPeriodoEscolar', $periodosescolares)->get();
       	// dd($periodospagos);

       	// Periodos de pago inscripciones
       	$periodospagos_ins=DB::table('costo_colegiaturas')
       				->join('periodospagos','costo_colegiaturas.id_costo_colegiatura','=','periodospagos.idCosto')
       				->where('periodospagos.idPeriodoEscolar','=',$periodoses)
       				->where('costo_colegiaturas.tipo_operacion','=',2)
       				->get();
       	// Periodos de pago colegiaturas
       	$periodospagos=DB::table('costo_colegiaturas')
       				->join('periodospagos','costo_colegiaturas.id_costo_colegiatura','=','periodospagos.idCosto')
       				->where('periodospagos.idPeriodoEscolar','=',$periodoses)
       				->where('costo_colegiaturas.tipo_operacion','=',1)
       				->get();

       	$periodospagos2 = PeriodosPagos::whereIn('idPeriodoEscolar', $periodosescolares)->pluck('idPeriodoPago');
// dd($periodospagos2);
       	// $periodospagos = PeriodosPagos::whereIn('idPeriodoEscolar', $periodosescolares)->


       	//obtener colegiaturas ya pagadas del alumno	
       	$colegiaturas = Colegiaturas::where('idAlumno', '=', $id)->whereIn('idPeriodoPago', $periodospagos2)->get();
       	// dd($colegiaturas);

       
		    $colegiaturas2 = Colegiaturas::where('idAlumno', '=', $id)->whereIn('idPeriodoPago', $periodospagos2)->pluck('idPeriodoPago'); 

		    $periodospagos3 = PeriodosPagos::whereNotIn('idPeriodoPago', $colegiaturas2)->get();

        foreach ($plucked as $pl) {
          $se = $pl;
        }
      // dd($se);	
        
        if($se == 1){
          $op = 178;
          $in = 155;
        }
        elseif($se == 2){
          $op = 179;
          $in = 156;
        }elseif($se == 3){
          $op = 180;
          $in = 157;
        }elseif($se == 4){
          $op = 181;
          $in = 301;
        }

      
        $operaciones = tipooperaciones::where('idTipoOperacion', $op)->get();
      // dd($operaciones);
        $prein = tipooperaciones::where('idTipoOperacion', '=', $in)->get();
        // dd($prein);
        $periodospagos_pre=DB::table('costo_colegiaturas')
       				->join('periodospagos','costo_colegiaturas.id_costo_colegiatura','=','periodospagos.idCosto')
       				->where('periodospagos.idPeriodoEscolar','=',$periodoses)
       				->where('costo_colegiaturas.tipo_operacion','=',3)
       				->get();
// dd($periodospagos_pre);


    	return view ('sistema.caja.detalles-pagos', ['alumnos' => $alumnos , 'plucked' => $plucked, 'periodospagos' => $periodospagos, 'colegiaturas' => $colegiaturas, 'periodospagos3' => $periodospagos3, 'operaciones' => $operaciones, 'prein' => $prein,'periodospagos_ins'=>$periodospagos_ins,'periodospagos_pre'=>$periodospagos_pre]);
    }    	
}
