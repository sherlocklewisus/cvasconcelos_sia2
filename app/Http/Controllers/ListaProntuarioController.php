<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Prontuario;
use App\CriteriosEvaluacion;
use App\Bloques;
use App\RasgoEvaluar;
use App\Archivos;
use DomPDF;
use DB;
use Auth;

class ListaProntuarioController extends Controller
{
	public function index() {
		// $listaProntuario = DB::table('prontuarios')
  //       ->select(
  //           'prontuarios.idProntuario',
  //           'asignatura',
  //           'grado',
  //           'profesor',
  //           'fecha_subida'
  //       )
  //       ->where('idProntuario', '<>' ,'0')
  //       ->orderBy('idProntuario', 'ASC')
  //       ->get();

  //       $profesores = DB::table('colaboradores')
  //       ->select(
  //           'colaboradores.idColaborador',
  //           'Nombre',
  //           'Apellido',
  //           'Email'
  //       )
  //       ->where('idColaborador', '<>' ,'0')
  //       ->orderBy('idColaborador', 'ASC')
  //       ->get();

  //       $grados = DB::table('grados')
  //       ->select(
  //           'grados.idGrado',
  //           'Grado'
  //       )
  //       ->where('idGrado', '<>' ,'0')
  //       ->orderBy('idGrado', 'ASC')
  //       ->get();

  //       $asignaturas = DB::table('materias')
  //       ->select(
  //           'materias.idMateria',
  //           'Nombre'
  //       )
  //       ->where('idMateria', '<>' ,'0')
  //       ->orderBy('idMateria', 'ASC')
  //       ->get();

        $profesor = Auth::user()->colaborador->idColaborador;
        $archivos = Archivos::where([
          ['status', '=', 1],
          ['idAlumno', '=', $profesor]
        ])->get();

        //dd($listaProntuario);
	    //return $listaProntuario;
        return view('sistema/profesores/lista-prontuarios', ['archivos' => $archivos]);
	    //return view('sistema/profesores/lista-prontuarios', ['listaProntuario' => $listaProntuario, 'profesores' => $profesores, 'grados' => $grados, 'asignaturas' => $asignaturas]);
	}

    public function imprimir_prontuario($id) {
        $prontuarios = Prontuario::where('idProntuario', '=', $id)->get();
        $prontuarios->each(function($prontuarios) {
            $prontuarios->materias;
            $prontuarios->graduados;
            $prontuarios->profesores;
            // $prontuarios->criterios;
            // $prontuarios->bloques;
            // $prontuarios->rasgos;
        });

        $criterios = CriteriosEvaluacion::where('idProntuario', '=', $id)->get();
        $bloques = Bloques::where('idProntuario', '=', $id)->get();
        $rasgos = RasgoEvaluar::where('idProntuario', '=', $id)->get();

        //dd($prontuarios);

        $pdf = DomPDF::loadView("SIAO.imprimir_prontuario",compact('prontuarios', 'criterios', 'bloques', 'rasgos'));
        return $pdf->stream("detalle_prontuario.pdf");
    }
}
