<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\Periodos;
use App\PeriodosEvaluacion;
use App\Secciones;
use DB;

class GenerarHorariosController extends Controller
{
    public function index() {
        $fecha = date("Y-m-d");

        $periodos = DB::table('periodosescolares')
        ->select(
            'periodosescolares.idPeriodo',
            'Periodo',
            'FechaInicio',
            'FechaFin',
            'Activo'
        )
        ->orderBy('idPeriodo', 'ASC')
        ->where('FechaInicio','>=',$fecha)
        ->get();

        $seccion = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->orderBy('idSeccion', 'ASC')
        ->get();

        $estructura = DB::table('horarios_estructura')
        ->select(
            'horarios_estructura.idHorario',
            'periodo_escolar',
            'nivel_educativo'
        )
        ->orderBy('idHorario', 'ASC')
        ->get();

        return view ('SIAO/generar-horarios', ['seccion' => $seccion, 'periodos' => $periodos, 'estructura' => $estructura]);
    }

    public function getPeriodos(Request $request, $id) {
        if($request->ajax()){
            $periodos = Periodos::periodosEval($id);
            return response()->json($periodos);
        }
    }

    public function getNiveles(Request $request, $id) {
        $seccional = Periodos::where('idPeriodo','=',$id)->get();

        foreach($seccional as $niv) {
            $value = $niv->idSeccion;

            if($request->ajax()){
                $evaluacion = Secciones::periodos($value);
                return response()->json($evaluacion);
            }
        }
    }

    public function store(Request $request) {
        $count = $request->cantidades;
        
        DB::beginTransaction();

        for($cont = 1; $cont <= $count; $cont++){

            $this->validate($request, [
                "periodo_escolar"=>"required",
                "nivel_educativo"=>"required",
                "ini_x".$cont=>"required",
                "fin_x".$cont=>"required",
            ]);

            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";

            $horarios = new Horarios();
            $horarios ->periodo_escolar = $request->periodo_escolar;
            $horarios ->nivel_educativo = $request->nivel_educativo;

            if(!$horarios ->horaInicio = $request['ini_x'.$cont]) {

            } else {
                $horarios ->horaInicio = $request['ini_x'.$cont];

                if(!$horarios ->horaFinal = $request['fin_x'.$cont]) {

                } else {
                    $horarios ->horaFinal = $request['fin_x'.$cont];

                    if(!$horarios ->receso = $request['receso_x'.$cont]) {
                        $horarios ->receso = 0;
                    } else {
                        $horarios ->receso = 1;
                    }

                    $horarios ->comentario = $request->comentarios;
                    $savehorarios = $horarios->save();
                }
            }
        }


        if(!$savehorarios) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "¡En hora buena! El horario se ha guardado correctamente.";
        }

        if(!$request->ajax()) {
            return redirect("llenado-horarios?value=8&nivel=".$request->nivel_educativo."&periodo=".$request->periodo_escolar);
        } else {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("generar-horarios?value=8");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El horario se ha guardado correctamente."]);
    }
}
