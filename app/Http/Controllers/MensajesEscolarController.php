<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Mensajes;
use App\Grados;
use App\Grupos;
use App\Colaboradores;
use DB;

class MensajesEscolarController extends Controller
{
    public function index() {
        $colaboradores = DB::table('colaboradores')
        ->select(
            'colaboradores.idColaborador',
            'Nombre',
            'Apellido',
            'idRol',
            'Email'
        )
        ->where('idColaborador', '<>' ,'0')
        ->orderBy('idColaborador', 'ASC')
        ->get();

        $tutores = DB::table('tutores')
        ->select(
            'tutores.idTutor',
            'Nombres',
            'Apellidos',
            'Email'
        )
        ->where('idTutor', '<>' ,'0')
        ->orderBy('idTutor', 'ASC')
        ->get();

        $alumnos = DB::table('alumnos')
        ->select(
            'alumnos.idAlumno',
            'Nombres',
            'ApellidoPaterno',
            'ApellidoMaterno',
            'Username'
        )
        ->where('idAlumno', '<>' ,'0')
        ->orderBy('idAlumno', 'ASC')
        ->get();

        $niveles = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion',
            'ClaveSEP',
            'idColegio'
        )
        ->where('idSeccion', '<>' ,'0')
        ->orderBy('idSeccion', 'ASC')
        ->get();

        $grados = DB::table('grados')
        ->select(
            'grados.idGrado',
            'idSeccion',
            'Grado'
        )
        ->where('idGrado','<>','0')
        ->orderBy('idGrado','ASC')
        ->get();

        $grupos = DB::table('grupos')
        ->select(
            'grupos.idGrupo',
            'idGrado',
            'Grupo'
        )
        ->where('idGrupo','<>','0')
        ->orderBy('idGrupo','ASC')
        ->get();
        //dd($usuarios);
        // //return $usuarios;
        // $secciones = Secciones::get();
        return view('sistema/colaboradores/message-chats', [
            'colaboradores' => $colaboradores, 
            'tutores' => $tutores, 
            'alumnos' => $alumnos, 
            'niveles' => $niveles,
            'grados'=>$grados,
            // 'secciones' => $secciones,
            'grupos'=>$grupos
            ]);
    }

    public function getterseccion(){
        $secciones = DB::table('secciones')
        ->select(
            'secciones.idSeccion',
            'Seccion'
        )
        ->get();
        return response()->json($secciones);
    }
    public function gettergrado(Request $request){
        $grado = Grados::where('idSeccion','=',$request->seccion)
        ->get();
        return response()->json($grado);
    }
    public function gettergrupo(Request $request){
        $grupo = Grupos::where('idGrado','=',$request->grado)
        ->get();
        return response()->json($grupo);
    }
    public function getGrado(Request $request, $id) {
        if($request->ajax()){
            $grado = Grados::grado($id);
            return response()->json($grado);
        }
    }

    public function getGrupo(Request $request, $id) {
        if($request->ajax()){
            $grupo = Grupos::grupo($id);
            return response()->json($grupo);
        }
    }

    public function store(Request $request) {
        $this->validate($request, [
            "envia"=>"required",
            "destino"=>"required",
            "asunt"=>"required",
            "relevancia"=>"required",
            "messagesbody"=>"required",
            "fechaenvio"=>"required",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
            
        DB::beginTransaction();

        $array = explode(', ', $request->destino);
        $numeroRecursos = count($array);

        for($i = 1; $i <= $numeroRecursos; $i++) {
            $mensajes = new Mensajes();
            $mensajes ->idUsuarioEnvia = $request->envia;
            $mensajes ->idUsuarioRecibe = $array[$i-1];
            $mensajes ->Asunto = $request->asunt;
            $mensajes ->Relevancia = $request->relevancia;
            $mensajes ->Mensaje = nl2br($request->messagesbody);
            $mensajes ->fechaenvio = $request->fechaenvio;
            
            if(count($request->file('file')) > 0) {

                $file = $request->file('file');
                $nombre = $file->getClientOriginalName();
                \Storage::disk('savePDF')->put($nombre, \File::get($file));

                $mensajes ->archivos_ruta = "assets/files/".$nombre;
            } else {
                $mensajes ->archivos_ruta = "";
            }

            $msj = $mensajes->save();
        }

        if(!$msj) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            // $array = explode(', ', $request->destino);
            // $numeroRecursos = count($array);

            // for($i = 1; $i <= $numeroRecursos; $i++) {
            //     $from = $array[$i-1];
                
            //     $remitente = Colaboradores::where("idColaborador", "=", $request->envia)->get();

            //     foreach($remitente as $val) {
            //         $datas = "From: ".$val->Nombre." ".$val->Apellido." < ".$val->Email." >";
                

            //         $mail = nl2br($request->messagesbody);
            //         //Titulo
            //         $titulo = $request->asunt;
            //         //cabecera
            //         $headers = "MIME-Version: 1.0\r\n"; 
            //         $headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
            //         //dirección del remitente 
            //         $headers .= $datas."\r\n";
            //         //Enviamos el mensaje a tu_dirección_email 
            //         $bool = mail("$from",$titulo,$mail,$headers);

            //         if($bool) {
            //             DB::rollback();
            //             $tipo_mensaje = "mensaje-danger";
            //             $texto_mensaje = "Parece que ocurrio un error al enviar el mensaje a el correo designado, por favor intentelo de nuevo.";
            //         }

                    DB::commit();
                    $tipo_mensaje = "mensaje-success";
                    $texto_mensaje = "¡En hora buena! El mensaje se ha guardado y enviado correctamente.";
            //     }
            // }
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("mensajes-chats");
        }

        return Response()->json(["mensaje"=>"¡En hora buena! El mensaje se ha enviado correctamente."]);
    }
}