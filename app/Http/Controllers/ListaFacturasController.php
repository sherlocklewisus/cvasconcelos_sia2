<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Facturacion;
use App\User;
use App\Mensajes;
use App\tutoresalumnos;
use App\Tutores;
use DB;
use App\Alumnos;

class ListaFacturasController extends Controller
{
	public function index() {
        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            }
	      $alumno = Alumnos::pluck("idAlumno");
  
      $facturacion = Facturacion::whereIn("facturas.idAlumno", $alumno)
      ->orderBy('idFactura', 'DESC')->paginate(15);

      $facturacion->each(function($facturacion) {
            $facturacion->alumnos;
            $facturacion->parentescos;
        });


        $users = User::orderBy('id', 'ASC')->get();

      return view ('sistema/caja/listado-facturas', ['facturacion' => $facturacion, 'users' => $users]);
	  
	}

	public function store(Request $request) {
    
        $this->validate($request,[
    	    "idalumnado"=>"required",
            "opciones"=>"required",
            // "emailalumno"=>"required|email",
            "file"=>"required",
		],[
		    "idalumnado.required"=>"Alumno es requerido",
		    "opciones.required"=>"Opciones es requerido",
		    "emailalumno.required"=>"El correo del alumno es requerido",
		    "file.required"=>"Archivo es requerido"
		
        ]);
                
                
                
        
        $date = date('Y-m-d');
        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
        $relaciones = tutoresalumnos::where('idAlumno', '=', $request->idalumnado)->get();
    
        if(count($relaciones) > 0) {
           foreach($relaciones as $relacion) {
               $tutores = Tutores::where('idTutor', '=', $relacion->idTutor)->get();

               foreach($tutores as $tutor) {
                   $mensajes = new Mensajes();
                   $mensajes ->idUsuarioEnvia = $request->idenvia;
                   $mensajes ->idUsuarioRecibe = $tutor->Email;
                   $mensajes ->Asunto = "Entrega de factura.";
                   $mensajes ->Relevancia = 2;
                   $mensajes ->Mensaje = "<p><strong>Estimado padre de familia:</strong></p><br/> <p>Le hacemos llegar el documento que contiene la factura que usted nos solicito, sin mas por el momento, buen día.</p>";
                   $mensajes ->fechaenvio = $date;

                   if(count($request->file('file')) > 0) {
                       $file = $request->file('file');
                       $nombre = $file->getClientOriginalName();
                       \Storage::disk('savePDF')->put($nombre, \File::get($file));

                       $mensajes ->archivos_ruta = "assets/files/".$nombre;
                   }

                   $msj = $mensajes->save();
               }
           }
       }

       if(!$msj) {
           DB::rollback();
           $tipo_mensaje = "mensaje-danger";
           $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
       } else {
            $server = "localhost";
            $user = "siacadem_root";
            $pass = "animatiomx2017";
            $bd = "siacadem_sia";

            // Creamos la conexión
            $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexión de la base de datos. HOLA");

            if($request->opciones == 1) {
            // Generamos la consulta
            $sql = "UPDATE facturas SET status = 1 WHERE idAlumno = ". $request->idalumnado;

            } else {
            // Generamos la consulta
            $sql = "UPDATE facturas SET status = 0 WHERE idAlumno = ". $request->idalumnado;
            }

            // Formato de datos utf8
            mysqli_set_charset($conexion, "utf8");

            //Guardamos en una variable los datos de consulta & conexión
        if(!$result = mysqli_query($conexion, $sql)) die("Ha sucedido un error.");

           if(!$result) {
               DB::rollback();
               $tipo_mensaje = "mensaje-danger";
               $texto_mensaje = "¡Ups! Parece que ocurrio un error, intentelo de nuevo.";
           } else {
           DB::commit();
           $tipo_mensaje = "mensaje-success";
           $texto_mensaje = "¡En hora buena! La factura se ha entregado al tutor del alumno correctamente.";
           }
       }

       if(!$request->ajax()) {
           Session::flash($tipo_mensaje,$texto_mensaje);
           return redirect("listado-facturas?value=6");
       }

       return Response()->json(["mensaje"=>"¡En hora buena! La factura cambio su estado correctamente."]);
   }
}
