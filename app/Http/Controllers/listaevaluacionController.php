<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

use App\PeriodosEvaluacion;
use App\CiclosEscolares;
use App\Secciones;
use DB;
use App\Colaboradores;
use App\User;
use Auth;

class listaevaluacionController extends Controller
{
    public function index() {
        $tipodeus = Auth::user()->rol->idRol == 9;
        if($tipodeus == 'true'){
            $tipo = Auth::user()->id;
            $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
            $seccion = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
            $listPeriodos = PeriodosEvaluacion::where('idSeccion','=',$usuario->id_nivel)->orderBy("idPeriodoEvaluacion","DESC")->whereNull('deleted_at')->get();
        }
        else{
            $listPeriodos = PeriodosEvaluacion::orderBy("idPeriodoEvaluacion","DESC")->whereNull('deleted_at')->get();
            $seccion = Secciones::get();
        }
 
        return view('sistema/colaboradores/lista-evaluacion',['listPeriodos' => $listPeriodos,'seccion'=>$seccion]);
    }

    public function show($id) {
        $periodo = PeriodosEvaluacion::where('idPeriodoEvaluacion', '=', $id)->first();
        $periodo->each(function($periodo) {
            $periodo->cicloEscolar;
        });
        $seccion = Secciones::get();
        
        return view("sistema/colaboradores/editar-periodos",['periodo' => $periodo, 'seccion'=>$seccion]);
    }

    public function update(Request $request, $id) {
    
        $this->validate($request, [
            "Descripcion"=>"required",
            "FechaEvaluacionInicio"=>"required",
            "FechaEvaluacionFin"=>"required",
            "seccions"=>"required",
        ],["seccions.required"=>"El campo seccion es requerido"]);

        $periodo = periodosevaluacion::find($id);
        $periodo->FechaEvaluacionInicial = $request->FechaEvaluacionInicio;
        $periodo->FechaEvaluacionFinal = $request->FechaEvaluacionFin;
        $periodo->Descripcion = $request->Descripcion;
        $periodo->idSeccion = $request->seccions;
        $periodo->save();

        if(!$periodo) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El periodo de evaluación se ha actualizado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("editar-periodo/".$periodo->idPeriodoEvaluacion.'?value=10');
        }

        return Response()->json(["mensaje"=>"El periodo de evaluación se ha actualizado correctamente."]);
    }

    public function store(Request $request) {
        $periodo = periodosevaluacion::findOrFail($request->delete);
        $periodo->delete();

        if(!$periodo) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El periodo de evaluación se ha eliminado correctamente.";
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("lista-evaluacion?value=10");
        }

        return Response()->json(["mensaje"=>"El periodo de evaluación se ha eliminado correctamente."]);
    }
}
