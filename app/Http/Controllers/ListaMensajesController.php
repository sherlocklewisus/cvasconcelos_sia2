<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Mensajes;
use DB;

class ListaMensajesController extends Controller
{
    public function index() {
    	$mensajes = Mensajes::orderBy('idMensaje', 'DESC')->get();

    	$users = DB::table('users')
    	->select(
    		'users.id',
    		'name'
    	)
    	->get();

    	return view ('sistema/colaboradores/lista-mensajes', ['mensajes' => $mensajes, 'users' => $users]);
    }
}
