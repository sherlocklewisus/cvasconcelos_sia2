<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\PeriodosEvaluacion;
use App\Periodos;
use App\Secciones;
use DB;
use App\Colaboradores;
use App\User;
use Auth;

class AgregarHorariosController extends Controller
{
    public function index() {
          $tipodeus = Auth::user()->rol->idRol == 9;
          if($tipodeus == 'true'){
              $tipo = Auth::user()->id;
              $usuario = Colaboradores::where('id_usuario','=',$tipo)->first();
              $secciones = Secciones::Where('idSeccion','=',$usuario->id_nivel)->get();
              $secciones2 = Secciones::Where('idSeccion','=',$usuario->id_nivel)->first();
                $periodos = Periodos::where('Activo','=',1)->where('idSeccion','=',$secciones2->idSeccion)->orderBy('idPeriodo', 'ASC')->get();
                $seccion = Secciones::orderBy('idSeccion', 'ASC')->get();
                $estructura = Horarios::groupBy('periodo_escolar')->orderBy('idHorario', 'ASC')->get();

          }
          else{
            $periodos = Periodos::where('Activo','=',1)->orderBy('idPeriodo', 'ASC')->get();
            $seccion = Secciones::orderBy('idSeccion', 'ASC')->get();
            $estructura = Horarios::groupBy('periodo_escolar')->orderBy('idHorario', 'ASC')->get();
          }


        return view ('sistema/colaboradores/formulario-horarios', ['seccion' => $seccion, 'periodos' => $periodos, 'estructura' => $estructura]);
    }

    public function getPeriodos(Request $request, $id) {
        if($request->ajax()){
            $periodos = PeriodosEvaluacion::periodosEval($id);
            return response()->json($periodos);
        }
    }

    public function getNiveles(Request $request, $id) {
        $seccional = Periodos::where('idPeriodo','=',$id)->get();

        foreach($seccional as $niv) {
            $value = $niv->idSeccion;

            if($request->ajax()){
                $evaluacion = Secciones::periodos($value);
                return response()->json($evaluacion);
            }
        }
    }

    public function store(Request $request) {
        $count = $request->cantidades;
        
        DB::beginTransaction();

        for($cont = 1; $cont <= $count; $cont++){

            $this->validate($request, [
                "periodo_escolar"=>"required",
                "nivel_educativo"=>"required",
                "ini_x".$cont=>"required",
                "fin_x".$cont=>"required",
            ]);

            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "";

            $horarios = new Horarios();
            $horarios ->periodo_escolar = $request->periodo_escolar;
            $horarios ->nivel_educativo = $request->nivel_educativo;

            if(!$horarios ->horaInicio = $request['ini_x'.$cont]) {
                $savehorarios = false;
            } else {
                $horarios ->horaInicio = $request['ini_x'.$cont];

                if(!$horarios ->horaFinal = $request['fin_x'.$cont]) {
                    $savehorarios = false;
                } else {
                    $horarios ->horaFinal = $request['fin_x'.$cont];

                    if(!$horarios ->receso = $request['receso_x'.$cont]) {
                        $horarios ->receso = 0;
                    } else {
                        $horarios ->receso = 1;
                    }

                    $horarios ->comentario = $request->comentarios;
                    $savehorarios = $horarios->save();
                }
            }
        }

        $period = Periodos::find($request->periodo_escolar);
        $period->status_horario = 1;
        $final = $period->save();

        if(!$savehorarios || !$final) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
        } else {
            DB::commit();
            $tipo_mensaje = "mensaje-success";
            $texto_mensaje = "El horario se ha guardado correctamente.";
        }

        if(!$request->ajax()) {

            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect("agregar-horarios");
        }

        return Response()->json(["mensaje"=>"El horario se ha guardado correctamente."]);
    }
}
