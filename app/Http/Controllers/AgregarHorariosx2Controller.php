<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Horarios;
use App\PeriodosEvaluacion;
use App\Periodos;
use DB;

class AgregarHorariosx2Controller extends Controller
{
    public function index() {
        $estructuras = DB::table('horarios_estructura')->get();

        $periodos = DB::table('periodosescolares')->get();

        $niveles = DB::table('secciones')->get();

        return view ('sistema/colaboradores/listado-horarios-estructura', ['estructuras' => $estructuras, 'periodos' => $periodos, 'niveles' => $niveles]);
    }

    public function store(Request $request) {
    	$nivel = $request->nivel;
    	$periodo = $request->periodo;

        $server = "localhost";
                    $user = "adminani_ipjroot";
                    $pass = "animatiomx2017";
                    $bd = "adminani_ipjv";

        $conexion = mysqli_connect($server, $user, $pass, $bd) or die("Ha sucedido un error inesperado en la conexion de la base de datos");

        $sql = "DELETE FROM `horarios_estructura` WHERE periodo_escolar = ".$periodo." AND nivel_educativo = ".$nivel;

        mysqli_set_charset($conexion, "utf8");

        $response = mysqli_query($conexion, $sql);

        $period = Periodos::find($periodo);
        $period->status_horario = NULL;
        $final = $period->save();

    	if(!$response || $final) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
        } else {
			DB::commit();
			$tipo_mensaje = "mensaje-success";
			$texto_mensaje = "La estructura de horario se ha eliminado correctamente.";
        }

        if(!$request->ajax()) {
            return redirect("seguimiento-horarios");
        }

        return Response()->json(["mensaje"=>"La estructura de horario se ha eliminado correctamente."]);
    }
}
