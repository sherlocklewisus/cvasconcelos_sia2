<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grados;

class GradosController extends Controller
{
    public function getGrados($id_seccion) {
    	$grados = Grados::whereRaw("idSeccion = ?",[$id_seccion])->get();
    	return view("sistema.grados.getGrados", compact("grados"));
    }
}
