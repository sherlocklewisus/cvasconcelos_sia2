<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Alumnos;
use App\CostoColegiaturas;
use App\PeriodosPagos;
use DB;

class ListadoPagosTutoresController extends Controller
{
    public function index() {
        $id = $_GET['id'];

        $alumnos = Alumnos::where('idAlumno', '=', $id)->get();
        $alumnos->each(function($alumnos) {
            $alumnos->seccion;
        });
        
        $colegiaturas = CostoColegiaturas::orderBy('id_costo_colegiatura', 'ASC')->where('tipo_operacion', '=', '1')->get();

        $pagos = PeriodosPagos::orderBy('idPeriodoPago', 'ASC')->get();
        $pagos->each(function($pagos) {
            $pagos->periodo;
        });

        return view ('sistema/tutores/listado-pagos', ['alumnos' => $alumnos, 'colegiaturas' => $colegiaturas, 'pagos' => $pagos]);
    }
}