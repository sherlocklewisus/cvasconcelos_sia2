<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;

use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\View;

use App\Prontuario;
use App\CriteriosEvaluacion;
use App\Bloques;
use App\RasgoEvaluar;
use DB;

class EditarProntuarioController extends Controller
{
	public function index() {

    }

    public function show($id) {
    	$prontuarios = DB::table('prontuarios')
        ->select(
            'prontuarios.idProntuario',
            'asignatura',
            'grado',
            'profesor',
            'correo',
            'secuencias',
            'descripcion_curso',
            'objetivos',
            'formatos',
            'contenido',
            'exposicion',
            'bibliografia',
            'fecha_subida'
        )
        ->where('idProntuario', '=' ,$id)
        ->get();

        $criterios = DB::table('criteriosevaluacion')
        ->select(
            'criteriosevaluacion.idCriterio',
            'idProntuario',
            'idProfesor',
            'descripcion',
            'porcentaje'
        )
        ->where('idProntuario', '=' ,$id)
        ->get();

        $rasgos = DB::table('rasgoaevaluar')
        ->select(
            'rasgoaevaluar.idRasgo',
            'idProntuario',
            'idProfesor',
            'descripcion'
        )
        ->where('idProntuario', '=' ,$id)
        ->get();

        $bloques = DB::table('bloques')
        ->select(
            'bloques.idBloque',
            'idProntuario',
            'idProfesor',
            'nombre_bloque',
            'subtemas',
            'proyecto',
            'fecha_entrega',
            'rubrica'
        )
        ->where('idProntuario', '=' ,$id)
        ->get();

        $materias = DB::table('materias')
        ->select(
            'materias.idMateria',
            'Nombre'
        )
        ->orderBy('idMateria', 'ASC')
        ->get();

        $relacionMaterias = DB::table('materia_profesor_grupo')
        ->select(
            'materia_profesor_grupo.id',
            'id_materia_grupo',
            'id_profesor'
        )
        ->orderBy('id', 'ASC')
        ->get();

        $materiasGrupos = DB::table('grupos_materias')
        ->select(
            'grupos_materias.id',
            'id_materia'
        )
        ->orderBy('id', 'ASC')
        ->get();

        $profesor = DB::table('colaboradores')
        ->select(
            'colaboradores.idColaborador',
            'Email',
            'Nombre',
            'Apellido'
        )
        ->orderBy('idColaborador', 'ASC')
        ->get();

        //dd($prontuarios, $criterios, $rasgos, $bloques);
	    //return $listaProntuario;

	    return view('sistema/profesores/editar-prontuario', ['prontuarios' => $prontuarios, 'profesor' => $profesor, 'criterios' => $criterios, 'rasgos' => $rasgos, 'bloques' => $bloques, 'materias' => $materias, 'relacionMaterias' => $relacionMaterias, 'materiasGrupos' => $materiasGrupos]);
    }

    public function update(Request $request, $id) {
        $prontuario = Prontuario::find($id);

        $this->validate($request, [
            "teacher"=>"required",
            "email"=>"required",
            "criterio_1"=>"required",
            "porciento_1"=>"required",
            "rasgo_1"=>"required",
            "namebloque_1"=>"required",
            "subtema_1"=>"required",
            "proyecto_1"=>"required",
            "fecha_1"=>"required",
            "rubrica_1"=>"required",
            "fechasubida"=>"required",
        ]);

        $tipo_mensaje = "mensaje-success";
        $texto_mensaje = "";
        $status = 200;
            
        DB::beginTransaction();

        $prontuario = Prontuario::findOrFail($id);
        $prontuario ->asignatura = $request->subject;
       	$prontuario ->grado = $request->grade;
       	$prontuario ->profesor = $request->teacher;
       	$prontuario ->correo = $request->email;
       	$prontuario ->secuencias = $request->sequences;
       	$prontuario ->descripcion_curso = nl2br($request->course_description);
		$prontuario ->objetivos = nl2br($request->objectives);
		$prontuario ->formatos = nl2br($request->formats);
		$prontuario ->contenido = nl2br($request->content);
		$prontuario ->exposicion = nl2br($request->exposition);
		$prontuario ->bibliografia = $request->bibliography;
		$prontuario ->fecha_subida = $request->fechasubida;

        $pront = $prontuario->save();

        if(!$pront) {
            DB::rollback();
            $tipo_mensaje = "mensaje-danger";
            $texto_mensaje = "Parece que ocurrio un error, intentelo de nuevo.";
            $status = 500;
        } else {
        	DB::commit();
			$tipo_mensaje = "mensaje-success";
			$texto_mensaje = "El prontuario se ha actualizado correctamente.";
            $status = 200;
        }

        if(!$request->ajax()) {
            Session::flash($tipo_mensaje,$texto_mensaje);
            return redirect('editar-prontuario/'.$prontuario->idProntuario.'?value=5');
        }

        return Response()->json(["mensaje"=>$texto_mensaje],$status);  
    }
}
