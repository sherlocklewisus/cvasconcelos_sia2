<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlumnosRequest;
use App\Http\Requests\AlumnosUpdateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Route;
use App\Alumnos;
use App\User;
use App\Religiones;
use App\Seguros;
use App\Secciones;
use App\Grados;
use App\Grupos;
use App\Estados;
use App\Municipios;
use App\Archivos;
use DB;
use Auth;
use App\Historico;
use App\CiclosEscolares;
use App\Calificaciones;
use App\Colaboradores;
use PDFS;
use App\DocumentosAlumnos;
use Exception;


class DetalleAlumnos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   

    public function detalleAlumnos($id){

        $alumno = Alumnos::findOrFail($id);

        $religion = Religiones::where("id","=",$alumno->id_religion)->first();

        $nivel = secciones::where("idSeccion","=",$alumno->idSeccion)->first(); 

        $grado = grados::where('idGrado',"=",$alumno->idGrado)->first();

        $grupo = grupos::where('idGrupo',"=",$alumno->idGrupo)->first();

        $estado = estados::where('idEstados',"=",$alumno->idEstado)->first();

        $ciudad = municipios::where('id',"=",$alumno->idCiudad)->first();

        

        $seguro = seguros::where("id","=",$alumno->id_seguro)->first();

        $archivos = Archivos::where("idAlumno", "=", $alumno->id_usuario)->get();

        

        

        return view('sistema/colaboradores/verAlumnos',compact("alumno","nivel","grado","grupo","estado","ciudad","seguro","archivos"));
    }
}
