<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\ColaboradoresRequest;
use App\Http\Requests\ColaboradoresUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;
use App\Colaboradores;
use App\Roles;
use App\User;
use Auth;
use Exception;

class ActividadesControllers extends Controller
{
    //
      public function index(Request $request) {

	    //$listColaboradores = DB::table('colaboradores')
	    $listColaboradores = Colaboradores::join("users","id","=","colaboradores.id_usuario")
                            ->where('users.typeUser' ,'=', 4)
                            ->whereRaw("colaboradores.Nombre LIKE ?",["%".$request->Nombre."%"])
							->orderBy("idColaborador","DESC")->paginate(10);

	     //dd($listColaboradores);
            /*->select(
	            'colaboradores.idColaborador',
	            'Nombre',
	            'Apellido',
	            'idRol'
	        )
	        ->where('idColaborador', '<>' ,'0')
	        ->orderBy('idColaborador', 'ASC')
	        ->get();*/

	        //dd($listColaboradores);
	        //return $listColaboradores;

	        return view('sistema/colaboradores/horarios_profesores', ['listColaboradores' => $listColaboradores]);
    }
}
