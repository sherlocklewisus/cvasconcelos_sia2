<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class MateriaHorarioController extends Controller
{
  
  public function index(Request $request){
  	$Seccion = DB::table('secciones')
  	->select('secciones.idSeccion',
  		'Seccion')
  	->get();
  	$detalle_horario = DB::table('detalle_horario')
  	->select('detalle_horario.idDetalleHorario',
  		'idSeccion',
  		'HoraInicial',
  		'HoraFinal'
  		)
  	->get();
  	return view("SIAO/materia-horario",['Seccion'=>$Seccion,'detalle_horario' => $detalle_horario]);
  }



 	public function getHorario(Request $request){

  		$hr = DB::table('detalle_horario')
  		->select('detalle_horario.idDetalleHorario',
  			'idSeccion',
  			'HoraInicial',
  			'HoraFinal',
        'deleted_at')
  		->where('idSeccion','=', $request->Seccion)
      ->where('deleted_at','=',NULL)
  		->get();
    	return Response()->json($hr);
	}

  public function getMateria(Request $request){

      $MT = DB::table('detalle_horario as x1')
      ->select('x1.idDetalleHorario',
        'x1.idSeccion',
        'x1.HoraInicial',
        'x1.HoraFinal',
        'x1.deleted_at',
        'x4.Nombre')
      ->join('materia_profesor_grupo as x2','x1.idDetalleHorario','=', 'x2.id_horario')
      ->join('grupos_materias as x3', 'x2.id_materia_grupo', '=', 'x3.id')
      ->leftjoin('materias as x4','x3.id_materia', '=', 'x4.idMateria')
      ->leftjoin('grupos as x5','x3.id_grupo', '=', 'x5.idGrupo')
      ->leftjoin('grados as x6','x5.idGrado', '=', 'x6.idGrado')
      ->where('x1.idSeccion','=', $request->Seccion)
      ->where('x6.idGrado','=', $request->Grado)
      ->where('x1.deleted_at','=',NULL)
      ->get();
   //   dd($MT);
      return Response()->json($MT);
  }
 }
// select x1.idDetalleHorario, x1.idSeccion,x1.HoraInicial, x1.HoraFinal, x1.deleted_at, x4.Nombre from detalle_horario as x1 inner join materia_profesor_grupo as x2 on x1.idDetalleHorario = x2.id_horario inner join grupos_materias as x3 on x2.id_materia_grupo = x3.id left join materias as x4 on x3.id_materia = x4.idMateria left join grupos as x5 on x3.id_grupo = x5.idGrupo left join grados as x6 on x5.idGrado = x6.idGrado where x1.idSeccion = 1 and x6.idGrado = 1 and x1.deleted_at is null