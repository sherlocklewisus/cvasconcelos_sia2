<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivosTareasAlumno extends Model
{
    protected $table = "archivos_tarea_alumno";
    protected $primaryKey = "id";
    
    public $timestamps = false;
}
