<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model {
    protected $table = "roles";
    protected $primaryKey = "idRol";

    public function user()
    {
    	return $this->belongsTo("App\User","typeUser","idRol");
    }
}

