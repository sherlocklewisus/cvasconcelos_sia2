<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salidas extends Model
{
    protected $table = "salidas";
    protected $primaryKey = "idFolio";

    public function tipodeOperacionEfectivo()
    {
        return $this->hasOne("App\TipoOperacionesEfectivo","idTipoOPeracionEfectivo","tipoOperacionEfectivo");
    }

    public function tipoOperacion()
    {
        return $this->hasOne("App\TipoOperaciones","idTipoOperacion","idTipoOperacion");
    }

    public function tipoPago()
    {
        return $this->hasOne("App\\TipoPagos","idTipoPago","idTipoPago");
    }
}
