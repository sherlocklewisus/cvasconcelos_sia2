<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDenominaciones extends Model
{
    protected $table = "tipodenominaciones";
    protected $primaryKey = "idTipoDenominacion";
}