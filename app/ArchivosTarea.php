<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Storage;

class ArchivosTarea extends Model
{
    protected $table = "archivos_tarea";
    protected $primaryKey = "id";
    protected $fillable = ["idTarea","archivo"];

    public function setArchivoAttribute($path)
    {
        $img = $path;
        /*si no existe mi varaiable Imagen Perfil y no me han mandado ninguna
            imagen signa laimagen por defecto
        */
            //guardo el nuevo archivo
           $route_file = time()."_".$img->getClientOriginalName();
            $this->attributes["archivo"] = $route_file;
            Storage::disk("ArchivosTareas")->put($route_file,file_get_contents($img->getRealPath()));
            Log::info("attibuto lleno && imagen llena");
    }

    public function tarea()
    {
    	return $this->belongsTo("App\Tareas","idTarea","id");
    }
}
