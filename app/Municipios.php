<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipios extends Model {
	protected $table = "municipios";
	protected $primaryKey = "id";
	protected $fillable = ['estado_id', 'nombre', 'id'];

	public static function ciudad($id) {
		return Municipios::where ('estado_id','=',$id) 
		->get();
	}

	public static function city($id) {
		return Municipios::where ('estado_id','=',$id)
		->get();
	}
}