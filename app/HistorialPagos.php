<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialPagos extends Model
{
    protected $table = 'historico_colegiaturas';
    protected $primaryKey = "idHistorico";
    public $timestamps = false;

    public function alumnos() {
    	return $this->hasOne('App\Alumnos','idAlumno','idAlumno');
    }
}
