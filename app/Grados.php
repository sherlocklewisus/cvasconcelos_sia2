<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grados extends Model
{
	protected $table = "grados";
    protected $primaryKey = "idGrado";
    protected $fillable = ['idGrado', 'idSeccion', 'Grado'];

    public static function grado($id) {
        return Grados::where ('idSeccion','=',$id)
        ->get();
    }

    public function seccion()
    {
    	return $this->belongsTo("App\Secciones","idSeccion","idSeccion");
    }

    public function grupos()
    {
    	return $this->hasMany("App\Grupos","idGrado","idGrado");
    }

    public static function grade($id) {
        return Grados::where ('idSeccion', '=', $id)
        ->get();
    }

    public static function gradosNivel($id) {
        return Self::where('idSeccion','=',$id)
        ->get();
    }
}
