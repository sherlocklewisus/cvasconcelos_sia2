<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodosPagos extends Model
{
    protected $table = "periodospagos";
    protected $primaryKey = "idPeriodoPago";
    public $timestamps = false;

    public function periodo() {
        return $this->hasOne("App\periodosescolares","idPeriodo","idPeriodoEscolar");
    }
}
