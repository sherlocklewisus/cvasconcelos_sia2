<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materias extends Model
{
    
	 use SoftDeletes;

      protected $table = 'materias';
      protected $primaryKey = 'idMateria';
  
  	 protected $dates = ["deleted_at"];

    public static function materias($id) {
        return Materias::where('idGrado', '=', $id)
        ->get();
    }

  	public function colaboradores()
  	{
  	 	return $this->belongsToMany("App\Colaboradores","materia_profesor_grupo","idMateria","idColaborador")->withPivot("id_grupo");
  	}

  	public function grupos()
  	{
  	 	return $this->belongsToMany("App\Grupos","materia_profesor_grupo","id_materia","idGrupo")->withPivot("id_profesor");
  	}

    public function grupo_materia()
    {
        return $this->belongsTo("App\GruposMaterias","idMateria","id_materia");
    }

    public function grado()
    {
        return $this->belongsTo("App\Grados","idGrado","idGrado");
    }

    public function seccion()
    {
        return $this->belongsTo("App\Secciones","idSeccion","idSeccion");
    }
    public function grupo()
    {
      return $this->belongsTo("App\Grupos","id_grupo","idGrupo");
    }

}
