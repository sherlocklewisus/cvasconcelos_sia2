<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Grupos extends Model
{
    use SoftDeletes;

    protected $table = "grupos";
    protected $primaryKey = "idGrupo";
    protected $fillable = ['idGrupo', 'idGrado', 'Grupo'];
    protected $dates = ["deleted_at"];

    public static function grupo($id) {
        return Grupos::where ('idGrado', '=', $id)
        ->get();
    }

    public static function group($id) {
        return Grupos::where ('idGrado', '=', $id)
        ->get();
    }

    public function grado()
    {
        return $this->belongsTo("App\Grados","idGrado","idGrado");
    }


    public function getTotalAlumnos()
    {
        return DB::table("grupos as x1")
            ->join("alumnos as x2","x1.idGrupo","=","x2.idGrupo")
            ->where("x2.idGrupo","=",$this->idGrupo)
            ->count();
    }

    public function colaboradores()
    {
        return $this->belongsToMany("App\Colaboradores","materia_profesor_grupo","idGrupo","id_profesor")->withPivot("id_materia");
    }

    public function materias()
    {
        return $this->belongsToMany("App\Materias","grupos_materias","id_grupo","id_materia");
    }

    public function alumnos()
    {
        return $this->hasMany("App\Alumnos","idGrupo","idGrupo");
    }

    public function grupos_materias()
    {
        return $this->hasMany("App\GruposMaterias","id_grupo","idGrupo");
    }

    public static function grupoGrado($id) {
        return Self::where('idGrado','=',$id)
        ->get();
    }
}
