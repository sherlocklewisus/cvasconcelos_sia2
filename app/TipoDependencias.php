<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDependencias extends Model
{
    protected $table = "tipodependencias";
    protected $primaryKey = "idTipoDependencias";
}
