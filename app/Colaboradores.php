<?php

namespace App;

use Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Colaboradores extends Model
{
    use SoftDeletes;

    protected $table = "colaboradores";
    protected $primaryKey = "idColaborador";
    protected $fillable = ["username","Password","Nombre","Apellido","idRol","Email","telefono","ImagenPerfil","Domicilio","id_usuario"];
    protected $dates = ["deleted_at"];
    protected $hidden = ["Password"];


    public function setPasswordAttribute($pw)
    {
        if(!empty($pw))
        {
            $this->attributes["Password"] = bcrypt($pw);
        }
    }

    public function setImagenPerfilAttribute($path)
    {
        $img = $path;
        /*si no existe mi varaiable Imagen Perfil y no me han mandado ninguna
            imagen signa laimagen por defecto
        */
        if(empty($this->attributes["ImagenPerfil"]) && $img == null)
        {
            $this->attributes["ImagenPerfil"] = "default-profile.png";
            Log::info("attibuto vacio && imagen vacia");
        }
        else if(!empty($this->attributes["ImagenPerfil"]) && $img == null)
        {
            /*si existe una imagen en Imagen perfil y no me han mandado
                ninguna imagen conserva la misma
            */
            $this->attributes["ImagenPerfil"];
            Log::info("attibuto lleno && imagen vacia");
        }
        else if(empty($this->attributes["ImagenPerfil"]) && $img != null)
        {
            $route_file = time()."_".$img->getClientOriginalName();
            $this->attributes["ImagenPerfil"] = $route_file;
            Storage::disk("imgColaboladores")->put($route_file,file_get_contents($img->getRealPath()));
            Log::info("attibuto vacio && imagen llena");
        }
        else
        {
            //verifico si no es la imagen por defecto sino la elimino
            if($this->attributes["ImagenPerfil"] != "default-profile.png")
            {
                //elimino el archivo anterior
                if(Storage::disk("imgColaboladores")->exists($this->attributes["ImagenPerfil"]))
                {
                    Storage::disk("imgColaboladores")->delete($this->attributes["ImagenPerfil"]);
                    Log::info("attibuto imagen eliminada");
                }
            }
            //guardo el nuevo archivo
           $route_file = time()."_".$img->getClientOriginalName();
            $this->attributes["ImagenPerfil"] = $route_file;
            Storage::disk("imgColaboladores")->put($route_file,file_get_contents($img->getRealPath()));
            Log::info("attibuto lleno && imagen llena");
        }
    }

    /*public function rol()
    {
        return $this->hasOne("App\Roles","idRol","idRol");
    }*/

    public function user()
    {
        /*
        relacion uno a uno
        con withTrashed para devolver todos los registros incluidos lo
        ocultos con sotfDeletes
        */
        return $this->hasOne("App\User","id","id_usuario")->withTrashed();
    }

    public function scopeobtProfesores($sql)
    {
        return $sql->where("idRol","=",4);
    }

    public function scopeobtControles($sql)
    {
        return $sql->where("idRol","=",5);
    }

    public function scopeobtCajas($sql)
    {
        return $sql->where("idRol","=",6);
    }

    public function scopeobtDirectivos($sql)
    {
        return $sql->where("idRol","=",2);
    }
    public function scopeobtDirector($sql)
    {
        return $sql->where("idRol","=",3);
    }


    public function grupos_materias() {
        return $this->belongsToMany("App\GruposMaterias","materia_profesor_grupo","id_profesor","id_materia_grupo");
    }

    public function grupos() {
        return $this->belongsToMany("App\Grupos","materia_profesor_grupo","id_profesor","idGrupo")
        ->withPivot("id_materia");
    }
    public function correos() {
        return $this->hasOne("App\Correos","Email","Email");
    }

}
