<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Denominaciones extends Model
{
    protected $table = "denominaciones";
    protected $primaryKey = "idDenominacion";
}
