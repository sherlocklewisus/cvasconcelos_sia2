<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReprobadosxMateria extends Model
{
    //
    protected $table ='reprobadosxmateria';
    protected $primarykey = 'idreprobadosxmateria';
    public $timestamps=false;
}
