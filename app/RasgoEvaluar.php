<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RasgoEvaluar extends Model
{
    protected $table = 'rasgoaevaluar';
    public $timestamps = false;
}
