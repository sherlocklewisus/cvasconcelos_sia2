<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prontuario extends Model
{
    protected $table = 'prontuarios';
    protected $primaryKey = 'idProntuario';
    public $timestamps = false;

    public function materias() {
    	return $this->belongsTo('App\Materias','asignatura','idMateria');
    }

    public function graduados() {
    	return $this->belongsTo('App\Grados','grado','idGrado');
    }

    public function profesores() {
    	return $this->belongsTo('App\Colaboradores','profesor','idColaborador');
    }

    // public function criterios() {
    // 	return $this->hasMany('App\CriteriosEvaluacion','idProntuario','idProntuario');
    // }

    // public function bloques() {
    // 	return $this->hasMany('App\Bloques','idProntuario','idProntuario');
    // }

    // public function rasgos() {
    // 	return $this->hasMany('App\RasgoEvaluar','idProntuario','idProntuario');
    // }
}
