<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Illuminate\Support\Facades\Log;

class DocumentosAlumnos extends Model
{
    use SoftDeletes;
    protected $table = "documentosalumno";
    protected $primaryKey = "id";
    protected $dates = ["deleted_at"];

    public function setrutaAttribute($path)
    {
        $img = $path;
        /*si no existe mi varaiable Imagen Perfil y no me han mandado ninguna
            imagen signa laimagen por defecto
        */
            //guardo el nuevo archivo
            $route_file = time()."_".$img->getClientOriginalName();
            $this->attributes["ruta"] = $route_file;
            Storage::disk("DocumentosAlumnos")->put($route_file,file_get_contents($img->getRealPath()));
            Log::info("attibuto lleno && imagen llena Documentos Alumnos");
    }

    public function alumno()
    {
        return $this->belognsTo("App\Alumnos","idAlumno","idAlumno");
    }

}
