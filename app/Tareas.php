<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tareas extends Model
{
	use SoftDeletes;

    protected $table = "tareas";
    protected $primaryKey = "idTarea";
    protected $dates = ["deleted_at"];

    public function archivosTareas()
    {
    	return $this->hasMany("App\ArchivosTarea","idTarea","idTarea");
    }

}
