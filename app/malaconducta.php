<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class malaconducta extends Model
{
    //
     protected $table ='malaconducta';
    protected $primarykey = 'idConducta';
    public $timestamps=false;
}
