<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoColegiaturas extends Model
{
    protected $table = "costo_colegiaturas";
    protected $primaryKey = "id_costo_colegiatura";
    public static function costo_colegiaturas($id)
    {
      return CostoColegiaturas::were('idTipoOperacion', '=',$id)
      ->get();  
    }

    public function seccion()
    {
        return $this->hasOne("App\Secciones","idSeccion","idSeccion2");
    }

    public function tipoOperacion()
    {
        return $this->hasOne("App\TipoOperaciones","idTipoOperacion","idTipoOperacion");
    }
}
