<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Tutores extends Model
{

	use SoftDeletes;

    protected $table = 'tutores';
    protected $primaryKey = 'idTutor';
  	
  	protected $hidden =[
  		'password', 'remember_token'
  	];
  	protected $dates = ["deleted_at"];

  	public function setPasswordAttribute($value)
    {
        if ( ! empty ($value))
        {
            $this->attributes['password'] = bcrypt($value);
        }
    }


    public function user()
    {
        return $this->belongsTo("App\User","id_usuario","id")->withTrashed();
    }


    public function alumnos_hijos()
    {
      return $this->belongsToMany("App\Alumnos","tutoresalumnos","idTutor","idAlumno");
    }

    public function tutoresalumnos()
    {
        return $this->belongsTo("App\\tutoresalumnos","idTutor","idTutor");
    }
    
}
