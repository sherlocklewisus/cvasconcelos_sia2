<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoResoluciones extends Model
{
    protected $table = "tiporesoluciones";
    protected $primaryKey = "idTipoResolucion";
}
