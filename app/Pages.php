<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = "pages";
    protected $primaryKey = "id";

    public  function user() {
    	return $this->belongsTo("App\User","id","id_usuario");
    }
}
