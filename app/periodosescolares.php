<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class periodosescolares extends Model
{
   
   //use SoftDeletes;
   protected $table = 'periodosescolares';
   protected $primaryKey = 'idPeriodo';
   public $timestamps = false;

   public static function periodosEval($id) {
      return Self::where ('idPeriodo','=',$id)
      ->get();
   }
   public function calificaciones()
   {
         return $this->hasMany("App\Calificaciones","idPeriodoEvaluacion","idPeriodoEvaluacion");
   }

   public function seccion()
   {
         return $this->belongsTo("App\Secciones","idSeccion","idSeccion");
   }

   public function cicloEscolar()
   {
      return $this->belongsTo("App\CiclosEscolares","idCicloEscolar","idPeriodo");
   }
}
