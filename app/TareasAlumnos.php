<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TareasAlumnos extends Model
{
    protected $table = "tareasalumno";
    protected $primaryKey = "idTareasAlumno";
    public $timestamps = false;
}