<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Religiones extends Model
{
	use SoftDeletes;

    protected $table = "religiones";
    protected $primaryKey = "id";
    protected $dates = ["deleted_at"];

}
