<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horarios extends Model
{
    protected $table = 'horarios_estructura';
    public $timestamps = false;

    protected $primaryKey = "idHorario";

    public function scopeSearch($query, $nivel) {
    	return $query->where('nivel', 'LIKE', "%$nivel%");
    }
}
