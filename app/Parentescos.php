<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;


class Parentescos extends Model
{
    protected $table = 'parentescos';
    protected $primaryKey = 'idParentescos';
  	public $timestamps = false;

}
